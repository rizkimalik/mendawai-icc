<?php
require_once "config/connection.php";
require_once "helper/response.php";

header('Content-type: application/json');
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Headers: *');

$action = $_GET['action'] ?? '';
$response = [];

if (!empty($action)) {
    switch ($action) {
        case "data":
            if ($_SERVER['REQUEST_METHOD'] == 'GET') {
                $skip = isset($_GET['skip']) ? $_GET['skip'] : 0;
                $take = isset($_GET['take']) ? $_GET['take'] : 15;
                $sort = isset($_GET['sort']) ? json_decode($_GET['sort'], true) : '';
                $filter = isset($_GET['filter']) ? json_decode($_GET['filter'], true) : '';
                $start_date = isset($_GET['start_date']) ? $_GET['start_date'] : '';
                $end_date = isset($_GET['end_date']) ? $_GET['end_date'] : '';

                $orderby = 'ORDER BY TicketNumber ASC';
                if ($sort) {
                    $desc = $sort[0]['desc'] == true ? 'desc' : 'asc';
                    $orderby = "ORDER BY ".$sort[0]['selector']." $desc";
                }

                $filtering = '';
                if ($filter) {
                    $filtering = "AND ".$filter[0]." LIKE '%".$filter[2]."%'";
                }

                $query = "SELECT * FROM vw_R_SLA WHERE DateCreate Between '$start_date 00:00:00' and '$end_date 23:59:00' and TicketNumber <> ''
                    $filtering
                    $orderby
                    OFFSET $skip ROWS FETCH NEXT $take ROWS ONLY";

                $sql = sqlsrv_query($db, $query);
                if ($sql) {
                    $data = [];
                    while ($row = sqlsrv_fetch_array($sql, SQLSRV_FETCH_ASSOC)) {
                        $data[] = $row;
                    }

                    $query_total = "SELECT COUNT(*) as total FROM vw_R_SLA WHERE DateCreate Between '$start_date 00:00:00' and '$end_date 23:59:00' and TicketNumber <> ''";
                    $sql_total = sqlsrv_query($db, $query_total);
                    $row_total = sqlsrv_fetch_array($sql_total, SQLSRV_FETCH_ASSOC);

                    $response = [
                        'status' => 200,
                        'message' => 'success',
                        'totalCount' => $row_total['total'],
                        'data' => $data
                    ];
                    echo json_encode($response);
                } else {
                    $response = response_error('report_sla', sqlsrv_errors());
                    echo json_encode($response);
                }
            } else {
                $response = response_method();
                echo json_encode($response);
            }
            break;

        case "export":
            if ($_SERVER['REQUEST_METHOD'] == 'GET') {
                $start_date = isset($_GET['start_date']) ? $_GET['start_date'] : '';
                $end_date = isset($_GET['end_date']) ? $_GET['end_date'] : '';

                $query = "SELECT * FROM vw_R_SLA WHERE DateCreate Between '$start_date 00:00:00' and '$end_date 23:59:00' and TicketNumber <> '' ORDER BY TicketNumber ASC";

                $sql = sqlsrv_query($db, $query);
                if ($sql) {
                    $data = [];
                    while ($row = sqlsrv_fetch_array($sql, SQLSRV_FETCH_ASSOC)) {
                        $data[] = $row;
                    }

                    $response = [
                        'status' => 200,
                        'message' => 'success',
                        'data' => $data
                    ];
                    echo json_encode($response);
                } else {
                    $response = response_error('report_sla', sqlsrv_errors());
                    echo json_encode($response);
                }
            } else {
                $response = response_method();
                echo json_encode($response);
            }
            break;
    }
} else {
    $response = response_error('report_sla', 'no parameter action.');
    echo json_encode($response);
}
