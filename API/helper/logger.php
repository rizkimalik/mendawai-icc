<?php
function log_it($dir, $message)
{
    // $dirlogs = $_SERVER['DOCUMENT_ROOT'] . "/logs";
    $dirlogs = "logs/" . $dir;
    if (!file_exists($dirlogs)) {
        mkdir($dirlogs, 0777, true);
    }
    $filename = $dirlogs . '/' . date('Ymd') . '.log';
    $message = '>>>(' . date("Y-m-d H:i:s") . ') = ' . print_r($message, true) . PHP_EOL;
    file_put_contents($filename, $message, FILE_APPEND);
}

function log_error($dir, $message)
{
    $dirlogs = "logs/log_error/" . $dir;
    if (!file_exists($dirlogs)) {
        mkdir($dirlogs, 0777, true);
    }
    $filename = $dirlogs . '/' . date('Ymd') . '.log';
    $message = '>>>(' . date("Y-m-d H:i:s") . ') = ' . print_r($message, true) . PHP_EOL;
    file_put_contents($filename, $message, FILE_APPEND);
}
