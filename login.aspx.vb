﻿Imports System
Imports System.Web.UI
Imports System.Data.SqlClient
Imports Microsoft.VisualBasic
Imports Newtonsoft.Json.Linq
Imports System.Web.Script.Serialization
Public Class login
    Inherits System.Web.UI.Page
    Public ConLDAP As String = ConfigurationManager.AppSettings.Item("LDAP")
    Public AddCookiess As String = ConfigurationManager.AppSettings.Item("AddCookiess")

    Dim Proses As New ClsConn
    Dim sqldr, readLDAP As SqlDataReader
    Dim sql As String
    Dim leveluser As String
    Dim value As String
    Dim CountLDAP As String = String.Empty
    Dim Con As New SqlConnection(ConfigurationManager.ConnectionStrings("DefaultConnection").ConnectionString)
    Dim sqlcon As New SqlConnection(ConfigurationManager.ConnectionStrings("DefaultConnection").ConnectionString)
    Dim sqlconaux As New SqlConnection(ConfigurationManager.ConnectionStrings("DefaultConnection").ConnectionString)
    Dim com, sqlcom, sqlcomaux As SqlCommand
    Dim loq As New cls_globe
    Dim connString As String = ConfigurationManager.ConnectionStrings("DefaultConnection").ConnectionString
    Dim recLDAP As Integer
    Dim recCount As Integer
    Dim sqlbackground As String = String.Empty
    Dim _ClassFunction As New WebServiceTransaction
    Dim strLogTime As String = DateTime.Now.ToString("yyyy")
    Dim VariabelCookiesUsername As New HttpCookie("CookiesUserName")
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Session.RemoveAll()
        FormsAuthentication.SignOut()
        'sqlbackground = "select Url from SML_ImageLogin where Status='Active'"
        'sqldr = Proses.ExecuteReader(sqlbackground)
        'Try
        '    If sqldr.HasRows Then
        '        sqldr.Read()
        '        Session("image") = sqldr("Url")
        '    End If
        '    sqldr.Close()
        'Catch ex As Exception
        '    Response.Write(ex.Message)
        'End Try
        If Request.QueryString("error") = "y" Then
            lblError.Visible = True
            lbl_Error.Text = "User is not active"
        Else
        End If
    End Sub
    Private Sub Btn_Simpan_ServerClick(sender As Object, e As EventArgs) Handles Btn_Simpan.ServerClick
        Dim strRecCount As String = "Select COUNT (ID) as LDAPCount from ICC_LDAP_Setting WHERE NA='Y'"
        Try
            Using conn As New SqlConnection(connString)
                conn.Open()
                Dim cmd As SqlCommand = New SqlCommand(strRecCount, conn)
                recCount = cmd.ExecuteScalar()
                _ClassFunction.LogSuccess(strLogTime, strRecCount)
            End Using
        Catch ex As Exception
            _ClassFunction.LogError(strLogTime, ex, strRecCount)
            Response.Write(ex.Message)
        End Try
        If recCount <> 0 Then
            Dim strCounting As String = String.Empty
            Dim LDAPServer As String = ConfigurationManager.AppSettings("LDAP")
            Dim _LDAPServerDB As String = String.Empty
            Dim _strLDAP As String = "Select LDAPServer from ICC_LDAP_Setting WHERE NA='Y'"
            readLDAP = Proses.ExecuteReader(_strLDAP)
            Try
                If readLDAP.HasRows Then
                    readLDAP.Read()
                    _LDAPServerDB = readLDAP("LDAPServer")
                Else
                End If
                readLDAP.Close()
            Catch ex As Exception
                Response.Write(ex.Message)
            End Try
            If ValidateActiveDirectoryLogin(_LDAPServerDB, txt_username.Text, txt_password.Text) = True Then
                If txt_username.Text <> "" Then
                    Try
                        Using conn As New SqlConnection(connString)
                            conn.Open()
                            strCounting = "Select COUNT (UserID) as userID from msUser where UserName=@uservalue"
                            Dim cmd As SqlCommand = New SqlCommand(strCounting, conn)
                            Dim uservalue As SqlParameter = New SqlParameter("@uservalue", SqlDbType.VarChar, 150)
                            uservalue.Value = txt_username.Text
                            cmd.Parameters.Add(uservalue)
                            recLDAP = cmd.ExecuteScalar()
                            If recLDAP = 1 Then
                                _ClassFunction.LogSuccess(strLogTime, strCounting)
                                AccessLogin(txt_username.Text)
                            Else
                                _ClassFunction.LogSuccess(strLogTime, strCounting)
                                lblError.Visible = True
                                lbl_Error.Text = "User Applikasi not found"
                            End If
                        End Using
                    Catch ex As Exception
                        _ClassFunction.LogError(strLogTime, ex, strCounting)
                        Response.Write(ex.Message)
                    End Try
                Else
                    _ClassFunction.LogSuccess(strLogTime, "login failed, username is empty")
                    lblError.Visible = True
                    lbl_Error.Text = "Please input username and password"
                End If
            Else

            End If
        Else
            If txt_username.Text <> "" Then
                Try
                    Using conn As New SqlConnection(connString)
                        conn.Open()
                        Dim strCounting As String = "Select COUNT (UserID) as userID from msUser where UserName=@uservalue and Password=@password"
                        Dim cmd As SqlCommand = New SqlCommand(strCounting, conn)
                        Dim uservalue As SqlParameter = New SqlParameter("@uservalue", SqlDbType.VarChar, 150)
                        Dim password As SqlParameter = New SqlParameter("@password", SqlDbType.VarChar, 150)
                        uservalue.Value = txt_username.Text
                        If (IsValidPasswordFormat(txt_password.Text) = True) Then
                            password.Value = txt_password.Text
                        Else
                            lblError.Visible = True
                            lbl_Error.Text = "User Applikasi not found"
                            Exit Sub
                        End If
                        cmd.Parameters.Add(uservalue)
                        cmd.Parameters.Add(password)
                        recLDAP = cmd.ExecuteScalar()
                        Dim _strLoquser As String = "Select COUNT (UserID) as userID from msUser where UserName='" & txt_username.Text & "' and Password='" & txt_password.Text & "'"
                        If recLDAP = 1 Then
                            _ClassFunction.LogSuccess(strLogTime, _strLoquser)
                            AccessLogin(txt_username.Text)
                        Else
                            _ClassFunction.LogSuccess(strLogTime, _strLoquser)
                            lblError.Visible = True
                            lbl_Error.Text = "User Applikasi not found"
                            Exit Sub
                        End If

                    End Using
                Catch ex As Exception
                    Response.Write(ex.Message)
                End Try
            Else
                _ClassFunction.LogSuccess(strLogTime, "LDAP empty")
                lblError.Visible = True
                lbl_Error.Text = "Please input username and password"
            End If
        End If
    End Sub
    Private Function ValidateActiveDirectoryLogin(ByVal Domain As String, ByVal Username As String, ByVal Password As String) As Boolean
        Dim Success As Boolean = False
        Dim Entry As New System.DirectoryServices.DirectoryEntry("LDAP://" & Domain & ":389", Username, Password)
        Dim Searcher As New System.DirectoryServices.DirectorySearcher(Entry)
        Searcher.SearchScope = DirectoryServices.SearchScope.OneLevel
        Try
            Dim Results As System.DirectoryServices.SearchResult = Searcher.FindOne
            Success = Not (Results Is Nothing)
            _ClassFunction.LogSuccess(strLogTime, "LDAP Success - " & txt_username.Text & "")
        Catch ex As Exception
            Success = False
            _ClassFunction.LogError(strLogTime, ex, "LDAP Failed - " & txt_username.Text & "")
            lblError.Visible = True
            lbl_Error.Text = "User LDAP not found"
        End Try
        Return Success
    End Function
    Function AccessLogin(ByVal username As String)
        sql = "EXEC SP_LOGIN_APPLIKASI  " & username & ""
        Try
            sqldr = Proses.ExecuteReader(sql)
            If sqldr.HasRows Then
                sqldr.Read()
                leveluser = sqldr("LAYER").ToString
                Session("UserName") = sqldr("UserName").ToString
                Session("lblUserName") = sqldr("UserName").ToString
                Session("UnitKerja") = sqldr("ORGANIZATION").ToString
                Session("Org") = sqldr("ORGANIZATION_NAME").ToString
                Session("NameKaryawan") = sqldr("NAME").ToString
                Session("LoginType") = sqldr("LAYER").ToString
                Session("lvluser") = sqldr("LevelUser").ToString
                Session("channel_code") = sqldr("CHANNEL_CODE").ToString
                Session("organization") = sqldr("ORGANIZATION").ToString
                Session("orgSupervisor") = sqldr("ORGANIZATION").ToString
                Session("lokasiPengaduan") = ""
                Session("sessionchat") = sqldr("CHAT").ToString
                Session("unitkerjaagent") = sqldr("ORGANIZATION").ToString
                Session("ROLE") = sqldr("LEVELUSER").ToString
                Session("LEVELUSERID") = sqldr("ROLE_ID").ToString
                Session("LoginTypeAngka") = sqldr("NumberNya").ToString
                Session("_LoginState") = sqldr("LoginState").ToString
                Session("NamaGrup") = sqldr("ORGANIZATION_NAME").ToString


                VariabelCookiesUsername.Values("CookiesUserName") = sqldr("UserName").ToString
                VariabelCookiesUsername.Expires = DateTime.Now.AddDays(AddCookiess)
                Response.Cookies.Add(VariabelCookiesUsername)
                sqldr.Close()

                Try
                    Dim query As String = "update msUser set SessionID='" & Session("SessionID") & "' where username='" & Session("username") & "'"
                    Proses.ExecuteNonQuery(query)
                Catch ex As Exception
                    Response.Write(ex.Message)
                End Try
            Else
                Response.Redirect("login.aspx?error=y")
            End If
            'sqldr.Close()
            '_ClassFunction.LogSuccess(strLogTime, sql)
        Catch ex As Exception
            '_ClassFunction.LogError(strLogTime, ex, sql)
            Response.Write(ex.Message)
        End Try

        Dim _uplogin As String = String.Empty
        Try
            _uplogin = "UPDATE MSUSER SET LOGIN='1', IdAUX='9',DescAUX='READY' WHERE USERNAME ='" & username & "'"
            Proses.ExecuteNonQuery(_uplogin)
            _ClassFunction.LogSuccess(strLogTime, _uplogin)
        Catch ex As Exception
            _ClassFunction.LogError(strLogTime, ex, _uplogin)
            Response.Write(ex.Message)
        End Try

        Dim _activity As String = String.Empty
        Try
            _activity = "INSERT INTO ICC_LOG_IN (USERID,ACTIVITY_DATE,AUX_DESCRIPTION) VALUES('" & username & "',GETDATE(),'LOGIN')"
            Proses.ExecuteNonQuery(_activity)
            _ClassFunction.LogSuccess(strLogTime, _activity)
        Catch ex As Exception
            _ClassFunction.LogError(strLogTime, ex, _activity)
            Response.Write(ex.Message)
        End Try
        TemplateStatus()
        Response.Redirect("HTML/new_inbox.aspx?idpage=1012&status=inbox")
    End Function
    Private Function LoginLDAP(LDAP As String, Username As String, Password As String) As Boolean
        If LDAP = "" Then Exit Function
        Dim Entry As New DirectoryServices.DirectoryEntry("LDAP://" & LDAP, Username, Password)
        Dim Searcher As New System.DirectoryServices.DirectorySearcher(Entry)
        Searcher.Filter = "(sAMAccountName=" & Username & ")"
        Try
            Dim Results As System.DirectoryServices.SearchResultCollection = Searcher.FindAll
            Session("NamaLengkap") = Results(0).GetDirectoryEntry.Properties("displayname").Value
            Session("email") = Results(0).GetDirectoryEntry.Properties("mail").Value
            Session("LoginID") = Results(0).GetDirectoryEntry.Properties("sAMAccountName").Value
            Session("NIK") = Results(0).GetDirectoryEntry.Properties("sAMAccountName").Value
            If Session("NIK") = "" Then
                Session("NIK") = Results(0).GetDirectoryEntry.Properties("displayname").Value
            End If
            Session("Jabatan") = Results(0).GetDirectoryEntry.Properties("description").Value
            Session("Department") = Results(0).GetDirectoryEntry.Properties("department").Value
            Session("LoginID") = Results(0).GetDirectoryEntry.Properties("sAMAccountName").Value
            Session("passAsli") = Password
            Return True
        Catch ex As Exception
            'lbl_Alert.Visible = True
            'lbl_Alert.Text = "User ID And Password Are Incorrect!"
        End Try
    End Function
    Private Sub TemplateStatus()
        Dim _strColumn As String = String.Empty
        _strColumn = "select count(*) as data from mStatus"
        Try
            sqldr = Proses.ExecuteReader(_strColumn)
            If sqldr.HasRows Then
                sqldr.Read()
                If sqldr("data").ToString = "1" Then
                    Session("_statusColumn") = "col-md-3 col-sm-3"
                ElseIf sqldr("data").ToString = "2" Then
                    Session("_statusColumn") = "col-md-3 col-sm-3"
                ElseIf sqldr("data").ToString = "3" Then
                    Session("_statusColumn") = "col-md-3 col-sm-3"
                ElseIf sqldr("data").ToString = "4" Then
                    Session("_statusColumn") = "col-md-3 col-sm-3"
                ElseIf sqldr("data").ToString = "5" Then
                    Session("_statusColumn") = "col-md-3 col-sm-3"
                ElseIf sqldr("data").ToString = "6" Then
                    Session("_statusColumn") = "col-md-3 col-sm-3"
                ElseIf sqldr("data").ToString = "7" Then
                    Session("_statusColumn") = "col-md-3 col-sm-3"
                ElseIf sqldr("data").ToString = "8" Then
                    Session("_statusColumn") = "col-md-3 col-sm-3"
                Else
                    Session("_statusColumn") = "col-md-3 col-sm-3"
                End If
            End If
            sqldr.Close()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Function IsValidPasswordFormat(ByVal s As String) As Boolean
        Try
            Return Regex.IsMatch(s, "^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&]{8,}")
        Catch
            Return False
        End Try
        Return True
    End Function
    Function LoginLicense(ByVal TrxUserName As String, ByVal TrxPassword As String)
        Dim strRecCount As String = "Select COUNT (ID) as LDAPCount from ICC_LDAP_Setting WHERE NA='Y'"
        Try
            Using conn As New SqlConnection(connString)
                conn.Open()
                Dim cmd As SqlCommand = New SqlCommand(strRecCount, conn)
                recCount = cmd.ExecuteScalar()
                _ClassFunction.LogSuccess(strLogTime, strRecCount)
            End Using
        Catch ex As Exception
            _ClassFunction.LogError(strLogTime, ex, strRecCount)
            Response.Write(ex.Message)
        End Try
        If recCount <> 0 Then
            Dim strCounting As String = String.Empty
            Dim LDAPServer As String = ConfigurationManager.AppSettings("LDAP")
            Dim _LDAPServerDB As String = String.Empty
            Dim _strLDAP As String = "Select LDAPServer from ICC_LDAP_Setting WHERE NA='Y'"
            readLDAP = Proses.ExecuteReader(_strLDAP)
            Try
                If readLDAP.HasRows Then
                    readLDAP.Read()
                    _LDAPServerDB = readLDAP("LDAPServer")
                Else
                End If
                readLDAP.Close()
            Catch ex As Exception
                Response.Write(ex.Message)
            End Try
            If ValidateActiveDirectoryLogin(_LDAPServerDB, TrxUserName, TrxPassword) = True Then
                If txt_username.Text <> "" Then
                    Try
                        Using conn As New SqlConnection(connString)
                            conn.Open()
                            strCounting = "Select COUNT (UserID) as userID from msUser where UserName=@uservalue"
                            Dim cmd As SqlCommand = New SqlCommand(strCounting, conn)
                            Dim uservalue As SqlParameter = New SqlParameter("@uservalue", SqlDbType.VarChar, 150)
                            uservalue.Value = TrxUserName
                            cmd.Parameters.Add(uservalue)
                            recLDAP = cmd.ExecuteScalar()
                            If recLDAP = 1 Then
                                _ClassFunction.LogSuccess(strLogTime, strCounting)
                                AccessLogin(TrxUserName)
                            Else
                                _ClassFunction.LogSuccess(strLogTime, strCounting)
                                lblError.Visible = True
                                lbl_Error.Text = "User Applikasi not found"
                            End If
                        End Using
                    Catch ex As Exception
                        _ClassFunction.LogError(strLogTime, ex, strCounting)
                        Response.Write(ex.Message)
                    End Try
                Else
                    _ClassFunction.LogSuccess(strLogTime, "login failed, username is empty")
                    lblError.Visible = True
                    lbl_Error.Text = "Please input username and password"
                End If
            Else

            End If
        Else
            If txt_username.Text <> "" Then
                Try
                    Using conn As New SqlConnection(connString)
                        conn.Open()
                        Dim strCounting As String = "Select COUNT (UserID) as userID from msUser where UserName=@uservalue and Password=@password"
                        Dim cmd As SqlCommand = New SqlCommand(strCounting, conn)
                        Dim uservalue As SqlParameter = New SqlParameter("@uservalue", SqlDbType.VarChar, 150)
                        Dim password As SqlParameter = New SqlParameter("@password", SqlDbType.VarChar, 150)
                        uservalue.Value = TrxUserName
                        If (IsValidPasswordFormat(TrxPassword) = True) Then
                            password.Value = TrxPassword
                        Else
                            lblError.Visible = True
                            lbl_Error.Text = "User Applikasi not found"
                            Exit Function
                        End If
                        cmd.Parameters.Add(uservalue)
                        cmd.Parameters.Add(password)
                        recLDAP = cmd.ExecuteScalar()
                        Dim _strLoquser As String = "Select COUNT (UserID) as userID from msUser where UserName='" & TrxUserName & "' and Password='" & TrxPassword & "'"
                        If recLDAP = 1 Then
                            _ClassFunction.LogSuccess(strLogTime, _strLoquser)
                            AccessLogin(txt_username.Text)
                        Else
                            _ClassFunction.LogSuccess(strLogTime, _strLoquser)
                            lblError.Visible = True
                            lbl_Error.Text = "User Applikasi not found"
                            Exit Function
                        End If

                    End Using
                Catch ex As Exception
                    Response.Write(ex.Message)
                End Try
            Else
                _ClassFunction.LogSuccess(strLogTime, "LDAP empty")
                lblError.Visible = True
                lbl_Error.Text = "Please input username and password"
            End If
        End If
    End Function
End Class