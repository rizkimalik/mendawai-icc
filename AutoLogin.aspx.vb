﻿Imports System
Imports System.Web.UI
Imports System.Data.SqlClient
Imports Microsoft.VisualBasic
Public Class AutoLogin
    Inherits System.Web.UI.Page

    Dim leveluser As String
    Dim value As String
    Dim CountLDAP As String = String.Empty
    Dim Con As New SqlConnection(ConfigurationManager.ConnectionStrings("DefaultConnection").ConnectionString)
    Dim sqlcon As New SqlConnection(ConfigurationManager.ConnectionStrings("DefaultConnection").ConnectionString)
    Dim sqlconaux As New SqlConnection(ConfigurationManager.ConnectionStrings("DefaultConnection").ConnectionString)
    Dim com, sqlcom, sqlcomaux As SqlCommand
    Dim loq As New cls_globe
    Dim connString As String = ConfigurationManager.ConnectionStrings("DefaultConnection").ConnectionString
    Dim recLDAP As Integer
    Dim recCount As Integer
    Dim Proses As New ClsConn
    Dim sqldr As SqlDataReader
    Dim sql As String
    Dim _ClassFunction As New WebServiceTransaction
    Dim strLogTime As String = DateTime.Now.ToString("yyyy")
    Dim strSql As String = String.Empty
    Dim ThreadID, ThreadSubject, ThreadPhoneChat As String
    Dim KeepSession As String = "Agent1"
    Dim _TrxLogConvertPhoneChat As String
    Dim _ReturnValue, _TrxValue, _TrxAccount As String
    Dim valuePhone As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim TrxCookiesUserName As String = String.Empty
        Dim CookiesUsername As HttpCookie = HttpContext.Current.Request.Cookies("CookiesUserName")
        TrxCookiesUserName = If(CookiesUsername IsNot Nothing, CookiesUsername.Value.Split("="c)(1), "undefined")
        'Response.Write("xxx" & TrxCookiesUserName)
        TemplateStatus()
        If (Request.QueryString("AgentID") <> "" Or Request.QueryString("AgentID") <> "undefined") Then
            If Request.QueryString("MediaType") <> "" Then
                If TrxCookiesUserName = "undefined" Then
                    'If (String.IsNullOrEmpty((Session("username")))) Then
                    If Request.QueryString("MediaType") = "voice" Then
                        'Response.Redirect("HTML/mainframe.aspx?agentid=" & Request.QueryString("AgentID") & "&channel=" & Request.QueryString("MediaType") & "&genesysid=" & Request.QueryString("CallUUID") & "&threadid=-&account=" & Request.QueryString("PhoneNumber") & "&accountid=" & Request.QueryString("ContactID") & "&subject=-&phoneChat=0&source=inbound&customerid=")
                        _ClassFunction.InsertThreadTicketNumber(Request.QueryString("CallUUID"), Request.QueryString("AgentID"), Request.QueryString("MediaType"))
                        ThreadID = "-"
                        ThreadSubject = "-"
                        ThreadPhoneChat = "-"
                        ThreadInsertTransaction(Request.QueryString("MediaType"), ThreadID, Request.QueryString("CallUUID"), Request.QueryString("PhoneNumber"), Request.QueryString("ContactID"), Request.QueryString("AgentID"), ThreadSubject, ThreadPhoneChat)
                    ElseIf Request.QueryString("MediaType") = "email" Then
                        'Response.Redirect("HTML/mainframe.aspx?agentid=" & Request.QueryString("AgentID") & "&channel=" & Request.QueryString("MediaType") & "&genesysid=" & Request.QueryString("IxnID") & "&threadid=" & Request.QueryString("ThreadID") & "&account=" & Request.QueryString("EmailAddress") & "&accountid=" & Request.QueryString("ContactID") & "&subject=" & Request.QueryString("Subject") & "&phoneChat=0&source=inbound&customerid=")
                        _ClassFunction.InsertThreadTicketNumber(Request.QueryString("IxnID"), Request.QueryString("AgentID"), Request.QueryString("MediaType"))
                        ThreadPhoneChat = "-"
                        ThreadInsertTransaction(Request.QueryString("MediaType"), Request.QueryString("ThreadID"), Request.QueryString("IxnID"), Request.QueryString("EmailAddress"), Request.QueryString("ContactID"), Request.QueryString("AgentID"), Request.QueryString("Subject"), ThreadPhoneChat)
                    ElseIf Request.QueryString("MediaType") = "chat" Then
                        ThreadID = "-"

                        ' 13 Juli 2012 - RFC Kondisi logic nomor telpon
                        If Request.QueryString("PhoneNumber") <> "" Then
                            Dim _subString As String = Left(Request.QueryString("PhoneNumber"), "1")
                            If _subString = "6" Then
                                valuePhone = "0"
                                _ReturnValue = ReplaceAt(Request.QueryString("PhoneNumber"), "0", "2", valuePhone)
                                _TrxValue = _ReturnValue
                            ElseIf _subString = " " Then
                                valuePhone = "0"
                                _ReturnValue = ReplaceAt(Request.QueryString("PhoneNumber"), "0", "3", valuePhone)
                                _TrxValue = _ReturnValue
                            ElseIf _subString = "8" Then
                                valuePhone = "08"
                                _ReturnValue = ReplaceAt(Request.QueryString("PhoneNumber"), "0", "1", valuePhone)
                                _TrxValue = _ReturnValue
                            Else
                                _TrxValue = Request.QueryString("PhoneNumber")
                            End If
                            _TrxAccount = _TrxValue
                            _TrxLogConvertPhoneChat = "ConvertPhoneChat - " + Request.QueryString("PhoneNumber") + " - " + Request.QueryString("AgentID") + " - " + _TrxAccount
                            _ClassFunction.LogSuccess(strLogTime, _TrxLogConvertPhoneChat)
                        Else
                            _TrxAccount = Request.QueryString("EmailAddress")
                            _TrxLogConvertPhoneChat = "ConvertPhoneChat - " + Request.QueryString("EmailAddress") + " - " + Request.QueryString("AgentID")
                            _ClassFunction.LogSuccess(strLogTime, _TrxLogConvertPhoneChat)
                        End If
                        _ClassFunction.InsertThreadTicketNumber(Request.QueryString("IxnID"), Request.QueryString("AgentID"), Request.QueryString("MediaType"))
                        ThreadInsertTransaction(Request.QueryString("MediaType"), ThreadID, Request.QueryString("IxnID"), _TrxAccount, Request.QueryString("ContactID"), Request.QueryString("AgentID"), Request.QueryString("Subject"), Request.QueryString("PhoneNumber"))
                        'Response.Redirect("HTML/mainframe.aspx?agentid=" & Request.QueryString("AgentID") & "&channel=" & Request.QueryString("MediaType") & "&genesysid=" & Request.QueryString("IxnID") & "&threadid=" & Request.QueryString("CIFNo") & "&account=" & Request.QueryString("EmailAddress") & "&accountid=" & Request.QueryString("ContactID") & "&subject=" & Request.QueryString("Subject") & "&phoneChat=" & Request.QueryString("PhoneNumber") & "&source=inbound&customerid=")
                    Else
                        AlertMessage("MediaType is empty")
                    End If
                    Response.Redirect("redirectinfo.html")
                Else
                    If TrxCookiesUserName = Request.QueryString("AgentID") Then
                        If Request.QueryString("MediaType") = "voice" Then
                            'Response.Redirect("HTML/mainframe.aspx?agentid=" & Request.QueryString("AgentID") & "&channel=" & Request.QueryString("MediaType") & "&genesysid=" & Request.QueryString("CallUUID") & "&threadid=-&account=" & Request.QueryString("PhoneNumber") & "&accountid=" & Request.QueryString("ContactID") & "&subject=-&phoneChat=0&source=inbound&customerid=")
                            _ClassFunction.InsertThreadTicketNumber(Request.QueryString("CallUUID"), Request.QueryString("AgentID"), Request.QueryString("MediaType"))
                            ThreadID = "-"
                            ThreadSubject = "-"
                            ThreadPhoneChat = "-"
                            ThreadInsertTransaction(Request.QueryString("MediaType"), ThreadID, Request.QueryString("CallUUID"), Request.QueryString("PhoneNumber"), Request.QueryString("ContactID"), Request.QueryString("AgentID"), ThreadSubject, ThreadPhoneChat)
                        ElseIf Request.QueryString("MediaType") = "email" Then
                            'Response.Redirect("HTML/mainframe.aspx?agentid=" & Request.QueryString("AgentID") & "&channel=" & Request.QueryString("MediaType") & "&genesysid=" & Request.QueryString("IxnID") & "&threadid=" & Request.QueryString("ThreadID") & "&account=" & Request.QueryString("EmailAddress") & "&accountid=" & Request.QueryString("ContactID") & "&subject=" & Request.QueryString("Subject") & "&phoneChat=0&source=inbound&customerid=")
                            _ClassFunction.InsertThreadTicketNumber(Request.QueryString("IxnID"), Request.QueryString("AgentID"), Request.QueryString("MediaType"))
                            ThreadPhoneChat = "-"
                            ThreadInsertTransaction(Request.QueryString("MediaType"), Request.QueryString("ThreadID"), Request.QueryString("IxnID"), Request.QueryString("EmailAddress"), Request.QueryString("ContactID"), Request.QueryString("AgentID"), Request.QueryString("Subject"), ThreadPhoneChat)
                        ElseIf Request.QueryString("MediaType") = "chat" Then
                            ThreadID = "-"
                            ' 13 Juli 2012 - RFC Kondisi logic nomor telpon                           
                            If Request.QueryString("PhoneNumber") <> "" Then
                                Dim _subString As String = Left(Request.QueryString("PhoneNumber"), "1")
                                If _subString = "6" Then
                                    valuePhone = "0"
                                    _ReturnValue = ReplaceAt(Request.QueryString("PhoneNumber"), "0", "2", valuePhone)
                                    _TrxValue = _ReturnValue
                                ElseIf _subString = " " Then
                                    valuePhone = "0"
                                    _ReturnValue = ReplaceAt(Request.QueryString("PhoneNumber"), "0", "3", valuePhone)
                                    _TrxValue = _ReturnValue
                                ElseIf _subString = "8" Then
                                    valuePhone = "08"
                                    _ReturnValue = ReplaceAt(Request.QueryString("PhoneNumber"), "0", "1", valuePhone)
                                    _TrxValue = _ReturnValue
                                Else
                                    _TrxValue = Request.QueryString("PhoneNumber")
                                End If
                                _TrxAccount = _TrxValue
                                _TrxLogConvertPhoneChat = "ConvertPhoneChat - " + Request.QueryString("PhoneNumber") + " - " + Request.QueryString("AgentID") + " - " + _TrxAccount
                                _ClassFunction.LogSuccess(strLogTime, _TrxLogConvertPhoneChat)
                            Else
                                _TrxAccount = Request.QueryString("EmailAddress")
                                _TrxLogConvertPhoneChat = "ConvertPhoneChat - " + Request.QueryString("EmailAddress") + " - " + Request.QueryString("AgentID")
                                _ClassFunction.LogSuccess(strLogTime, _TrxLogConvertPhoneChat)
                            End If
                            _ClassFunction.InsertThreadTicketNumber(Request.QueryString("IxnID"), Request.QueryString("AgentID"), Request.QueryString("MediaType"))
                            ThreadInsertTransaction(Request.QueryString("MediaType"), ThreadID, Request.QueryString("IxnID"), _TrxAccount, Request.QueryString("ContactID"), Request.QueryString("AgentID"), Request.QueryString("Subject"), _TrxValue)
                            'Response.Redirect("HTML/mainframe.aspx?agentid=" & Request.QueryString("AgentID") & "&channel=" & Request.QueryString("MediaType") & "&genesysid=" & Request.QueryString("IxnID") & "&threadid=" & Request.QueryString("CIFNo") & "&account=" & Request.QueryString("EmailAddress") & "&accountid=" & Request.QueryString("ContactID") & "&subject=" & Request.QueryString("Subject") & "&phoneChat=" & Request.QueryString("PhoneNumber") & "&source=inbound&customerid=")
                        Else
                            AlertMessage("MediaType is empty")
                        End If
                        AccessLogin(Request.QueryString("AgentID"))
                    Else
                        Response.Redirect("redirectinfo.html")
                    End If
                End If
            Else
                AlertMessage("Media Type is empty")
            End If
        Else
            AlertMessage("AgentID is empty")
        End If
        If Request.QueryString("error") = "y" Then
            lblError.Visible = True
            lbl_Error.Text = "User is not active"
        Else
        End If
    End Sub
    Function ThreadCountingGenesys(ByVal GenesysID As String)
        strSql = "SELECT COUNT (*) AS DATA FROM TR_THREAD WHERE GenesysNumber='" & GenesysID & "'"
        sqldr = Proses.ExecuteReader(strSql)
        Try
            If sqldr.HasRows() Then
                sqldr.Read()
                If (sqldr("DATA").ToString = "0" Or sqldr("DATA").ToString = "") Then
                    Return False
                Else
                    Return True
                End If
            Else
            End If
            sqldr.Close()
            _ClassFunction.LogSuccess(strLogTime, strSql)
        Catch ex As Exception
            _ClassFunction.LogError(strLogTime, ex, strSql)
            Response.Write(ex.Message)
        End Try
    End Function
    Function ThreadCustomerCountingAccount(ByVal Account As String, ByVal MediaType As String, ByVal PhoneChat As String)
        If MediaType = "chat" Then
            If PhoneChat <> "" Then
                strSql = "SELECT COUNT (*) AS DATA FROM mCustomerChannel WHERE ValueChannel='" & PhoneChat & "'"
            Else
                strSql = "SELECT COUNT (*) AS DATA FROM mCustomerChannel WHERE ValueChannel='" & Account & "'"
            End If
        Else
            strSql = "SELECT COUNT (*) AS DATA FROM mCustomerChannel WHERE ValueChannel='" & Account & "'"
        End If
        sqldr = Proses.ExecuteReader(strSql)
        Try
            If sqldr.HasRows() Then
                sqldr.Read()
                If (sqldr("DATA").ToString = "0" Or sqldr("DATA").ToString = "") Then
                    _ClassFunction.LogSuccess(strLogTime, strSql)
                    Return False
                Else
                    _ClassFunction.LogSuccess(strLogTime, strSql)
                    Return True
                End If
            Else
            End If
            sqldr.Close()
        Catch ex As Exception
            _ClassFunction.LogError(strLogTime, ex, strSql)
            Response.Write(ex.Message)
        End Try
    End Function
    Function ThreadInsertTransaction(ByVal MediaType As String, ByVal ThreadID As String, ByVal GenesysNumber As String, ByVal Account As String, ByVal AccountContactID As String, ByVal AgentID As String, ByVal Subject As String, ByVal PhoneChat As String)
        Dim strTime As String = DateTime.Now.ToString("yyyyMMddhhmmss")
        If ThreadCountingGenesys(GenesysNumber) = False Then
            If (ThreadCustomerCountingAccount(Account, MediaType, PhoneChat)) = True Then
                Try
                    strSql = "INSERT INTO TR_THREAD (ValueThread, ThreadID, GenesysNumber, Account, AccountContactID, AgentID, Subject, PhoneChat, CustomerID, ThreadTicket) VALUES('" & MediaType & "','" & ThreadID & "','" & GenesysNumber & "','" & Account & "','" & AccountContactID & "','" & AgentID & "','" & Subject & "','" & PhoneChat & "', '" & GetValueCustomerID(Account, PhoneChat) & "', '" & GetValueThreadTicketID(GenesysNumber, AgentID) & "')"
                    Proses.ExecuteNonQuery(strSql)
                    _ClassFunction.LogSuccess(strLogTime, "Insert Tabel TR_THREAD - " & strSql)
                Catch ex As Exception
                    _ClassFunction.LogError(strLogTime, ex, "Insert Tabel TR_THREAD - " & strSql)
                End Try
            Else
                Try
                    strSql = "INSERT INTO TR_THREAD (ValueThread, ThreadID, GenesysNumber, Account, AccountContactID, AgentID, Subject, PhoneChat, ThreadTicket) VALUES('" & MediaType & "','" & ThreadID & "','" & GenesysNumber & "','" & Account & "','" & AccountContactID & "','" & AgentID & "','" & Subject & "','" & PhoneChat & "', '" & GetValueThreadTicketID(GenesysNumber, AgentID) & "')"
                    Proses.ExecuteNonQuery(strSql)
                    _ClassFunction.LogSuccess(strLogTime, "Insert Tabel TR_THREAD - " & strSql)
                Catch ex As Exception
                    _ClassFunction.LogError(strLogTime, ex, "Insert Tabel TR_THREAD - " & strSql)
                End Try
            End If
        Else
            _ClassFunction.LogSuccess(strLogTime, "Check Data Genesys Number - " & GenesysNumber)
        End If
    End Function
    Function GetValueCustomerID(ByVal _Value As String, ByVal _Value2 As String)
        strSql = "select CustomerID from mCustomerChannel WHERE (ValueChannel='" & _Value & "' or ValueChannel='" & _Value2 & "')"
        sqldr = Proses.ExecuteReader(strSql)
        Try
            If sqldr.HasRows() Then
                sqldr.Read()
                Return sqldr("CustomerID").ToString
            Else
                Return ""
            End If
            sqldr.Close()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Function
    Function AccessLogin(ByVal AgentID As String)
        'sql = "EXEC SP_LOGIN_APPLIKASI  " & AgentID & ""
        'Try
        '    sqldr = Proses.ExecuteReader(sql)
        '    If sqldr.HasRows Then
        '        sqldr.Read()
        '        leveluser = sqldr("LAYER").ToString
        '        Session("UserName") = sqldr("UserName").ToString
        '        Session("lblUserName") = sqldr("UserName").ToString
        '        Session("UnitKerja") = sqldr("UNITKERJA").ToString
        '        Session("Org") = sqldr("ORGANIZATION_NAME").ToString
        '        Session("NameKaryawan") = sqldr("NAME").ToString
        '        Session("LoginType") = sqldr("LAYER").ToString
        '        Session("lvluser") = sqldr("LevelUser").ToString
        '        Session("channel_code") = sqldr("CHANNEL_CODE").ToString
        '        Session("organization") = sqldr("ORGANIZATION").ToString
        '        Session("orgSupervisor") = sqldr("ORGANIZATION").ToString
        '        Session("lokasiPengaduan") = ""
        '        Session("sessionchat") = sqldr("CHAT").ToString
        '        Session("unitkerjaagent") = sqldr("IdGrup").ToString
        '        Session("ROLE") = sqldr("LEVELUSER").ToString
        '        Session("LEVELUSERID") = sqldr("ROLE_ID").ToString
        '        Session("LoginTypeAngka") = sqldr("NumberNya").ToString
        '        Session("_LoginState") = sqldr("LoginState").ToString
        '        Session("NamaGrup") = sqldr("NamaGrup").ToString
        '    Else
        '        Response.Redirect("login.aspx?error=y")
        '    End If
        '    sqldr.Close()
        '    _ClassFunction.LogSuccess(strLogTime, sql)
        'Catch ex As Exception
        '    _ClassFunction.LogError(strLogTime, ex, sql)
        '    Response.Write(ex.Message)
        'End Try

        Dim updlogin As String = String.Empty
        Try
            updlogin = "UPDATE MSUSER SET LOGIN='1', IdAUX='9', DescAUX='READY' WHERE USERNAME ='" & AgentID & "'"
            Proses.ExecuteNonQuery(updlogin)
            _ClassFunction.LogSuccess(strLogTime, updlogin)
        Catch ex As Exception
            _ClassFunction.LogError(strLogTime, ex, updlogin)
            Response.Write(ex.Message)
        End Try

        Dim ilogin As String = "INSERT INTO ICC_LOG_IN (USERID,ACTIVITY_DATE,AUX_DESCRIPTION) VALUES('" & AgentID & "',GETDATE(),'LOGIN')"
        sqlcom = New SqlCommand(ilogin, sqlcon)
        Try
            sqlcon.Open()
            sqlcom.ExecuteNonQuery()
            sqlcon.Close()
            _ClassFunction.LogSuccess(strLogTime, ilogin)
        Catch ex As Exception
            _ClassFunction.LogError(strLogTime, ex, ilogin)
            Response.Write(ex.Message)
        End Try

        If Request.QueryString("MediaType") = "voice" Then
            Response.Redirect("HTML/mainframe.aspx?agentid=" & Request.QueryString("AgentID") & "&channel=" & Request.QueryString("MediaType") & "&genesysid=" & Request.QueryString("CallUUID") & "&threadid=-&account=" & Request.QueryString("PhoneNumber") & "&accountid=" & Request.QueryString("ContactID") & "&subject=-&phoneChat=0&source=inbound&customerid=")
        ElseIf Request.QueryString("MediaType") = "email" Then
            Response.Redirect("HTML/mainframe.aspx?agentid=" & Request.QueryString("AgentID") & "&channel=" & Request.QueryString("MediaType") & "&genesysid=" & Request.QueryString("IxnID") & "&threadid=" & Request.QueryString("ThreadID") & "&account=" & Request.QueryString("EmailAddress") & "&accountid=" & Request.QueryString("ContactID") & "&subject=" & Request.QueryString("Subject") & "&phoneChat=0&source=inbound&customerid=")
        ElseIf Request.QueryString("MediaType") = "chat" Then
            ' 13 Juli 2012 - RFC Kondisi logic nomor telpon
            If Request.QueryString("CIFNo") = "" Then
                ThreadID = "-"
            Else
                ThreadID = Request.QueryString("CIFNo")
            End If
            Dim _ReturnValue, _TrxValue As String
            If Request.QueryString("PhoneNumber") <> "" Then
                Dim _subString As String = Left(Request.QueryString("PhoneNumber"), "1")
                If _subString = "6" Then
                    valuePhone = "0"
                    _ReturnValue = ReplaceAt(Request.QueryString("PhoneNumber"), "0", "2", valuePhone)
                    _TrxValue = _ReturnValue
                ElseIf _subString = " " Then
                    valuePhone = "0"
                    _ReturnValue = ReplaceAt(Request.QueryString("PhoneNumber"), "0", "3", valuePhone)
                    _TrxValue = _ReturnValue
                ElseIf _subString = "8" Then
                    valuePhone = "08"
                    _ReturnValue = ReplaceAt(Request.QueryString("PhoneNumber"), "0", "1", valuePhone)
                    _TrxValue = _ReturnValue
                Else
                    _TrxValue = Request.QueryString("PhoneNumber")
                End If
                _TrxAccount = _TrxValue
                Response.Redirect("HTML/mainframe.aspx?agentid=" & Request.QueryString("AgentID") & "&channel=" & Request.QueryString("MediaType") & "&genesysid=" & Request.QueryString("IxnID") & "&threadid=" & ThreadID & "&account=" & _TrxAccount & "&accountid=" & Request.QueryString("ContactID") & "&subject=" & Request.QueryString("Subject") & "&phoneChat=" & _TrxValue & "&emailaddress=" & Request.QueryString("EmailAddress") & "&source=inbound&customerid=")
            Else
                _TrxAccount = Request.QueryString("EmailAddress")
                Response.Redirect("HTML/mainframe.aspx?agentid=" & Request.QueryString("AgentID") & "&channel=" & Request.QueryString("MediaType") & "&genesysid=" & Request.QueryString("IxnID") & "&threadid=" & ThreadID & "&account=" & _TrxAccount & "&accountid=" & Request.QueryString("ContactID") & "&subject=" & Request.QueryString("Subject") & "&phoneChat=" & _TrxValue & "&source=inbound&customerid=")
            End If
        Else
            AlertMessage("MediaType is empty")
        End If
    End Function
    Function AlertMessage(ByVal alert As String)
        Dim message As String = alert
        Dim sb As New System.Text.StringBuilder()
        sb.Append("<script type = 'text/javascript'>")
        sb.Append("window.onload=function(){")
        sb.Append("alert('")
        sb.Append(message)
        sb.Append("')};")
        sb.Append("</script>")
        ClientScript.RegisterClientScriptBlock(Me.GetType(), "alert", sb.ToString())
        Response.Write("<script>window.close();</script>")
    End Function
    Private Function ValidateActiveDirectoryLogin(ByVal Domain As String, ByVal Username As String, ByVal Password As String) As Boolean
        Dim Success As Boolean = False
        Dim Entry As New System.DirectoryServices.DirectoryEntry("LDAP://" & Domain, Username, Password)
        Dim Searcher As New System.DirectoryServices.DirectorySearcher(Entry)
        Searcher.SearchScope = DirectoryServices.SearchScope.OneLevel
        Try
            Dim Results As System.DirectoryServices.SearchResult = Searcher.FindOne
            Success = Not (Results Is Nothing)
            _ClassFunction.LogSuccess(strLogTime, "login success, user LDAP exits " & txt_username.Text & "")
        Catch ex As Exception
            Success = False
            _ClassFunction.LogError(strLogTime, ex, "login failed, user LDAP not found")
            lblError.Visible = True
            lbl_Error.Text = "User LDAP not found"
        End Try
        Return Success
    End Function
    Private Sub Btn_Simpan_ServerClick(sender As Object, e As EventArgs) Handles Btn_Simpan.ServerClick
        Dim strRecCount As String = "Select COUNT (ID) as LDAPCount from ICC_LDAP_Setting WHERE NA='Y'"
        Try
            Using conn As New SqlConnection(connString)
                conn.Open()
                Dim cmd As SqlCommand = New SqlCommand(strRecCount, conn)
                recCount = cmd.ExecuteScalar()
                _ClassFunction.LogSuccess(strLogTime, strRecCount)
            End Using
        Catch ex As Exception
            _ClassFunction.LogError(strLogTime, ex, strRecCount)
            Response.Write(ex.Message)
        End Try
        If recCount <> 0 Then
            Dim strCounting As String = String.Empty
            Dim LDAPServer As String = ConfigurationManager.AppSettings("LDAP")
            If ValidateActiveDirectoryLogin(LDAPServer, txt_username.Text, txt_password.Text) = True Then
                If txt_username.Text <> "" Then
                    Try
                        Using conn As New SqlConnection(connString)
                            conn.Open()
                            strCounting = "Select COUNT (UserID) as userID from msUser where UserName=@uservalue"
                            Dim cmd As SqlCommand = New SqlCommand(strCounting, conn)
                            Dim uservalue As SqlParameter = New SqlParameter("@uservalue", SqlDbType.VarChar, 150)
                            uservalue.Value = txt_username.Text
                            cmd.Parameters.Add(uservalue)
                            recLDAP = cmd.ExecuteScalar()
                            If recLDAP = 1 Then
                                _ClassFunction.LogSuccess(strLogTime, strCounting)
                                AccessLogin(txt_username.Text)
                            Else
                                _ClassFunction.LogSuccess(strLogTime, strCounting)
                                lblError.Visible = True
                                lbl_Error.Text = "User Applikasi not found"
                            End If
                        End Using
                    Catch ex As Exception
                        _ClassFunction.LogError(strLogTime, ex, strCounting)
                        Response.Write(ex.Message)
                    End Try
                Else
                    _ClassFunction.LogSuccess(strLogTime, "login failed, username is empty")
                    lblError.Visible = True
                    lbl_Error.Text = "Please input username and password"
                End If
            Else

            End If
        Else
            If txt_username.Text <> "" Then
                Try
                    Using conn As New SqlConnection(connString)
                        conn.Open()
                        Dim strCounting As String = "Select COUNT (UserID) as userID from msUser where UserName=@uservalue"
                        Dim cmd As SqlCommand = New SqlCommand(strCounting, conn)
                        Dim uservalue As SqlParameter = New SqlParameter("@uservalue", SqlDbType.VarChar, 150)
                        uservalue.Value = txt_username.Text
                        cmd.Parameters.Add(uservalue)
                        recLDAP = cmd.ExecuteScalar()

                        If recLDAP = 1 Then
                            _ClassFunction.LogSuccess(strLogTime, strCounting)
                            AccessLogin(txt_username.Text)
                        Else
                            _ClassFunction.LogSuccess(strLogTime, strCounting)
                            lblError.Visible = True
                            lbl_Error.Text = "User Applikasi not found"
                        End If

                    End Using
                Catch ex As Exception
                    Response.Write(ex.Message)
                End Try
            Else
                _ClassFunction.LogSuccess(strLogTime, "LDAP empty")
                lblError.Visible = True
                lbl_Error.Text = "Please input username and password"
            End If
        End If
        TemplateStatus()
    End Sub
    Private Sub TemplateStatus()
        'Dim _strColumn As String = String.Empty
        '_strColumn = "select count(*) as data from mStatus"
        'Try
        '    sqldr = Proses.ExecuteReader(_strColumn)
        '    If sqldr.HasRows Then
        '        sqldr.Read()
        '        If sqldr("data").ToString = "1" Then
        '            Session("_statusColumn") = "col-md-12 col-sm-1"
        '        ElseIf sqldr("data").ToString = "2" Then
        '            Session("_statusColumn") = "col-md-6 col-sm-2"
        '        ElseIf sqldr("data").ToString = "3" Then
        '            Session("_statusColumn") = "col-md-4 col-sm-3"
        '        ElseIf sqldr("data").ToString = "4" Then
        '            Session("_statusColumn") = "col-md-3 col-sm-4"
        '        ElseIf sqldr("data").ToString = "5" Then
        '            Session("_statusColumn") = "col-md-2 col-sm-2"
        '        ElseIf sqldr("data").ToString = "6" Then
        '            Session("_statusColumn") = "col-md-2 col-sm-2"
        '        ElseIf sqldr("data").ToString = "7" Then
        '            Session("_statusColumn") = "col-md-2 col-sm-2"
        '        ElseIf sqldr("data").ToString = "8" Then
        '            Session("_statusColumn") = "col-md-3 col-sm-4"
        '        Else
        '            Session("_statusColumn") = "col-md-3 col-sm-4"
        '        End If
        '    End If
        '    sqldr.Close()
        'Catch ex As Exception
        '    Response.Write(ex.Message)
        'End Try
    End Sub
    Private Sub Paremeter()
        'param1=AgentID, param2=channel, param3=genesysID, param4=ThreadID, param5=account, param6=accountid= param7=subject
        'Dim urlString = "AutoLogin.aspx?param1=agent1&param2=email&param3=7993048&param4=799304&param5=085782431288&param6=200107114154&param7=-"
        'param1=AgentID, param2=channel, param3=genesysID, param4=ThreadID, param5=account, param6=accountid= param7=subject
        'Dim urlString = "AutoLogin.aspx?MediaType=voice&CallUUID=G1UIAPR46P4CFB6IPN41KG9RJK00009L&PhoneNumber=7993048&AgentID=799304&param5=085782431288&param6=200107114154&param7=-"
    End Sub
    Function ReplaceAt(ByVal str As String, ByVal index As Integer, ByVal length As Integer, ByVal replace As String) As String
        ' 13 Juli 2012 - RFC Kondisi logic nomor telpon
        Dim TmpStr As String
        TmpStr = str
        Dim subString As String = Left(TmpStr, "1")
        If subString = "6" Then
            Return TmpStr.Remove(index, Math.Min(length, TmpStr.Length - index)).Insert(index, replace)
        ElseIf subString = "8" Then
            Return TmpStr.Remove(index, Math.Min(length, TmpStr.Length - index)).Insert(index, replace)
        ElseIf subString = " " Then
            Return TmpStr.Remove(index, Math.Min(length, TmpStr.Length - index)).Insert(index, replace)
        Else
            ReplaceAt = TmpStr
        End If
    End Function
    Function GetValueThreadTicketID(ByVal _Value1 As String, ByVal _Value2 As String)
        Dim _TrxThreadTicketID As String = DateTime.Now.ToString("yyyyMMddhhmmssfff")
        Dim _strSql As String
        _strSql = "Select ThreadTicket from Temp_ThreadTicketID WHERE GenesysNumber='" & _Value1 & "' And Agent='" & _Value2 & "'"
        sqldr = Proses.ExecuteReader(_strSql)
        Try
            If sqldr.HasRows() Then
                sqldr.Read()
                _ClassFunction.LogSuccess(strLogTime, _strSql)
                Return sqldr("ThreadTicket").ToString
            Else
                _ClassFunction.LogSuccess(strLogTime, "GetValueThreadTicketID - " & _strSql)
                Return _TrxThreadTicketID
            End If
            sqldr.Close()
        Catch ex As Exception
            _ClassFunction.LogError(strLogTime, ex, "GetValueThreadTicketID - " & _strSql)
        End Try
    End Function
End Class