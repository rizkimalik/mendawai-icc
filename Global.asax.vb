﻿Imports System.Data.SqlClient
Imports System.Web.SessionState

Public Class Global_asax
    Inherits System.Web.HttpApplication


    Private Shared ReadOnly _sessions As New List(Of String)()
    Public Shared ReadOnly Property Sessions As List(Of String)
        Get
            Return _sessions
        End Get
    End Property

    Dim Proses As New ClsConn
    Dim sql As String

    Sub Application_Start(ByVal sender As Object, ByVal e As EventArgs)
        ' Fires when the application is started
    End Sub

    Sub Session_Start(ByVal sender As Object, ByVal e As EventArgs)
        ' Fires when the session is started
        _sessions.Add(Session.SessionID)
        Session("SessionID") = Session.SessionID.ToString
    End Sub

    Sub Application_BeginRequest(ByVal sender As Object, ByVal e As EventArgs)
        ' Fires at the beginning of each request
    End Sub

    Sub Application_AuthenticateRequest(ByVal sender As Object, ByVal e As EventArgs)
        ' Fires upon attempting to authenticate the use
    End Sub

    Sub Application_Error(ByVal sender As Object, ByVal e As EventArgs)
        ' Fires when an error occurs
    End Sub

    Sub Session_End(ByVal sender As Object, ByVal e As EventArgs)
        ' Fires when the session ends
        _sessions.Remove(Session.SessionID)
        UpdateSessionUser(Session.SessionID)
    End Sub

    Sub Application_End(ByVal sender As Object, ByVal e As EventArgs)
        ' Fires when the application ends
    End Sub

    Private Sub UpdateSessionUser(ByVal SessionID As String)
        Try
            sql = "update msUser set login='0' where SessionID='" & SessionID & "'"
            Proses.ExecuteReader(sql)
            Response.Redirect("local.aspx")
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

End Class