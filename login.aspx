﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="login.aspx.vb" Inherits="ICC.login" %>

<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxDataView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxHtmlEditor.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxHtmlEditor" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxCallbackPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxFormLayout" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxCallback" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>

<!DOCTYPE html>

<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Login</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="helpdesk ticketing selindo mendawai" />
    <meta name="author" content="selindo" />
    <link rel="icon" type="image/svg+xml" href="favicon.ico" />

    <!-- Bootstrap core CSS -->
    <link href="HTML/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
    <!-- Font Awesome -->
    <link href="HTML/css/font-awesome.min.css" rel="stylesheet" />
    <!-- Endless -->
    <link href="HTML/css/endless.min.css" rel="stylesheet" />
    <script>
        function GetPassword() {
            var username = document.getElementById('txt_forgetusername').value;
            var email = document.getElementById('txt_forgetemail').value;
            //alert("username = " + username +" | email = "+ email);
            $.ajax({
                type: 'GET',
                async: false,
                url: "html/Ajaxpages/loadProblem.aspx?ket=getpassword&username=" + username + "&email=" + email,
                cache: false,
                success: function (result) {
                    //alert(result);
                    document.getElementById('txt_forgetpassword').value = result;
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    counter++;
                    messageDiv.empty();
                    messageDiv.append("thrown error: " + thrownError);
                    messageDiv.append("<br />");
                    messageDiv.append("status text: " + xhr.statusText);
                    messageDiv.append("<br />");
                    messageDiv.append("counter = " + counter);
                }
            });
        }

        function forgot() {
            var username = document.getElementById("ASPxPopupControl2_txt_forgot_username").value;
            var email = document.getElementById("ASPxPopupControl2_txt_forgot_email").value;
            //alert(username);
            //alert(email);
            //alert(repassword);
            if (username == "") {
                alert("Username is empty");
                return true
            }
            if (email == "") {
                alert("Email is empty");
                return true
            }

            var test = new FormData();
            $.ajax({
                type: 'POST',
                url: "HTML/Ajaxpages/forgotPassword.aspx?username=" + username + "&email=" + email,
                contentType: false,
                processData: false,
                data: test,
                success: function (result) {
                    //alert(result)
                    if (result == "1") {
                        alert("Password send to email, please check email");
                        popuppasword.Hide();
                    }
                    else {
                        alert("User or password incorrect");
                    }
                }
            });
        };
  
        function ShowPop() {
            popuppasword.Show();
        }
    </script>
</head>

<body style="background-image: url(<%= Session("image")%>); background-size: 1360px;">
    <form runat="server">
        <div class="login-wrapper">
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <div class="login-widget animation-delay1">
                <div class="panel panel-default">
                    <div class="panel-heading clearfix">
                        <div class="pull-right">
                            <img src="HTML/img/mendawai-hitam.png" width="160" height="50" class="img-responsive" />
                        </div>
                    </div>
                    <div class="panel-body">
                        <div class="form-group">
                            <label><i class="fa fa-user"></i>&nbsp;Username</label>
                            <asp:TextBox ID="txt_username" runat="server" CssClass="form-control input-sm bounceIn animation-delay2"></asp:TextBox>
                        </div>
                        <div class="form-group">
                            <label><i class="fa fa-question-circle"></i>&nbsp;Password</label>
                            <asp:TextBox ID="txt_password" runat="server" CssClass="form-control input-sm bounceIn animation-delay4" TextMode="Password"></asp:TextBox>
                        </div>
                         <div class="form-group" style="margin-bottom:-5px;">
                            <span class="badge badge-info" style="margin-bottom: 5px;"><a href="local.aspx" style="color:white;">Using Login To Layer 3</a></span>
                        </div>
                        <div class="row" id="lblError" runat="server" visible="false">
                            <div class="col-sm-12">
                                <div class="alert alert-danger">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true" id="B_notError" runat="server">&times;</button>
                                    <strong>
                                        <asp:Label ID="lbl_Error" runat="server"></asp:Label>
                                    </strong>
                                </div>
                            </div>
                        </div>
                    </div>
                     <div class="panel-footer">
                        <div class="text-right">
                            <button id="Btn_Simpan" runat="server" class="btn btn-info" type="submit">
                                <i class="fa fa-sign-in"></i>&nbsp;Sign in</button>
                        </div>
                    </div>
                    <br />
                    <!-- /.modal -->
                </div>
                <!-- /panel -->
            </div>
            <!-- /login-widget -->
        </div>
        <!-- /login-wrapper -->
        <div class="modal fade" id="simpleModal">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4>Forget Password</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-lg-4">
                                <div class="form-group">
                                    USERNAME
                                        <asp:TextBox ID="txt_forgetusername" runat="server" CssClass="form-control input-sm bounceIn animation-delay2" AutoCompleteType="Disabled"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="form-group">
                                    EMAIL
                                        <asp:TextBox ID="txt_forgetemail" runat="server" CssClass="form-control input-sm bounceIn animation-delay3" AutoCompleteType="Disabled"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="form-group">
                                    PASSWORD
                                        <asp:TextBox ID="txt_forgetpassword" runat="server" CssClass="form-control input-sm bounceIn animation-delay4" AutoCompleteType="Disabled" ReadOnly="true" Font-Bold="true"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <%--   <div class="row">
                            <div class="col-lg-2">
                                <div style="padding: 8px"></div>
                              
                            </div>                            
                        </div>
                        <div class="row">
                           
                        </div>--%>
                    </div>
                    <div class="modal-footer">
                        <a href="#" onclick="GetPassword()" class="btn btn-sm btn-info"><i class="fa fa-retweet"></i>Submit</a>
                        <button class="btn btn-sm btn-info" data-dismiss="modal" aria-hidden="true"><i class="fa fa-arrow-circle-left"></i>Cancel</button>
                    </div>
                </div>
                <!-- /.modal-content -->
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
        <dx:ASPxPopupControl ID="ASPxPopupControl2" ClientInstanceName="popuppasword" runat="server" CloseAction="CloseButton" Modal="true" Width="800px" Height="300px"
            closeonescape="true"
            PopupVerticalAlign="WindowCenter"
            PopupHorizontalAlign="WindowCenter" AllowDragging="true" Theme="SoftOrange"
            ShowFooter="True" HeaderText="Forgot Password" FooterText="" AutoUpdatePosition="true">
            <ContentCollection>
                <dx:PopupControlContentControl ID="PopupControlContentControl2" runat="server">
                    <div class="row">
                        <div class="col-md-12 col-sm-12">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="row">
                                        <div class="col-md-12 col-sm-12">
                                            <label><i class="fa fa-user"></i>&nbsp;Username</label>
                                            <asp:TextBox ID="txt_forgot_username" runat="server" CssClass="form-control input-sm bounceIn animation-delay1"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <br />
                            <div class="row" id="repass" runat="server" visible="true">
                                <div class="col-md-12 col-sm-12">
                                    <div class="row">
                                        <div class="col-md-12 col-sm-12">
                                            <label><i class="fa fa-envelope"></i>&nbsp;Email</label>
                                            <asp:TextBox ID="txt_forgot_email" runat="server" CssClass="form-control input-sm bounceIn animation-delay2"></asp:TextBox>
                                        </div>
                                    </div>
                                    <br />
                                    <div class="row">
                                        <div class="col-md-12 col-sm-12">
                                            <div class="form-group">
                                                <div class="text-right">
                                                    <a onclick="forgot()" class="btn btn-danger" id="btn_change"><i class="fa fa-save"></i>&nbsp;Save</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </dx:PopupControlContentControl>
            </ContentCollection>
        </dx:ASPxPopupControl>
    </form>

    <!-- Le javascript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <!-- Jquery -->
    <script src="HTML/js/jquery-1.10.2.min.js"></script>
    <!-- Bootstrap -->
    <script src="HTML/bootstrap/js/bootstrap.min.js"></script>
    <!-- Modernizr -->
    <script src="HTML/js/modernizr.min.js"></script>
    <!-- Pace -->
    <script src="HTML/js/pace.min.js"></script>
    <!-- Popup Overlay -->
    <script src="HTML/js/jquery.popupoverlay.min.js"></script>
    <!-- Slimscroll -->
    <script src="HTML/js/jquery.slimscroll.min.js"></script>
    <!-- Cookie -->
    <script src="HTML/js/jquery.cookie.min.js"></script>
    <!-- Endless -->
    <script src="HTML/js/endless/endless.js"></script>
</body>
</html>
