﻿Imports System.ComponentModel
Imports System.Web.Services
Imports System.Web.Script.Services
Imports System.Web.Script.Serialization
Imports System.Data.SqlClient

<System.Web.Services.WebService(Namespace:="http://tempuri.org/")>
<System.Web.Services.WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)>
<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
<ScriptService()>
<ToolboxItem(False)>
Public Class TicketHistoryTransaction
    Inherits System.Web.Services.WebService

    Private ReadOnly js As New JavaScriptSerializer()
    Private ReadOnly sqlcon As New SqlConnection(ConfigurationManager.ConnectionStrings("DefaultConnection").ConnectionString)
    Dim sqlcom As SqlCommand
    Dim sqldr As SqlDataReader
    Dim strQuery As String = String.Empty

    Public Class ListUser
        Public Property Username As String
        Public Property Name As String
    End Class


    <WebMethod()>
    <ScriptMethod(UseHttpGet:=False, ResponseFormat:=ResponseFormat.Json)>
    Public Function ListAllUser() As String
        Try
            strQuery = "SELECT top 5 * FROM msuser"
            sqlcom = New SqlCommand(strQuery, sqlcon)
            sqlcon.Open()
            sqldr = sqlcom.ExecuteReader()

            Dim DataListUser As New List(Of ListUser)()
            While sqldr.Read()
                Dim obj As New ListUser With {
                    .Username = sqldr("USERNAME").ToString(),
                    .Name = sqldr("NAME").ToString()
                }
                DataListUser.Add(obj)
            End While
            Return js.Serialize(DataListUser)

        Catch ex As Exception
            Return js.Serialize(ex.Message)
        End Try
    End Function

End Class


