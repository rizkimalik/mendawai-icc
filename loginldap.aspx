﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="loginldap.aspx.vb" Inherits="ICC.loginldap" %>

<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxDataView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxHtmlEditor.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxHtmlEditor" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxCallbackPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxFormLayout" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxCallback" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>


<!DOCTYPE html>

<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Login</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Bootstrap core CSS -->
    <link href="HTML/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
    <link href="documentation/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
    <!-- Font Awesome -->
    <link href="HTML/css/font-awesome.min.css" rel="stylesheet" />
    <!-- Endless -->
    <link href="HTML/css/endless.min.css" rel="stylesheet" />
    <script>
        function GetPassword() {
            var username = document.getElementById('txt_forgetusername').value;
            var email = document.getElementById('txt_forgetemail').value;
            //alert("username = " + username +" | email = "+ email);
            $.ajax({
                type: 'GET',
                async: false,
                url: "html/Ajaxpages/loadProblem.aspx?ket=getpassword&username=" + username + "&email=" + email,
                cache: false,
                success: function (result) {
                    //alert(result);
                    document.getElementById('txt_forgetpassword').value = result;
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    counter++;
                    messageDiv.empty();
                    messageDiv.append("thrown error: " + thrownError);
                    messageDiv.append("<br />");
                    messageDiv.append("status text: " + xhr.statusText);
                    messageDiv.append("<br />");
                    messageDiv.append("counter = " + counter);
                }
            });
        }
    </script>
     <script>
         function forgot() {
             var username = document.getElementById("ASPxPopupControl2_txt_forgot_username").value;
             var email = document.getElementById("ASPxPopupControl2_txt_forgot_email").value;
             //alert(username);
             //alert(email);
             //alert(repassword);
             if (username == "") {
                 alert("Username is empty");
                 return true
             }
             if (email == "") {
                 alert("Email is empty");
                 return true
             }

             var test = new FormData();
             $.ajax({
                 type: 'POST',
                 url: "HTML/Ajaxpages/forgotPassword.aspx?username=" + username + "&email=" + email,
                 contentType: false,
                 processData: false,
                 data: test,
                 success: function (result) {
                     //alert(result)
                     if (result == "1") {
                         alert("Password send to email, please check email");
                         popuppasword.Hide();
                     }
                     else {
                         alert("User or password incorrect");
                     }
                 }
             });
         };
    </script>
     <script type="text/javascript">
         function ShowPop() {
             popuppasword.Show();
         }
    </script>
</head>

<%--<body class="overflow-hidden" style="background-image:url(Images/SMLBackground.jpeg); background-size:1360px;">--%>
<body style="background-image: url(<%= Session("image")%>); background-size: 1360px;">
    <form id="Form1" runat="server">
        <div class="login-wrapper">
            <br />
            <br />
            <div class="login-widget animation-delay1">
                <div class="panel panel-default">
                    <div class="panel-heading clearfix">
                        <center><img src="HTML/img/logobtn.jpg" width="160" class="img-responsive" /></center>
                        <div class="pull-left">
                            <i class="fa fa-lock fa-lg"></i>&nbsp;Login				
                        </div>
                    </div>
                    <div class="panel-body">
                        <div class="form-group">
                            <label>&nbsp;Nama Outlet</label>
                            <%--                            <input type="text" placeholder="Username" class="form-control input-sm bounceIn animation-delay2">--%>
                            <dx:ASPxComboBox ID="BTN_cmbDataKantor" ClientInstanceName="BTN_cmbDataKantor" runat="server" TextField="NamaKCP" Theme="MetropolisBlue" CssClass="form-control input-sm"
                                ValueField="Kode" EnableCallbackMode="true" IncrementalFilteringMode="Contains"
                                ItemStyle-HoverStyle-BackColor="#F37021" DataSourceID="dsBTN_DataKantor"
                                Width="100%">
                                <ItemStyle>
                                    <HoverStyle BackColor="#0076c4" ForeColor="#ffffff">
                                    </HoverStyle>
                                </ItemStyle>
                            </dx:ASPxComboBox>
                            <asp:SqlDataSource ID="dsBTN_DataKantor" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>" SelectCommand="select Kode,Kode +'-'+  Jenis +'-'+ NamaDaerah as NamaKCP from BTN_DataKantor where NamaDaerah is not null Group By Kode,NamaDaerah,Jenis order by NamaDaerah"></asp:SqlDataSource>
                        </div>
                        <div class="form-group">
                            <label><i class="fa fa-user"></i>&nbsp;Username</label>
                            <asp:TextBox ID="txt_username" runat="server" CssClass="form-control input-sm bounceIn animation-delay2"></asp:TextBox>
                        </div>
                        <div class="form-group">
                            <label><i class="fa fa-question-circle"></i>&nbsp;Password</label>
                            <asp:TextBox ID="txt_password" runat="server" CssClass="form-control input-sm bounceIn animation-delay4" TextMode="Password"></asp:TextBox>
                        </div>
                        <div class="seperator"></div>
                        <div class="form-group">
                            Forgot your password? Click <a href="" onclick="ShowPop()" data-toggle="modal">here</a>
                            <br />
                        </div>
                        <div class="row" id="lblError" runat="server" visible="false">
                            <div class="col-sm-12">
                                <div class="alert alert-danger">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true" id="B_notError" runat="server">&times;</button>
                                    <strong>
                                        <asp:Label ID="lbl_Error" runat="server">User Tidak Terdaftar</asp:Label>
                                    </strong>
                                </div>
                            </div>
                        </div>
                        <div class="panel-footer">
                            <div class="text-right">
                                <button id="Btn_Simpan" runat="server" class="btn btn-danger" type="submit"><i class="fa fa-sign-in"></i>&nbsp;Sign in</button>
                            </div>
                        </div>
                        <!-- /.modal -->
                    </div>
                </div>
                <!-- /panel -->
            </div>
            <!-- /login-widget -->
        </div>
        <!-- /login-wrapper -->
        <div class="modal fade" id="simpleModal">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4>Forget Password</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-lg-4">
                                <div class="form-group">
                                    USERNAME
                                        <asp:TextBox ID="txt_forgetusername" runat="server" CssClass="form-control input-sm bounceIn animation-delay2" AutoCompleteType="Disabled"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="form-group">
                                    EMAIL
                                        <asp:TextBox ID="txt_forgetemail" runat="server" CssClass="form-control input-sm bounceIn animation-delay3" AutoCompleteType="Disabled"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="form-group">
                                    PASSWORD
                                        <asp:TextBox ID="txt_forgetpassword" runat="server" CssClass="form-control input-sm bounceIn animation-delay4" AutoCompleteType="Disabled" ReadOnly="true" Font-Bold="true"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <a href="#" onclick="GetPassword()" class="btn btn-sm btn-info"><i class="fa fa-retweet"></i>Submit</a>
                        <button class="btn btn-sm btn-info" data-dismiss="modal" aria-hidden="true"><i class="fa fa-arrow-circle-left"></i>Cancel</button>
                    </div>
                </div>
                <!-- /.modal-content -->
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
        <dx:ASPxPopupControl ID="ASPxPopupControl2" ClientInstanceName="popuppasword" runat="server" CloseAction="CloseButton" Modal="true" Width="800px" Height="300px"
            closeonescape="true"
            PopupVerticalAlign="WindowCenter"
            PopupHorizontalAlign="WindowCenter" AllowDragging="true" Theme="SoftOrange"
            ShowFooter="True" HeaderText="Forgot Password" FooterText="" AutoUpdatePosition="true">
            <contentcollection>
                <dx:PopupControlContentControl ID="PopupControlContentControl2" runat="server">
                                <div class="row">
                                    <div class="col-md-12 col-sm-12">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="row">
                                                    <div class="col-md-12 col-sm-12">
                                                        <label><i class="fa fa-user"></i>&nbsp;Username</label>
                                                        <asp:TextBox ID="txt_forgot_username" runat="server" CssClass="form-control input-sm bounceIn animation-delay1"></asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <br />
                                        <div class="row" id="repass" runat="server" visible="true">
                                            <div class="col-md-12 col-sm-12">
                                                <div class="row">
                                                    <div class="col-md-12 col-sm-12">
                                                        <label><i class="fa fa-envelope"></i>&nbsp;Email</label>
                                                        <asp:TextBox ID="txt_forgot_email" runat="server" CssClass="form-control input-sm bounceIn animation-delay2"></asp:TextBox>
                                                    </div>
                                                </div>
                                                <br />
                                                <div class="row">
                                                    <div class="col-md-12 col-sm-12">
                                                        <div class="form-group">
                                                            <div class="text-right">
                                                                <a onclick="forgot()" class="btn btn-danger" id="btn_change"><i class="fa fa-save"></i>&nbsp;Save</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                </dx:PopupControlContentControl>
            </contentcollection>
        </dx:ASPxPopupControl>
    </form>


    <!-- Le javascript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->

    <!-- Jquery -->
    <script src="js/jquery-1.10.2.min.js"></script>
    <script src="HTML/js/jquery-1.10.2.min.js"></script>
    <!-- Bootstrap -->
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <script src="HTML/bootstrap/js/bootstrap.min.js"></script>
    <!-- Modernizr -->
    <script src="HTML/js/modernizr.min.js"></script>
    <!-- Pace -->
    <script src="HTML/js/pace.min.js"></script>
    <!-- Popup Overlay -->
    <script src="HTML/js/jquery.popupoverlay.min.js"></script>
    <!-- Slimscroll -->
    <script src="HTML/js/jquery.slimscroll.min.js"></script>
    <!-- Cookie -->
    <script src="HTML/js/jquery.cookie.min.js"></script>
    <!-- Endless -->
    <script src="HTML/js/endless/endless.js"></script>
</body>
</html>
