﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/HTML/Ticket.Master" CodeBehind="calltype_two_rev.aspx.vb" Inherits="ICC.calltype_two_rev" %>

<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxGridLookup" TagPrefix="dx" %>

<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>

<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxPanel" TagPrefix="dx" %>

<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxCallbackPanel" TagPrefix="dx" %>

<%@ Register Assembly="DevExpress.Web.ASPxHtmlEditor.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxHtmlEditor" TagPrefix="dx" %>

<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="FeaturedContent" runat="server">
<script language="javascript" type="text/javascript">
    function OnJenisTransaksiChange(cmbParent) {

        var comboValue = cmbParent.GetSelectedItem().value;
        if (comboValue)
            ASPxGridView1.GetEditor("UnitKerja").PerformCallback(comboValue.toString());
            //alert(comboValue.toString());
    }
    </script>
    <script>
        function OnGridFocusedRowChanged() {
            var grid = ASPxGridLookup1.GetGridView();
            grid.GetRowValues(grid.GetFocusedRowIndex(), 'ORGANIZATION_ID', OnGetRowValues);
        }
        function OnGetRowValues(values) {
            //alert(values)
            var temp = test.GetText();
            temp = (temp == null || temp == "") ? "" : temp + ',';
            test.SetText(temp + values);
        }
        function Clear() {
            ASPxGridLookup1.GetGridView().UnselectAllRowsOnPage(); ASPxGridLookup1.HideDropDown();
        }
    </script>
    <script>
        function OnStartEditing(s, e) {
            if (e.focusedColumn.fieldName === "TagIDs") {
                gl.GetGridView().UnselectAllRowsOnPage();
                gl.SetValue(e.rowValues[e.focusedColumn.index].value);
                prevColumnIndex = e.focusedColumn.index;
            }

        }

        function OnEndEditing(s, e) {
            if (prevColumnIndex == null) return;
            e.rowValues[prevColumnIndex].value = gl.GetGridView().GetSelectedKeysOnPage();
            e.rowValues[prevColumnIndex].text = gl.GetText();
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div id="div_calltype_two" runat="server">
        <h4 class="headline">Master Data Handling Division
			<span class="line bg-danger"></span>
        </h4>
        <div class="padding-md" style="margin-top: -20px;">
            <div class="row">
                <div style="overflow: auto;">
                    <dx:ASPxGridView ID="ASPxGridView1" ClientInstanceName="ASPxGridView1" Width="100%" runat="server" DataSourceID="dsSubject"
                        KeyFieldName="ID" SettingsPager-PageSize="10" Theme="MetropolisBlue">
                        <SettingsPager>
                            <AllButton Text="All">
                            </AllButton>
                            <NextPageButton Text="Next &gt;">
                            </NextPageButton>
                            <PrevPageButton Text="&lt; Prev">
                            </PrevPageButton>
                        </SettingsPager>
                        <SettingsEditing Mode="inline" />
                        <Settings ShowFilterRow="true" ShowFilterRowMenu="false" ShowGroupPanel="true" />
                        <SettingsBehavior ConfirmDelete="true" />
                        <Columns>
                            <dx:GridViewCommandColumn Caption="Action" HeaderStyle-HorizontalAlign="Center" VisibleIndex="0"
                                ButtonType="Image" FixedStyle="Left" CellStyle-BackColor="#ffffd6" Width="150px">
                                <HeaderTemplate>
                                    <dx:ASPxHyperLink ID="lnkClearFilter" runat="server" ForeColor="White" Font-Bold="true" Text="Clear Filter" NavigateUrl="javascript:void(0);">
                                        <ClientSideEvents Click="function(s, e) {
                                        ASPxGridView1.ClearFilter();
                                    }" />
                                    </dx:ASPxHyperLink>
                                </HeaderTemplate>
                                <EditButton Visible="True">
                                    <Image ToolTip="Edit" Url="img/icon/Text-Edit-icon2.png" />
                                </EditButton>
                                <NewButton Visible="True">
                                    <Image ToolTip="New" Url="img/icon/Apps-text-editor-icon2.png" />
                                </NewButton>
                                <DeleteButton Visible="false">
                                    <Image ToolTip="Delete" Url="img/icon/Actions-edit-clear-icon2.png" />
                                </DeleteButton>
                                <CancelButton>
                                    <Image ToolTip="Cancel" Url="img/icon/cancel1.png">
                                    </Image>
                                </CancelButton>
                                <UpdateButton>
                                    <Image ToolTip="Update" Url="img/icon/Updated1.png" />
                                </UpdateButton>
                                <CellStyle BackColor="#FFFFD6">
                                </CellStyle>
                            </dx:GridViewCommandColumn>
                            <dx:GridViewDataTextColumn Caption="ID" FieldName="ID" Visible="false"
                                Width="10px" HeaderStyle-HorizontalAlign="Center">
                            </dx:GridViewDataTextColumn>                           
                            <dx:GridViewDataComboBoxColumn PropertiesComboBox-ValueField="CategoryID" PropertiesComboBox-TextField="JenisTransaksi" FieldName="JenisTransaksi" Caption="Level 1"  Width="250px" >
                                <PropertiesComboBox IncrementalFilteringMode="Contains" DisplayFormatString ="{1}" TextFormatString="{1}" TextField="Name" ValueField="CategoryID" DataSourceID="dsmCategory">
                                    <Columns>
                                        <dx:ListBoxColumn Caption="ID" FieldName="CategoryID" Width="150px" />
                                        <dx:ListBoxColumn Caption="Jenis Transaksi" FieldName="Name" Width="150px" />
                                    </Columns>
                                    <ClientSideEvents SelectedIndexChanged="function(s, e) { OnJenisTransaksiChange(s); }"></ClientSideEvents>
                                </PropertiesComboBox>
                            </dx:GridViewDataComboBoxColumn>
                            <dx:GridViewDataComboBoxColumn FieldName="UnitKerja" Caption="Level 2" Settings-FilterMode="DisplayText" Settings-AutoFilterCondition="Contains" Width="250px">
                                <PropertiesComboBox IncrementalFilteringMode="Contains" TextFormatString="{1}" TextField="SubName" ValueField="SubCategory1ID" DataSourceID="dsmSubCategoryLv1">
                                    <%-- <Columns>
                                    <dx:ListBoxColumn Caption="ID" FieldName="SubCategory1ID" Width="80px" />
                                    <dx:ListBoxColumn Caption="Jenis Transaksi" FieldName="SubName" Width="150px" />
                                </Columns>--%>
                                </PropertiesComboBox>
                            </dx:GridViewDataComboBoxColumn>
                            <dx:GridViewDataTextColumn Caption="Description" FieldName="SubjectTable" Settings-FilterMode="DisplayText" Settings-AutoFilterCondition="Contains" Width="250px">
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataComboBoxColumn FieldName="IDHandling" Caption="Handling Division" >
                                <EditItemTemplate>
                                    <dx:ASPxGridLookup ID="ASPxGridLookup1" runat="server" SelectionMode="Multiple" DataSourceID="sqlDataSource3" ClientInstanceName="ASPxGridLookup1"
                                        KeyFieldName="ORGANIZATION_ID" Width="100%" TextFormatString="{0}" MultiTextSeparator=", " Theme="MetropolisBlue" Value='<%# Bind("IDHandling")%>'>
                                        <Columns>
                                            <dx:GridViewCommandColumn ShowSelectCheckbox="True" />
                                            <dx:GridViewDataColumn Caption="ID" FieldName="ORGANIZATION_ID" />
                                            <dx:GridViewDataColumn Caption="Handling Division" FieldName="ORGANIZATION_NAME" Width="80%" />
                                        </Columns>
                                        <GridViewClientSideEvents FocusedRowChanged="function(s, e) { OnGridFocusedRowChanged() }" />
                                    </dx:ASPxGridLookup>
                                </EditItemTemplate>
                            </dx:GridViewDataComboBoxColumn>
                            <dx:GridViewDataComboBoxColumn Caption="Status" FieldName="NA" HeaderStyle-HorizontalAlign="Center"
                                Width="100px">
                                <PropertiesComboBox>
                                    <Items>
                                        <dx:ListEditItem Text="Active" Value="Y" />
                                        <dx:ListEditItem Text="In Active" Value="N" />
                                    </Items>
                                </PropertiesComboBox>
                            </dx:GridViewDataComboBoxColumn>
                        </Columns>
                        <%--<ClientSideEvents BatchEditStartEditing="OnStartEditing" BatchEditEndEditing="OnEndEditing" />--%>                        
                    </dx:ASPxGridView>

                    <asp:SqlDataSource ID="sqlDataSource3" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>" SelectCommand="select ORGANIZATION_ID, ORGANIZATION_NAME from mOrganization where flag='1'"></asp:SqlDataSource>
                    <asp:SqlDataSource ID="dsSubject" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>"></asp:SqlDataSource>
                    <asp:SqlDataSource ID="dsmCategory" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>"></asp:SqlDataSource>
                    <asp:SqlDataSource ID="dsmSubCategoryLv1" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>"
                        SelectCommand="select * from mSubCategoryLv1 Where NA='Y' and CategoryID=@CategoryID">
                        <SelectParameters>
                            <asp:Parameter DefaultValue="0" Name="CategoryID" Type="String" />
                        </SelectParameters>
                    </asp:SqlDataSource>
                </div>
            </div>
        </div>
    </div>
    <div style="visibility: hidden; height: 0px;">
        <dx:ASPxTextBox ID="test" runat="server" ClientInstanceName="test" />
    </div>
</asp:Content>
