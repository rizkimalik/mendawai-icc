﻿Imports System.Data
Imports System.Data.SqlClient
Public Class calltype_one
    Inherits System.Web.UI.Page

    Dim Proses As New ClsConn
    Dim Sqldr As SqlDataReader
    Dim sql As String
    Dim con As New SqlConnection(ConfigurationManager.ConnectionStrings("DefaultConnection").ConnectionString)
    Dim sqlcom As New SqlCommand
    Dim loq As New cls_globe
    Dim _strScript As String = String.Empty
    Dim _strlogTime As String = DateTime.Now.ToString("yyyy")
    Dim _Number As String = String.Empty
    Dim _LastNumber As String = String.Empty
    Dim _strSql As String = String.Empty
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        sql_calltype_one.SelectCommand = "select mSubCategoryLv1.SubCategory1ID, mSubCategoryLv1.CategoryID, mCategory.Name, mSubCategoryLv1.SubName, mSubCategoryLv1.NA from mSubCategoryLv1 left outer join mCategory on mSubCategoryLv1.CategoryID = mCategory.CategoryID"
        sql_transaction_type.SelectCommand = "select * from mCategory where NA='Y'"
        Session("NamaForm") = IO.Path.GetFileName(Request.Path)
        updateAlert()
    End Sub

    Private Sub gv_calltype_one_Load(sender As Object, e As EventArgs) Handles gv_calltype_one.Load
        sql_calltype_one.SelectCommand = "select mSubCategoryLv1.ID, mSubCategoryLv1.SubCategory1ID, mSubCategoryLv1.CategoryID, mCategory.Name, mSubCategoryLv1.SubName, mSubCategoryLv1.NA from mSubCategoryLv1 left outer join mCategory on mSubCategoryLv1.CategoryID = mCategory.CategoryID"
    End Sub

    Private Sub gv_transaction_type_RowInserting(sender As Object, e As DevExpress.Web.Data.ASPxDataInsertingEventArgs) Handles gv_calltype_one.RowInserting
        Dim _Name As String = e.NewValues("Name")
        Dim _SubName As String = e.NewValues("SubName")
        _strSql = "select SUBSTRING(SubCategory1ID,5,5) as _Number from mSubCategoryLv1 order by ID desc"
        Try
            Sqldr = Proses.ExecuteReader(_strSql)
            If Sqldr.HasRows Then
                Sqldr.Read()
                _Number = Sqldr("_Number").ToString
            Else
            End If
            Sqldr.Close()
            Proses.LogSuccess(_strlogTime, " _Number mSubCategoryLv1 - " & _strSql)
        Catch ex As Exception
            Proses.LogError(_strlogTime, ex, " _Number mSubCategoryLv1 - " & _strSql)
            Response.Write(ex.Message)
        End Try
        
        If _Number = "" Then
            _strScript = "select  _LastNumber = Right(100000 + COUNT(ID) + 0, 5)  from mSubCategoryLv1"
        Else
            _strScript = "select  _LastNumber = Right(100" & _Number & " + 1 + 0, 5)  from mSubCategoryLv1"
        End If
        Try
            Sqldr = Proses.ExecuteReader(_strScript)
            If Sqldr.HasRows Then
                Sqldr.Read()
                _LastNumber = Sqldr("_LastNumber").ToString
            Else
            End If
            Sqldr.Close()
            Proses.LogSuccess(_strlogTime, "_LastNumber mSubCategoryLv1 - " & _strScript)
        Catch ex As Exception
            Proses.LogError(_strlogTime, ex, "_LastNumber mSubCategoryLv1 - " & _strScript)
            Response.Write(ex.Message)
        End Try

        Dim _GenerateNoID As String = String.Empty
        _GenerateNoID = "CT1-" & _LastNumber & ""

        Try
            _strScript = "insert into mSubCategoryLv1 (CategoryID, SubCategory1ID, SubName, UserCreate,DateCreate) values('" & _Name & "', '" & _GenerateNoID & "', '" & _SubName & "', '" & Session("UserName") & "',GETDATE())"
            sql_calltype_one.InsertCommand = _strScript
            Proses.LogSuccess(_strlogTime, "insert mSubCategoryLv1 - " & _strScript)
        Catch ex As Exception
            Proses.LogError(_strlogTime, ex, "insert mSubCategoryLv1 - " & _strScript)
            Response.Write(ex.Message)
        End Try
    End Sub

    Private Sub gv_transaction_type_RowUpdating(sender As Object, e As DevExpress.Web.Data.ASPxDataUpdatingEventArgs) Handles gv_calltype_one.RowUpdating
        Dim _ID As String = e.Keys("ID")
        Dim _SubName As String = e.NewValues("SubName")
        Dim _NA As String = e.NewValues("NA")
        Try
            _strScript = "UPDATE mSubCategoryLv1 SET SUBNAME='" & _SubName & "', NA='" & _NA & "', USERUPDATE='" & Session("UserName") & "', DATEUPDATE=GETDATE() WHERE ID='" & _ID & "'"
            sql_calltype_one.UpdateCommand = _strScript
            Proses.LogSuccess(_strlogTime, "update mSubCategoryLv1 - " & _strScript)
        Catch ex As Exception
            Proses.LogError(_strlogTime, ex, "update mSubCategoryLv1 - " & _strScript)
            Response.Write(ex.Message)
        End Try
    End Sub

    Private Sub updateAlert()
        Dim updateActivity As String = "update user1 set Activity='N'"
        Try
            sqlcom = New SqlCommand(updateActivity, con)
            con.Open()
            sqlcom.ExecuteNonQuery()
            con.Close()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
        Dim IdupdateActivity As String = "update user1 set Activity='Y' where MenuID='" & Request.QueryString("idpage") & "'"
        Try
            sqlcom = New SqlCommand(IdupdateActivity, con)
            con.Open()
            sqlcom.ExecuteNonQuery()
            con.Close()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
End Class