﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/HTML/Ticket.Master" CodeBehind="MasterJourney.aspx.vb" Inherits="ICC.MasterJourney" %>
<asp:Content ID="Content1" ContentPlaceHolderID="FeaturedContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<div style="width:100%;overflow-x: scroll;
  overflow-y: hidden;
  white-space: nowrap;">
  <div class="Timeline">
    <div class="now">
        NOW
    </div>  
    <svg height="5" width="200">
      <line x1="0" y1="0" x2="200" y2="0" style="stroke:#004165;stroke-width:5" />
      Sorry, your browser does not support inline SVG.
    </svg>

  <div class="event1">
    
    <div class="event1Bubble">
      <div class="eventTime">
        <div class="DayDigit">01</div>
        <div class="Day">
           Wednesday
          <div class="MonthYear">february 2016</div>
        </div>
      </div>
      <div class="eventTitle">Profile Created</div>
    </div>
    <div class="eventAuthor">by Youri Nelson</div>
    <svg height="20" width="20">
       <circle cx="10" cy="11" r="5" fill="#004165" />
     </svg>
    <div class="time">9 : 27 AM</div>
    
  </div>
  <svg height="5" width="200">
  <line x1="0" y1="0" x2="200" y2="0" style="stroke:#004165;stroke-width:5" />
  Sorry, your browser does not support inline SVG.
</svg>

  <div class="event2">
    
    <div class="event2Bubble">
      <div class="eventTime">
        <div class="DayDigit">04</div>
        <div class="Day">
           Thursday
          <div class="MonthYear">April 2016</div>
        </div>
      </div>
      <div class="eventTitle">Phone Interview</div>
    </div>
    <div class="event2Author">by Tom Eggleston</div>
 <svg height="20" width="20">
    <circle cx="10" cy="11" r="5" fill="#004165" />
    </svg>
    <div class="time2">1 : 32 PM</div>
  </div>  
  <svg height="5" width="300">
  <line x1="0" y1="0" x2="300" y2="0" style="stroke:#004165;stroke-width:5" />
  Sorry, your browser does not support inline SVG.
</svg>

  <div class="event2">
    
    <div class="event2Bubble">
      <div class="eventTime">
        <div class="DayDigit">04</div>
        <div class="Day">
           Thursday
          <div class="MonthYear">April 2016</div>
        </div>
      </div>
      <div class="eventTitle">Phone Interview</div>
    </div>
    <div class="event2Author">by Tom Eggleston</div>
 <svg height="20" width="20">
    <circle cx="10" cy="11" r="5" fill="#004165" />
    </svg>
    <div class="time2">1 : 32 PM</div>
  </div>    
      <svg height="5" width="200">
      <line x1="0" y1="0" x2="200" y2="0" style="stroke:#004165;stroke-width:5" />
      Sorry, your browser does not support inline SVG.
      </svg>

  <div class="event1">
    
    <div class="event1Bubble">
      <div class="eventTime">
        <div class="DayDigit">05</div>
        <div class="Day">
           Wednesday
          <div class="MonthYear">february 2016</div>
        </div>
      </div>
      <div class="eventTitle">Profile Created</div>
    </div>
    <div class="eventAuthor">by Youri Nelson</div>
    <svg height="20" width="20">
       <circle cx="10" cy="11" r="5" fill="#004165" />
     </svg>
    <div class="time">9 : 27 AM</div>
    
  </div>
      <svg height="5" width="200">
      <line x1="0" y1="0" x2="600" y2="0" style="stroke:#004165;stroke-width:5" />
      Sorry, your browser does not support inline SVG.
    </svg>

<div class="event3">
    
    <div class="event2Bubble">
      <div class="eventTime">
        <div class="DayDigit">06</div>
        <div class="Day">
           Thursday
          <div class="MonthYear">April 2016</div>
        </div>
      </div>
      <div class="eventTitle">Phone Interview</div>
    </div>
    <div class="event2Author">by Tom Eggleston</div>
      <svg height="20" width="20">
       <circle cx="10" cy="11" r="5" fill="#004165" />
     </svg>
    <div class="time2">1 : 32 PM</div>
  </div>
  <svg height="5" width="250">
  <line x1="0" y1="0" x2="250" y2="0" style="stroke:#004165;stroke-width:5" />
  Sorry, your browser does not support inline SVG.
</svg>

<svg height="20" width="42">
<line x1="1" y1="0" x2="1" y2="20" style="stroke:#004165;stroke-width:2" /> 
<circle cx="11" cy="10" r="3" fill="#004165" />  
<circle cx="21" cy="10" r="3" fill="#004165" />  
<circle cx="31" cy="10" r="3" fill="#004165" />    
<line x1="41" y1="0" x2="41" y2="20" style="stroke:#004165;stroke-width:2" /> 
</svg>  
</div>
</div>
  
</asp:Content>
