﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/HTML/Ticket.Master" CodeBehind="n_backuprestore.aspx.vb" Inherits="ICC.n_backuprestore" %>

<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxCallbackPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxGridView.Export" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="FeaturedContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div>
        <h4 class="headline">Backup Data Transaction
			<span class="line bg-warning"></span>
        </h4>
        <br />
        <dx:ASPxLabel Visible="false" runat="server" ID="sqlOutput"></dx:ASPxLabel>
    </div>
    <div class="row">
        <div class="col-sm-2">
            <label>Start Date</label>
            <dx:ASPxDateEdit ID="dt_strdate" runat="server" CssClass="form-control input-sm" Width="100%" EditFormatString="yyyy-MM-dd">
                <ValidationSettings ErrorTextPosition="Bottom" ErrorDisplayMode="ImageWithText" ValidationGroup="_Validation">
                    <RequiredField IsRequired="true" ErrorText="Must be filled" />
                </ValidationSettings>
            </dx:ASPxDateEdit>
        </div>
        <div class="col-sm-2">
            <label>End Date</label>
            <dx:ASPxDateEdit ID="dt_endate" runat="server" CssClass="form-control input-sm" Width="100%" EditFormatString="yyyy-MM-dd">
                <ValidationSettings ErrorTextPosition="Bottom" ErrorDisplayMode="ImageWithText" ValidationGroup="_Validation">
                    <RequiredField IsRequired="true" ErrorText="Must be filled" />
                </ValidationSettings>
            </dx:ASPxDateEdit>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <label>Description</label>
            <dx:ASPxMemo Height="130px" Width="100%" runat="server" ID="txtDescription"></dx:ASPxMemo>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-2">
            <br />
            <dx:ASPxButton ID="btn_Submit" runat="server" Theme="Metropolis" Text="Submit" Width="100%" AutoPostBack="false"
                HoverStyle-BackColor="#EE4D2D" Height="30px" ValidationGroup="_Validation">
            </dx:ASPxButton>
        </div>
        <div class="col-sm-2">
            <br />
            <dx:ASPxButton ID="_btnCancel" runat="server" Theme="Metropolis" Text="Cancel" Width="100%" AutoPostBack="false"
                HoverStyle-BackColor="#EE4D2D" Height="30px">
            </dx:ASPxButton>
        </div>
    </div>
    <hr />
    <div class="row">
        <div class="col-sm-12">
            <dx:ASPxGridView ID="ASPxGridView1" DataSourceID="ds_backup" runat="server" Width="100%" Theme="Metropolis">
                <Settings ShowFilterRow="true" ShowFilterRowMenu="false" ShowGroupPanel="true" ShowVerticalScrollBar="false" ShowHorizontalScrollBar="false" />
                <SettingsPager>
                    <AllButton Text="All">
                    </AllButton>
                    <NextPageButton Text="Next &gt;">
                    </NextPageButton>
                    <PrevPageButton Text="&lt; Prev">
                    </PrevPageButton>
                    <PageSizeItemSettings Visible="true" Items="25, 50, 75" ShowAllItem="true" />
                </SettingsPager>
                <SettingsPager PageSize="10" />
                <Columns>
                    <dx:GridViewDataTextColumn Caption="ID" FieldName="IDBackup" Width="200px"></dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="Date Start" FieldName="DateStart" Width="200px"></dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="Date End" FieldName="DateEnd" Width="200px"></dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="Date Create" FieldName="DateCreate" Width="200px"></dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="User Create" FieldName="UserCreate" Width="200px"></dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="Description" FieldName="DescriptionBackup" Width="200px"></dx:GridViewDataTextColumn>
                </Columns>
            </dx:ASPxGridView>
        </div>
    </div>
    <asp:SqlDataSource ID="sql_transaksi" runat="server"
        ConnectionString="<%$ ConnectionStrings:DefaultConnection %>"
        SelectCommand="SP_HelpdeskRestore" SelectCommandType="StoredProcedure">
        <SelectParameters>
            <asp:SessionParameter DbType="String" Name="IDBackup"
                SessionField="IDBackup" />

        </SelectParameters>
    </asp:SqlDataSource>
    <hr />
    <div class="row">
        <div class="col-sm-2">
            <asp:DropDownList runat="server" ID="ddList" Height="30" CssClass="form-control input-sm">
                <asp:ListItem Value="xlsx" Text="Excel" />
                <asp:ListItem Value="xls" Text="Excel 97-2003" />
                <asp:ListItem Value="pdf" Text="PDF" />
                <asp:ListItem Value="rtf" Text="RTF" />
                <asp:ListItem Value="csv" Text="CSV" />
            </asp:DropDownList>
        </div>
        <div class="col-sm-2">
            <%--<dx:ASPxButton ID="btn_Export" runat="server" Text="Export" CssClass="form-control input-sm"></dx:ASPxButton>--%>
            <dx:ASPxButton ID="btn_Export" runat="server" Theme="Metropolis" Text="Export" Width="100%" AutoPostBack="false"
                HoverStyle-BackColor="#EE4D2D" Height="30px" ValidationGroup="_Validation">
            </dx:ASPxButton>
        </div>
    </div>
    <hr />
    <dx:ASPxGridViewExporter ID="ASPxGridViewExporter1" runat="server" GridViewID="gv_transaksi"></dx:ASPxGridViewExporter>
    <asp:SqlDataSource ID="ds_backup" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>" SelectCommand="select * from Temp_BackupRestore"></asp:SqlDataSource>
    <asp:SqlDataSource ID="sql_channel" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>"></asp:SqlDataSource>
    <asp:SqlDataSource ID="sql_transaction_type" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>"></asp:SqlDataSource>
    <asp:SqlDataSource ID="sql_SPrpt" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>"></asp:SqlDataSource>
    <asp:SqlDataSource ID="sql_product" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>"></asp:SqlDataSource>
    <asp:SqlDataSource ID="sql_status" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>" SelectCommand="select '0' as id,0 as urutan,'All' as status, 'All' as lblStatus union select id,Urutan,status,lblStatus from mStatus order by Urutan asc"></asp:SqlDataSource>
</asp:Content>
