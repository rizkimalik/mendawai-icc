﻿Imports System.Data
Imports System.Data.SqlClient
Public Class Transaction_Type
    Inherits System.Web.UI.Page

    Dim Proses As New ClsConn
    Dim sqldr As SqlDataReader
    Dim sql As String
    Dim con As New SqlConnection(ConfigurationManager.ConnectionStrings("DefaultConnection").ConnectionString)
    Dim sqlcom As New SqlCommand
    Dim loq As New cls_globe
    Dim _strScript As String = String.Empty
    Dim _strlogTime As String = DateTime.Now.ToString("yyyy")

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        sql_transaction_type.SelectCommand = "SELECT * FROM MCATEGORY"
        Session("NamaForm") = IO.Path.GetFileName(Request.Path)
        updateAlert()
    End Sub
    Private Sub gv_transaction_type_RowDeleting(sender As Object, e As DevExpress.Web.Data.ASPxDataDeletingEventArgs) Handles gv_transaction_type.RowDeleting
        Dim _CategoryID As String = e.Keys("CategoryID")
        Try
            _strScript = "Delete from mcategory where CategoryID='" & _CategoryID & "'"
            sql_transaction_type.DeleteCommand = _strScript
            Proses.LogSuccess(_strlogTime, _strScript)
        Catch ex As Exception
            Proses.LogError(_strlogTime, ex, _strScript)
            Response.Write(ex.Message)
        End Try
    End Sub
    Private Sub gv_transaction_type_RowInserting(sender As Object, e As DevExpress.Web.Data.ASPxDataInsertingEventArgs) Handles gv_transaction_type.RowInserting
        Dim cekJenisTransaksi As String
        cekJenisTransaksi = e.NewValues("Name").ToString()

        Dim jTransaksi As String
        sql = "Select COUNT (Name) as CekTransaksi from mCategory where Name='" & cekJenisTransaksi & "'"
        sqldr = Proses.ExecuteReader(sql)
        If sqldr.Read Then
            jTransaksi = sqldr("CekTransaksi")
        End If
        sqldr.Close()
      
        If jTransaksi > 0 Then
            e.Cancel = True
            Throw New Exception(cekJenisTransaksi & " already exists")
        Else
            e.Cancel = False
        End If

        Dim NoUrut, Angka, GenerateNoID As String
        sql = "select SUBSTRING(CategoryID,5,5) as NoUrut from mCategory order by ID desc"
        Try
            sqldr = Proses.ExecuteReader(sql)
            If sqldr.Read Then
                NoUrut = sqldr("NoUrut").ToString
            End If
            sqldr.Close()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
       
        If NoUrut = "" Then
            sql = "select  Angka = Right(10001 + COUNT(ID) + 0, 5)  from mCategory"
        Else
            sql = "select  Angka = Right(100" & NoUrut & " + 1 + 0, 5)  from mCategory "
        End If
        Try
            sqldr = Proses.ExecuteReader(sql)
            If sqldr.Read Then
                Angka = sqldr("Angka").ToString
            End If
            sqldr.Close()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
        GenerateNoID = "CAT-" & Angka & ""

        Try
            _strScript = "INSERT INTO mCategory(CategoryID, Name, UserCreate, DateCreate) values('" & GenerateNoID & "', '" & cekJenisTransaksi & "', '" & Session("UserName") & "', GETDATE()) "
            sql_transaction_type.InsertCommand = _strScript
            Proses.LogSuccess(_strlogTime, _strScript)
        Catch ex As Exception
            Proses.LogError(_strlogTime, ex, _strScript)
            Response.Write(ex.Message)
        End Try
    End Sub
    Private Sub gv_transaction_type_RowUpdating(sender As Object, e As DevExpress.Web.Data.ASPxDataUpdatingEventArgs) Handles gv_transaction_type.RowUpdating
        Dim _CategoryID As String = e.Keys("CategoryID")
        Dim _nameCatgeori As String = e.NewValues("Name")
        Try
            _strScript = "UPDATE mCategory SET Name='" & _nameCatgeori & "', NA=@NA, UserUpdate='" & Session("UserName") & "', DateUpdate=Getdate() where CategoryID='" & _CategoryID & "'"
            sql_transaction_type.UpdateCommand = _strScript
            Proses.LogSuccess(_strlogTime, _strScript)
        Catch ex As Exception
            Proses.LogError(_strlogTime, ex, _strScript)
            Response.Write(ex.Message)
        End Try
    End Sub
    Private Sub updateAlert()
        Dim updateActivity As String = "update user1 set Activity='N'"
        Try
            sqlcom = New SqlCommand(updateActivity, con)
            con.Open()
            sqlcom.ExecuteNonQuery()
            con.Close()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
        Dim IdupdateActivity As String = "update user1 set Activity='Y' where MenuID='" & Request.QueryString("idpage") & "'"
        Try
            sqlcom = New SqlCommand(IdupdateActivity, con)
            con.Open()
            sqlcom.ExecuteNonQuery()
            con.Close()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
        Dim _page As String = String.Empty
        Try
            _page = "update user2 set Activity='N'"
            Proses.ExecuteNonQuery(_page)
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
        Try
            _page = "update user2 set Activity='Y' where SubMenuID='" & Request.QueryString("idtable") & "'"
            Proses.ExecuteNonQuery(_page)
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
End Class