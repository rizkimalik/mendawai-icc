﻿Public Class n_restore
    Inherits System.Web.UI.Page

    Dim proses As New ClsConn
    Dim _upage As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        _updatePage()
        Report(cmb_Restore.Value())
        Session("IDBackup") = cmb_Restore.Value()
    End Sub
    Private Sub btnRestoreData_Click(sender As Object, e As EventArgs) Handles btnRestoreData.Click
        Report(cmb_Restore.Value())
        Session("IDBackup") = cmb_Restore.Value()
    End Sub
    Function Report(ByVal IDBackup As String) As String
        If IDBackup = "" Then
        Else
            Dim strSql As String
            strSql = "exec SP_HelpdeskRestore '" & IDBackup & "'"
            sql_SPrpt.SelectCommand = strSql
            lblErr.Text = strSql & "_" & cmb_Restore.Text()
        End If
    End Function
    Private Sub _updatePage()
        Try
            _upage = "update user1 set Activity='N'"
            proses.ExecuteNonQuery(_upage)
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
        Try
            _upage = "update user1 set Activity='Y' where MenuID='" & Request.QueryString("idpage") & "'"
            proses.ExecuteNonQuery(_upage)
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
        Try
            _upage = "update user2 set Activity='N'"
            proses.ExecuteNonQuery(_upage)
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
        Try
            _upage = "update user2 set Activity='Y' where SubMenuID='" & Request.QueryString("idtable") & "'"
            proses.ExecuteNonQuery(_upage)
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Private Sub gv_transaksi_Load(sender As Object, e As EventArgs) Handles gv_transaksi.Load
        Report(cmb_Restore.Value())
        Session("IDBackup") = cmb_Restore.Value()
    End Sub
    Private Sub gv_transaksi_Init(sender As Object, e As EventArgs) Handles gv_transaksi.Init
        Report(cmb_Restore.Value())
        Session("IDBackup") = cmb_Restore.Value()
    End Sub
End Class