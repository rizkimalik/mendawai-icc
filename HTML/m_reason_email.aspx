﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/HTML/Ticket.Master" CodeBehind="m_reason_email.aspx.vb" Inherits="ICC.m_reason_email" %>

<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxCallbackPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxHtmlEditor.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxHtmlEditor" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxSpellChecker.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxSpellChecker" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="FeaturedContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <h4 class="headline">Add Email Content Reason
			<span class="line bg-warning"></span>
    </h4>
    <dx:ASPxGridView ID="ASPxGridView1" ClientInstanceName="ASPxGridView1" runat="server" KeyFieldName="ID"
        DataSourceID="ds_Data" Width="100%" Theme="Metropolis" Styles-Header-Font-Bold="true" Font-Size="X-Small">
        <SettingsPager>
            <AllButton Text="All">
            </AllButton>
            <NextPageButton Text="Next &gt;">
            </NextPageButton>
            <PrevPageButton Text="&lt; Prev">
            </PrevPageButton>
            <PageSizeItemSettings Visible="true" Items="10, 15, 20" ShowAllItem="true" />
        </SettingsPager>
        <SettingsEditing Mode="EditForm" />
        <Settings ShowFilterRow="true" ShowFilterRowMenu="false" ShowGroupPanel="true"
            ShowVerticalScrollBar="false" ShowHorizontalScrollBar="false" />
        <SettingsBehavior ConfirmDelete="true" />
        <Templates>
            <EditForm>
                <div class="row">
                    <div class="col-md-9">
                        <label>Subject Email :</label>
                        <dx:ASPxTextBox runat="server" ID="txtSubject" Text='<%#Eval("SubjectEmail")%>' Width="100%" Theme="Metropolis" Height="25px"></dx:ASPxTextBox>
                    </div>
                    <div class="col-md-3">
                        <label>Status :</label>
                        <dx:ASPxComboBox ID="cmbStatus" runat="server" TextField='<%#Eval("NA")%>' Theme="Metropolis" Height="25px" Width="100%">
                            <Items>
                                <dx:ListEditItem Text="Active" Value="Y" />
                                <dx:ListEditItem Text="In Active" Value="N" />
                            </Items>
                        </dx:ASPxComboBox>
                    </div>
                </div>
                <br />
                <br />
                <label>Body Email :</label>
                <dx:ASPxHtmlEditor Settings-AllowDesignView="true" Html='<%#Eval("BodyEmail")%>' Settings-AllowHtmlView="true" ID="html_editor_Body" Width="100%" Height="500px" runat="server">
                </dx:ASPxHtmlEditor>
                <br />
                <div style="text-align: right; padding: 2px 2px 2px 2px">
                    <dx:ASPxGridViewTemplateReplacement ID="UpdateButton" ReplacementType="EditFormUpdateButton"
                        runat="server"></dx:ASPxGridViewTemplateReplacement>
                    <dx:ASPxGridViewTemplateReplacement ID="CancelButton" ReplacementType="EditFormCancelButton"
                        runat="server"></dx:ASPxGridViewTemplateReplacement>
                </div>
            </EditForm>
        </Templates>
        <Columns>
            <dx:GridViewCommandColumn Caption="Action" HeaderStyle-HorizontalAlign="Center" VisibleIndex="0"
                ButtonType="Image" FixedStyle="Left" Width="80px">
                <EditButton Visible="True">
                    <Image ToolTip="Edit" Url="img/icon/Text-Edit-icon2.png" />
                </EditButton>
                <NewButton Visible="True">
                    <Image ToolTip="New" Url="img/icon/Apps-text-editor-icon2.png" />
                </NewButton>
                <DeleteButton Visible="false">
                    <Image ToolTip="Delete" Url="img/icon/Actions-edit-clear-icon2.png" />
                </DeleteButton>
                <CancelButton>
                    <Image ToolTip="Cancel" Url="img/icon/cancel1.png">
                    </Image>
                </CancelButton>
                <UpdateButton>
                    <Image ToolTip="Update" Url="img/icon/Updated1.png" />
                </UpdateButton>
            </dx:GridViewCommandColumn>
            <dx:GridViewDataTextColumn Caption="ID" FieldName="ID" ReadOnly="true" Visible="false"></dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="Reason Code ID" FieldName="ReasonCodeID" Width="100px" EditFormSettings-Visible="True" ReadOnly="true"></dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="Subject Email" FieldName="SubjectEmail" Width="300px"></dx:GridViewDataTextColumn>
            <dx:GridViewDataMemoColumn Caption="Body Email" FieldName="BodyEmail" PropertiesMemoEdit-Height="500">
                <PropertiesMemoEdit EncodeHtml="false"></PropertiesMemoEdit>
            </dx:GridViewDataMemoColumn>
            <dx:GridViewDataTextColumn Caption="User Create" FieldName="UserCreate" ReadOnly="true" Width="130px"></dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="Date Create" FieldName="DateCreate" ReadOnly="true" Width="130px"></dx:GridViewDataTextColumn>
            <dx:GridViewDataComboBoxColumn Caption="State" FieldName="NA" Width="50px">
                <PropertiesComboBox>
                    <Items>
                        <dx:ListEditItem Text="Active" Value="Y" />
                        <dx:ListEditItem Text="In Active" Value="N" />
                    </Items>
                </PropertiesComboBox>
            </dx:GridViewDataComboBoxColumn>
        </Columns>
    </dx:ASPxGridView>
    <asp:SqlDataSource ID="ds_Data" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>" SelectCommand="select * from Temp_EmailReasonCode"
        UpdateCommand="update Temp_EmailReasonCode set SubjectEmail=@SubjectEmail,BodyEmail=@BodyEmail,NA=@NA,UserUpdate=@Username,DateUpdate=GETDATE() where ID=@ID">
        <UpdateParameters>
            <asp:Parameter Name="Username" Type="String" />
            <asp:Parameter Name="SubjectEmail" Type="String" />
            <asp:Parameter Name="BodyEmail" Type="String" />
            <asp:Parameter Name="NA" Type="String" />
        </UpdateParameters>
    </asp:SqlDataSource>
</asp:Content>
