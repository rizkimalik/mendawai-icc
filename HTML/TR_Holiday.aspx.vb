﻿Public Class TR_Holiday
    Inherits System.Web.UI.Page

    Dim Trtime As String = DateTime.Now.ToString("dd-MM-yyyy hhmmssfff")
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'ds_select.InsertParameters("username").DefaultValue = Session("username")
    End Sub

    Private Sub ASPxGridView1_RowDeleting(sender As Object, e As DevExpress.Web.Data.ASPxDataDeletingEventArgs) Handles ASPxGridView1.RowDeleting
        ds_select.DeleteCommand = "DELETE FROM LIBUR WHERE ID=@ID"
    End Sub

    Private Sub ASPxGridView1_RowInserting(sender As Object, e As DevExpress.Web.Data.ASPxDataInsertingEventArgs) Handles ASPxGridView1.RowInserting
        ds_select.InsertCommand = "INSERT INTO [libur] ([NAME], [START_DATE], [END_DATE], [USER_CREATE], [DATECREATE]) VALUES (@NAME, @START_DATE, @END_DATE, '" & Session("username") & "', GETDATE())"
    End Sub

    Private Sub ASPxGridView1_RowUpdating(sender As Object, e As DevExpress.Web.Data.ASPxDataUpdatingEventArgs) Handles ASPxGridView1.RowUpdating
        ds_select.UpdateCommand = "UPDATE LIBUR SET [NAME]=@NAME, [START_DATE]=@START_DATE, [END_DATE]=@END_DATE, [USER_CREATE]='" & Session("username") & "', [DATECREATE]=GETDATE() WHERE ID=@ID"
    End Sub
End Class