﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/HTML/Ticket.Master" CodeBehind="call_history.aspx.vb" Inherits="ICC.call_history" %>

<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxCallbackPanel" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="FeaturedContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <h4 class="headline">Data Call History
        <span class="line bg-danger" style="margin-top: 15px;"></span>
    </h4>
    <dx:ASPxGridView ID="ASPxGridView_CallHistory" ClientInstanceName="ASPxGridView_CallHistory" Width="100%" runat="server" DataSourceID="sql_call_history" KeyFieldName="ID"
        SettingsPager-PageSize="10" Theme="MetropolisBlue">
        <SettingsPager>
            <AllButton Text="All">
            </AllButton>
            <NextPageButton Text="Next &gt;">
            </NextPageButton>
            <PrevPageButton Text="&lt; Prev">
            </PrevPageButton>
            <PageSizeItemSettings Visible="true" Items="10, 15, 20" ShowAllItem="true" />
        </SettingsPager>
        <SettingsEditing Mode="Inline" />
        <Settings ShowFilterRow="true" ShowGroupPanel="true" ShowHorizontalScrollBar="true" />
        <SettingsBehavior ConfirmDelete="true" />
        <Columns>
            <dx:GridViewDataTextColumn Caption="CALL ID" FieldName="CALL_ID" VisibleIndex="1" HeaderStyle-HorizontalAlign="left" Width="20%">
            </dx:GridViewDataTextColumn>
            <%--<dx:GridViewDataTextColumn Caption="Name" FieldName="name" VisibleIndex="2" HeaderStyle-HorizontalAlign="left" Width="20%">
            </dx:GridViewDataTextColumn>--%>
            <dx:GridViewDataTextColumn Caption="CustomerID" FieldName="CustomerID" VisibleIndex="2" HeaderStyle-HorizontalAlign="left" Width="20%">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="Email" FieldName="Email" VisibleIndex="2" HeaderStyle-HorizontalAlign="left" Width="15%">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="Start Call" FieldName="CALL_START" VisibleIndex="4" HeaderStyle-HorizontalAlign="left" Width="20%">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="End Call" FieldName="CALL_END" VisibleIndex="4" HeaderStyle-HorizontalAlign="left" Width="20%">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="Agent" FieldName="AGENT" VisibleIndex="2" HeaderStyle-HorizontalAlign="left" Width="15%">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="Duration" FieldName="TALK_TIME" VisibleIndex="2" HeaderStyle-HorizontalAlign="left" Width="20%">
            </dx:GridViewDataTextColumn>
        </Columns>
    </dx:ASPxGridView>
    <asp:SqlDataSource ID="sql_call_history" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>"></asp:SqlDataSource>

</asp:Content>

