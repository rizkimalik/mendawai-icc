﻿Imports System
Imports System.Web.UI
Imports System.Data.SqlClient
Imports Microsoft.VisualBasic
Imports System.DirectoryServices.Protocols
Imports System.Net
Public Class LdapTest
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        btn_ldap1.Visible = False
        sql_ldap.UpdateCommand = "update icc_ldap_setting set LDAPServer=@LDAPServer, Username=@Username, Password=@Password, NA=@NA where ID=@ID"
    End Sub
    Private Function ValidateActiveDirectoryLogin(ByVal Domain As String, ByVal Username As String, ByVal Password As String) As Boolean
        Dim Success As Boolean = False
        'Dim Entry As New System.DirectoryServices.DirectoryEntry("LDAP://" & Domain, Username, Password)
        Dim Entry As New System.DirectoryServices.DirectoryEntry("LDAP://" & Domain & ":389", Username, Password)
        TextBox1.Text = "LDAP://" & Domain & ":389"
        Dim Searcher As New System.DirectoryServices.DirectorySearcher(Entry)
        Searcher.SearchScope = DirectoryServices.SearchScope.OneLevel
        Try
            Dim Results As System.DirectoryServices.SearchResult = Searcher.FindOne
            Success = Not (Results Is Nothing)
            lblError.Visible = True
            lbl_Error.Text = "Success LDAP 2"
        Catch ex As Exception
            Success = False
            lblError.Visible = True
            lbl_Error.Text = ex.Message & " Failed LDAP 2 "
        End Try
        Return Success
    End Function
    Private Sub btn_ldap2_ServerClick(sender As Object, e As EventArgs) Handles btn_ldap2.ServerClick
        If ValidateActiveDirectoryLogin(txt_ldap.Text, txt_username.Text, txt_password.Text) = True Then
        Else
        End If
    End Sub
    Private Sub btn_ldap1_ServerClick(sender As Object, e As EventArgs) Handles btn_ldap1.ServerClick
        LoginLDAP(txt_ldap.Text, txt_username.Text, txt_password.Text)
    End Sub
    Private Function LoginLDAP(LDAP As String, Username As String, Password As String) As Boolean
        If LDAP = "" Then Exit Function
        Dim Entry As New DirectoryServices.DirectoryEntry("LDAP://" & LDAP, Username, Password)
        Dim Searcher As New System.DirectoryServices.DirectorySearcher(Entry)
        Searcher.Filter = "(sAMAccountName=" & Username & ")"
        Try
            Dim Results As System.DirectoryServices.SearchResultCollection = Searcher.FindAll
            lblError.Visible = True
            lbl_Error.Text = "Success LDAP 1"
        Catch ex As Exception
            Response.Write(ex.Message)
            lblError.Visible = True
            lbl_Error.Text = ex.Message & " Failed LDAP 1 "
        End Try
    End Function
    Private Sub submitLDAP()
        ' setup your creds, domain, and ldap prop array 
        Dim username As String = txt_username.Text
        Dim pwd As String = txt_password.Text
        Dim domain As String = "DC=xxx,DC=corp,DC=xxx,DC=ca"
        Dim propArray() As String = {"employeenumber"}

        ' setup your ldap connection, and domain component
        Dim ldapCon As LdapConnection = New LdapConnection("xxx.test.intranet.xxx.ca:389")
        Dim networkCreds As NetworkCredential = New NetworkCredential(username, pwd, domain)

        ' configure the connection and bind
        ldapCon.AuthType = AuthType.Negotiate
        ldapCon.Bind(networkCreds)

        ' if the above succceeded, you should now be able to issue search requests directly against the directory
        Dim searchRequest = New SearchRequest(domain, "(employeenumber=6012589)", SearchScope.Subtree, propArray)

        ' issue the search request, and check the results
        Dim searchResult As SearchResponse = ldapCon.SendRequest(searchRequest)
        Dim searchResultEntry As SearchResultEntry

        If (searchResult.Entries.Count > 0) Then ' we know we've located at least one match from the search

            ' if you're only expecting to get one entry back, get the first item off the entries list
            searchResultEntry = searchResult.Entries.Item(0)

            ' continue to do whatever processing you wish against the returned SearchResultEntry

        End If
    End Sub
End Class