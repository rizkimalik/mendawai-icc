﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/HTML/Ticket.Master" CodeBehind="email_send.aspx.vb" Inherits="ICC.email_send" %>

<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxCallbackPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxHtmlEditor.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxHtmlEditor" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxSpellChecker.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxSpellChecker" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="FeaturedContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <h4 class="headline">Data Email Send
         <div class="btn-group pull-right">
             <div class="btn-group">
                 <button class="btn btn-default">Action</button>
                 <button class="btn btn-default dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                 <ul class="dropdown-menu slidedown">
                     <li><a href="email_inbox.aspx?status=inbox">Inbox</a><asp:Literal ID="lit_TNew" runat="server"></asp:Literal></li>
                     <li><a href="email_inbox.aspx?status=queing&idpage=2027">Queing</a></li>
                     <li><a href="email_inbox.aspx?status=revert&idpage=2027">Revert</a></li>
                     <li><a href="email_send.aspx?status=send">Sent Mail</a></li>
                     <li><a href="email_inbox.aspx?status=compose">Compose</a></li>
                 </ul>
             </div>
             <asp:Button ID="btn_compose" runat="server" CssClass="btn btn-default" Text="Compose" Visible="false" />
         </div>
        <span class="line bg-danger" style="margin-top: 15px;"></span>
    </h4>

    <dx:ASPxGridView ID="ASPxGridView_Send" ClientInstanceName="ASPxGridView_Send" Width="100%" runat="server" DataSourceID="sql_send" KeyFieldName="ID"
        SettingsPager-PageSize="10" Theme="MetropolisBlue">
        <SettingsPager>
            <AllButton Text="All" />
            <NextPageButton Text="Next &gt;" />
            <PrevPageButton Text="&lt; Prev" />
            <PageSizeItemSettings Visible="true" Items="10, 15, 20" ShowAllItem="true" />
        </SettingsPager>
        <SettingsEditing Mode="Inline" />
        <Settings ShowFilterRow="true" ShowGroupPanel="true" ShowHorizontalScrollBar="true" />
        <SettingsBehavior ConfirmDelete="true" />
        <Columns>
            <dx:GridViewDataTextColumn Caption="Action" VisibleIndex="0" CellStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" Width="10%">
                <DataItemTemplate>
                    <a href="?status=compose&idpage=<%= Request.QueryString("idpage") %>">
                        <asp:Image ImageUrl="img/icon/Apps-text-editor-icon2.png" ID="img_insert" runat="server" ToolTip="New Email" />
                    </a>
                    <a href="?ivcid=<%# Eval("IVC_ID")%>&emailid=<%# Eval("EMAIL_ID")%>&from=<%# Eval("EFROM")%>&to<%# Eval("ETO")%>&status=detail&idpage=<%= Request.QueryString("idpage") %>&detail=send">
                        <asp:Image ImageUrl="img/icon/clone.png" ID="Image2" runat="server" ToolTip="Detail Email" />
                    </a>
                </DataItemTemplate>
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="Subject" FieldName="ESUBJECT" HeaderStyle-HorizontalAlign="left" Width="30%" />
            <dx:GridViewDataTextColumn Caption="Email" FieldName="ETO" HeaderStyle-HorizontalAlign="left" Width="30%" />
            <dx:GridViewDataTextColumn Caption="Email Date" FieldName="Email_Date" HeaderStyle-HorizontalAlign="left" Width="15%" />
            <dx:GridViewDataTextColumn Caption="Send Date" FieldName="SentDate" HeaderStyle-HorizontalAlign="left" Width="15%" />
        </Columns>
    </dx:ASPxGridView>
    <asp:SqlDataSource ID="sql_send" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>"></asp:SqlDataSource>


    <div id="div_send" runat="server">
        <div class="input-group">
            <span class="input-group-addon"><i class="fa fa-user"></i></span>
            <asp:TextBox ID="txtto" runat="server" CssClass="form-control" placeholder="To"></asp:TextBox>
        </div>
        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" validationgroup="btn_send"
            ErrorMessage="* Invalid Email Format" ControlToValidate="txtto" ForeColor="red"
            SetFocusOnError="True"
            ValidationExpression="((\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*)*([;])*)*">
        </asp:RegularExpressionValidator>
        <br />
        <div class="input-group">
            <span class="input-group-addon"><i class="fa fa-user"></i></span>
            <asp:TextBox ID="txtcc" runat="server" CssClass="form-control" placeholder="Cc"></asp:TextBox>
        </div>
        <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" validationgroup="btn_send"
            ErrorMessage="* Invalid Email Format" ControlToValidate="txtcc" ForeColor="red"
            SetFocusOnError="True"
            ValidationExpression="((\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*)*([;])*)*">
        </asp:RegularExpressionValidator>
        <br />
        <div class="input-group">
            <span class="input-group-addon"><i class="fa fa-edit"></i></span>
            <asp:TextBox ID="txtsubject" runat="server" CssClass="form-control" placeholder="Subject"></asp:TextBox>
        </div>
        <br />
        <dx:ASPxHtmlEditor ID="ASPxHtmlEditor1" runat="server" Width="100%">
            <Toolbars>
                <dx:HtmlEditorToolbar Name="StandardToolbar2">
                    <Items>
                        <dx:ToolbarParagraphFormattingEdit Width="120px">
                            <Items>
                                <dx:ToolbarListEditItem Text="Normal" Value="p" />
                                <dx:ToolbarListEditItem Text="Heading  1" Value="h1" />
                                <dx:ToolbarListEditItem Text="Heading  2" Value="h2" />
                                <dx:ToolbarListEditItem Text="Heading  3" Value="h3" />
                                <dx:ToolbarListEditItem Text="Heading  4" Value="h4" />
                                <dx:ToolbarListEditItem Text="Heading  5" Value="h5" />
                                <dx:ToolbarListEditItem Text="Heading  6" Value="h6" />
                                <dx:ToolbarListEditItem Text="Address" Value="address" />
                                <dx:ToolbarListEditItem Text="Normal (DIV)" Value="div" />
                            </Items>
                        </dx:ToolbarParagraphFormattingEdit>
                        <dx:ToolbarFontNameEdit>
                            <Items>
                                <dx:ToolbarListEditItem Text="Times New Roman" Value="Times New Roman" />
                                <dx:ToolbarListEditItem Text="Tahoma" Value="Tahoma" />
                                <dx:ToolbarListEditItem Text="Verdana" Value="Verdana" />
                                <dx:ToolbarListEditItem Text="Arial" Value="Arial" />
                                <dx:ToolbarListEditItem Text="MS Sans Serif" Value="MS Sans Serif" />
                                <dx:ToolbarListEditItem Text="Courier" Value="Courier" />
                            </Items>
                        </dx:ToolbarFontNameEdit>
                        <dx:ToolbarFontSizeEdit>
                            <Items>
                                <dx:ToolbarListEditItem Text="1 (8pt)" Value="1" />
                                <dx:ToolbarListEditItem Text="2 (10pt)" Value="2" />
                                <dx:ToolbarListEditItem Text="3 (12pt)" Value="3" />
                                <dx:ToolbarListEditItem Text="4 (14pt)" Value="4" />
                                <dx:ToolbarListEditItem Text="5 (18pt)" Value="5" />
                                <dx:ToolbarListEditItem Text="6 (24pt)" Value="6" />
                                <dx:ToolbarListEditItem Text="7 (36pt)" Value="7" />
                            </Items>
                        </dx:ToolbarFontSizeEdit>
                        <dx:ToolbarBoldButton BeginGroup="True">
                        </dx:ToolbarBoldButton>
                        <dx:ToolbarItalicButton>
                        </dx:ToolbarItalicButton>
                        <dx:ToolbarUnderlineButton>
                        </dx:ToolbarUnderlineButton>
                        <dx:ToolbarStrikethroughButton>
                        </dx:ToolbarStrikethroughButton>
                        <dx:ToolbarJustifyLeftButton BeginGroup="True">
                        </dx:ToolbarJustifyLeftButton>
                        <dx:ToolbarJustifyCenterButton>
                        </dx:ToolbarJustifyCenterButton>
                        <dx:ToolbarJustifyRightButton>
                        </dx:ToolbarJustifyRightButton>
                        <dx:ToolbarJustifyFullButton>
                        </dx:ToolbarJustifyFullButton>
                        <dx:ToolbarBackColorButton BeginGroup="True">
                        </dx:ToolbarBackColorButton>
                        <dx:ToolbarFontColorButton>
                        </dx:ToolbarFontColorButton>
                    </Items>
                </dx:HtmlEditorToolbar>
            </Toolbars>
        </dx:ASPxHtmlEditor>
        <br />
        <dx:ASPxMemo ID="ASPxMemo1" runat="server" Visible="false"></dx:ASPxMemo>
        <asp:TextBox ID="txt_body" runat="server" Height="200px" Width="100%"></asp:TextBox>
        <br />
        <iframe id="iframe_body" runat="server" width="100%" height="400px" frameborder="1px" style="border-style: solid"></iframe>
        <%--<dx:ASPxHtmlEditor ID="html_reply" Width="100%" Height="250px" runat="server">
            <Settings AllowHtmlView="false" AllowPreview="false" />
        </dx:ASPxHtmlEditor>--%>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ControlToValidate="txt_body" ForeColor="Red"
            runat="server" Display="Dynamic" Text="* Body Not Empty" ValidationGroup="btn_send"
            ErrorMessage="Please enter a value.">
        </asp:RequiredFieldValidator>
        <br />
        <div class="upload-file">
            <label data-title="Select file" for="upload-demo">
                <asp:FileUpload ID="fu_EmailAttach" runat="server" Visible="true" CssClass="upload-demo" onchange="get_filename(this);" />
                <span data-title="No file selected..."></span>
            </label>
        </div>
        <br />
        <div id="div_attachment" runat="server" visible="true">
            <table class="table-bordered table-condensed table-hover table-striped" id="Table1" style="width: 100%; border: 0px;">
                <thead>
                    <tr>
                        <th style="font-size: x-small; text-align: left;">Url attchment</th>
                    </tr>
                </thead>
                <tbody>
                    <asp:Literal ID="ltr_attchment" runat="server"></asp:Literal>
                </tbody>
            </table>
        </div>
        <div class="text-right">
            <button id="btn_update" runat="server" visible="false" type="submit" class="btn btn-danger" validationgroup="btnsend"><i class="fa fa-save"></i>&nbsp;Update</button>
            <button id="btn_send" runat="server" type="submit" class="btn btn-danger" validationgroup="btn_send"><i class="fa fa-send"></i>&nbsp;Send</button>
            <button id="btn_cancel" runat="server" class="btn btn-danger" type="submit"><i class="fa fa-arrow-circle-left"></i>&nbsp;Cancel</button>
        </div>
    </div>

    
</asp:Content>
