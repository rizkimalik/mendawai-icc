﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/HTML/Ticket.Master" CodeBehind="tr_release.aspx.vb" Inherits="ICC.tr_release" %>

<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxCallbackPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxHtmlEditor.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxHtmlEditor" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxCallback" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="FeaturedContent" runat="server">
    <script>
        function releaseMultiple() {
            if ($('#MainContent_TrxTicketNumber').val() == "") {
                alert("Please, select your data transaction")
                return false;
            } else {
                $('#MainContent_TrxTypeRelease').val("multiple");
                ASPxPopupControl1.Show();
            }
        }
    </script>
    <script>
        function release(TrxTicketNumber, TrxFromLayer) {
            $('#MainContent_TrxFromLayer').val(TrxFromLayer)
            $('#MainContent_TrxTicketNumber').val(TrxTicketNumber)
            $('#MainContent_TrxTypeRelease').val("single");
            ASPxPopupControl1.Show();
        }
    </script>
    <script>
        function selectUser() {
            Remove_selectUser();
            var EscalationLayer = $('#cmbEscalationLayer option:selected').val()
            $.ajax({
                type: "POST",
                url: "WebServiceTransaction.asmx/SelectUserLogin",
                data: "{filterData: '" + EscalationLayer + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    var json = JSON.parse(data.d);
                    var i, x = "";
                    var result = "";
                    $('#cmbEscalationUser').append('<option value="selected">--Select--</option>')
                    for (i = 0; i < json.length; i++) {
                        if (EscalationLayer === '1') {
                            if (json[i].Result == 'False') {
                                alert("Layer 1 not found, Please login")
                                $("#divlabel").css("visibility", "hidden");
                                $("#cmbEscalationUser").css("visibility", "hidden");
                                BTN_Release.SetVisible(false);
                                BTN_Cancel.SetVisible(false);

                            } else {
                                $("#divlabel").css("visibility", "visible");
                                $("#cmbEscalationUser").css("visibility", "visible");
                                $('#cmbEscalationUser').append('<option value="' + json[i].UserName + '">' + json[i].UserName + '</option>');

                            }
                        }
                        else if (EscalationLayer === '2') {
                            if (json[i].Result == 'True') {
                                $("#divlabel").css("visibility", "hidden");
                                $("#cmbEscalationUser").css("visibility", "hidden");
                                BTN_Release.SetVisible(true);
                                BTN_Cancel.SetVisible(true);
                            }

                        }
                        else if (EscalationLayer === '3') {
                            if (json[i].Result == 'False') {
                                $("#divlabel").css("visibility", "hidden");
                                $("#cmbEscalationUser").css("visibility", "hidden");
                                BTN_Release.SetVisible(false);
                                BTN_Cancel.SetVisible(false);

                            } else {
                                $("#divlabel").css("visibility", "visible");
                                $("#cmbEscalationUser").css("visibility", "visible");
                                $('#cmbEscalationUser').append('<option value="' + json[i].UserName + '">' + json[i].UserName + '</option>');
                            }
                        }
                    }
                },
                error: function (xmlHttpRequest, textStatus, errorThrown) {
                    console.log(xmlHttpRequest.responseText);
                    console.log(textStatus);
                    console.log(errorThrown);
                }
            })
        }
    </script>
    <script>
        function execEscalation() {
            var ReleaseFromLayer = $('#MainContent_TrxFromLayer').val();
            var TrxTicketNumber = $('#MainContent_TrxTicketNumber').val();
            var TrxUserName = $('#MainContent_TrxUserName').val();
            var ReleaseToLayer = $('#cmbEscalationLayer option:selected').val();
            var ReleaseUser = $('#cmbEscalationUser option:selected').val();
            var ReleaseReason = $('#MainContent_ASPxPopupControl1_ASPxCallbackPanel1_TxtPelaporAddress_I').val();
            var ReleaseKategori = $('#MainContent_TrxReleaseKategori').val();
            if (ReleaseToLayer === '0') {
                alert("Escalation To Layer is empty")
                return false;
            }
            if (ReleaseToLayer === '1') {
                if (ReleaseUser === 'selected') {
                    alert("Escalation To User is empty")
                    return false;
                }
            }
            else if (ReleaseToLayer === '3') {
                if (ReleaseUser === 'selected') {
                    alert("Escalation To Group is empty")
                    return false;
                }
            }
            else {
                if (ReleaseUser === 'selected') {
                    ReleaseUser = '0';
                }
            }

            if (ReleaseReason === '') {
                alert("Escalation Reason is empty")
                return false;
            }
            if (confirm("Do you want to process?")) {
                $.ajax({
                    type: "POST",
                    url: "WebServiceTransaction.asmx/ReleaseTransaction",
                    data: "{TrxUserName: '" + TrxUserName + "', TrxTicketNumber: '" + TrxTicketNumber + "', ReleaseFromLayer: '" + ReleaseFromLayer + "', ReleaseToLayer: '" + ReleaseToLayer + "', ReleaseUser: '" + ReleaseUser + "', ReleaseReason: '" + ReleaseReason + "', ReleaseKategori: '" + ReleaseKategori + "'}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        var json = JSON.parse(data.d);
                        var i, x = "";
                        var result = "";
                        for (i = 0; i < json.length; i++) {
                            if (json[i].Result == "True") {
                                alert(json[i].msgSystem)
                                ASPxPopupControl1.Hide();
                                window.location.href = "tr_release.aspx";
                            } else {
                                alert(json[i].msgSystem)
                                return false;
                            }
                        }
                    },
                    error: function (xmlHttpRequest, textStatus, errorThrown) {
                        console.log(xmlHttpRequest.responseText);
                        console.log(textStatus);
                        console.log(errorThrown);
                    }
                })
            }
            else
                return false;
        }
    </script>
    <script>
        function Remove_selectUser() {
            $('#cmbEscalationUser')
                .empty()
        }
    </script>
    <script>
        function execCancel() {
            ASPxPopupControl1.Hide();
        }
    </script>
    <script type="text/javascript">
        function ASPxGridView1_SelectionChanged(s, e) {
            s.GetSelectedFieldValues("TicketNumber", GetSelectedFieldValuesCallback);
        }
        function GetSelectedFieldValuesCallback(values) {
            alert(values)
            $('#MainContent_TrxTicketNumber').val(values)
        }
    </script>
    <script>
        function showComplaint(v) {
            $("#MainContent_TrxTicketNumber").val(v);
            $.ajax({
                type: "POST",
                url: "WebServiceTransaction.asmx/Select_Data_TransactionTicket",
                data: "{ filterData: '" + $("#MainContent_TrxTicketNumber").val() + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    var json = JSON.parse(data.d);
                    var i, x = "";
                    var tblTickets = "";

                    for (i = 0; i < json.length; i++) {
                        $("#MainContent_ASPxPopupControl2_ASPxMemo1_I").val(json[i].TrxDetailComplaint);
                        ASPxPopupControl2.Show();

                    }
                },
                error: function (xmlHttpRequest, textStatus, errorThrown) {
                    console.log(xmlHttpRequest.responseText);
                    console.log(textStatus);
                    console.log(errorThrown);
                }
            })
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:HiddenField ID="TrxFromLayer" runat="server" />
    <asp:HiddenField ID="TrxTicketNumber" runat="server" />
    <asp:HiddenField ID="TrxTicketNumberAll" runat="server" />
    <asp:HiddenField ID="TrxUserName" runat="server" />
    <asp:HiddenField ID="TrxReleaseKategori" runat="server" />
    <h4 class="headline">Release Data Transaction
			<span class="line bg-warning"></span>
    </h4>
    <dx:ASPxGridView ID="ASPxGridView1" ClientInstanceName="ASPxGridView1" Width="100%" runat="server"
        DataSourceID="ds_Query" KeyFieldName="NumberRow" SettingsPager-PageSize="15" Theme="Metropolis" Styles-Header-Font-Bold="true" Font-Size="X-Small" Visible="true">
        <SettingsPager>
            <AllButton Text="All">
            </AllButton>
            <NextPageButton Text="Next &gt;">
            </NextPageButton>
            <PrevPageButton Text="&lt; Prev">
            </PrevPageButton>
            <PageSizeItemSettings Visible="true" Items="15, 25, 50" ShowAllItem="true" />
        </SettingsPager>
        <SettingsEditing Mode="Inline" />
        <Settings ShowFilterRow="true" ShowGroupPanel="true" ShowHorizontalScrollBar="true" />
        <SettingsBehavior ConfirmDelete="true" />
        <Columns>
            <dx:GridViewCommandColumn ShowSelectCheckbox="True" VisibleIndex="15" Width="115px" CellStyle-HorizontalAlign="Center">
                <HeaderTemplate>
                    <dx:ASPxButton ID="btn_update_unit" runat="server" AutoPostBack="false" UseSubmitBehavior="false" Theme="Youthful"
                        Width="100px" Height="25px" VerticalAlign="Middle" Font-Bold="true" Font-Size="X-Small" Text="Multiple Release">
                        <ClientSideEvents Click="function() { releaseMultiple() }" />
                    </dx:ASPxButton>
                </HeaderTemplate>
            </dx:GridViewCommandColumn>
            <dx:GridViewDataTextColumn Caption="Action" VisibleIndex="0" CellStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" Width="50px">
                <DataItemTemplate>
                    <a href="#" onclick="release('<%# Eval("TicketNumber")%>','<%# Eval("TicketPosition")%>')">
                        <asp:Image ImageUrl="img/icon/Text-Edit-icon2.png" ID="img_insert" runat="server" ToolTip="Release Transaction" />
                    </a>
                </DataItemTemplate>
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="Ticket Number" FieldName="TicketNumber" HeaderStyle-HorizontalAlign="left" Width="110px" Settings-AutoFilterCondition="Contains"></dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="Customer ID" FieldName="NIK" Width="110px" Settings-AutoFilterCondition="Contains"></dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="Customer Name" FieldName="NamePIC" HeaderStyle-HorizontalAlign="left" Width="150px" Settings-AutoFilterCondition="Contains"></dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="Interaction ID" FieldName="GenesysID" Width="200px" Settings-AutoFilterCondition="Contains"></dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="Thread ID" FieldName="ThreadID" Width="160px" Settings-AutoFilterCondition="Contains"></dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="Account" FieldName="AccountInbound" Width="160px" Settings-AutoFilterCondition="Contains"></dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="User Issue Remark" FieldName="DetailComplaint" HeaderStyle-HorizontalAlign="left" Width="300px" Settings-AutoFilterCondition="Contains">
                <DataItemTemplate>
                    <a href="#" onclick="showComplaint('<%# Eval("TicketNumber")%>')" style="color: black;" title="Show User Issue Remark"><%#Eval("DetailComplaint")%></a>
                </DataItemTemplate>
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="From Name" FieldName="NAMA_PELAPOR" HeaderStyle-HorizontalAlign="left" Width="160px" Settings-AutoFilterCondition="Contains"></dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="From Phone" FieldName="PHONE_PELAPOR" HeaderStyle-HorizontalAlign="left" Width="160px" Settings-AutoFilterCondition="Contains"></dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="Created By" FieldName="CreatedBy" HeaderStyle-HorizontalAlign="left" Width="160px" Settings-AutoFilterCondition="Contains"></dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="Status" FieldName="Status" HeaderStyle-HorizontalAlign="left" Width="160px" Settings-AutoFilterCondition="Contains"></dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="Ticket On Layer" FieldName="TicketPosition" HeaderStyle-HorizontalAlign="left" Width="160px" Settings-AutoFilterCondition="Contains"></dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="SLA" FieldName="SLA" HeaderStyle-HorizontalAlign="left" Width="160px" CellStyle-HorizontalAlign="Left" Settings-AutoFilterCondition="Contains"></dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="SLA Description" FieldName="UsedDaySLAOK" HeaderStyle-HorizontalAlign="left" Width="160px" Settings-AutoFilterCondition="Contains"></dx:GridViewDataTextColumn>
        </Columns>
        <ClientSideEvents SelectionChanged="ASPxGridView1_SelectionChanged" />
    </dx:ASPxGridView>
    <asp:SqlDataSource ID="ds_Query" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>"></asp:SqlDataSource>
    <asp:SqlDataSource ID="ds_Layer" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>"></asp:SqlDataSource>
    <dx:ASPxPopupControl ID="ASPxPopupControl1" ClientInstanceName="ASPxPopupControl1" runat="server" CloseAction="CloseButton" Modal="true" Width="800px"
        closeonescape="true"
        PopupVerticalAlign="WindowCenter"
        PopupHorizontalAlign="WindowCenter" AllowDragging="True" Theme="SoftOrange"
        ShowFooter="True" HeaderText="Form Escalation Release" FooterText="" AutoUpdatePosition="true">
        <ContentCollection>
            <dx:PopupControlContentControl ID="PopupControlContentControl1" runat="server">
                <dx:ASPxCallbackPanel ID="ASPxCallbackPanel1" runat="server" Width="800px" ShowLoadingPanel="false" ClientInstanceName="ASPxCallbackPanel1">
                    <PanelCollection>
                        <dx:PanelContent>
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Escalation To Layer</label>
                                    <select class="Metropolis" id="cmbEscalationLayer" name="cmbEscalationLayer" onchange="selectUser()" style="width: 100%; height: 30px;">
                                        <option value="0" selected="selected">--Select--</option>
                                        <asp:Literal runat="server" ID="ltrEscalationLayer"></asp:Literal>
                                    </select>
                                </div>
                                <div class="col-md-6">
                                    <label id="divlabel" style="visibility: hidden;">Esalation To User or Group</label>
                                    <select class="Metropolis" id="cmbEscalationUser" name="cmbEscalationUser" style="width: 100%; height: 30px; visibility: hidden;">
                                        <option value="0" selected="selected">--Select--</option>
                                    </select>
                                </div>
                            </div>
                            <br />
                            <div class="row">
                                <div class="col-md-12">
                                    <label>Escalation Reason</label>
                                    <dx:ASPxMemo ID="TxtPelaporAddress" runat="server" Width="100%" Rows="10" Theme="Metropolis" />
                                </div>
                            </div>
                            <br />
                            <div class="row">
                                <div class="col-md-8">
                                </div>
                                <div class="col-md-2">
                                    <dx:ASPxButton ID="BTN_Release" runat="server" Theme="Metropolis" Text="Submit" Width="100%" AutoPostBack="false"
                                        HoverStyle-BackColor="#EE4D2D" Height="30px" ClientInstanceName="BTN_Release">
                                        <ClientSideEvents Click="function(s, e) { execEscalation(); }" />
                                    </dx:ASPxButton>
                                </div>
                                <div class="col-md-2">
                                    <dx:ASPxButton ID="BTN_Cancel" runat="server" Theme="Metropolis" Text="Cancel" Width="100%" AutoPostBack="false"
                                        HoverStyle-BackColor="#EE4D2D" Height="30px" ClientInstanceName="BTN_Cancel">
                                        <ClientSideEvents Click="function(s, e) { execCancel(); }" />
                                    </dx:ASPxButton>
                                </div>
                            </div>
                        </dx:PanelContent>
                    </PanelCollection>
                </dx:ASPxCallbackPanel>
            </dx:PopupControlContentControl>
        </ContentCollection>
    </dx:ASPxPopupControl>
    <dx:ASPxPopupControl ID="ASPxPopupControl2" ClientInstanceName="ASPxPopupControl2" runat="server" CloseAction="CloseButton" Modal="true" Width="800px"
        closeonescape="true" Height="450px"
        PopupVerticalAlign="WindowCenter"
        PopupHorizontalAlign="WindowCenter" AllowDragging="True" Theme="SoftOrange"
        ShowFooter="True" HeaderText="Form User Issue Remark" FooterText="" AutoUpdatePosition="true">
        <ContentCollection>
            <dx:PopupControlContentControl ID="PopupControlContentControl2" runat="server">
                <div class="row">
                    <div class="col-md-12">
                        <label>Detail Complaint</label>
                        <dx:ASPxMemo ID="ASPxMemo1" runat="server" Width="100%" Rows="20" Theme="Metropolis" ReadOnly="true" />
                    </div>
                </div>
            </dx:PopupControlContentControl>
        </ContentCollection>
    </dx:ASPxPopupControl>
</asp:Content>
