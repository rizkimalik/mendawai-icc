﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/HTML/Ticket.Master" CodeBehind="chat_live.aspx.vb" Inherits="ICC.chat_live" %>

<asp:Content ID="Content1" ContentPlaceHolderID="FeaturedContent" runat="server">
    <script type="text/javascript">
        document.addEventListener("DOMContentLoaded", function (e) {
            const username = '<%= Session("username")%>';
            const omnichannel = './omnichannel/main.php';
            document.getElementById('iframe_sosmed').src = `${omnichannel}?username=${username}`;
        })
    </script>
   
</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <!-- <asp:HiddenField ID="HD_USERNAME" runat="server"  /> -->

    <div class="chat-wrapper" style="z-index: 999;">        
        <!-- <div class="chat-sidebar border-right bg-white" style="height: calc(100vh - 45px); width: calc(100vw - 194px); "> -->
        <div class="chat-sidebar border-right bg-white" style="height: 100vh; width: calc(100vw - 194px);">
            <iframe id="iframe_sosmed" src="" height="100%" width="100%" frameborder="0" ></iframe>
        </div>
    </div>
    <!-- /chat-wrapper -->

</asp:Content>
