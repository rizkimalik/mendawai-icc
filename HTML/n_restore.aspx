﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/HTML/Ticket.Master" CodeBehind="n_restore.aspx.vb" Inherits="ICC.n_restore" %>

<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxCallbackPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxGridView.Export" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="FeaturedContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div>
        <h4 class="headline">Restore Data
			<span class="line bg-warning"></span>
        </h4>
        <br />
        <dx:ASPxLabel Visible="false" runat="server" ID="ASPxLabel1"></dx:ASPxLabel>
    </div>
    <div class="row">
        <div class="col-sm-4">
            <label>Select Data</label>
            <asp:Label runat="server" ID="lblErr" Visible="false"></asp:Label>
            <dx:ASPxComboBox ID="cmb_Restore" ClientInstanceName="cmb_Restore" Height="30px" runat="server" Theme="Metropolis" Width="100%"
                DataSourceID="sql_Restore" TextField="DescriptionBackup" ValueField="IDBackup" DisplayFormatString="{0}" CssClass="form-control chzn-select">
                <Columns>
                    <dx:ListBoxColumn Caption="ID Data Backup" FieldName="IDBackup" Width="150px" />
                    <dx:ListBoxColumn Caption="Description Backup" FieldName="DescriptionBackup" Width="350px" />
                </Columns>
                <ValidationSettings ErrorTextPosition="Bottom" ErrorDisplayMode="ImageWithText" ValidationGroup="_Validation">
                    <RequiredField IsRequired="true" ErrorText="Must be filled" />
                </ValidationSettings>
            </dx:ASPxComboBox>
            <asp:SqlDataSource ID="sql_Restore" SelectCommand="select * from Temp_BackupRestore order by DateCreate Desc" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>"></asp:SqlDataSource>
        </div>
        <div class="col-sm-2" style="margin-top: 5px;">
            <br />
            <%--<dx:ASPxButton ID="btnRestoreData" runat="server" Text="Restore Data" CssClass="form-control"></dx:ASPxButton>--%>
            <dx:ASPxButton ID="btnRestoreData" runat="server" Theme="Metropolis" Text="Restore Data" Width="100%" AutoPostBack="false"
                HoverStyle-BackColor="#EE4D2D" Height="30px" ValidationGroup="_Validation">
            </dx:ASPxButton>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <div>
                <dx:ASPxGridView ID="gv_transaksi" DataSourceID="sql_transaksi" runat="server" Width="100%" Theme="MetropolisBlue">
                    <Settings ShowFilterRow="true" ShowFilterRowMenu="false" ShowGroupPanel="true" ShowVerticalScrollBar="false" ShowHorizontalScrollBar="true" />
                    <Columns>
                        <%--<dx:GridViewDataTextColumn Caption="No" FieldName="No" Width="30px"></dx:GridViewDataTextColumn>--%>
                        <dx:GridViewDataTextColumn Caption="Ticket Number" FieldName="TicketNumber" Width="200px"></dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="Account Pengaduan" FieldName="AccountInbound" Width="200px"></dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="Kategori" FieldName="CategoryName"></dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="Group Kategori Product" FieldName="SubCategory1Name"></dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="Kategori Product" FieldName="SubCategory2Name"></dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="Kategori Permasalahan" FieldName="SubCategory3Name"></dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="Description" FieldName="DetailComplaint" Width="200px"></dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="Penyebab" FieldName="StrPenyebab" HeaderStyle-HorizontalAlign="left" Width="200px"></dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="Skala Prioritas" FieldName="SkalaPrioritas" HeaderStyle-HorizontalAlign="left" Width="200px"></dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="Jenis Nasabah" FieldName="JenisNasabah" HeaderStyle-HorizontalAlign="left" Width="200px"></dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="Pelanggan" FieldName="NAMA_PELAPOR" Width="200px"></dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="Email" FieldName="EMAIL_PELAPOR" Width="200px"></dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="HP" FieldName="PHONE_PELAPOR" Width="200px"></dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="Type Pelanggan" FieldName="JenisNasabah" Width="200px"></dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="Nomor Rekening" FieldName="NomorRekening" HeaderStyle-HorizontalAlign="left" Width="200px"></dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="Alamat" FieldName="ALAMAT_PELAPOR" Width="200px"></dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="Channel" FieldName="TicketSourceName" Width="200px"></dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="Ticket Status" FieldName="Status" Width="200px"></dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="Created Date" FieldName="DateCreate" Width="200px"></dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="Closed Date" FieldName="DateClose" Width="200px"></dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="Created By" FieldName="AgentCreateInteraction" Width="200px"></dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="Closed By" FieldName="ClosedBy" Width="200px"></dx:GridViewDataTextColumn>
                    </Columns>
                </dx:ASPxGridView>
            </div>
        </div>
    </div>
    <asp:SqlDataSource ID="sql_transaksi" runat="server"
        ConnectionString="<%$ ConnectionStrings:DefaultConnection %>"
        SelectCommand="SP_HelpdeskRestore" SelectCommandType="StoredProcedure">
        <SelectParameters>
            <asp:SessionParameter DbType="String" Name="IDBackup"
                SessionField="IDBackup" />
        </SelectParameters>
    </asp:SqlDataSource>
    <hr />
    <div class="row">
        <div class="col-sm-2">
            <asp:DropDownList runat="server" ID="ddList" Height="30" CssClass="form-control input-sm">
                <asp:ListItem Value="xlsx" Text="Excel" />
                <asp:ListItem Value="xls" Text="Excel 97-2003" />
                <asp:ListItem Value="pdf" Text="PDF" />
                <asp:ListItem Value="rtf" Text="RTF" />
                <asp:ListItem Value="csv" Text="CSV" />
            </asp:DropDownList>
        </div>
        <div class="col-sm-2">
            <%--<dx:ASPxButton ID="btn_Export" runat="server" Text="Export" CssClass="form-control input-sm"></dx:ASPxButton>--%>
            <dx:ASPxButton ID="ASPxButton1" runat="server" Theme="Metropolis" Text="Export" Width="100%" AutoPostBack="false"
                HoverStyle-BackColor="#EE4D2D" Height="30px" ValidationGroup="_Validation">
            </dx:ASPxButton>
        </div>
    </div>
    <hr />
    <dx:ASPxGridViewExporter ID="ASPxGridViewExporter1" runat="server" GridViewID="gv_transaksi"></dx:ASPxGridViewExporter>
    <asp:SqlDataSource ID="sql_SPrpt" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>"></asp:SqlDataSource>
</asp:Content>
