﻿Imports System.IO
Imports System.Data
Imports System.Data.SqlClient
Imports ICC.ClsConn
Imports System.Web.UI.Control
Imports DevExpress.Web.ASPxGridView
Imports DevExpress.Web.ASPxEditors
Imports System.Data.OleDb
Imports System.Configuration
Public Class n_backuprestore
    Inherits System.Web.UI.Page

    Dim clsglobe As New cls_globe
    Dim strsql, sb, SheetName, ConStr, fullnameExcel As String
    Dim dtExcelSchema, tbldata As DataTable
    Dim DtSet As Data.DataSet
    Dim cmdExcel As New OleDbCommand()
    Dim oda As New OleDbDataAdapter()
    Dim dt As New DataTable()
    Dim con As New SqlCommand
    Dim connection As New SqlConnection(ConfigurationManager.ConnectionStrings("DefaultConnection").ConnectionString)
    Dim com As SqlCommand
    Dim sqldr As SqlDataReader
    Dim proses As New ClsConn
    Dim _upage As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        _updatePage()
    End Sub

    Private Sub btn_Submit_Click(sender As Object, e As EventArgs) Handles btn_Submit.Click
        Dim Data, str As String
        str = "exec SP_HelpdeskBackup '" & Session("username") & "','" & dt_strdate.Text & "','" & dt_endate.Text & "','" & txtDescription.Text & "'"
        Try
            com = New SqlCommand(str, connection)
            connection.Open()
            com.ExecuteNonQuery()
            connection.Close()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Private Sub _updatePage()
        Try
            _upage = "update user1 set Activity='N'"
            proses.ExecuteNonQuery(_upage)
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
        Try
            _upage = "update user1 set Activity='Y' where MenuID='" & Request.QueryString("idpage") & "'"
            proses.ExecuteNonQuery(_upage)
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
        Try
            _upage = "update user2 set Activity='N'"
            proses.ExecuteNonQuery(_upage)
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
        Try
            _upage = "update user2 set Activity='Y' where SubMenuID='" & Request.QueryString("idtable") & "'"
            proses.ExecuteNonQuery(_upage)
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Private Sub _btnCancel_Click(sender As Object, e As EventArgs) Handles _btnCancel.Click
        Response.Redirect("n_backuprestore.aspx")
    End Sub
End Class