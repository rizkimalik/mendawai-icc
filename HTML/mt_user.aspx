﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/HTML/Ticket.Master" CodeBehind="mt_user.aspx.vb" Inherits="ICC.mt_user" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="FeaturedContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <h4 class="headline">Setting User Privilige
			<span class="line bg-warning" style="background-color:#f37021;"></span>
    </h4>
    <dx:ASPxGridView ID="ASPxGridView1" ClientInstanceName="ASPxGridView1" runat="server" Theme="Metropolis"
        DataSourceID="ds_user" KeyFieldName="Role_id" Width="100%" Styles-Header-Font-Bold="true" Font-Size="X-Small">
        <SettingsPager>
            <AllButton Text="All">
            </AllButton>
            <NextPageButton Text="Next &gt;">
            </NextPageButton>
            <PrevPageButton Text="&lt; Prev">
            </PrevPageButton>
            <PageSizeItemSettings Visible="true" Items="25, 50, 75" ShowAllItem="true" />
        </SettingsPager>
        <SettingsPager PageSize="15" />
        <SettingsEditing Mode="Inline" />
        <Settings ShowFilterRow="true" ShowGroupPanel="true" ShowHorizontalScrollBar="false" />
        <SettingsBehavior ConfirmDelete="true" />
        <Columns>
            <dx:GridViewCommandColumn Caption="Action" ButtonType="Image" VisibleIndex="0" Width="130px" HeaderStyle-HorizontalAlign="Center" Visible="false">
                <NewButton Visible="false">
                    <Image Url="img/Icon/Apps-text-editor-icon2.png"></Image>
                </NewButton>
                <DeleteButton Visible="false">
                    <Image Url="img/Icon/Actions-edit-clear-icon2.png"></Image>
                </DeleteButton>
                <EditButton Visible="false">
                    <Image ToolTip="Edit" Url="img/Icon/Text-Edit-icon2.png" />
                </EditButton>
                <CancelButton Visible="false">
                    <Image ToolTip="Cancel" Url="img/Icon/cancel1.png">
                    </Image>
                </CancelButton>
                <UpdateButton Visible="false">
                    <Image ToolTip="Update" Url="img/Icon/Updated1.png" />
                </UpdateButton>
            </dx:GridViewCommandColumn>
            <dx:GridViewDataColumn Caption="Level User" FieldName="Role_user" ReadOnly="false" Settings-AutoFilterCondition="Contains" Width="250px" />
            <dx:GridViewDataColumn Caption="Description" FieldName="Description" Settings-AutoFilterCondition="Contains" Width="250px" />
        </Columns>
        <Templates>
            <DetailRow>
                <dx:ASPxGridView ID="ASPxGridView2" ClientInstanceName="ASPxGridView2" Width="1200px" runat="server" DataSourceID="ds_one_menu" KeyFieldName="MenuID"
                    SettingsBehavior-AllowFocusedRow="true" OnBeforePerformDataSelect="onemenu_select" Theme="Metropolis" Styles-Header-Font-Bold="true" Font-Size="X-Small">
                    <SettingsPager>
                        <AllButton Text="All">
                        </AllButton>
                        <NextPageButton Text="Next &gt;">
                        </NextPageButton>
                        <PrevPageButton Text="&lt; Prev">
                        </PrevPageButton>
                    </SettingsPager>
                    <SettingsPager PageSize="10" />
                    <SettingsEditing Mode="Inline" />
                    <Settings ShowGroupPanel="true" ShowHorizontalScrollBar="false" ShowFilterRow="true" />
                    <SettingsBehavior ConfirmDelete="true" />
                    <Columns>
                        <dx:GridViewCommandColumn Caption="Action" ButtonType="Image" Width="150px" HeaderStyle-HorizontalAlign="Center">
                            <NewButton Visible="true">
                                <Image Url="img/Icon/Apps-text-editor-icon2.png"></Image>
                            </NewButton>
                            <DeleteButton Visible="true">
                                <Image Url="img/Icon/Actions-edit-clear-icon2.png"></Image>
                            </DeleteButton>
                            <CancelButton Visible="true">
                                <Image ToolTip="Cancel" Url="img/Icon/cancel1.png">
                                </Image>
                            </CancelButton>
                            <UpdateButton Visible="true">
                                <Image ToolTip="Update" Url="img/Icon/Updated1.png" />
                            </UpdateButton>
                        </dx:GridViewCommandColumn>
                        <dx:GridViewDataTextColumn Caption="ID" FieldName="MenuID" Settings-AutoFilterCondition="Contains" Width="100px"
                            CellStyle-HorizontalAlign="left" HeaderStyle-HorizontalAlign="left">
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataComboBoxColumn Caption="Menu" FieldName="MenuName" HeaderStyle-HorizontalAlign="left">
                            <PropertiesComboBox TextField="MenuName" ValueField="MenuID" EnableSynchronization="False"
                                TextFormatString="{1}" IncrementalFilteringMode="Contains" DataSourceID="sql_user_satu">
                                <Columns>
                                    <dx:ListBoxColumn Caption="ID" FieldName="MenuID" Width="50px" />
                                    <dx:ListBoxColumn Caption="Menu" FieldName="MenuName" Width="200px" />
                                </Columns>
                            </PropertiesComboBox>
                        </dx:GridViewDataComboBoxColumn>
                    </Columns>
                    <Templates>
                        <DetailRow>
                            <dx:ASPxGridView ID="GridList" Width="100%" runat="server" DataSourceID="sql_sub_menu" Font-Size="X-Small"
                                KeyFieldName="SubMenuID" OnBeforePerformDataSelect="GridList_DataSelect" Theme="Metropolis" Styles-Header-Font-Bold="true">
                                <SettingsEditing Mode="Inline" />
                                <Settings VerticalScrollBarStyle="Standard" ShowGroupPanel="true" ShowHorizontalScrollBar="false" ShowFilterRow="true" />
                                <SettingsBehavior ConfirmDelete="true" />
                                <Columns>
                                    <dx:GridViewCommandColumn Caption="Action" ButtonType="Image" Width="150px" HeaderStyle-HorizontalAlign="Center">
                                        <NewButton Visible="true">
                                            <Image Url="img/Icon/Apps-text-editor-icon2.png"></Image>
                                        </NewButton>
                                        <DeleteButton Visible="true">
                                            <Image Url="img/Icon/Actions-edit-clear-icon2.png"></Image>
                                        </DeleteButton>
                                        <CancelButton Visible="true">
                                            <Image ToolTip="Cancel" Url="img/Icon/cancel1.png">
                                            </Image>
                                        </CancelButton>
                                        <UpdateButton Visible="true">
                                            <Image ToolTip="Update" Url="img/Icon/Updated1.png" />
                                        </UpdateButton>
                                    </dx:GridViewCommandColumn>
                                    <dx:GridViewDataTextColumn Caption="ID" FieldName="SubMenuID" Settings-AutoFilterCondition="Contains"
                                        Width="50px" CellStyle-HorizontalAlign="left" HeaderStyle-HorizontalAlign="left">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataComboBoxColumn Caption="Sub Menu" FieldName="SubMenuName" HeaderStyle-HorizontalAlign="left">
                                        <PropertiesComboBox TextField="SubMenuName" ValueField="SubMenuID" EnableSynchronization="False"
                                            TextFormatString="{1}" IncrementalFilteringMode="Contains" DataSourceID="sql_subMenu_dr">
                                            <Columns>
                                                <dx:ListBoxColumn Caption="ID" FieldName="SubMenuID" Width="50px" />
                                                <dx:ListBoxColumn Caption="Sub Menu" FieldName="SubMenuName" Width="200px" />
                                            </Columns>
                                        </PropertiesComboBox>
                                    </dx:GridViewDataComboBoxColumn>
                                </Columns>
                                <Templates>
                                    <DetailRow>
                                        <dx:ASPxGridView ID="gv_menu_tree" runat="server" Width="100%" DataSourceID="sql_menu_tree" Font-Size="X-Small"
                                            KeyFieldName="SubMenuIDTree" OnBeforePerformDataSelect="gv_menu_tree_DataSelect" Theme="Metropolis" Styles-Header-Font-Bold="true">
                                            <SettingsEditing Mode="Inline" />
                                            <SettingsBehavior ConfirmDelete="true" />
                                            <Settings VerticalScrollBarStyle="Standard" ShowGroupPanel="true" ShowHorizontalScrollBar="false" ShowFilterRow="true" />
                                            <Columns>
                                                <dx:GridViewCommandColumn Caption="Action" ButtonType="Image" Width="150px" HeaderStyle-HorizontalAlign="Center">
                                                    <NewButton Visible="true">
                                                        <Image Url="img/Icon/Apps-text-editor-icon2.png"></Image>
                                                    </NewButton>
                                                    <DeleteButton Visible="true">
                                                        <Image Url="img/Icon/Actions-edit-clear-icon2.png"></Image>
                                                    </DeleteButton>
                                                    <CancelButton Visible="true">
                                                        <Image ToolTip="Cancel" Url="img/Icon/cancel1.png">
                                                        </Image>
                                                    </CancelButton>
                                                    <UpdateButton Visible="true">
                                                        <Image ToolTip="Update" Url="img/Icon/Updated1.png" />
                                                    </UpdateButton>
                                                </dx:GridViewCommandColumn>
                                                <dx:GridViewDataTextColumn Caption="ID" FieldName="SubMenuIDTree" Settings-AutoFilterCondition="Contains"
                                                    Width="50px" CellStyle-HorizontalAlign="left" HeaderStyle-HorizontalAlign="left">
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataComboBoxColumn Caption="Menu Tree" FieldName="MenuTreeName" HeaderStyle-HorizontalAlign="left">
                                                    <PropertiesComboBox TextField="MenuTreeName" ValueField="SubMenuIDTree" EnableSynchronization="False"
                                                        TextFormatString="{1}" IncrementalFilteringMode="Contains" DataSourceID="ds_user_3">
                                                        <Columns>
                                                            <dx:ListBoxColumn Caption="ID" FieldName="SubMenuIDTree" Width="50px" />
                                                            <dx:ListBoxColumn Caption="Menu Tree" FieldName="MenuTreeName" Width="200px" />
                                                        </Columns>
                                                    </PropertiesComboBox>
                                                </dx:GridViewDataComboBoxColumn>
                                            </Columns>
                                        </dx:ASPxGridView>
                                    </DetailRow>
                                </Templates>
                                <SettingsDetail ShowDetailRow="True" />
                                <Settings ShowFooter="True" />
                            </dx:ASPxGridView>
                        </DetailRow>
                    </Templates>
                    <SettingsDetail ShowDetailRow="true" />
                    <Settings ShowFooter="True" />
                </dx:ASPxGridView>
            </DetailRow>
        </Templates>
        <SettingsDetail ShowDetailRow="true" />
        <Settings ShowFooter="True" />
    </dx:ASPxGridView>
       <asp:SqlDataSource ID="ds_one_menu" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>"></asp:SqlDataSource>
    <asp:SqlDataSource ID="sql_sub_menu" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>"></asp:SqlDataSource>
    <asp:SqlDataSource ID="ds_user" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>"></asp:SqlDataSource>
    <asp:SqlDataSource ID="sql_user_satu" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>" SelectCommand="SELECT * FROM User1"></asp:SqlDataSource>
    <asp:SqlDataSource ID="sql_subMenu_dr" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>"></asp:SqlDataSource>
    <asp:SqlDataSource ID="sql_menu_tree" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>"></asp:SqlDataSource>
    <asp:SqlDataSource ID="ds_user_3" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>"></asp:SqlDataSource>
    <asp:SqlDataSource ID="sql_level_user" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>"></asp:SqlDataSource>
    <asp:SqlDataSource ID="sql_organization" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>"></asp:SqlDataSource>
    <asp:SqlDataSource ID="sql_karyawan" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>"></asp:SqlDataSource>
</asp:Content>
