﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/HTML/Ticket.Master" CodeBehind="mt_login.aspx.vb" Inherits="ICC.mt_login" %>

<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxCallbackPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxHtmlEditor.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxHtmlEditor" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxCallback" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="FeaturedContent" runat="server">
    <script>
        function ShowRelease(userName) {
            $("#MainContent_TrxUserID").val(userName);
            ASPxPopupControl1.Show();
        }
    </script>
     <script>
         function encodeData(s) {
             return encodeURIComponent(s).replace(/\-/g, "%2D").replace(/\_/g, "%5F").replace(/\./g, "%2E").replace(/\!/g, "%21").replace(/\~/g, "%7E").replace(/\*/g, "%2A").replace(/\'/g, "%27").replace(/\(/g, "%28").replace(/\)/g, "%29");
         }
    </script>
    <script type="text/javascript">
        function execUpdateRelease() {
            var TrxUsername = $("#MainContent_TrxUserName").val();
            var TrxUserID = $("#MainContent_TrxUserID").val();
            var TrxReason = $("#MainContent_ASPxPopupControl1_ASPxMemo1_I").val();
            if (TrxReason === '') {
                alert("Reason is empty")
                return false;
            }
            if (confirm("Do you want to process?")) {
                $.ajax({
                    type: "POST",
                    url: "WebServiceTransaction.asmx/UpdateReleaseUser",
                    data: "{ TrxUsername: '" + TrxUsername + "',TrxUserID: '" + TrxUserID + "',TrxReason: '" + encodeData(TrxReason) + "'}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        var json = JSON.parse(data.d);
                        var i, x = "";
                        var tblTickets = "";

                        for (i = 0; i < json.length; i++) {
                            //alert(json[i].Result)
                            if (json[i].Result === 'True') {
                                alert(json[i].msgSystem)
                                window.location.href = "mt_login.aspx?id=<%=Request.QueryString("id")%>";
                            } else {
                                alert(json[i].msgSystem)
                                return false;
                            }

                        }
                    },
                    error: function (xmlHttpRequest, textStatus, errorThrown) {
                        console.log(xmlHttpRequest.responseText);
                        console.log(textStatus);
                        console.log(errorThrown);
                    }
                })
            } else {
                return false;
            }
        }
    </script>
   <script>
       function execCancelTransaction() {
           ASPxPopupControl1.Hide();
       }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:HiddenField ID="TrxUserName" runat="server" />
    <asp:HiddenField ID="TrxUserID" runat="server" />
    <div id="div_all" runat="server">
        <h4 class="headline">Data Monitoring Login
			<span class="line bg-warning"></span>
        </h4>
        <div class="btn-group pull-right" style="margin-top: -40px;">
            <a href="mt_login.aspx?idpage=1012" title="Refresh page"><i class="fa fa-refresh"></i></a>
        </div>
        <div class="padding-md" style="margin-top: -20px; margin-left: -20px;">
            <div class="row">
                <div id="idv_ticket" runat="server">
                    <a href="RedirectAccess.aspx?idx=1" data-toggle="modal" id="modal_ticket_open" runat="server">
                        <div class="col-md-4 col-sm-4">
                            <div class="panel panel-default panel-stat2 bg-success">
                                <div class="panel-body">
                                    <span class="stat-icon">
                                        <i class="fa fa-user"></i>
                                    </span>
                                    <div class="pull-right text-right">
                                        <div class="value">
                                            <asp:Label ID="lblicense" runat="server" ForeColor="White" Text="0"></asp:Label>
                                        </div>
                                        <div class="title">
                                            Agent License
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>

                    <div class="col-md-4 col-sm-4">
                        <a href="mt_login.aspx?id=1" data-toggle="modal" id="modal_ticket_progress" runat="server">
                            <div class="panel panel-default panel-stat2 bg-warning">
                                <div class="panel-body">
                                    <span class="stat-icon">
                                        <i class="fa fa-user"></i>
                                    </span>
                                    <div class="pull-right text-right">
                                        <div class="value">
                                            <asp:Label ID="lblogin" runat="server" ForeColor="White"></asp:Label>
                                        </div>
                                        <div class="title">
                                            Agent Login
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-md-4 col-sm-4">
                        <a href="mt_login.aspx?id=2" data-toggle="modal" id="modal_ticket_pending" runat="server">
                            <div class="panel panel-default panel-stat2 bg-info">
                                <div class="panel-body">
                                    <span class="stat-icon">
                                        <i class="fa fa-user"></i>
                                    </span>
                                    <div class="pull-right text-right">
                                        <div class="value">
                                            <asp:Label ID="lblnotlogin" runat="server" ForeColor="White"></asp:Label>
                                        </div>
                                        <div class="title">
                                            Agent Not Login
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </a>
                    </div>
                    <%--<div class="col-md-3 col-sm-4">
                        <a href="mt_login.aspx?id=3" data-toggle="modal" id="modal_ticket_close" runat="server">
                            <div class="panel panel-default panel-stat2 bg-danger">
                                <div class="panel-body">
                                    <span class="stat-icon">
                                        <i class="fa fa-user"></i>
                                    </span>
                                    <div class="pull-right text-right">
                                        <div class="value">
                                            <asp:Label ID="lbaux" runat="server" ForeColor="White"></asp:Label>
                                        </div>
                                        <div class="title">
                                            Agent AUX
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>--%>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <dx:ASPxGridView ID="ASPxGridView1" ClientInstanceName="ASPxGridView1" runat="server" Theme="Metropolis"
                        DataSourceID="ds_login" KeyFieldName="USERID" Width="100%" Styles-Header-Font-Bold="true" Font-Size="X-Small">
                        <SettingsPager>
                            <AllButton Text="All">
                            </AllButton>
                            <NextPageButton Text="Next &gt;">
                            </NextPageButton>
                            <PrevPageButton Text="&lt; Prev">
                            </PrevPageButton>
                            <PageSizeItemSettings Visible="true" Items="25, 50, 75" ShowAllItem="true" />
                        </SettingsPager>
                        <SettingsPager PageSize="10" />
                        <SettingsEditing Mode="Inline" />
                        <Settings ShowFilterRow="true" ShowGroupPanel="true" ShowHorizontalScrollBar="false" />
                        <SettingsBehavior ConfirmDelete="true" />
                        <Columns>
                            <%--     <dx:GridViewDataColumn Width="10px" Caption="Action" HeaderStyle-HorizontalAlign="Center" CellStyle-HorizontalAlign="Center">
                                <DataItemTemplate>
                                    <asp:Button ID="btnRelease" runat="server" OnClick="btnRelease_Click" Text="Release" CommandArgument='<%# Eval("USERID")%>' ToolTip="Release Login" />
                                </DataItemTemplate>
                            </dx:GridViewDataColumn>--%>
                            <dx:GridViewDataTextColumn Caption="Action" VisibleIndex="0" CellStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" Width="50px" ToolTip="Preview Data Transaction">
                                <DataItemTemplate>
                                    <a href="#" id="TicketID" onclick="ShowRelease('<%# Eval("USERNAME")%>')">Release</a>
                                </DataItemTemplate>
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn Caption="Username" FieldName="USERNAME" ReadOnly="false" Settings-AutoFilterCondition="Contains" Width="150px" />
                            <dx:GridViewDataTextColumn Caption="Name" FieldName="NAME" Settings-AutoFilterCondition="Contains" Width="150px" />
                            <dx:GridViewDataTextColumn Caption="Level user" FieldName="LEVELUSER" HeaderStyle-HorizontalAlign="left" Width="150px" />
                            <dx:GridViewDataTextColumn Caption="Login State" FieldName="LOGIN" ReadOnly="false" CellStyle-HorizontalAlign="Left" Settings-AutoFilterCondition="Contains" Width="150px" />
                            <dx:GridViewDataTextColumn Caption="Aux Description" FieldName="DescAUX" ReadOnly="false" CellStyle-HorizontalAlign="Left" Settings-AutoFilterCondition="Contains" Width="150px" />
                        </Columns>
                    </dx:ASPxGridView>
                </div>
            </div>
        </div>
    </div>
    <asp:SqlDataSource ID="ds_login" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>"></asp:SqlDataSource>
    <dx:ASPxPopupControl ID="ASPxPopupControl1" ClientInstanceName="ASPxPopupControl1" runat="server" CloseAction="CloseButton" Modal="false"
        closeonescape="true" Height="400" AllowResize="true" Width="900px"
        PopupVerticalAlign="WindowCenter" PopupHorizontalAlign="WindowCenter" AllowDragging="True" Theme="SoftOrange" ShowPageScrollbarWhenModal="true" ScrollBars="None"
        ShowFooter="True" HeaderText="Form Release User" FooterText="" AutoUpdatePosition="true" ClientSideEvents-CloseUp="function(s, e) { closeReloadPage(); }">
        <ContentCollection>
            <dx:PopupControlContentControl ID="PopupControlContentControl5" runat="server">
                <div class="row">
                    <div class="col-md-12">
                        <dx:ASPxMemo ID="ASPxMemo1" runat="server" Theme="Metropolis" Width="100%" Height="300px"></dx:ASPxMemo>
                    </div>
                </div>
                <br />
                <div class="row">
                    <div class="col-md-8">
                    </div>
                    <div class="col-md-2">
                        <dx:ASPxButton ID="BTNSubmit" runat="server" Theme="Metropolis" Text="Submit" Width="100%" AutoPostBack="false"
                            HoverStyle-BackColor="#EE4D2D" Height="30px" ClientInstanceName="BTNSubmit">
                            <ClientSideEvents Click="function(s, e) { execUpdateRelease(); }" />
                        </dx:ASPxButton>
                    </div>
                    <div class="col-md-2">
                        <dx:ASPxButton ID="BTNCancel" runat="server" Theme="Metropolis" Text="Cancel" Width="100%" AutoPostBack="false"
                            HoverStyle-BackColor="#EE4D2D" Height="30px" ClientInstanceName="BTNCancel">
                            <ClientSideEvents Click="function(s, e) { execCancelTransaction(); }" />
                        </dx:ASPxButton>
                    </div>
                </div>
            </dx:PopupControlContentControl>
        </ContentCollection>
    </dx:ASPxPopupControl>
</asp:Content>
