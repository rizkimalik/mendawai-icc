﻿
Imports System.ComponentModel
Imports System.Web
Imports System.Web.Services
Imports System.Web.Script.Services
Imports System.Web.Services.Protocols
Imports System.Web.Script.Serialization
Imports System.Data
Imports System.Data.SqlClient
Imports System.Net
Imports System.IO
Imports System.Xml
Imports System.Data.OleDb
Imports System.Data.Common

' To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line.
' <System.Web.Script.Services.ScriptService()> _
<System.Web.Services.WebService(Namespace:="http://tempuri.org/")>
<System.Web.Services.WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)>
<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
<ScriptService()>
<ToolboxItem(False)>
Public Class WebServiceTransaction
    Inherits System.Web.Services.WebService

    Dim sqlcom As SqlCommand
    Dim sqlcon As New SqlConnection(ConfigurationManager.ConnectionStrings("DefaultConnection").ConnectionString)
    Dim strSql As String
    Dim sqldr As SqlDataReader
    Dim strTime As String = DateTime.Now.ToString("yyyyMMddhhmmssfff")
    Dim strLogTime As String = DateTime.Now.ToString("yyyy")
    Dim TrxEmailForm As String = ConfigurationManager.AppSettings("EmailForm")
    Dim Proses As New ClsConn
    Public Class resultInsert
        Public Property Result As String
        Public Property UserID As String
        Public Property NamaNya As String
        Public Property ChatID As String
        Public Property msgSystem As String
        Public Property TicketNumber As String
        Public Property CustomerID As String
        Public Property ParentNumberID As String
    End Class
    Public Class listKategori
        Public Property Result As String
        Public Property CategoriID As String
        Public Property CategoriName As String
        Public Property CategoriLevel1ID As String
        Public Property CategoriLevel1Name As String
        Public Property CategoriLevel2ID As String
        Public Property CategoriLevel2Name As String
        Public Property CategoriLevel3ID As String
        Public Property CategoriLevel3Name As String
    End Class
    Public Class listUser
        Public Property Result As String
        Public Property UserName As String
    End Class
    Public Class listOrganization
        Public Property Result As String
        Public Property OrganizationID As String
        Public Property OrganizationName As String
    End Class
    Public Class listCounting
        Public Property Result As String
        Public Property Counting As String
    End Class
    Public Class listCustomer
        Public Property Result As String
        Public Property CustomerID As String
        Public Property CustomerName As String
        Public Property CustomerEmail As String
        Public Property CustomerHP As String
        Public Property CustomerGender As String
        Public Property CustomerBirth As String
        Public Property CustomerCIF As String
        Public Property CustomerNIK As String
        Public Property CustomerAddress As String
        Public Property CustomerTahunlahir As String
        Public Property CustomerBulanlahir As String
        Public Property CustomerHarilahir As String
    End Class
    Public Class listTransaction
        Public Property Result As String
        Public Property TrxID As String
        Public Property TrxCustomerid As String
        Public Property TrxCustomerName As String
        Public Property TrxTicketNumber As String
        Public Property TrxTicketSourceName As String
        Public Property TrxCategoryID As String
        Public Property TrxCategoryName As String
        Public Property TrxLevel1ID As String
        Public Property TrxLevel1Name As String
        Public Property TrxLevel2ID As String
        Public Property TrxLevel2Name As String
        Public Property TrxLevel3ID As String
        Public Property TrxLevel3Name As String
        Public Property TrxDetailComplaint As String
        Public Property TrxResponComplaint As String
        Public Property TrxDivisi As String
        Public Property TrxSLA As String
        Public Property TrxStatus As String
        Public Property TrxUserCreate As String
        Public Property TxtThreadID As String
        Public Property TrxInteractionAccount As String
        Public Property TrxNamaPelapor As String
        Public Property TrxEmailPelapor As String
        Public Property TrxPhonePelapor As String
        Public Property TrxAlamatPelapor As String
        Public Property TrxNomorRekening As String
        Public Property TrxSumberInformasi As String
        Public Property TrxEskalasiUnit As String
        Public Property TrxKejadian As String
        Public Property TrxPenyebab As String
        Public Property TrxStatusPelapor As String
        Public Property TrxPenerimaPengaduan As String
        Public Property TrxSkalaPrioritas As String
        Public Property TrxJenisNasabah As String
        Public Property TrxThreadID As String
        Public Property TrxGenesysID As String
        Public Property TrxExtendID As String
        Public Property TrxExtendSLA As String
        Public Property TrxExtendName As String
        Public Property TrxTicketPosition As String
        Public Property TrxTahun As String
        Public Property TrxBulan As String
        Public Property TrxHari As String
        Public Property TrxReleaseUser As String
        Public Property TrxmsgSystem As String
        Public Property TrxDateCreate As String
        Public Property TrxDateClose As String
        Public Property TrxAmount As String
        Public Property TrxAmountDecimal As String
    End Class
    Public Class listExtendSLA
        Public Property Result As String
        Public Property TrxExtendSLA As String
        Public Property TrxNameExtend As String
    End Class
    Public Class listEmailOut
        Public Property Result As String
        Public Property TrxBodyHTML As String
        Public Property TrxIVCID As String
    End Class
    Public Function ConvertDataTabletoString(ByVal dt As DataTable) As String
        Dim serializer As System.Web.Script.Serialization.JavaScriptSerializer = New System.Web.Script.Serialization.JavaScriptSerializer()
        Dim rows As List(Of Dictionary(Of String, Object)) = New List(Of Dictionary(Of String, Object))()
        Dim row As Dictionary(Of String, Object)

        For Each dr As DataRow In dt.Rows
            row = New Dictionary(Of String, Object)()

            For Each col As DataColumn In dt.Columns
                row.Add(col.ColumnName, dr(col))
            Next

            rows.Add(row)
        Next

        Return serializer.Serialize(rows)
    End Function

    Public Function replaceAgentLogin(ByVal str2)
        Dim TmpStr As String
        'Dim filePathReplace As String = ConfigurationManager.AppSettings("filePath")
        'Response.Write(filePathReplace)
        TmpStr = str2
        TmpStr = Replace(TmpStr, "EXEC SP_LOGIN_APPLIKASI_LOCAL", "")
        TmpStr = Replace(TmpStr, "  ", "")
        TmpStr = Replace(TmpStr, "'", "")
        TmpStr = Replace(TmpStr, "\", " ")
        TmpStr = Replace(TmpStr, "/", " ")
        TmpStr = Replace(TmpStr, ":", " ")
        TmpStr = Replace(TmpStr, "*", " ") ' \/: *?"<>|
        TmpStr = Replace(TmpStr, "?", " ")
        TmpStr = Replace(TmpStr, """", " ")
        TmpStr = Replace(TmpStr, "<", " ")
        TmpStr = Replace(TmpStr, ">", " ")
        TmpStr = Replace(TmpStr, "|", " ")
        TmpStr = Replace(TmpStr, "SELECT mCustomer.* FROM mCustomer left outer join mCustomerChannel on mCustomer.CustomerID = mCustomerChannel.CustomerID where mCustomerChannel.ValueChannel=", " ")
        TmpStr = Replace(TmpStr, "SELECT mCustomer.  FROM mCustomer left outer join mCustomerChannel on mCustomer.CustomerID = mCustomerChannel.CustomerID where mCustomerChannel.ValueChannel=", "")

        replaceAgentLogin = TmpStr

    End Function

    Public Sub LogSuccess(ByVal agentName As String, strValue As String)
        Dim getAgentName As String = ""
        Dim mystr As String = ""
        Dim FileCounter As Integer = 1
        Dim pathX As String = ""
        Dim message As String = String.Format("Time: {0}", DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss:fff tt"))
        message += Environment.NewLine
        message += "---------------------------Success-------------------------------------------------------"
        message += Environment.NewLine
        message += String.Format("Message: {0}", strValue)
        message += Environment.NewLine
        message += "---------------------------Success-------------------------------------------------------"
        message += Environment.NewLine

        Try
            Dim mysplit As String() = strValue.Split(",")
            'getAgentName = strValue.Substring(strValue, 0).ToString
            'mystr += mysplit(0).Replace("EXEC SP_LOGIN_APPLIKASI_LOCAL", "")
            'mystr = replaceAgentLogin(mysplit(0))
            getAgentName = mystr

            'EXEC SP_LOGIN_APPLIKASI_LOCAL  'admin', '12345'   'admin'

            Dim DirectoryX As String = Path.Combine(Server.MapPath("~/HTML/ErrorLog/" & agentName & "/" & DateTime.Now.ToString("ddMMyyyy")))

            If Not System.IO.Directory.Exists(DirectoryX) Then
                System.IO.Directory.CreateDirectory(DirectoryX)
            End If
        Catch exX As Exception
            Return
        End Try

        'Try

        '    Dim maxSize As String = "7" '7 MB / 10287512 kb
        '    Dim FileName As String
        '    Dim writeText As String = message

        '    FileCounter += 1
        '    pathX = HttpContext.Current.Server.MapPath("~/HTML/ErrorLog/" & agentName & "/" & DateTime.Now.ToString("ddMMyyyy") & "/" & DateTime.Now.ToString("ddMMyyyy") & "_" & FileCounter & ".txt")
        '    FileName = pathX

        '    If File.Exists(FileName) Then
        '        Dim str As String = File.ReadAllText(FileName)

        '        If (str.Length + writeText.Length) / (1024 * 1024) > maxSize Then
        '            FileCounter += 1
        '            FileName = HttpContext.Current.Server.MapPath("~/HTML/ErrorLog/" & agentName & "/" & DateTime.Now.ToString("ddMMyyyy") & "/" & DateTime.Now.ToString("ddMMyyyy") & "_" & FileCounter & ".txt")

        '            Dim _sw As StreamWriter = New StreamWriter(FileName, True)
        '            _sw = File.CreateText(FileName)
        '            '_sw.WriteLine(writeText)
        '            '_sw.Close()
        '            'Else
        '            '    Dim _sw As StreamWriter = New StreamWriter(FileName, True)
        '            '    _sw.WriteLine(writeText)
        '            '    _sw.Flush()
        '            '    _sw.Close()
        '        End If
        '    End If

        'Catch exx As Exception

        'End Try

        'Try
        '    FileCounter += 1
        '    pathX = HttpContext.Current.Server.MapPath("~/HTML/ErrorLog/" & agentName & "/" & DateTime.Now.ToString("ddMMyyyy") & "/" & DateTime.Now.ToString("ddMMyyyy") & "_" & FileCounter & ".txt")

        '    Using writer As New StreamWriter(pathX, True)
        '        writer.WriteLine(message)
        '        writer.Flush()
        '        writer.Close()
        '    End Using
        'Catch ex As Exception

        'End Try



        Try
            pathX = HttpContext.Current.Server.MapPath("~/HTML/ErrorLog/" & agentName & "/" & DateTime.Now.ToString("ddMMyyyy") & "/" & DateTime.Now.ToString("ddMMyyyy") & ".txt")
            Using writer As New StreamWriter(pathX, True)
                writer.WriteLine(message)
                writer.Close()
            End Using
        Catch exx As Exception

        End Try



    End Sub

    Public Sub LogError(ByVal agentName As String, ex As Exception, strUser As String)
        Dim getUserLog As String = ""
        Dim mystr As String = ""
        Dim message As String = String.Format("Time: {0}", DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss:fff tt"))
        message += Environment.NewLine
        message += "---------------------------Failed-------------------------------------------------------"
        message += Environment.NewLine
        message += String.Format("Message: {0}", strUser)
        message += Environment.NewLine
        message += String.Format("Message: {0}", ex.Message)
        message += Environment.NewLine
        message += String.Format("StackTrace: {0}", ex.StackTrace)
        message += Environment.NewLine
        message += String.Format("Source: {0}", ex.Source)
        message += Environment.NewLine
        message += String.Format("TargetSite: {0}", ex.TargetSite.ToString())
        message += Environment.NewLine
        message += "---------------------------Failed-------------------------------------------------------"
        message += Environment.NewLine

        Try
            Dim mysplit As String() = strUser.Split(",")
            'getAgentName = strValue.Substring(strValue, 0).ToString
            'mystr += mysplit(0).Replace("EXEC SP_LOGIN_APPLIKASI_LOCAL", "")
            'mystr = replaceAgentLogin(mysplit(0))
            getUserLog = mystr

            'EXEC SP_LOGIN_APPLIKASI_LOCAL  'admin', '12345'   'admin'
            Dim DirectoryX As String = Path.Combine(Server.MapPath("~/HTML/ErrorLog/" & agentName & "/" & DateTime.Now.ToString("ddMMyyyy")))
            If Not System.IO.Directory.Exists(DirectoryX) Then
                System.IO.Directory.CreateDirectory(DirectoryX)
            End If
        Catch exX As Exception
            Return
        End Try

        'Try
        '    Dim mysplit As String() = strUser.Split(",")
        '    'getAgentName = strValue.Substring(strValue, 0).ToString
        '    'mystr += mysplit(0).Replace("EXEC SP_LOGIN_APPLIKASI_LOCAL", "")
        '    mystr = replaceAgentLogin(mysplit(0))
        '    getUserLog = mystr

        '    Dim DirectoryX As String = Path.Combine(Server.MapPath("~/HTML/ErrorLog/" & agentName & "/" & DateTime.Now.ToString("ddMMyyyy") & "/" & getUserLog))
        '    If Not System.IO.Directory.Exists(DirectoryX) Then
        '        System.IO.Directory.CreateDirectory(DirectoryX)
        '    End If
        'Catch exX As Exception
        '    ''Try catch untuk error create folder
        '    Dim pathXX As String = HttpContext.Current.Server.MapPath("~/HTML/ErrorLog/" & agentName & "/" & DateTime.Now.ToString("ddMMyyyy") & "/" & getUserLog & "/" & DateTime.Now.ToString("ddMMyyyy") & ".txt")
        '    Dim messageXX As String = String.Format("Time: {0}", DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss:fff tt"))
        '    messageXX += Environment.NewLine
        '    messageXX += "---------------------------Failed-----------------------------------------------"
        '    messageXX += Environment.NewLine
        '    messageXX += String.Format("Message: {0}", strUser)
        '    messageXX += Environment.NewLine
        '    messageXX += String.Format("Message: {0}", exX.Message)
        '    messageXX += Environment.NewLine
        '    messageXX += String.Format("StackTrace: {0}", exX.StackTrace)
        '    messageXX += Environment.NewLine
        '    messageXX += String.Format("Source: {0}", exX.Source)
        '    messageXX += Environment.NewLine
        '    messageXX += String.Format("TargetSite: {0}", exX.TargetSite.ToString())
        '    messageXX += Environment.NewLine
        '    messageXX += "---------------------------Failed------------------------------------------------"
        '    messageXX += Environment.NewLine
        '    Using writer As New StreamWriter(pathXX, True)
        '        writer.WriteLine(messageXX)
        '        writer.Close()
        '    End Using
        'Finally
        '    Dim pathX As String = HttpContext.Current.Server.MapPath("~/HTML/ErrorLog/" & agentName & "/" & DateTime.Now.ToString("ddMMyyyy") & "/" & getUserLog & "/" & DateTime.Now.ToString("ddMMyyyy") & ".txt")
        '    Using writer As New StreamWriter(pathX, True)
        '        writer.WriteLine(message)
        '        writer.Close()
        '    End Using
        'End Try

        Try
            Dim pathX As String = HttpContext.Current.Server.MapPath("~/HTML/ErrorLog/" & agentName & "/" & DateTime.Now.ToString("ddMMyyyy") & "/" & DateTime.Now.ToString("ddMMyyyy") & ".txt")
            Using writer As New StreamWriter(pathX, True)
                writer.WriteLine(message)
                writer.Close()
            End Using
        Catch exx As Exception

        End Try

    End Sub
    <WebMethod()>
    Public Function HelloWorld() As String
        Return "Hello World"
    End Function
    <WebMethod()>
    <ScriptMethod(UseHttpGet:=False, ResponseFormat:=ResponseFormat.Json)>
    Public Function insert_Sample(ByVal Name As String) As String
        Dim listTickets As List(Of resultInsert) = New List(Of resultInsert)()
        Dim constr As String = ConfigurationManager.ConnectionStrings("DefaultConnection").ConnectionString
        Dim strExec As String = String.Empty
        Try
            Using con As New SqlConnection(constr)
                Dim sqlComm As New SqlCommand()
                sqlComm.Connection = con
                sqlComm.CommandText = "insert_sample"
                sqlComm.CommandType = CommandType.StoredProcedure
                sqlComm.Parameters.AddWithValue("Name", Name)
                con.Open()
                sqlComm.ExecuteNonQuery()
                strExec = "exec insert_sample " & "'" & Name & "'"
                LogSuccess(strLogTime, strExec)
            End Using
        Catch ex As Exception
            Dim objectTickets As resultInsert = New resultInsert()
            objectTickets.Result = "False"
            objectTickets.msgSystem = ex.Message()
            listTickets.Add(objectTickets)
            strExec = "exec insert_sample " & "'" & Name & "'"
            LogError(strLogTime, ex, strExec)
        Finally
            Dim objectTickets As resultInsert = New resultInsert()
            objectTickets.Result = "True"
            objectTickets.msgSystem = "Berhasil Insert Customer "
            listTickets.Add(objectTickets)
        End Try
        Dim js As JavaScriptSerializer = New JavaScriptSerializer()
        Return js.Serialize(listTickets)
    End Function
    <WebMethod()>
    <ScriptMethod(UseHttpGet:=False, ResponseFormat:=ResponseFormat.Json)>
    Public Function Insert_TransactionTicket(ByVal TrxUsername As String, ByVal TrxCustomerID As String, ByVal TxtThreadID As String, ByVal TxtAccount As String, ByVal TrxPelapor As String, ByVal TrxPelaporEmail As String,
                                             ByVal TrxPelaporPhone As String, ByVal TrxPelaporAddress As String, ByVal TrxKejadian As String, ByVal TrxPenyebab As String, ByVal TrxPenerimaPengaduan As String, ByVal TrxStatusPelapor As String,
                                             ByVal TrxSkalaPrioritas As String, ByVal TrxJenisNasabah As String, ByVal TrxNomorRekening As String, ByVal TrxSumberInformasi As String, ByVal TrxCategory As String, ByVal TrxLevel1 As String,
                                             ByVal TrxLevel2 As String, ByVal TrxLevel3 As String, ByVal TrxComplaint As String, ByVal TrxResponse As String, ByVal TrxChannel As String, ByVal TrxStatus As String, ByVal TrxEskalasi As String,
                                             ByVal TrxSLA As String, ByVal TrxExtendCategory As String, ByVal TrxLayer As String, ByVal TrxThreadID As String, ByVal TrxGenesysID As String, ByVal TxtContactID As String,
                                             ByVal TrxAmount As String) As String
        Dim listTickets As List(Of listTransaction) = New List(Of listTransaction)()
        Dim query As String
        Dim constr As String = ConfigurationManager.ConnectionStrings("DefaultConnection").ConnectionString
        Dim strExec As String = String.Empty
        Try
            Using con As New SqlConnection(constr)
                Dim sqlComm As New SqlCommand()
                sqlComm.Connection = con
                sqlComm.CommandText = "Insert_Transaction"
                sqlComm.CommandType = CommandType.StoredProcedure
                sqlComm.Parameters.AddWithValue("Customerid", TrxCustomerID)
                sqlComm.Parameters.AddWithValue("TicketNumber", strTime)
                sqlComm.Parameters.AddWithValue("TicketSourceName", TrxChannel)
                sqlComm.Parameters.AddWithValue("CategoryID", TrxCategory)
                sqlComm.Parameters.AddWithValue("SubCategory1ID", TrxLevel1)
                sqlComm.Parameters.AddWithValue("SubCategory2ID", TrxLevel2)
                sqlComm.Parameters.AddWithValue("SubCategory3ID", TrxLevel3)
                sqlComm.Parameters.AddWithValue("DetailComplaint", HttpUtility.UrlDecode(TrxComplaint))
                sqlComm.Parameters.AddWithValue("ResponComplaint", HttpUtility.UrlDecode(TrxResponse))
                sqlComm.Parameters.AddWithValue("SLA", TrxSLA)
                sqlComm.Parameters.AddWithValue("Status", TrxStatus)
                sqlComm.Parameters.AddWithValue("UserCreate", TrxUsername)
                sqlComm.Parameters.AddWithValue("IdTabel", TxtThreadID)
                sqlComm.Parameters.AddWithValue("NamaPelapor", HttpUtility.UrlDecode(TrxPelapor))
                sqlComm.Parameters.AddWithValue("EmailPelapor", TrxPelaporEmail)
                sqlComm.Parameters.AddWithValue("PhonePelapor", TrxPelaporPhone)
                sqlComm.Parameters.AddWithValue("AlamatPelapor", HttpUtility.UrlDecode(TrxPelaporAddress))
                sqlComm.Parameters.AddWithValue("AccountInbound", TxtAccount)
                sqlComm.Parameters.AddWithValue("NomorRekening", TrxNomorRekening)
                sqlComm.Parameters.AddWithValue("SkalaPrioritas", TrxSkalaPrioritas)
                sqlComm.Parameters.AddWithValue("JenisNasabah", TrxJenisNasabah)
                sqlComm.Parameters.AddWithValue("IDLevel3", TrxLevel3)
                sqlComm.Parameters.AddWithValue("SumberInformasi", TrxSumberInformasi)
                sqlComm.Parameters.AddWithValue("TrxKejadian", TrxKejadian)
                sqlComm.Parameters.AddWithValue("TrxPenyebab", TrxPenyebab)
                sqlComm.Parameters.AddWithValue("TrxStatusPelapor", TrxStatusPelapor)
                sqlComm.Parameters.AddWithValue("TrxPenerimaPengaduan", TrxPenerimaPengaduan)
                sqlComm.Parameters.AddWithValue("TrxEscalationUnit", TrxEskalasi)
                sqlComm.Parameters.AddWithValue("TrxLayer", TrxLayer)
                sqlComm.Parameters.AddWithValue("TrxThreadID", TrxThreadID)
                sqlComm.Parameters.AddWithValue("TrxGenesysID", TrxGenesysID)
                sqlComm.Parameters.AddWithValue("TxtContactID", TxtContactID)
                sqlComm.Parameters.AddWithValue("IDExtend", TrxExtendCategory)
                sqlComm.Parameters.AddWithValue("TrxAmount", TrxAmount)
                con.Open()
                sqlComm.ExecuteNonQuery()
            End Using
        Catch ex As Exception
            'Response.Write(ex.Message)
            Dim objectTickets As listTransaction = New listTransaction()
            objectTickets.Result = "False"
            objectTickets.TrxmsgSystem = ex.Message()
            listTickets.Add(objectTickets)
            strExec = "exec Insert_Transaction " & "'" & TrxCustomerID & "'," & "'" & strTime & "'," & "'" & TrxChannel & "'," & "'" & TrxCategory & "'," & "'" & TrxLevel1 & "'," & "'" & TrxLevel2 & "'," & "'" & TrxLevel3 & "'," & "'" & HttpUtility.UrlDecode(TrxComplaint) & "'," & "'" & HttpUtility.UrlDecode(TrxResponse) & "'," & "'" & TrxSLA & "'," & "'" & TrxStatus & "'," & "'" & TrxUsername & "'," &
                      "" & "'" & TxtThreadID & "'," & "'" & TrxPelapor & "'," & "'" & TrxPelaporEmail & "'," & "'" & TrxPelaporPhone & "'," & "'" & HttpUtility.UrlDecode(TrxPelaporAddress) & "'," & "'" & TxtAccount & "'," & "'" & TrxNomorRekening & "'," & "'" & TrxSkalaPrioritas & "'," & "'" & TrxJenisNasabah & "'," & "'" & TrxLevel3 & "'," & "'" & TrxSumberInformasi & "'," &
                      "" & "'" & TrxKejadian & "'," & "'" & TrxPenyebab & "'," & "'" & TrxStatusPelapor & "'," & "'" & TrxPenerimaPengaduan & "'," & "'" & TrxEskalasi & "'," & "'" & TrxLayer & "'," & "'" & TrxThreadID & "', " & "'" & TrxGenesysID & "', " & "'" & TxtContactID & "'," & "'" & TrxExtendCategory & "'," & "'" & TrxAmount & "'"
            LogError(strLogTime, ex, strExec)
        Finally
            Dim objectTickets As listTransaction = New listTransaction()
            objectTickets.Result = "True"
            objectTickets.TrxTicketNumber = strTime
            objectTickets.TrxmsgSystem = "Transaction Has Been Save"
            listTickets.Add(objectTickets)
            strExec = "exec Insert_Transaction " & "'" & TrxCustomerID & "'," & "'" & strTime & "'," & "'" & TrxChannel & "'," & "'" & TrxCategory & "'," & "'" & TrxLevel1 & "'," & "'" & TrxLevel2 & "'," & "'" & TrxLevel3 & "'," & "'" & HttpUtility.UrlDecode(TrxComplaint) & "'," & "'" & HttpUtility.UrlDecode(TrxResponse) & "'," & "'" & TrxSLA & "'," & "'" & TrxStatus & "'," & "'" & TrxUsername & "'," &
                        "" & "'" & TxtThreadID & "'," & "'" & TrxPelapor & "'," & "'" & TrxPelaporEmail & "'," & "'" & TrxPelaporPhone & "'," & "'" & HttpUtility.UrlDecode(TrxPelaporAddress) & "'," & "'" & TxtAccount & "'," & "'" & TrxNomorRekening & "'," & "'" & TrxSkalaPrioritas & "'," & "'" & TrxJenisNasabah & "'," & "'" & TrxLevel3 & "'," & "'" & TrxSumberInformasi & "'," &
                       "" & "'" & TrxKejadian & "'," & "'" & TrxPenyebab & "'," & "'" & TrxStatusPelapor & "'," & "'" & TrxPenerimaPengaduan & "'," & "'" & TrxEskalasi & "'," & "'" & TrxLayer & "'," & "'" & TrxThreadID & "', " & "'" & TrxGenesysID & "', " & "'" & TxtContactID & "'," & "'" & TrxExtendCategory & "'," & "'" & TrxAmount & "'"
            LogSuccess(strLogTime, strExec)
        End Try
        Dim js As JavaScriptSerializer = New JavaScriptSerializer()
        Return js.Serialize(listTickets)
    End Function
    <WebMethod()>
    <ScriptMethod(UseHttpGet:=False, ResponseFormat:=ResponseFormat.Json)>
    Public Function Update_Publish_TransactionTicket(ByVal TrxUsername As String, ByVal TrxCustomerID As String, ByVal TrxGenesysID As String) As String
        Dim listTickets As List(Of resultInsert) = New List(Of resultInsert)()
        Dim strExec As String = String.Empty
        Dim constr As String = ConfigurationManager.ConnectionStrings("DefaultConnection").ConnectionString
        Try
            Using con As New SqlConnection(constr)
                Dim sqlComm As New SqlCommand()
                sqlComm.Connection = con
                sqlComm.CommandText = "Publish_Transaction"
                sqlComm.CommandType = CommandType.StoredProcedure
                sqlComm.Parameters.AddWithValue("TrxCustomerID", TrxCustomerID)
                sqlComm.Parameters.AddWithValue("TrxGenesysID", TrxGenesysID)
                sqlComm.Parameters.AddWithValue("TrxUsername", TrxUsername)
                con.Open()
                sqlComm.ExecuteNonQuery()
            End Using
        Catch ex As Exception
            Dim objectTickets As resultInsert = New resultInsert()
            objectTickets.Result = "False"
            objectTickets.msgSystem = ex.Message()
            listTickets.Add(objectTickets)
            strExec = "exec Publish_Transaction " & "'" & TrxGenesysID & "','" & TrxCustomerID & "'," & "'" & TrxUsername & "'"
            LogError(strLogTime, ex, strExec)
        Finally
            Dim objectTickets As resultInsert = New resultInsert()
            objectTickets.Result = "True"
            objectTickets.msgSystem = "Transaction Has Been Publish"
            listTickets.Add(objectTickets)
            strExec = "exec Publish_Transaction " & "'" & TrxGenesysID & "','" & TrxCustomerID & "'," & "'" & TrxUsername & "'"
            LogSuccess(strLogTime, strExec)
        End Try
        Dim js As JavaScriptSerializer = New JavaScriptSerializer()
        Return js.Serialize(listTickets)
    End Function
    <WebMethod()>
    <ScriptMethod(UseHttpGet:=False, ResponseFormat:=ResponseFormat.Json)>
    Public Function Update_TransactionTicket(ByVal TrxTicketNumber As String, ByVal TrxResponse As String, ByVal TrxStatus As String, ByVal TrxUsername As String,
                                             ByVal TrxChannel As String, ByVal TrxThreadID As String, ByVal TrxGenesysID As String, ByVal TrxEscalasiUnit As String,
                                             ByVal TrxAmount As String) As String
        Dim listTickets As List(Of resultInsert) = New List(Of resultInsert)()
        Dim strExec As String = String.Empty
        Dim constr As String = ConfigurationManager.ConnectionStrings("DefaultConnection").ConnectionString
        Try
            Using con As New SqlConnection(constr)
                Dim sqlComm As New SqlCommand()
                sqlComm.Connection = con
                sqlComm.CommandText = "Update_TransactionTicket"
                sqlComm.CommandType = CommandType.StoredProcedure
                sqlComm.Parameters.AddWithValue("TrxTicketNumber", TrxTicketNumber)
                sqlComm.Parameters.AddWithValue("TrxResponse", HttpUtility.UrlDecode(TrxResponse))
                sqlComm.Parameters.AddWithValue("TrxStatus", TrxStatus)
                sqlComm.Parameters.AddWithValue("TrxChannel", TrxChannel)
                sqlComm.Parameters.AddWithValue("TrxThreadID", TrxThreadID)
                sqlComm.Parameters.AddWithValue("TrxUsername", TrxUsername)
                sqlComm.Parameters.AddWithValue("TrxGenesysID", TrxGenesysID)
                sqlComm.Parameters.AddWithValue("TrxEscalasiUnit", TrxEscalasiUnit)
                sqlComm.Parameters.AddWithValue("TrxAmount", TrxAmount)
                con.Open()
                sqlComm.ExecuteNonQuery()
            End Using
        Catch ex As Exception
            Dim objectTickets As resultInsert = New resultInsert()
            objectTickets.Result = "False"
            objectTickets.msgSystem = ex.Message()
            listTickets.Add(objectTickets)
            strExec = "exec Update_TransactionTicket " & "'" & TrxTicketNumber & "'," & "'" & HttpUtility.UrlDecode(TrxResponse) & "'," & "'" & TrxStatus & "'," & "'" & TrxUsername & "', " & "'" & TrxEscalasiUnit & "'," & "'" & TrxGenesysID & "', " & "'" & TrxChannel & "', " & "'" & TrxThreadID & "', " & "'" & TrxAmount & "'"
            LogError(strLogTime, ex, strExec)
        Finally
            Dim objectTickets As resultInsert = New resultInsert()
            objectTickets.Result = "True"
            objectTickets.msgSystem = "Transaction Has Been Update"
            listTickets.Add(objectTickets)
            strExec = "exec Update_TransactionTicket " & "'" & TrxTicketNumber & "'," & "'" & HttpUtility.UrlDecode(TrxResponse) & "'," & "'" & TrxStatus & "'," & "'" & TrxUsername & "', " & "'" & TrxEscalasiUnit & "'," & "'" & TrxGenesysID & "', " & "'" & TrxChannel & "', " & "'" & TrxThreadID & "', " & "'" & TrxAmount & "'"
            LogSuccess(strLogTime, strExec)
            ''End
        End Try
        Dim js As JavaScriptSerializer = New JavaScriptSerializer()
        Return js.Serialize(listTickets)
    End Function
    <WebMethod()>
    <ScriptMethod(UseHttpGet:=False, ResponseFormat:=ResponseFormat.Json)>
    Public Function Select_Data_TransactionTicket(ByVal filterData As String) As String
        Dim listTickets As List(Of listTransaction) = New List(Of listTransaction)()
        Dim cs As String = ConfigurationManager.ConnectionStrings("DefaultConnection").ConnectionString
        Dim strQuery As String = String.Empty
        Try
            strQuery = "Exec SP_SelectDataTransaction '" & filterData & "'"
            Using con As SqlConnection = New SqlConnection(cs)
                Dim cmd As SqlCommand
                cmd = New SqlCommand(strQuery, con)
                cmd.CommandType = CommandType.Text
                con.Open()
                Dim rdr As SqlDataReader = cmd.ExecuteReader()

                While rdr.Read()
                    Dim objectTickets As listTransaction = New listTransaction()
                    objectTickets.Result = "True"
                    objectTickets.TrxID = rdr("ID").ToString()
                    objectTickets.TrxCustomerid = rdr("NIK").ToString()
                    objectTickets.TrxTicketNumber = rdr("TicketNumber").ToString()
                    objectTickets.TrxTicketSourceName = rdr("TicketSourceName").ToString()
                    objectTickets.TrxCategoryID = rdr("CategoryID").ToString()
                    objectTickets.TrxCategoryName = rdr("CategoryName").ToString()
                    objectTickets.TrxLevel1ID = rdr("SubCategory1ID").ToString()
                    objectTickets.TrxLevel1Name = rdr("SubCategory1Name").ToString()
                    objectTickets.TrxLevel2ID = rdr("SubCategory2ID").ToString()
                    objectTickets.TrxLevel2Name = rdr("SubCategory2Name").ToString()
                    objectTickets.TrxLevel3ID = rdr("SubCategory3ID").ToString()
                    objectTickets.TrxLevel3Name = rdr("SubCategory3Name").ToString()
                    objectTickets.TrxDetailComplaint = rdr("DetailComplaint").ToString()
                    objectTickets.TrxResponComplaint = rdr("ResponComplaint").ToString()
                    objectTickets.TrxDivisi = rdr("Divisi").ToString()
                    objectTickets.TrxSLA = rdr("SLA").ToString()
                    objectTickets.TrxStatus = rdr("Status").ToString()
                    objectTickets.TrxUserCreate = rdr("UserCreate").ToString()
                    objectTickets.TxtThreadID = rdr("IdTabel").ToString()
                    objectTickets.TrxInteractionAccount = rdr("AccountInbound").ToString()
                    objectTickets.TrxNamaPelapor = rdr("NAMA_PELAPOR").ToString()
                    objectTickets.TrxEmailPelapor = rdr("EMAIL_PELAPOR").ToString()
                    objectTickets.TrxPhonePelapor = rdr("PHONE_PELAPOR").ToString()
                    objectTickets.TrxAlamatPelapor = rdr("ALAMAT_PELAPOR").ToString()
                    objectTickets.TrxNomorRekening = rdr("NomorRekening").ToString()
                    objectTickets.TrxSumberInformasi = rdr("SumberInformasi").ToString()
                    objectTickets.TrxEskalasiUnit = rdr("ORGANIZATION_NAME").ToString()
                    objectTickets.TrxSkalaPrioritas = rdr("SkalaPrioritas").ToString()
                    objectTickets.TrxJenisNasabah = rdr("JenisNasabah").ToString()
                    objectTickets.TrxStatusPelapor = rdr("StrStatusPelapor").ToString()
                    objectTickets.TrxKejadian = rdr("TglKejadian").ToString()
                    objectTickets.TrxPenyebab = rdr("StrPenyebab").ToString()
                    objectTickets.TrxPenerimaPengaduan = rdr("StrPenerima").ToString()
                    objectTickets.TrxThreadID = rdr("ThreadID").ToString()
                    objectTickets.TrxGenesysID = rdr("GenesysID").ToString()
                    objectTickets.TrxTicketPosition = rdr("TicketPosition").ToString()
                    objectTickets.TrxTicketPosition = rdr("TicketPosition").ToString()
                    objectTickets.TrxExtendID = rdr("ExtendID").ToString()
                    objectTickets.TrxExtendSLA = rdr("ExtendSLA").ToString()
                    objectTickets.TrxExtendName = rdr("ExtendName").ToString()
                    objectTickets.TrxReleaseUser = rdr("ReleaseUser").ToString()
                    objectTickets.TrxTahun = rdr("tahun").ToString()
                    objectTickets.TrxBulan = rdr("bulan").ToString()
                    objectTickets.TrxHari = rdr("hari").ToString()
                    objectTickets.TrxAmount = rdr("Amount").ToString()
                    objectTickets.TrxAmountDecimal = rdr("AmountDecimal").ToString()

                    listTickets.Add(objectTickets)
                End While
            End Using
        Catch ex As Exception
            LogError(strLogTime, ex, strQuery)
        Finally
            LogSuccess(strLogTime, strQuery)
        End Try

        Dim js As JavaScriptSerializer = New JavaScriptSerializer()
        Return js.Serialize(listTickets)
    End Function
    <WebMethod()>
    <ScriptMethod(UseHttpGet:=False, ResponseFormat:=ResponseFormat.Json)>
    Public Function GetWhereRecords(ByVal tableType As String, ByVal tableName As String, ByVal paramQuery As String) As String
        Dim connstring As String = ConfigurationManager.ConnectionStrings("DefaultConnection").ConnectionString
        Dim dt As DataTable = New DataTable()
        Dim sql As String = ""
        Using conn As SqlConnection = New SqlConnection(connstring)
            conn.Open()
            If tableType = "AllWhereData" Then

                sql = "select * from [" & tableName & "] " & paramQuery & " "
            End If
            Dim ad As SqlDataAdapter = New SqlDataAdapter(sql, conn)
            Dim ds As DataSet = New DataSet()
            ad.Fill(ds)
            dt = ds.Tables(0)
            conn.Close()
        End Using
        Dim tableJson As String = ConvertDataTabletoString(dt)
        Return tableJson
    End Function
    <WebMethod()>
    <ScriptMethod(UseHttpGet:=False, ResponseFormat:=ResponseFormat.Json)>
    Public Function GetExtendCategory(ByVal paramQuery As String) As String
        Dim connstring As String = ConfigurationManager.ConnectionStrings("DefaultConnection").ConnectionString
        Dim dt As DataTable = New DataTable()
        Dim sql As String = ""

        Using conn As SqlConnection = New SqlConnection(connstring)
            conn.Open()
            sql = "select * from Temp_ExtendSubCategory where CategoryID='" & paramQuery & "'"
            Dim ad As SqlDataAdapter = New SqlDataAdapter(sql, conn)
            Dim ds As DataSet = New DataSet()
            ad.Fill(ds)
            dt = ds.Tables(0)
            conn.Close()
        End Using

        Dim tableJson As String = ConvertDataTabletoString(dt)
        Return tableJson
    End Function
    <WebMethod()>
    <ScriptMethod(UseHttpGet:=False, ResponseFormat:=ResponseFormat.Json)>
    Public Function SelectKategori(ByVal filterData As String) As String
        Dim listTickets As List(Of listKategori) = New List(Of listKategori)()
        Dim cs As String = ConfigurationManager.ConnectionStrings("DefaultConnection").ConnectionString
        Dim strQuery As String = String.Empty
        Try
            strQuery = "Exec SP_SelectMasterCategory"
            Using con As SqlConnection = New SqlConnection(cs)
                Dim cmd As SqlCommand
                cmd = New SqlCommand(strQuery, con)
                cmd.CommandType = CommandType.Text
                con.Open()
                Dim rdr As SqlDataReader = cmd.ExecuteReader()

                While rdr.Read()
                    Dim objectTickets As listKategori = New listKategori()
                    objectTickets.Result = "True"
                    objectTickets.CategoriID = rdr("CategoryID").ToString()
                    objectTickets.CategoriName = rdr("Name").ToString()
                    listTickets.Add(objectTickets)
                End While

            End Using
        Catch ex As Exception
            LogError(strLogTime, ex, strQuery)
        Finally
            LogSuccess(strLogTime, strQuery)
        End Try

        Dim js As JavaScriptSerializer = New JavaScriptSerializer()
        Return js.Serialize(listTickets)
    End Function
    <WebMethod()>
    <ScriptMethod(UseHttpGet:=False, ResponseFormat:=ResponseFormat.Json)>
    Public Function SelectKategoriPermasalahan(ByVal filterData As String) As String
        Dim listTickets As List(Of listKategori) = New List(Of listKategori)()
        Dim cs As String = ConfigurationManager.ConnectionStrings("DefaultConnection").ConnectionString
        Dim strQuery As String = String.Empty
        Try
            strQuery = "Exec SP_SelectReasonCode '" & filterData & "'"
            Using con As SqlConnection = New SqlConnection(cs)
                Dim cmd As SqlCommand
                cmd = New SqlCommand(strQuery, con)
                cmd.CommandType = CommandType.Text
                con.Open()
                Dim rdr As SqlDataReader = cmd.ExecuteReader()

                While rdr.Read()
                    Dim objectTickets As listKategori = New listKategori()
                    objectTickets.Result = "True"
                    objectTickets.CategoriID = rdr("CategoryID").ToString()
                    objectTickets.CategoriName = rdr("Name").ToString()
                    objectTickets.CategoriLevel1ID = rdr("SubCategory1ID").ToString()
                    objectTickets.CategoriLevel1Name = rdr("NameLevel1").ToString()
                    objectTickets.CategoriLevel2ID = rdr("SubCategory2ID").ToString()
                    objectTickets.CategoriLevel2Name = rdr("NameLevel2").ToString()
                    objectTickets.CategoriLevel3ID = rdr("SubCategory3ID").ToString()
                    objectTickets.CategoriLevel3Name = rdr("SubName").ToString()
                    listTickets.Add(objectTickets)
                End While

            End Using
        Catch ex As Exception
            LogError(strLogTime, ex, strQuery)
        Finally
            LogSuccess(strLogTime, strQuery)
        End Try

        Dim js As JavaScriptSerializer = New JavaScriptSerializer()
        Return js.Serialize(listTickets)
    End Function
    <WebMethod()>
    <ScriptMethod(UseHttpGet:=False, ResponseFormat:=ResponseFormat.Json)>
    Public Function SelectUserLogin(ByVal filterData As String) As String
        Dim listTickets As List(Of listUser) = New List(Of listUser)()
        Dim cs As String = ConfigurationManager.ConnectionStrings("DefaultConnection").ConnectionString
        Dim strQuery As String = String.Empty
        Dim LevelUser As String = String.Empty
        If filterData = "1" Then
            LevelUser = "Layer 1"
        ElseIf filterData = "2" Then
            LevelUser = "Layer 2"
        ElseIf filterData = "3" Then
            LevelUser = "Layer 3"
        ElseIf filterData = "4" Then
            LevelUser = "Supervisor"
        End If

        If LevelUser = "Layer 1" Then
            If CountingAgentLogin(LevelUser) = True Then
                Try
                    strQuery = "select * from msUser where LevelUser='" & LevelUser & "' and login='1'"
                    Using con As SqlConnection = New SqlConnection(cs)
                        Dim cmd As SqlCommand
                        cmd = New SqlCommand(strQuery, con)
                        cmd.CommandType = CommandType.Text
                        con.Open()
                        Dim rdr As SqlDataReader = cmd.ExecuteReader()

                        While rdr.Read
                            Dim objectTickets As listUser = New listUser()
                            objectTickets.Result = "True"
                            objectTickets.UserName = rdr("UserName").ToString()
                            listTickets.Add(objectTickets)
                        End While

                    End Using
                Catch ex As Exception
                    LogError(strLogTime, ex, strQuery)
                    Dim objectTickets As listUser = New listUser()
                    objectTickets.Result = "False"
                    objectTickets.UserName = "Data user login is empty."
                    listTickets.Add(objectTickets)
                Finally
                    LogSuccess(strLogTime, strQuery)
                End Try
            Else
                Dim objectTickets As listUser = New listUser()
                objectTickets.Result = "False"
                listTickets.Add(objectTickets)
                LogSuccess(strLogTime, "Data user login is empty.")
            End If
        ElseIf LevelUser = "Layer 3" Then
            Try
                strQuery = "SELECT ORGANIZATION_NAME FROM MORGANIZATION WHERE ORGANIZATION_NAME <> 'All'"
                Using con As SqlConnection = New SqlConnection(cs)
                    Dim cmd As SqlCommand
                    cmd = New SqlCommand(strQuery, con)
                    cmd.CommandType = CommandType.Text
                    con.Open()
                    Dim rdr As SqlDataReader = cmd.ExecuteReader()

                    While rdr.Read
                        Dim objectTickets As listUser = New listUser()
                        objectTickets.Result = "True"
                        objectTickets.UserName = rdr("ORGANIZATION_NAME").ToString()
                        listTickets.Add(objectTickets)
                    End While

                End Using
            Catch ex As Exception
                LogError(strLogTime, ex, strQuery)
                Dim objectTickets As listUser = New listUser()
                objectTickets.Result = "False"
                objectTickets.UserName = "Data Layer 3 is empty."
                listTickets.Add(objectTickets)
            Finally
                LogSuccess(strLogTime, strQuery)
            End Try
        Else
            Dim objectTickets As listUser = New listUser()
            objectTickets.Result = "True"
            listTickets.Add(objectTickets)
        End If
        Dim js As JavaScriptSerializer = New JavaScriptSerializer()
        Return js.Serialize(listTickets)
    End Function
    <WebMethod()>
    <ScriptMethod(UseHttpGet:=False, ResponseFormat:=ResponseFormat.Json)>
    Public Function ReleaseTransaction(ByVal TrxTicketNumber As String, ByVal ReleaseFromLayer As String, ByVal ReleaseToLayer As String, ByVal ReleaseUser As String, ByVal ReleaseReason As String, ByVal TrxUserName As String, ByVal ReleaseKategori As String) As String
        Dim listTickets As List(Of resultInsert) = New List(Of resultInsert)()
        Dim strExec As String = String.Empty
        Dim constr As String = ConfigurationManager.ConnectionStrings("DefaultConnection").ConnectionString
        'Kondisi Looping Value Ticket Number
        'Dim _TrxValue As String = String.Empty
        'Dim _strValue As String = String.Empty
        'Dim _strArray() As String
        '_TrxValue = TrxTicketNumber
        '_strArray = _TrxValue.Split(",")
        Try
            'For i = 0 To _strArray.Count - 1
            Using con As New SqlConnection(constr)
                Dim sqlComm As New SqlCommand()
                sqlComm.Connection = con
                sqlComm.CommandText = "Tr_Release_Transaction"
                sqlComm.CommandType = CommandType.StoredProcedure
                sqlComm.Parameters.AddWithValue("TrxTicketNumber", TrxTicketNumber)
                sqlComm.Parameters.AddWithValue("ReleaseType", "Release")
                sqlComm.Parameters.AddWithValue("ReleaseUser", ReleaseUser)
                sqlComm.Parameters.AddWithValue("ReleaseFromLayer", ReleaseFromLayer)
                sqlComm.Parameters.AddWithValue("ReleaseToLayer", ReleaseToLayer)
                sqlComm.Parameters.AddWithValue("ReleaseReason", ReleaseReason)
                sqlComm.Parameters.AddWithValue("ReleaseBy", TrxUserName)
                sqlComm.Parameters.AddWithValue("ReleaseKategori", ReleaseKategori)
                con.Open()
                sqlComm.ExecuteNonQuery()
            End Using
            'Next
        Catch ex As Exception
            Dim objectTickets As resultInsert = New resultInsert()
            objectTickets.Result = "False"
            objectTickets.msgSystem = ex.Message()
            listTickets.Add(objectTickets)
            strExec = "exec Tr_Release_Transaction " & "'" & TrxTicketNumber & "'," & "'" & ReleaseUser & "'," & "'" & ReleaseFromLayer & "'," & "'" & ReleaseToLayer & "', " & "'" & ReleaseReason & "', " & "'" & TrxUserName & "', " & "'" & ReleaseKategori & "'"
            LogError(strLogTime, ex, strExec)
        Finally
            Dim objectTickets As resultInsert = New resultInsert()
            objectTickets.Result = "True"
            objectTickets.msgSystem = "Data Transaction Has Been Release"
            listTickets.Add(objectTickets)
            strExec = "exec Tr_Release_Transaction " & "'" & TrxTicketNumber & "'," & "'" & ReleaseUser & "'," & "'" & ReleaseFromLayer & "'," & "'" & ReleaseToLayer & "', " & "'" & ReleaseReason & "', " & "'" & TrxUserName & "', " & "'" & ReleaseKategori & "'"
            LogSuccess(strLogTime, strExec)
            ''End
        End Try
        Dim js As JavaScriptSerializer = New JavaScriptSerializer()
        Return js.Serialize(listTickets)
    End Function
    <WebMethod()>
    <ScriptMethod(UseHttpGet:=False, ResponseFormat:=ResponseFormat.Json)>
    Public Function InsertDataCustomer(ByVal TrxCustomerID As String, ByVal TrxName As String, ByVal TrxEmail As String, ByVal TrxPhone As String, ByVal TrxGender As String, ByVal TrxCIF As String, ByVal TrxNIK As String, ByVal TrxBirth As String, ByVal TrxAddress As String, ByVal TrxUserName As String, ByVal TrxMenu As String, ByVal TrxGenesysID As String) As String
        Dim listTickets As List(Of resultInsert) = New List(Of resultInsert)()
        Dim strExec As String = String.Empty
        Dim _CustomerID As String = String.Empty
        Dim _Email As String = String.Empty
        Dim _Phone As String = String.Empty
        Dim _CIF As String = String.Empty
        Dim _NIK As String = String.Empty
        Dim _Result As String = String.Empty
        Dim constr As String = ConfigurationManager.ConnectionStrings("DefaultConnection").ConnectionString
        Try
            Using con As New SqlConnection(constr)
                Dim sqlComm As New SqlCommand()
                sqlComm.Connection = con
                sqlComm.CommandText = "Tr_Customer"
                sqlComm.CommandType = CommandType.StoredProcedure
                sqlComm.Parameters.AddWithValue("TrxCustomerID", TrxCustomerID)
                sqlComm.Parameters.AddWithValue("TrxName", HttpUtility.UrlDecode(TrxName))
                sqlComm.Parameters.AddWithValue("TrxEmail", TrxEmail)
                sqlComm.Parameters.AddWithValue("TrxPhone", TrxPhone)
                sqlComm.Parameters.AddWithValue("TrxGender", TrxGender)
                sqlComm.Parameters.AddWithValue("TrxBirth", TrxBirth)
                sqlComm.Parameters.AddWithValue("TrxCIF", TrxCIF)
                sqlComm.Parameters.AddWithValue("TrxNIK", TrxNIK)
                sqlComm.Parameters.AddWithValue("TrxAddress", HttpUtility.UrlDecode(TrxAddress))
                sqlComm.Parameters.AddWithValue("TrxUserName", TrxUserName)
                sqlComm.Parameters.AddWithValue("TrxMenu", TrxMenu)
                sqlComm.Parameters.AddWithValue("TrxGenesysID", TrxGenesysID)
                con.Open()
                sqldr = sqlComm.ExecuteReader()
                While sqldr.Read()
                    _Result &= sqldr("ResultNya").ToString
                    _CustomerID &= sqldr("CustomerID").ToString
                End While
                sqldr.Close()
                con.Close()
            End Using
        Catch ex As Exception
            Dim objectTickets As resultInsert = New resultInsert()
            objectTickets.Result = "False"
            objectTickets.msgSystem = ex.Message()
            listTickets.Add(objectTickets)
            strExec = "exec Tr_Customer " & "'" & TrxCustomerID & "','" & HttpUtility.UrlDecode(TrxName) & "'," & "'" & TrxEmail & "'," & "'" & TrxPhone & "'," & "'" & TrxGender & "', " & "'" & TrxBirth & "'," & "'" & TrxCIF & "', " & "'" & TrxNIK & "', " & "'" & HttpUtility.UrlDecode(TrxAddress) & "',  " & "'" & TrxUserName & "', " & "'" & TrxMenu & "', " & "'" & TrxGenesysID & "'"
            LogError(strLogTime, ex, strExec)
        Finally
            Dim objectTickets As resultInsert = New resultInsert()
            If _Result = "InsertSuccess" Then
                objectTickets.Result = "True"
                objectTickets.CustomerID = _CustomerID
                objectTickets.msgSystem = "Data has been save"
                listTickets.Add(objectTickets)
                strExec = "exec Tr_Customer - " & "'" & TrxCustomerID & "','" & HttpUtility.UrlDecode(TrxName) & "'," & "'" & TrxEmail & "'," & "'" & TrxPhone & "'," & "'" & TrxGender & "', " & "'" & TrxBirth & "'," & "'" & TrxCIF & "', " & "'" & TrxNIK & "'," & "'" & HttpUtility.UrlDecode(TrxAddress) & "',  " & "'" & TrxUserName & "', " & "'" & TrxMenu & "', " & "'" & TrxGenesysID & "'"
                LogSuccess(strLogTime, strExec)
            Else
                objectTickets.Result = "False"
                objectTickets.CustomerID = _CustomerID
                objectTickets.msgSystem = _Result
                listTickets.Add(objectTickets)
                strExec = "exec Tr_Customer - " & "'" & TrxCustomerID & "','" & HttpUtility.UrlDecode(TrxName) & "'," & "'" & TrxEmail & "'," & "'" & TrxPhone & "'," & "'" & TrxGender & "', " & "'" & TrxBirth & "'," & "'" & TrxCIF & "', " & "'" & TrxNIK & "', " & "'" & HttpUtility.UrlDecode(TrxAddress) & "',  " & "'" & TrxUserName & "', " & "'" & TrxMenu & "', " & "'" & TrxGenesysID & "'"
                LogSuccess(strLogTime, strExec)
                LogSuccess(strLogTime, _Result)
            End If
        End Try
        Dim js As JavaScriptSerializer = New JavaScriptSerializer()
        Return js.Serialize(listTickets)
    End Function
    <WebMethod()>
    <ScriptMethod(UseHttpGet:=False, ResponseFormat:=ResponseFormat.Json)>
    Public Function SelectDataCustomer(ByVal TrxCustomerID As String) As String
        Dim listTickets As List(Of listCustomer) = New List(Of listCustomer)()
        Dim cs As String = ConfigurationManager.ConnectionStrings("DefaultConnection").ConnectionString
        Dim strQuery As String = String.Empty
        Dim LevelUser As String = String.Empty
        Try
            strQuery = "Exec SP_SelectDataCustomer '" & TrxCustomerID & "'"
            Using con As SqlConnection = New SqlConnection(cs)
                Dim cmd As SqlCommand
                cmd = New SqlCommand(strQuery, con)
                cmd.CommandType = CommandType.Text
                con.Open()
                Dim rdr As SqlDataReader = cmd.ExecuteReader()

                While rdr.Read()
                    Dim objectTickets As listCustomer = New listCustomer()
                    objectTickets.Result = "True"
                    objectTickets.CustomerID = rdr("CustomerID").ToString()
                    objectTickets.CustomerName = rdr("Name").ToString()
                    objectTickets.CustomerEmail = rdr("Email").ToString()
                    objectTickets.CustomerHP = rdr("PhoneNumber").ToString()
                    objectTickets.CustomerGender = rdr("JenisKelamin").ToString()
                    objectTickets.CustomerBirth = rdr("Birth").ToString()
                    objectTickets.CustomerCIF = rdr("CIF").ToString()
                    objectTickets.CustomerNIK = rdr("NIK").ToString()
                    objectTickets.CustomerAddress = rdr("Alamat").ToString()
                    objectTickets.CustomerTahunlahir = rdr("tahun").ToString()
                    objectTickets.CustomerBulanlahir = rdr("bulan").ToString()
                    objectTickets.CustomerHarilahir = rdr("hari").ToString()
                    listTickets.Add(objectTickets)
                End While

            End Using
        Catch ex As Exception
            LogError(strLogTime, ex, strQuery)
        Finally
            LogSuccess(strLogTime, strQuery)
        End Try

        Dim js As JavaScriptSerializer = New JavaScriptSerializer()
        Return js.Serialize(listTickets)
    End Function
    <WebMethod()>
    <ScriptMethod(UseHttpGet:=False, ResponseFormat:=ResponseFormat.Json)>
    Public Function UpdateDataCustomer(ByVal TrxCustomerID As String, ByVal TrxName As String, ByVal TrxEmail As String, ByVal TrxPhone As String, ByVal TrxGender As String, ByVal TrxBirth As String, ByVal TrxCIF As String, ByVal TrxNIK As String, ByVal TrxAddress As String, ByVal TrxUserName As String) As String
        Dim listTickets As List(Of resultInsert) = New List(Of resultInsert)()
        Dim strExec As String = String.Empty
        Dim constr As String = ConfigurationManager.ConnectionStrings("DefaultConnection").ConnectionString
        Dim _Result As String = String.Empty
        Try
            Using con As New SqlConnection(constr)
                Dim sqlComm As New SqlCommand()
                sqlComm.Connection = con
                sqlComm.CommandTimeout = 3600
                sqlComm.CommandText = "Tr_Customer_Update"
                sqlComm.CommandType = CommandType.StoredProcedure
                sqlComm.Parameters.AddWithValue("TrxName", HttpUtility.UrlDecode(TrxName))
                sqlComm.Parameters.AddWithValue("TrxEmail", TrxEmail)
                sqlComm.Parameters.AddWithValue("TrxPhone", TrxPhone)
                sqlComm.Parameters.AddWithValue("TrxGender", TrxGender)
                sqlComm.Parameters.AddWithValue("TrxBirth", TrxBirth)
                sqlComm.Parameters.AddWithValue("TrxCIF", TrxCIF)
                sqlComm.Parameters.AddWithValue("TrxNIK", TrxNIK)
                sqlComm.Parameters.AddWithValue("TrxAddress", HttpUtility.UrlDecode(TrxAddress))
                sqlComm.Parameters.AddWithValue("TrxUserName", TrxUserName)
                sqlComm.Parameters.AddWithValue("TrxCustomerID", TrxCustomerID)
                con.Open()
                sqldr = sqlComm.ExecuteReader()
                While sqldr.Read()
                    _Result &= sqldr("ResultNya").ToString
                End While
                sqldr.Close()
                con.Close()
            End Using
        Catch ex As Exception
            Dim objectTickets As resultInsert = New resultInsert()
            objectTickets.Result = "False"
            objectTickets.msgSystem = ex.Message()
            listTickets.Add(objectTickets)
            strExec = "exec Tr_Customer_Update " & "'" & HttpUtility.UrlDecode(TrxName) & "'," & "'" & TrxEmail & "'," & "'" & TrxPhone & "'," & "'" & TrxGender & "', " & "'" & TrxBirth & "',  " & "'" & TrxCIF & "',  " & "'" & TrxNIK & "', " & "'" & HttpUtility.UrlDecode(TrxAddress) & "',  " & "'" & TrxUserName & "', " & "'" & TrxCustomerID & "'"
            LogError(strLogTime, ex, strExec)
        Finally
            Dim objectTickets As resultInsert = New resultInsert()
            If _Result = "UpdateSuccess" Then
                objectTickets.Result = "True"
                objectTickets.msgSystem = "Data has been update"
                listTickets.Add(objectTickets)
                strExec = "exec Tr_Customer_Update " & "'" & HttpUtility.UrlDecode(TrxName) & "'," & "'" & TrxEmail & "'," & "'" & TrxPhone & "'," & "'" & TrxGender & "', " & "'" & TrxBirth & "', " & "'" & TrxCIF & "',  " & "'" & TrxNIK & "', " & "'" & HttpUtility.UrlDecode(TrxAddress) & "',  " & "'" & TrxUserName & "', " & "'" & TrxCustomerID & "'"
                LogSuccess(strLogTime, strExec)
            Else
                objectTickets.Result = "False"
                objectTickets.msgSystem = _Result
                listTickets.Add(objectTickets)
                strExec = "exec Tr_Customer_Update " & "'" & HttpUtility.UrlDecode(TrxName) & "'," & "'" & TrxEmail & "'," & "'" & TrxPhone & "'," & "'" & TrxGender & "', " & "'" & TrxBirth & "', " & "'" & TrxCIF & "',  " & "'" & TrxNIK & "', " & "'" & HttpUtility.UrlDecode(TrxAddress) & "',  " & "'" & TrxUserName & "', " & "'" & TrxCustomerID & "'"
                LogSuccess(strLogTime, strExec)
                LogSuccess(strLogTime, _Result)
            End If
            ''End
        End Try
        Dim js As JavaScriptSerializer = New JavaScriptSerializer()
        Return js.Serialize(listTickets)
    End Function
    <WebMethod()>
    <ScriptMethod(UseHttpGet:=False, ResponseFormat:=ResponseFormat.Json)>
    Public Function DeleteDataCustomer(ByVal TrxCustomerID As String, ByVal TrxUserName As String) As String
        Dim listTickets As List(Of resultInsert) = New List(Of resultInsert)()
        Dim strExec As String = String.Empty
        Dim constr As String = ConfigurationManager.ConnectionStrings("DefaultConnection").ConnectionString
        Try
            Using con As New SqlConnection(constr)
                Dim sqlComm As New SqlCommand()
                sqlComm.Connection = con
                sqlComm.CommandText = "Tr_Customer_Delete"
                sqlComm.CommandType = CommandType.StoredProcedure
                sqlComm.Parameters.AddWithValue("TrxCustomerID", TrxCustomerID)
                con.Open()
                sqlComm.ExecuteNonQuery()
            End Using
        Catch ex As Exception
            Dim objectTickets As resultInsert = New resultInsert()
            objectTickets.Result = "False"
            objectTickets.msgSystem = ex.Message()
            listTickets.Add(objectTickets)
            strExec = "exec Tr_Customer_Delete " & "'" & TrxCustomerID & "', " & "'" & TrxUserName & "'"
            LogError(strLogTime, ex, strExec)
        Finally
            Dim objectTickets As resultInsert = New resultInsert()
            objectTickets.Result = "True"
            objectTickets.msgSystem = "Data Customer Has Been Delete"
            listTickets.Add(objectTickets)
            strExec = "exec Tr_Customer_Delete " & "'" & TrxCustomerID & "', " & "'" & TrxUserName & "'"
            LogSuccess(strLogTime, strExec)
            ''End
        End Try
        Dim js As JavaScriptSerializer = New JavaScriptSerializer()
        Return js.Serialize(listTickets)
    End Function
    <WebMethod()>
    <ScriptMethod(UseHttpGet:=False, ResponseFormat:=ResponseFormat.Json)>
    Public Function CountingDataCustomerChannel(ByVal TrxValue As String) As String
        Dim listTickets As List(Of listCounting) = New List(Of listCounting)()
        Dim strCounting As Integer
        Dim cs As String = ConfigurationManager.ConnectionStrings("DefaultConnection").ConnectionString
        Using con As SqlConnection = New SqlConnection(cs)
            Dim cmd As SqlCommand
            cmd = New SqlCommand("select count(*) as ResultValue from mCustomerChannel where ValueChannel='" & TrxValue & "'", con)
            cmd.CommandType = CommandType.Text
            con.Open()
            Dim rdr As SqlDataReader = cmd.ExecuteReader()
            If rdr.Read() Then
                strCounting = rdr("ResultValue").ToString()
            Else
            End If
        End Using
        If strCounting > 0 Then
            Dim objectTickets As listCounting = New listCounting()
            objectTickets.Result = "True"
            listTickets.Add(objectTickets)
        Else
            Dim objectTickets As listCounting = New listCounting()
            objectTickets.Result = "False"
            listTickets.Add(objectTickets)
        End If

        'Return String.Format(Name & ";" & State)
        'Return String.Format("Name: {0}{2} State: {1}{2}", Name, State)
        Dim js As JavaScriptSerializer = New JavaScriptSerializer()
        Return js.Serialize(listTickets)
    End Function
    <WebMethod()>
    <ScriptMethod(UseHttpGet:=False, ResponseFormat:=ResponseFormat.Json)>
    Public Function FunctionDataSetting(ByVal TrxType As String) As String
        Dim listTickets As List(Of listCounting) = New List(Of listCounting)()
        Dim cs As String = ConfigurationManager.ConnectionStrings("DefaultConnection").ConnectionString
        Dim strQuery As String = String.Empty
        Dim strValue As String = String.Empty
        Try
            strQuery = "select * from TR_Data_Setting where Type='" & TrxType & "'"
            Using con As SqlConnection = New SqlConnection(cs)

                Dim cmd As SqlCommand
                cmd = New SqlCommand(strQuery, con)
                cmd.CommandType = CommandType.Text
                con.Open()
                Dim rdr As SqlDataReader = cmd.ExecuteReader()

                If rdr.Read() Then
                    strValue = rdr("Value").ToString()
                End If

            End Using
        Catch ex As Exception
            LogError(strLogTime, ex, strQuery)
        Finally
            LogSuccess(strLogTime, strQuery)
        End Try
        Return strValue
    End Function
    <WebMethod()>
    <ScriptMethod(UseHttpGet:=False, ResponseFormat:=ResponseFormat.Json)>
    Public Function GetExtendCategorySLA(ByVal filterData As String) As String
        Dim listTickets As List(Of listExtendSLA) = New List(Of listExtendSLA)()
        Dim cs As String = ConfigurationManager.ConnectionStrings("DefaultConnection").ConnectionString
        Dim strQuery As String = String.Empty
        Dim LevelUser As String = String.Empty
        Try
            strQuery = "select * from Temp_ExtendSubCategory where ID='" & filterData & "'"
            Using con As SqlConnection = New SqlConnection(cs)
                Dim cmd As SqlCommand
                cmd = New SqlCommand(strQuery, con)
                cmd.CommandType = CommandType.Text
                con.Open()
                Dim rdr As SqlDataReader = cmd.ExecuteReader()

                While rdr.Read()
                    Dim objectTickets As listExtendSLA = New listExtendSLA()
                    objectTickets.Result = "True"
                    objectTickets.TrxExtendSLA = rdr("SLA").ToString()
                    objectTickets.TrxNameExtend = rdr("NameExtend").ToString()
                    listTickets.Add(objectTickets)
                End While

            End Using
        Catch ex As Exception
            LogError(strLogTime, ex, strQuery)
        Finally
            LogSuccess(strLogTime, strQuery)
        End Try

        Dim js As JavaScriptSerializer = New JavaScriptSerializer()
        Return js.Serialize(listTickets)
    End Function
    Public Function CheckNotifikasiEmail(ByVal Type As String)
        Dim strTR_TypeNotifikasiEmail As String = String.Empty
        strTR_TypeNotifikasiEmail = "select State from TR_TypeNotifikasiEmail where Type='" & Type & "'"
        sqldr = Proses.ExecuteReader(strTR_TypeNotifikasiEmail)
        Try
            If sqldr.HasRows() Then
                sqldr.Read()
                If sqldr("State").ToString = "Y" Then
                    LogSuccess(strLogTime, strTR_TypeNotifikasiEmail)
                    Return True
                Else
                    LogSuccess(strLogTime, strTR_TypeNotifikasiEmail)
                    Return False
                End If
            Else
            End If
            sqldr.Close()
        Catch ex As Exception
            LogError(strLogTime, ex, strTR_TypeNotifikasiEmail)
        End Try
    End Function
    <WebMethod()>
    <ScriptMethod(UseHttpGet:=False, ResponseFormat:=ResponseFormat.Json)>
    Public Function NotifikasiEmailCreateTransaction(ByVal TrxTO As String, ByVal TrxCC As String, ByVal TrxBCC As String, ByVal TrxAgent As String, ByVal TrxTicketNumber As String, ByVal TrxType As String) As String
        If (CheckNotifikasiEmail("Create")) = True Then

            Dim TrxSubject As String = String.Empty
            Dim TrxBody As String = String.Empty
            Dim strTemplate As String = String.Empty
            strTemplate = "select * from TR_TemplateNotifikasiEmail where Type='Create'"
            sqldr = Proses.ExecuteReader(strTemplate)
            Try
                If sqldr.HasRows() Then
                    sqldr.Read()
                    TrxSubject = sqldr("Subject").ToString
                    TrxBody = sqldr("Body").ToString
                    LogSuccess(strLogTime, strTemplate)
                Else
                    LogSuccess(strLogTime, strTemplate)
                End If
                sqldr.Close()
            Catch ex As Exception
                LogError(strLogTime, ex, strTemplate)
            End Try

            Dim TrxEmailID As String = "ICC " - DateTime.Now.ToString("yyyyMMddhhmmssfff")
            Dim listTickets As List(Of resultInsert) = New List(Of resultInsert)()
            Dim strExec As String = String.Empty
            Dim constr As String = ConfigurationManager.ConnectionStrings("DefaultConnection").ConnectionString
            Try
                Using con As New SqlConnection(constr)
                    Dim sqlComm As New SqlCommand()
                    sqlComm.Connection = con
                    sqlComm.CommandText = "TR_Notifikasi_Ticket"
                    sqlComm.CommandType = CommandType.StoredProcedure
                    sqlComm.Parameters.AddWithValue("EMAILID", TrxEmailID)
                    sqlComm.Parameters.AddWithValue("EFROM", TrxEmailForm)
                    sqlComm.Parameters.AddWithValue("ETO", TrxTO)
                    sqlComm.Parameters.AddWithValue("ECC", TrxCC)
                    sqlComm.Parameters.AddWithValue("EBCC", TrxBCC)
                    sqlComm.Parameters.AddWithValue("ESUBJECT", TrxSubject)
                    sqlComm.Parameters.AddWithValue("EBODY", TrxBody)
                    sqlComm.Parameters.AddWithValue("EAGENT", TrxAgent)
                    sqlComm.Parameters.AddWithValue("ETICKETID", TrxTicketNumber)
                    sqlComm.Parameters.AddWithValue("ETYPE", TrxType)
                    con.Open()
                    sqlComm.ExecuteNonQuery()
                End Using
            Catch ex As Exception
                Dim objectTickets As resultInsert = New resultInsert()
                objectTickets.Result = "False"
                objectTickets.msgSystem = ex.Message()
                listTickets.Add(objectTickets)
                strExec = "exec TR_Notifikasi_Ticket - Email Create Ticket " & "'" & TrxEmailID & "'," & "'" & TrxEmailForm & "'," & "'" & TrxTO & "'," & "'" & TrxCC & "', " & "'" & TrxBCC & "', " & "'" & TrxSubject & "',  " & "'" & TrxBody & "', " & "'" & TrxAgent & "', " & "'" & TrxTicketNumber & "', " & "'" & TrxType & "'"
                LogError(strLogTime, ex, strExec)
            Finally
                Dim objectTickets As resultInsert = New resultInsert()
                objectTickets.Result = "True"
                objectTickets.msgSystem = "Data Customer Has Been Update"
                listTickets.Add(objectTickets)
                strExec = "exec TR_Notifikasi_Ticket - Email Create Ticket " & "'" & TrxEmailID & "'," & "'" & TrxEmailForm & "'," & "'" & TrxTO & "'," & "'" & TrxCC & "', " & "'" & TrxBCC & "', " & "'" & TrxSubject & "',  " & "'" & TrxBody & "', " & "'" & TrxAgent & "', " & "'" & TrxTicketNumber & "', " & "'" & TrxType & "'"
                LogSuccess(strLogTime, strExec)
                ''End
            End Try
            Dim js As JavaScriptSerializer = New JavaScriptSerializer()
            Return js.Serialize(listTickets)
        Else
            Dim strExec As String = String.Empty
            strExec = "exec TR_Notifikasi_Ticket - Email Create Ticket - Setting Notif Email Tidak Ada"
            LogSuccess(strLogTime, strExec)
        End If
    End Function
    <WebMethod()>
    <ScriptMethod(UseHttpGet:=False, ResponseFormat:=ResponseFormat.Json)>
    Public Function UpdateThread(ByVal TrxGenesysID As String, ByVal TrxReason As String, ByVal TrxUserName As String) As String
        Dim listTickets As List(Of resultInsert) = New List(Of resultInsert)()
        Dim strExec As String = String.Empty
        Dim constr As String = ConfigurationManager.ConnectionStrings("DefaultConnection").ConnectionString
        Try
            Using con As New SqlConnection(constr)
                Dim sqlComm As New SqlCommand()
                sqlComm.Connection = con
                sqlComm.CommandText = "TR_Update_Thread"
                sqlComm.CommandType = CommandType.StoredProcedure
                sqlComm.Parameters.AddWithValue("TrxGenesysID", TrxGenesysID)
                sqlComm.Parameters.AddWithValue("TrxReason", TrxReason)
                sqlComm.Parameters.AddWithValue("TrxUserName", TrxUserName)
                con.Open()
                sqlComm.ExecuteNonQuery()
            End Using
        Catch ex As Exception
            Dim objectTickets As resultInsert = New resultInsert()
            objectTickets.Result = "False"
            objectTickets.msgSystem = ex.Message()
            listTickets.Add(objectTickets)
            strExec = "exec TR_Update_Thread " & "'" & TrxGenesysID & "'," & "'" & TrxReason & "'," & "'" & TrxUserName & "'"
            LogError(strLogTime, ex, strExec)
        Finally
            Dim objectTickets As resultInsert = New resultInsert()
            objectTickets.Result = "True"
            objectTickets.msgSystem = "Data Thread Has Been Update"
            listTickets.Add(objectTickets)
            strExec = "exec TR_Update_Thread " & "'" & TrxGenesysID & "'," & "'" & TrxReason & "'," & "'" & TrxUserName & "'"
            LogSuccess(strLogTime, strExec)
            ''End
        End Try
        Dim js As JavaScriptSerializer = New JavaScriptSerializer()
        Return js.Serialize(listTickets)
    End Function
    <WebMethod()>
    <ScriptMethod(UseHttpGet:=False, ResponseFormat:=ResponseFormat.Json)>
    Public Function EscalationTransactionTicket(ByVal TrxTicketNumber As String, ByVal TrxResponse As String, ByVal TrxLayer As String, ByVal TrxStatus As String, ByVal TrxUsername As String,
                                                ByVal TrxChannel As String, ByVal TrxThreadID As String, ByVal TrxGenesysID As String, ByVal TrxEscalasiUnit As String, ByVal TrxAmount As String) As String
        Dim listTickets As List(Of resultInsert) = New List(Of resultInsert)()
        Dim strExec As String = String.Empty
        Dim constr As String = ConfigurationManager.ConnectionStrings("DefaultConnection").ConnectionString
        Try
            Using con As New SqlConnection(constr)
                Dim sqlComm As New SqlCommand()
                sqlComm.Connection = con
                sqlComm.CommandText = "Tr_EscalationTransactionTicket"
                sqlComm.CommandType = CommandType.StoredProcedure
                sqlComm.Parameters.AddWithValue("TrxTicketNumber", TrxTicketNumber)
                sqlComm.Parameters.AddWithValue("TrxUsername", TrxUsername)
                sqlComm.Parameters.AddWithValue("TrxLayer", TrxLayer)
                sqlComm.Parameters.AddWithValue("TrxStatus", TrxStatus)
                sqlComm.Parameters.AddWithValue("TrxEscalasiUnit", TrxEscalasiUnit)
                sqlComm.Parameters.AddWithValue("TrxChannel", TrxChannel)
                sqlComm.Parameters.AddWithValue("TrxResponse", HttpUtility.UrlDecode(TrxResponse))
                sqlComm.Parameters.AddWithValue("TrxThreadID", TrxThreadID)
                sqlComm.Parameters.AddWithValue("TrxGenesysID", TrxGenesysID)
                sqlComm.Parameters.AddWithValue("TrxAmount", TrxAmount)
                con.Open()
                sqlComm.ExecuteNonQuery()
            End Using
        Catch ex As Exception
            Dim objectTickets As resultInsert = New resultInsert()
            objectTickets.Result = "False"
            objectTickets.msgSystem = ex.Message()
            listTickets.Add(objectTickets)
            strExec = "exec Tr_EscalationTransactionTicket " & "'" & TrxTicketNumber & "'," & "'" & TrxUsername & "'," & "'" & TrxLayer & "'," & "'" & TrxStatus & "'," & "'" & TrxEscalasiUnit & "'," & "'" & TrxChannel & "', " & "'" & HttpUtility.UrlDecode(TrxResponse) & "', " & "'" & TrxThreadID & "', " & "'" & TrxGenesysID & "', " & "'" & TrxAmount & "'"
            LogError(strLogTime, ex, strExec)
        Finally
            Dim objectTickets As resultInsert = New resultInsert()
            objectTickets.Result = "True"
            objectTickets.msgSystem = "Transaction Has Been Escalation"
            listTickets.Add(objectTickets)
            strExec = "exec Tr_EscalationTransactionTicket " & "'" & TrxTicketNumber & "'," & "'" & TrxUsername & "'," & "'" & TrxLayer & "'," & "'" & TrxStatus & "'," & "'" & TrxEscalasiUnit & "'," & "'" & TrxChannel & "', " & "'" & HttpUtility.UrlDecode(TrxResponse) & "', " & "'" & TrxThreadID & "', " & "'" & TrxGenesysID & "', " & "'" & TrxAmount & "'"
            LogSuccess(strLogTime, strExec)
            ''End
        End Try
        Dim js As JavaScriptSerializer = New JavaScriptSerializer()
        Return js.Serialize(listTickets)
    End Function
    <WebMethod()>
    <ScriptMethod(UseHttpGet:=False, ResponseFormat:=ResponseFormat.Json)>
    Public Function SelectOrganizationCase(ByVal filterData As String) As String
        Dim listTickets As List(Of listOrganization) = New List(Of listOrganization)()
        Dim cs As String = ConfigurationManager.ConnectionStrings("DefaultConnection").ConnectionString
        Dim strQuery As String = String.Empty
        Try
            strQuery = "SELECT * FROM mOrganization where ORGANIZATION_NAME='" & filterData & "'"
            Using con As SqlConnection = New SqlConnection(cs)
                Dim cmd As SqlCommand
                cmd = New SqlCommand(strQuery, con)
                cmd.CommandType = CommandType.Text
                con.Open()
                Dim rdr As SqlDataReader = cmd.ExecuteReader()

                While rdr.Read()
                    Dim objectTickets As listOrganization = New listOrganization()
                    objectTickets.Result = "True"
                    objectTickets.OrganizationID = rdr("ORGANIZATION_ID").ToString()
                    objectTickets.OrganizationName = rdr("ORGANIZATION_NAME").ToString()
                    listTickets.Add(objectTickets)
                End While

            End Using
        Catch ex As Exception
            LogError(strLogTime, ex, strQuery)
        Finally
            LogSuccess(strLogTime, strQuery)
        End Try

        Dim js As JavaScriptSerializer = New JavaScriptSerializer()
        Return js.Serialize(listTickets)
    End Function
    <WebMethod()>
    <ScriptMethod(UseHttpGet:=False, ResponseFormat:=ResponseFormat.Json)>
    Public Function SelectHistoryTransaction(ByVal filterData As String) As String
        Dim listTickets As List(Of listTransaction) = New List(Of listTransaction)()
        Dim cs As String = ConfigurationManager.ConnectionStrings("DefaultConnection").ConnectionString
        Dim strQuery As String = String.Empty
        Try
            strQuery = "SELECT * FROM tTicket where TicketNumber='" & filterData & "'"
            Using con As SqlConnection = New SqlConnection(cs)
                Dim cmd As SqlCommand
                cmd = New SqlCommand(strQuery, con)
                cmd.CommandType = CommandType.Text
                con.Open()
                Dim rdr As SqlDataReader = cmd.ExecuteReader()

                While rdr.Read()
                    Dim objectTickets As listTransaction = New listTransaction()
                    objectTickets.Result = "True"
                    objectTickets.TrxID = rdr("ID").ToString()
                    objectTickets.TrxCustomerid = rdr("NIK").ToString()
                    objectTickets.TrxTicketNumber = rdr("TicketNumber").ToString()
                    objectTickets.TrxTicketSourceName = rdr("TicketSourceName").ToString()
                    objectTickets.TrxCategoryID = rdr("CategoryID").ToString()
                    objectTickets.TrxCategoryName = rdr("CategoryName").ToString()
                    objectTickets.TrxLevel1ID = rdr("SubCategory1ID").ToString()
                    objectTickets.TrxLevel1Name = rdr("SubCategory1Name").ToString()
                    objectTickets.TrxLevel2ID = rdr("SubCategory2ID").ToString()
                    objectTickets.TrxLevel2Name = rdr("SubCategory2Name").ToString()
                    objectTickets.TrxLevel3ID = rdr("SubCategory3ID").ToString()
                    objectTickets.TrxLevel3Name = rdr("SubCategory3Name").ToString()
                    objectTickets.TrxDetailComplaint = rdr("DetailComplaint").ToString()
                    objectTickets.TrxResponComplaint = rdr("ResponComplaint").ToString()
                    listTickets.Add(objectTickets)
                End While

            End Using
        Catch ex As Exception
            LogError(strLogTime, ex, strQuery)
        Finally
            LogSuccess(strLogTime, strQuery)
        End Try

        Dim js As JavaScriptSerializer = New JavaScriptSerializer()
        Return js.Serialize(listTickets)
    End Function
    <WebMethod()>
    <ScriptMethod(UseHttpGet:=False, ResponseFormat:=ResponseFormat.Json)>
    Public Function CountingAgentLogin(ByVal TrxLevelUser As String) As String
        Dim listTickets As List(Of listCounting) = New List(Of listCounting)()
        Dim cs As String = ConfigurationManager.ConnectionStrings("DefaultConnection").ConnectionString
        Dim strQuery As String = String.Empty
        Dim strCounting As String = String.Empty
        Try
            strQuery = "select count (*) as Data from msUser where LevelUser='" & TrxLevelUser & "' and LOGIN='1'"
            Using con As SqlConnection = New SqlConnection(cs)
                Dim cmd As SqlCommand
                cmd = New SqlCommand(strQuery, con)
                cmd.CommandType = CommandType.Text
                con.Open()
                Dim rdr As SqlDataReader = cmd.ExecuteReader()

                If rdr.Read() Then
                    strCounting = rdr("Data").ToString()
                End If

            End Using
        Catch ex As Exception
            LogError(strLogTime, ex, strQuery)
        Finally
            LogSuccess(strLogTime, strQuery)
        End Try
        If strCounting > 0 Then
            Return True
        Else
            Return False
        End If
    End Function
    <WebMethod()>
    <ScriptMethod(UseHttpGet:=False, ResponseFormat:=ResponseFormat.Json)>
    Public Function UpdateThreadMultipleAccounut(ByVal TrxCustomerID As String, ByVal TrxGenesysID As String) As String
        Dim listTickets As List(Of resultInsert) = New List(Of resultInsert)()
        Dim strExec As String = String.Empty
        Dim constr As String = ConfigurationManager.ConnectionStrings("DefaultConnection").ConnectionString
        Try
            Using con As New SqlConnection(constr)
                Dim sqlComm As New SqlCommand()
                sqlComm.Connection = con
                sqlComm.CommandText = "SP_Temp_DoubleCustomer"
                sqlComm.CommandType = CommandType.StoredProcedure
                sqlComm.Parameters.AddWithValue("TrxCustomerID", TrxCustomerID)
                sqlComm.Parameters.AddWithValue("TrxGenesysID", TrxGenesysID)
                con.Open()
                sqlComm.ExecuteNonQuery()
            End Using
        Catch ex As Exception
            Dim objectTickets As resultInsert = New resultInsert()
            objectTickets.Result = "False"
            objectTickets.msgSystem = ex.Message()
            listTickets.Add(objectTickets)
            strExec = "exec SP_Temp_DoubleCustomer " & "'" & TrxCustomerID & "'," & "'" & TrxGenesysID & "'"
            LogError(strLogTime, ex, strExec)
        Finally
            Dim objectTickets As resultInsert = New resultInsert()
            objectTickets.Result = "True"
            objectTickets.msgSystem = "Data Thread Has Been Update"
            listTickets.Add(objectTickets)
            strExec = "exec SP_Temp_DoubleCustomer " & "'" & TrxCustomerID & "'," & "'" & TrxGenesysID & "'"
            LogSuccess(strLogTime, strExec)
            ''End
        End Try
        Dim js As JavaScriptSerializer = New JavaScriptSerializer()
        Return js.Serialize(listTickets)
    End Function
    <WebMethod()>
    <ScriptMethod(UseHttpGet:=False, ResponseFormat:=ResponseFormat.Json)>
    Public Function TempHistoryTransactionUI(ByVal filterData As String) As String
        Dim listTickets As List(Of listTransaction) = New List(Of listTransaction)()
        Dim cs As String = ConfigurationManager.ConnectionStrings("DefaultConnection").ConnectionString
        Dim strQuery As String = String.Empty
        Try
            strQuery = "exec sp_TicketHistory " & filterData & ""
            Using con As SqlConnection = New SqlConnection(cs)
                Dim cmd As SqlCommand
                cmd = New SqlCommand(strQuery, con)
                cmd.CommandType = CommandType.Text
                con.Open()
                Dim rdr As SqlDataReader = cmd.ExecuteReader()

                While rdr.Read()
                    Dim objectTickets As listTransaction = New listTransaction()
                    objectTickets.Result = "True"
                    'objectTickets.TrxID = rdr("ID").ToString()
                    objectTickets.TrxCustomerid = rdr("NIK").ToString()
                    objectTickets.TrxCustomerName = rdr("Name").ToString()
                    objectTickets.TrxTicketNumber = rdr("TicketNumber").ToString()
                    objectTickets.TrxTicketSourceName = rdr("TicketSourceName").ToString()
                    'objectTickets.TrxCategoryID = rdr("CategoryID").ToString()
                    'objectTickets.TrxCategoryName = rdr("CategoryName").ToString()
                    'objectTickets.TrxLevel1ID = rdr("SubCategory1ID").ToString()
                    'objectTickets.TrxLevel1Name = rdr("SubCategory1Name").ToString()
                    'objectTickets.TrxLevel2ID = rdr("SubCategory2ID").ToString()
                    'objectTickets.TrxLevel2Name = rdr("SubCategory2Name").ToString()
                    'objectTickets.TrxLevel3ID = rdr("SubCategory3ID").ToString()
                    objectTickets.TrxLevel3Name = rdr("SubCategory3Name").ToString()
                    objectTickets.TrxStatus = rdr("Status").ToString()
                    objectTickets.TrxDetailComplaint = rdr("DetailComplaint").ToString()
                    objectTickets.TrxGenesysID = rdr("GenesysNumber").ToString()
                    objectTickets.TrxThreadID = rdr("ThreadID").ToString()
                    objectTickets.TrxInteractionAccount = rdr("Account").ToString()
                    objectTickets.TrxUserCreate = rdr("UserCreate").ToString()
                    objectTickets.TrxDateCreate = rdr("DateCreate").ToString()
                    objectTickets.TrxDateClose = rdr("DateClose").ToString()
                    listTickets.Add(objectTickets)
                End While

            End Using
        Catch ex As Exception
            LogError(strLogTime, ex, strQuery)
        Finally
            LogSuccess(strLogTime, strQuery)
        End Try

        Dim js As JavaScriptSerializer = New JavaScriptSerializer()
        Return js.Serialize(listTickets)
    End Function
    <WebMethod()>
    <ScriptMethod(UseHttpGet:=False, ResponseFormat:=ResponseFormat.Json)>
    Public Function New_DashboardSection(ByVal Data1 As String, ByVal Data2 As String, ByVal Data3 As String, ByVal Data4 As String, ByVal Data5 As String, ByVal Data6 As String) As String
        Dim connstring As String = ConfigurationManager.ConnectionStrings("DefaultConnection").ConnectionString
        Dim dt As DataTable = New DataTable()
        Dim sql As String = ""
        Using conn As SqlConnection = New SqlConnection(connstring)
            conn.Open()

            sql = "exec DASHBOARD_TICKET '" & Data1 & "','" & Data2 & "','" & Data3 & "','" & Data4 & "','" & Data5 & "', '" & Data6 & "'"

            Dim ad As SqlDataAdapter = New SqlDataAdapter(sql, conn)
            Dim ds As DataSet = New DataSet()
            ad.Fill(ds)
            dt = ds.Tables(0)
            conn.Close()
        End Using
        Dim tableJson As String = ConvertDataTabletoString(dt)
        Return tableJson
    End Function
    <WebMethod()>
    <ScriptMethod(UseHttpGet:=False, ResponseFormat:=ResponseFormat.Json)>
    Public Function Temp_SelectEmailOut(ByVal TxrIVCID As String) As String
        Dim listTickets As List(Of listEmailOut) = New List(Of listEmailOut)()
        Dim cs As String = ConfigurationManager.ConnectionStrings("DefaultConnection").ConnectionString
        Dim strQuery As String = String.Empty
        Dim LevelUser As String = String.Empty
        Try
            strQuery = "select * from ICC_EMAIL_OUT where IVC_ID='" & TxrIVCID & "'"
            Using con As SqlConnection = New SqlConnection(cs)
                Dim cmd As SqlCommand
                cmd = New SqlCommand(strQuery, con)
                cmd.CommandType = CommandType.Text
                con.Open()
                Dim rdr As SqlDataReader = cmd.ExecuteReader()

                While rdr.Read()
                    Dim objectTickets As listEmailOut = New listEmailOut()
                    objectTickets.Result = "True"
                    objectTickets.TrxIVCID = rdr("IVC_ID").ToString()
                    objectTickets.TrxBodyHTML = rdr("EBODY_HTML").ToString()
                    listTickets.Add(objectTickets)
                End While

            End Using
        Catch ex As Exception
            LogError(strLogTime, ex, strQuery)
        Finally
            LogSuccess(strLogTime, strQuery)
        End Try

        Dim js As JavaScriptSerializer = New JavaScriptSerializer()
        Return js.Serialize(listTickets)
    End Function
    <WebMethod()>
    <ScriptMethod(UseHttpGet:=False, ResponseFormat:=ResponseFormat.Json)>
    Public Function GroupingDataCustomer(ByVal TrxFromCustomerID As String, ByVal TrxToCustomerID As String, ByVal TrxUserName As String) As String
        Dim listTickets As List(Of resultInsert) = New List(Of resultInsert)()
        Dim strExec As String = String.Empty
        Dim constr As String = ConfigurationManager.ConnectionStrings("DefaultConnection").ConnectionString
        Try
            Using con As New SqlConnection(constr)
                Dim sqlComm As New SqlCommand()
                sqlComm.Connection = con
                sqlComm.CommandText = "TR_GroupingCustomer"
                sqlComm.CommandType = CommandType.StoredProcedure
                sqlComm.Parameters.AddWithValue("TrxFromCustomerID", TrxFromCustomerID)
                sqlComm.Parameters.AddWithValue("TrxToCustomerID", TrxToCustomerID)
                sqlComm.Parameters.AddWithValue("TrxUserName", TrxUserName)
                con.Open()
                sqlComm.ExecuteNonQuery()
            End Using
        Catch ex As Exception
            Dim objectTickets As resultInsert = New resultInsert()
            objectTickets.Result = "False"
            objectTickets.msgSystem = ex.Message()
            listTickets.Add(objectTickets)
            strExec = "exec GroupingDataCustomer " & "'" & TrxFromCustomerID & "'," & "'" & TrxToCustomerID & "'," & "'" & TrxUserName & "'"
            LogError(strLogTime, ex, strExec)
        Finally
            Dim objectTickets As resultInsert = New resultInsert()
            objectTickets.Result = "True"
            objectTickets.msgSystem = "Data Grouping Customer Has Been Success"
            listTickets.Add(objectTickets)
            strExec = "exec GroupingDataCustomer " & "'" & TrxFromCustomerID & "'," & "'" & TrxToCustomerID & "'," & "'" & TrxUserName & "'"
            LogSuccess(strLogTime, strExec)
            ''End
        End Try
        Dim js As JavaScriptSerializer = New JavaScriptSerializer()
        Return js.Serialize(listTickets)
    End Function
    <WebMethod()>
    <ScriptMethod(UseHttpGet:=False, ResponseFormat:=ResponseFormat.Json)>
    Public Function DeleteGroupingDataCustomer(ByVal TrxFromCustomerID As String, ByVal TrxUserName As String) As String
        Dim listTickets As List(Of resultInsert) = New List(Of resultInsert)()
        Dim strExec As String = String.Empty
        Dim constr As String = ConfigurationManager.ConnectionStrings("DefaultConnection").ConnectionString
        Try
            Using con As New SqlConnection(constr)
                Dim sqlComm As New SqlCommand()
                sqlComm.Connection = con
                sqlComm.CommandText = "TR_DeleteGroupingCustomer"
                sqlComm.CommandType = CommandType.StoredProcedure
                sqlComm.Parameters.AddWithValue("TrxFromCustomerID", TrxFromCustomerID)
                sqlComm.Parameters.AddWithValue("TrxUserName", TrxUserName)
                con.Open()
                sqlComm.ExecuteNonQuery()
            End Using
        Catch ex As Exception
            Dim objectTickets As resultInsert = New resultInsert()
            objectTickets.Result = "False"
            objectTickets.msgSystem = ex.Message()
            listTickets.Add(objectTickets)
            strExec = "exec DeleteGroupingDataCustomer " & "'" & TrxFromCustomerID & "'," & "'" & TrxUserName & "'"
            LogError(strLogTime, ex, strExec)
        Finally
            Dim objectTickets As resultInsert = New resultInsert()
            objectTickets.Result = "True"
            objectTickets.msgSystem = "Delete Data Grouping Customer Has Been Success"
            listTickets.Add(objectTickets)
            strExec = "exec DeleteGroupingDataCustomer " & "'" & TrxFromCustomerID & "'," & "'" & TrxUserName & "'"
            LogSuccess(strLogTime, strExec)
            ''End
        End Try
        Dim js As JavaScriptSerializer = New JavaScriptSerializer()
        Return js.Serialize(listTickets)
    End Function
    <WebMethod()>
    <ScriptMethod(UseHttpGet:=False, ResponseFormat:=ResponseFormat.Json)>
    Public Function ParentChildNumberID(ByVal TrxTicketNumberTo As String, ByVal TrxTicketNumberFrom As String, ByVal TrxUserName As String, ByVal TrxReason As String) As String
        Dim listTickets As List(Of resultInsert) = New List(Of resultInsert)()
        Dim strExec As String = String.Empty
        Dim constr As String = ConfigurationManager.ConnectionStrings("DefaultConnection").ConnectionString
        Try
            Using con As New SqlConnection(constr)
                Dim sqlComm As New SqlCommand()
                sqlComm.Connection = con
                sqlComm.CommandText = "SP_Temp_ParentChildTicket"
                sqlComm.CommandType = CommandType.StoredProcedure
                sqlComm.Parameters.AddWithValue("TrxTicketNumberFrom", TrxTicketNumberFrom)
                sqlComm.Parameters.AddWithValue("TrxTicketNumberTo", TrxTicketNumberTo)
                sqlComm.Parameters.AddWithValue("TrxUserName", TrxUserName)
                sqlComm.Parameters.AddWithValue("TrxReason", HttpUtility.UrlDecode(TrxReason))
                con.Open()
                sqlComm.ExecuteNonQuery()
            End Using
        Catch ex As Exception
            Dim objectTickets As resultInsert = New resultInsert()
            objectTickets.Result = "False"
            objectTickets.msgSystem = ex.Message()
            strExec = "exec SP_Temp_ParentChildTicket " & "" & "'" & TrxTicketNumberFrom & "','" & TrxTicketNumberTo & "'," & "'" & TrxUserName & "'," & "'" & HttpUtility.UrlDecode(TrxReason) & "'"
            LogError(strLogTime, ex, strExec)
        Finally
            Dim objectTickets As resultInsert = New resultInsert()
            objectTickets.Result = "True"
            objectTickets.msgSystem = "Data Transaction Has Been Update"
            listTickets.Add(objectTickets)
            strExec = "exec SP_Temp_ParentChildTicket " & "" & "'" & TrxTicketNumberFrom & "','" & TrxTicketNumberTo & "'," & "'" & TrxUserName & "'," & "'" & HttpUtility.UrlDecode(TrxReason) & "'"
            LogSuccess(strLogTime, strExec)
            ''End
        End Try
        Dim js As JavaScriptSerializer = New JavaScriptSerializer()
        Return js.Serialize(listTickets)
    End Function
    <WebMethod()>
    <ScriptMethod(UseHttpGet:=False, ResponseFormat:=ResponseFormat.Json)>
    Public Function DeleteParentChildNumberID(ByVal TrxUsername As String, ByVal TrxTicketNumber As String) As String
        Dim listTickets As List(Of resultInsert) = New List(Of resultInsert)()
        Dim strExec As String = String.Empty
        Dim constr As String = ConfigurationManager.ConnectionStrings("DefaultConnection").ConnectionString
        Try
            Using con As New SqlConnection(constr)
                Dim sqlComm As New SqlCommand()
                sqlComm.Connection = con
                sqlComm.CommandText = "SP_Temp_DeleteParentNumberID"
                sqlComm.CommandType = CommandType.StoredProcedure
                sqlComm.Parameters.AddWithValue("TrxTicketNumber", TrxTicketNumber)
                sqlComm.Parameters.AddWithValue("TrxUsername", TrxUsername)
                con.Open()
                sqlComm.ExecuteNonQuery()
            End Using
        Catch ex As Exception
            Dim objectTickets As resultInsert = New resultInsert()
            objectTickets.Result = "False"
            objectTickets.msgSystem = ex.Message()
            listTickets.Add(objectTickets)
            strExec = "exec Temp_DeleteParentNumberID " & "'" & TrxTicketNumber & "','" & TrxUsername & "'"
            LogError(strLogTime, ex, strExec)
        Finally
            Dim objectTickets As resultInsert = New resultInsert()
            objectTickets.Result = "True"
            objectTickets.msgSystem = "Data Parent Child Ticket Has Been Delete"
            listTickets.Add(objectTickets)
            strExec = "exec Temp_DeleteParentNumberID " & "'" & TrxTicketNumber & "','" & TrxUsername & "'"
            LogSuccess(strLogTime, strExec)
        End Try
        Dim js As JavaScriptSerializer = New JavaScriptSerializer()
        Return js.Serialize(listTickets)
    End Function
    <WebMethod()>
    <ScriptMethod(UseHttpGet:=False, ResponseFormat:=ResponseFormat.Json)>
    Public Function ChangePassword(ByVal TrxPassword As String, ByVal TrxNewPassword As String, ByVal TrxUserName As String, ByVal TrxUserCreated As String) As String
        Dim listTickets As List(Of resultInsert) = New List(Of resultInsert)()
        Dim strExec As String = String.Empty
        Dim constr As String = ConfigurationManager.ConnectionStrings("DefaultConnection").ConnectionString
        Dim _Result As String = String.Empty
        Try
            Using con As New SqlConnection(constr)
                Dim sqlComm As New SqlCommand()
                sqlComm.Connection = con
                sqlComm.CommandText = "SP_Temp_ChangePassword"
                sqlComm.CommandType = CommandType.StoredProcedure
                sqlComm.Parameters.AddWithValue("TrxPassword", TrxPassword)
                sqlComm.Parameters.AddWithValue("TrxNewPassword", TrxNewPassword)
                sqlComm.Parameters.AddWithValue("TrxUserName", TrxUserName)
                sqlComm.Parameters.AddWithValue("TrxUserCreated", TrxUserCreated)
                con.Open()
                sqldr = sqlComm.ExecuteReader()
                While sqldr.Read()
                    _Result &= sqldr("TrxResult").ToString
                End While
                sqldr.Close()
                con.Close()
            End Using
        Catch ex As Exception
            Dim objectTickets As resultInsert = New resultInsert()
            objectTickets.Result = "False"
            objectTickets.msgSystem = ex.Message()
            listTickets.Add(objectTickets)
            strExec = "exec SP_Temp_ChangePassword " & "'" & TrxPassword & "'," & "'" & TrxNewPassword & "'," & "'" & TrxUserName & "'," & "'" & TrxUserCreated & "'"
            LogError(strLogTime, ex, strExec)
        Finally
            Dim objectTickets As resultInsert = New resultInsert()
            If _Result = "Success" Then
                objectTickets.Result = "True"
                objectTickets.msgSystem = "Change Password Success"
                listTickets.Add(objectTickets)
                strExec = "exec SP_Temp_ChangePassword " & "'" & TrxPassword & "'," & "'" & TrxNewPassword & "'," & "'" & TrxUserName & "'," & "'" & _Result & "'," & "'" & TrxUserCreated & "'"
                LogSuccess(strLogTime, strExec)
            Else
                objectTickets.Result = "False"
                objectTickets.msgSystem = _Result
                listTickets.Add(objectTickets)
                strExec = "exec SP_Temp_ChangePassword " & "'" & TrxPassword & "'," & "'" & TrxNewPassword & "'," & "'" & TrxUserName & "'," & "'" & _Result & "'," & "'" & TrxUserCreated & "'"
                LogSuccess(strLogTime, strExec)
            End If
        End Try
        Dim js As JavaScriptSerializer = New JavaScriptSerializer()
        Return js.Serialize(listTickets)
    End Function
    <WebMethod()>
    <ScriptMethod(UseHttpGet:=False, ResponseFormat:=ResponseFormat.Json)>
    Public Function UpdateReleaseUser(ByVal TrxUsername As String, ByVal TrxUserID As String, ByVal TrxReason As String) As String
        Dim listTickets As List(Of resultInsert) = New List(Of resultInsert)()
        Dim strExec As String = String.Empty
        Dim constr As String = ConfigurationManager.ConnectionStrings("DefaultConnection").ConnectionString
        Try
            Using con As New SqlConnection(constr)
                Dim sqlComm As New SqlCommand()
                sqlComm.Connection = con
                sqlComm.CommandText = "New_Temp_Release_Username"
                sqlComm.CommandType = CommandType.StoredProcedure
                sqlComm.Parameters.AddWithValue("TrxUsername", TrxUsername)
                sqlComm.Parameters.AddWithValue("TrxUserID", TrxUserID)
                sqlComm.Parameters.AddWithValue("TrxReason", HttpUtility.UrlDecode(TrxReason))
                con.Open()
                sqlComm.ExecuteNonQuery()
            End Using
        Catch ex As Exception
            Dim objectTickets As resultInsert = New resultInsert()
            objectTickets.Result = "False"
            objectTickets.msgSystem = ex.Message()
            listTickets.Add(objectTickets)
            strExec = "exec New_Temp_Release_Username " & "'" & TrxUsername & "'," & "'" & TrxUserID & "'," & "'" & TrxReason & "'"
            LogError(strLogTime, ex, strExec)
        Finally
            Dim objectTickets As resultInsert = New resultInsert()
            objectTickets.Result = "True"
            objectTickets.msgSystem = "Data User Has Been Release"
            listTickets.Add(objectTickets)
            strExec = "exec New_Temp_Release_Username " & "'" & TrxUsername & "'," & "'" & TrxUserID & "'," & "'" & TrxReason & "'"
            LogSuccess(strLogTime, strExec)
            ''End
        End Try
        Dim js As JavaScriptSerializer = New JavaScriptSerializer()
        Return js.Serialize(listTickets)
    End Function
    '' Insert Data Activity User Management
    <WebMethod()>
    <ScriptMethod(UseHttpGet:=False, ResponseFormat:=ResponseFormat.Json)>
    Public Function InsertActivityManagementUser(ByVal _Action As String, ByVal _UserID As String, ByVal _UserName As String, ByVal _Name As String, ByVal _Password As String, ByVal _LevelUser As String,
                                                 ByVal _Organization As String, ByVal _EmailAddress As String, ByVal _KirimEmailNotif As String, ByVal _StatuNotif As String,
                                                 ByVal _StatusUser As String, ByVal _UserCreate As String) As String
        Dim listTickets As List(Of resultInsert) = New List(Of resultInsert)()
        Dim strExec As String = String.Empty
        Dim _StringOrganization, _StringEmailAddress, _StringKirimEmailNotif, _StringStatuNotif, _StringStatusUser As String

        If _Organization = "" Then
            _StringOrganization = "-"
        Else
            _StringOrganization = _Organization
        End If
        If _EmailAddress = "" Then
            _StringEmailAddress = "-"
        Else
            _StringEmailAddress = _EmailAddress
        End If
        If _KirimEmailNotif = "" Then
            _StringKirimEmailNotif = "Y"
        Else
            _StringKirimEmailNotif = _KirimEmailNotif
        End If
        If _StatuNotif = "" Then
            _StringStatuNotif = "Y"
        Else
            _StringStatuNotif = _StatuNotif
        End If
        If _StatusUser = "" Then
            _StringStatusUser = "Y"
        Else
            _StringStatusUser = _StatusUser
        End If
        Dim constr As String = ConfigurationManager.ConnectionStrings("DefaultConnection").ConnectionString
        Try
            Using con As New SqlConnection(constr)
                Dim sqlComm As New SqlCommand()
                sqlComm.Connection = con
                sqlComm.CommandText = "SP_Temp_ActivityManagementUserInsert"
                sqlComm.CommandType = CommandType.StoredProcedure
                sqlComm.Parameters.AddWithValue("ACTION", "Insert")
                sqlComm.Parameters.AddWithValue("USERID", _UserID)
                sqlComm.Parameters.AddWithValue("USERNAME", _UserName)
                sqlComm.Parameters.AddWithValue("NAME", _Name)
                sqlComm.Parameters.AddWithValue("PASSWORD", _Password)
                sqlComm.Parameters.AddWithValue("ORGANIZATION", _StringOrganization)
                sqlComm.Parameters.AddWithValue("LEVELUSER", _LevelUser)
                sqlComm.Parameters.AddWithValue("UNITKERJA", "")
                sqlComm.Parameters.AddWithValue("USERCREATE", _UserCreate)
                sqlComm.Parameters.AddWithValue("EMAIL_ADDRESS", _StringEmailAddress)
                sqlComm.Parameters.AddWithValue("KIRIMEMAIL", _StringKirimEmailNotif)
                sqlComm.Parameters.AddWithValue("STATUS_USER", _StringStatusUser)
                sqlComm.Parameters.AddWithValue("NA", _StringStatuNotif)
                con.Open()
                sqlComm.ExecuteNonQuery()
            End Using
        Catch ex As Exception
            Dim objectTickets As resultInsert = New resultInsert()
            objectTickets.Result = "False"
            objectTickets.msgSystem = ex.Message()
            listTickets.Add(objectTickets)
            strExec = "exec SP_Temp_ActivityManagementUserInsert " & "'Insert'," & "'" & _UserID & "'," & "'" & _UserName & "'," & "'" & _Name & "'," & "'" & _Password & "'," & "'" & _Organization & "'," & "'" & _LevelUser & "'," & "''," & "'" & _UserCreate & "'," & "'" & _EmailAddress & "'," & "'" & _StringKirimEmailNotif & "'," & "'" & _StringStatusUser & "'," & "'" & _StringStatuNotif & "'"
            LogError(strLogTime, ex, strExec)
        Finally
            Dim objectTickets As resultInsert = New resultInsert()
            objectTickets.Result = "True"
            objectTickets.msgSystem = "Data User Has Been " & _Action
            listTickets.Add(objectTickets)
            strExec = "exec SP_Temp_ActivityManagementUserInsert " & "'Insert'," & "'" & _UserID & "'," & "'" & _UserName & "'," & "'" & _Name & "'," & "'" & _Password & "'," & "'" & _Organization & "'," & "'" & _LevelUser & "'," & "''," & "'" & _UserCreate & "'," & "'" & _EmailAddress & "'," & "'" & _StringKirimEmailNotif & "'," & "'" & _StringStatusUser & "'," & "'" & _StringStatuNotif & "'"
            LogSuccess(strLogTime, strExec)
            ''End
        End Try
        Dim js As JavaScriptSerializer = New JavaScriptSerializer()
        Return js.Serialize(listTickets)
    End Function
    '' Insert Data Activity User Management
    <WebMethod()>
    <ScriptMethod(UseHttpGet:=False, ResponseFormat:=ResponseFormat.Json)>
    Public Function UpdateActivityManagementUser(ByVal _Action As String, ByVal _UserID As String, ByVal _UserName As String, ByVal _Name As String, ByVal _LevelUser As String,
                                                 ByVal _Organization As String, ByVal _EmailAddress As String, ByVal _KirimEmailNotif As String, ByVal _StatuNotif As String,
                                                 ByVal _StatusUser As String, ByVal _UserCreate As String) As String
        Dim listTickets As List(Of resultInsert) = New List(Of resultInsert)()
        Dim strExec As String = String.Empty
        Dim _StringOrganization, _StringEmailAddress, _StringPassword As String

        If _Organization = "" Then
            _StringOrganization = "-"
        Else
            _StringOrganization = _Organization
        End If
        If _EmailAddress = "" Then
            _StringEmailAddress = "-"
        Else
            _StringEmailAddress = _EmailAddress
        End If
        Dim constr As String = ConfigurationManager.ConnectionStrings("DefaultConnection").ConnectionString
        Try
            Using con As New SqlConnection(constr)
                Dim sqlComm As New SqlCommand()
                sqlComm.Connection = con
                sqlComm.CommandText = "SP_Temp_ActivityManagementUserUpdate"
                sqlComm.CommandType = CommandType.StoredProcedure
                sqlComm.Parameters.AddWithValue("ACTION", "Edit")
                sqlComm.Parameters.AddWithValue("USERID", _UserID)
                sqlComm.Parameters.AddWithValue("USERNAME", _UserName)
                sqlComm.Parameters.AddWithValue("NAME", _Name)
                sqlComm.Parameters.AddWithValue("ORGANIZATION", _StringOrganization)
                sqlComm.Parameters.AddWithValue("LEVELUSER", _LevelUser)
                sqlComm.Parameters.AddWithValue("UNITKERJA", "")
                sqlComm.Parameters.AddWithValue("USERCREATE", _UserCreate)
                sqlComm.Parameters.AddWithValue("EMAIL_ADDRESS", _StringEmailAddress)
                sqlComm.Parameters.AddWithValue("KIRIMEMAIL", _KirimEmailNotif)
                sqlComm.Parameters.AddWithValue("STATUS_USER", _StatusUser)
                sqlComm.Parameters.AddWithValue("NA", _StatuNotif)
                con.Open()
                sqlComm.ExecuteNonQuery()
            End Using
        Catch ex As Exception
            Dim objectTickets As resultInsert = New resultInsert()
            objectTickets.Result = "False"
            objectTickets.msgSystem = ex.Message()
            listTickets.Add(objectTickets)
            strExec = "exec SP_Temp_ActivityManagementUserUpdate " & "'Edit'," & "'" & _UserID & "'," & "'" & _UserName & "'," & "'" & _Name & "'," & "'" & _Organization & "'," & "'" & _LevelUser & "'," & "''," & "'" & _UserCreate & "'," & "'" & _EmailAddress & "'," & "'" & _KirimEmailNotif & "'," & "'" & _StatusUser & "'," & "'" & _StatuNotif & "'"
            LogError(strLogTime, ex, strExec)
        Finally
            Dim objectTickets As resultInsert = New resultInsert()
            objectTickets.Result = "True"
            objectTickets.msgSystem = "Data User Has Been Update"
            listTickets.Add(objectTickets)
            strExec = "exec SP_Temp_ActivityManagementUserUpdate " & "'Edit'," & "'" & _UserID & "'," & "'" & _UserName & "'," & "'" & _Name & "'," & "'" & _Organization & "'," & "'" & _LevelUser & "'," & "''," & "'" & _UserCreate & "'," & "'" & _EmailAddress & "'," & "'" & _KirimEmailNotif & "'," & "'" & _StatusUser & "'," & "'" & _StatuNotif & "'"
            LogSuccess(strLogTime, strExec)
            ''End
        End Try
        Dim js As JavaScriptSerializer = New JavaScriptSerializer()
        Return js.Serialize(listTickets)
    End Function
    <WebMethod()>
    <ScriptMethod(UseHttpGet:=False, ResponseFormat:=ResponseFormat.Json)>
    Public Function InsertActivityManagementUserDelete(ByVal _Action As String, ByVal _UserID As String, ByVal _UserCreate As String) As String
        Dim listTickets As List(Of resultInsert) = New List(Of resultInsert)()
        Dim strExec As String = String.Empty
        Dim constr As String = ConfigurationManager.ConnectionStrings("DefaultConnection").ConnectionString
        Try
            Using con As New SqlConnection(constr)
                Dim sqlComm As New SqlCommand()
                sqlComm.Connection = con
                sqlComm.CommandText = "SP_Temp_ActivityManagementUserDelete"
                sqlComm.CommandType = CommandType.StoredProcedure
                sqlComm.Parameters.AddWithValue("ACTION", _Action)
                sqlComm.Parameters.AddWithValue("USERID", _UserID)
                sqlComm.Parameters.AddWithValue("USERCREATE", _UserCreate)
                con.Open()
                sqlComm.ExecuteNonQuery()
            End Using
        Catch ex As Exception
            Dim objectTickets As resultInsert = New resultInsert()
            objectTickets.Result = "False"
            objectTickets.msgSystem = ex.Message()
            listTickets.Add(objectTickets)
            strExec = "exec SP_Temp_ActivityManagementUserDelete " & "'" & _Action & "'," & "'" & _UserID & "'," & "'" & _UserCreate & "'"
            LogError(strLogTime, ex, strExec)
        Finally
            Dim objectTickets As resultInsert = New resultInsert()
            objectTickets.Result = "True"
            objectTickets.msgSystem = "Data User Has Been " & _Action
            listTickets.Add(objectTickets)
            strExec = "exec SP_Temp_ActivityManagementUserDelete " & "'" & _Action & "'," & "'" & _UserID & "'," & "'" & _UserCreate & "'"
            LogSuccess(strLogTime, strExec)
            ''End
        End Try
        Dim js As JavaScriptSerializer = New JavaScriptSerializer()
        Return js.Serialize(listTickets)
    End Function
    <WebMethod()>
    <ScriptMethod(UseHttpGet:=False, ResponseFormat:=ResponseFormat.Json)>
    Public Function InsertThreadTicketNumber(ByVal _TrxGenesysNumber As String, ByVal _TrxAgent As String, ByVal _TrxChannel As String) As String
        Dim listTickets As List(Of resultInsert) = New List(Of resultInsert)()
        Dim strExec As String = String.Empty
        Dim constr As String = ConfigurationManager.ConnectionStrings("DefaultConnection").ConnectionString
        Dim _TrxThreadTicketID As String = "TH" + DateTime.Now.ToString("yyyyMMddhhmmssfff")
        Try
            Using con As New SqlConnection(constr)
                Dim sqlComm As New SqlCommand()
                sqlComm.Connection = con
                sqlComm.CommandText = "SP_Temp_ThreadTicketNumber"
                sqlComm.CommandType = CommandType.StoredProcedure
                sqlComm.Parameters.AddWithValue("TrxThreadTicketID", _TrxThreadTicketID)
                sqlComm.Parameters.AddWithValue("TrxGenesysNumber", _TrxGenesysNumber)
                sqlComm.Parameters.AddWithValue("TrxAgent", _TrxAgent)
                sqlComm.Parameters.AddWithValue("TrxChannel", _TrxChannel)
                con.Open()
                sqlComm.ExecuteNonQuery()
            End Using
        Catch ex As Exception
            Dim objectTickets As resultInsert = New resultInsert()
            objectTickets.Result = "False"
            objectTickets.msgSystem = ex.Message()
            listTickets.Add(objectTickets)
            strExec = "exec SP_Temp_ThreadTicketNumber " & "'" & _TrxThreadTicketID & "'," & "'" & _TrxGenesysNumber & "'," & "'" & _TrxAgent & "'," & "'" & _TrxChannel & "'"
            LogError(strLogTime, ex, strExec)
        Finally
            Dim objectTickets As resultInsert = New resultInsert()
            objectTickets.Result = "True"
            objectTickets.msgSystem = "Data Thread Ticket Number Has Been Save"
            listTickets.Add(objectTickets)
            strExec = "exec SP_Temp_ThreadTicketNumber " & "'" & _TrxThreadTicketID & "'," & "'" & _TrxGenesysNumber & "'," & "'" & _TrxAgent & "'," & "'" & _TrxChannel & "'"
            LogSuccess(strLogTime, strExec)
            ''End
        End Try
        Dim js As JavaScriptSerializer = New JavaScriptSerializer()
        Return js.Serialize(listTickets)
    End Function
    Public Function ReplaceSpecialLetter(ByVal str)
        Dim TmpStr As String
        TmpStr = str
        TmpStr = Replace(TmpStr, " ", "")
        TmpStr = Replace(TmpStr, ".00", "")
        TmpStr = Replace(TmpStr, ",", ".")
        TmpStr = Replace(TmpStr, ".", ",")
        ReplaceSpecialLetter = TmpStr
    End Function
End Class