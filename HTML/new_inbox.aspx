﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/HTML/Ticket.Master" CodeBehind="new_inbox.aspx.vb" Inherits="ICC.new_inbox" %>

<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxTabControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxHtmlEditor.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxHtmlEditor" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxCallbackPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxCallback" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxRoundPanel" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="FeaturedContent" runat="server">
    <script>
        function onclickDefault() {
            var TicketNumber = $("#MainContent_hd_ticketid").val();
            $.ajax({
                type: "POST",
                url: "WebServiceTransaction.asmx/Select_Data_TransactionTicket",
                data: "{ filterData: '" + TicketNumber + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    var json = JSON.parse(data.d);
                    var i, x = "";
                    var tblTickets = "";

                    for (i = 0; i < json.length; i++) {
                        
                        $("#MainContent_ASPxPopupControl5_ASPxCallbackPanel12_TxtAmount_I").val(json[i].TrxAmountDecimal);

                    }
                },
                error: function (xmlHttpRequest, textStatus, errorThrown) {
                    console.log(xmlHttpRequest.responseText);
                    console.log(textStatus);
                    console.log(errorThrown);
                }
            })
        }
   
        function onclickAmount() {
            $('#MainContent_ASPxPopupControl2_TxtInputAmount_I').val("");
            ASPxPopupControl2.Show();
        }
   
          function price_InitAndKeyUp(s, e) {
              var rupeeIndian = Intl.NumberFormat("en-ID", {
                  style: "currency",
                  currency: "IDR",
              });
              var idr = rupeeIndian.format($('#MainContent_ASPxPopupControl2_TxtInputAmount_I').val());
              //alert(idr)
              var mon = idr.split('IDR').join('');
              $('#MainContent_ASPxPopupControl5_ASPxCallbackPanel12_TxtAmount_I').val(mon);
              ASPxPopupControl2.Hide();
              //console.log("Dollars: " + rupeeIndian.format($('#MainContent_Amount_I').val()));
          }
   
        function tb_KeyPress() {
            var TrxAmount = $("#MainContent_ASPxPopupControl1_ASPxCallbackPanel1_TxtAmount_I").val()
            if (TrxAmount != '') {
                var numberNya = /^[0-9]+$/;
                if (TrxAmount.match(numberNya)) {
                    //var PhoneLengt = TrxPelaporPhone.toString().length;
                    //if (PhoneLengt > '6') {

                    //} else {
                    //    alert("Format phone number is not valid")
                    //    return false;
                    //}
                    //alert(TrxAmount)
                } else {
                    alert("Format Amount is not valid")
                    return false;
                }
            }
        }
   
        function selectUser() {
            var EscalationLayer = $('#cmbEscalationToLayer option:selected').val()
            $.ajax({
                type: "POST",
                url: "WebServiceTransaction.asmx/SelectUserLogin",
                data: "{filterData: '" + EscalationLayer + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    var json = JSON.parse(data.d);
                    var i, x = "";
                    var result = "";

                    $('#cmbEscalationToUser').append('<option value="selected">--Select--</option>')
                    for (i = 0; i < json.length; i++) {
                        //alert(json[i].UserName)
                        $('#cmbEscalationToUser').append('<option value="' + json[i].UserName + '">' + json[i].UserName + '</option>');

                    }
                },
                error: function (xmlHttpRequest, textStatus, errorThrown) {
                    console.log(xmlHttpRequest.responseText);
                    console.log(textStatus);
                    console.log(errorThrown);
                }
            })
        }
   
        function directPopup() {
            Showdata('13455', 'NS190528001', 'Complaint', 'Penghimpunan Dana', 'Giro', 'Biaya Administrasi/Provisi/Transaksi ', 'SQND', 'ab', 'Progress', '', '20', 'None', '2');
        }
        function loadSelectedProblem() {
            var messageDiv = $('#contentLoadProblem');
            var cmbProblemID = $('#cmbProblem option:selected').val();

            //alert(cmbTrxID);
            $.ajax({
                type: 'GET',
                async: false,
                url: "AjaxPages/SelectedProblem.aspx?id=" + cmbProblemID,
                cache: false,
                success: function (result) {
                    var s = result;
                    var fields = s.split("|");
                    var idprob = fields[0];
                    var idkamus = fields[1];
                    var categoryid = fields[2];
                    var subCategory1id = fields[3];
                    var subCategory2id = fields[4];
                    var priority = fields[5];
                    var severity = fields[6];
                    var sla = fields[7];
                    var subCategory3id = fields[8];
                    var catname = fields[9];
                    var subname1 = fields[10];
                    var subname2 = fields[11];
                    var subname3 = fields[12];
                    var responseAgent = fields[13];

                    //alert(categoryid)

                    $("#cmbTransaksi option:selected").text(catname);
                    $("#subname1 option:selected").text(subname1);
                    $("#subname2 option:selected").text(subname2);
                    $("#priority option:selected").text(priority);
                    $("#severity option:selected").text(severity);
                    // $("#MainContent_ASPxComboBox1_I").val(subname1);
                    cmb_brand.SetValue(subCategory1id);
                    cmb_brand.SetText(subname1);
                    cmb_product.SetValue(subCategory2id);
                    cmb_product.SetText(subname2);
                    cmb_priority.SetValue(priority);
                    cmb_severity.SetValue(severity);
                    Dev_lbl_sla.SetText(sla);
                    Dev_lbl_sla.SetValue(sla);

                    // get value dengan textbox
                    $('#idprob').val(idprob);
                    $('#idkamus').val(idkamus);
                    $('#categoryID').val(categoryid);
                    $('#subCategory1ID').val(subCategory1id);
                    $('#subCategory2ID').val(subCategory2id);
                    //$('#vpriority').val(priority);
                    //$('#vseverity').val(severity);
                    $('#inptsla').val(sla);
                    $('#subCategory3ID').val(subCategory3id);
                    $('#subname3').val(subname3);
                    $('#categoriName').val(catname);
                    $('#responseAgent').val(responseAgent);
                    //$('#lbl_sla').val(sla);

                },
                error: function (xhr, ajaxOptions, thrownError) {
                    counter++;
                    messageDiv.empty();
                    messageDiv.append("thrown error: " + thrownError);
                    messageDiv.append("<br />");
                    messageDiv.append("status text: " + xhr.statusText);
                    messageDiv.append("<br />");
                    messageDiv.append("counter = " + counter);
                }

            });
        }
    
        function closeReloadPage() {
            window.location.reload()
        }
        function GetDetailTransaction(ticketNumber, category) {
            var messageDiv = $('#DivLoadSLA');
            var TrxLoginTypeAngka = $("#MainContent_TrxLoginTypeAngka").val()
            $.ajax({
                type: 'GET',
                async: false,
                url: "AjaxPages/level4.aspx?jenis=edit&id=" + ticketNumber + "&Category=" + category + "&LoginAngka=" + TrxLoginTypeAngka,
                cache: false,
                success: function (result) {
                    //alert("ss" + result)
                    messageDiv.empty();
                    messageDiv.append(result);
                    messageDiv.append("<br />");
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    counter++;
                    messageDiv.empty();
                    messageDiv.append("thrown error: " + thrownError);
                    messageDiv.append("<br />");
                    messageDiv.append("status text: " + xhr.statusText);
                    messageDiv.append("<br />");
                    messageDiv.append("counter = " + counter);
                }
            });
        }
        function loadDataDispatch(id) {
            var messageDiv = $('#loadFormDispatch');
            $.ajax({
                type: 'GET',
                async: false,
                url: "AjaxPages/FormDispatch.aspx?id=" + id,
                cache: false,
                success: function (result) {
                    messageDiv.empty();
                    messageDiv.empty();
                    messageDiv.append(result);
                    //dataTableDivNya.append("<br />");
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    counter++;
                    messageDiv.empty();
                    messageDiv.append("thrown error: " + thrownError);
                    messageDiv.append("<br />");
                    messageDiv.append("status text: " + xhr.statusText);
                    messageDiv.append("<br />");
                    messageDiv.append("counter = " + counter);
                }

            });
        }
   
        function loaddataiframe(id) {
            document.getElementById('MainContent_ASPxPopupControl5_ASPxCallbackPanel12_frameattachment').src = "uploadcontrol.aspx?idticketutama=" + id + "";
        }
    
        function detailcomplaint(ticketid) {
            var test = new FormData();
            $.ajax({
                type: "POST",
                url: "AjaxPages/detailticket.aspx?ticketid=" + ticketid,
                contentType: false,
                processData: false,
                data: test,
                success: function (response) {
                    var res = response.replace('"', '');
                    $("#MainContent_ASPxPopupControl5_ASPxCallbackPanel12_ASPxDescription_I").val(response);
                }
            });
            return false;
        }
  
        function actionClick(p, c) {
            $("#MainContent_hd_id").val(p);
            $("#MainContent_hd_ticketid").val(c);
            ASPxCallbackPanel1.PerformCallback(p);
        }
   
        function encodeData(s) {
            return encodeURIComponent(s).replace(/\-/g, "%2D").replace(/\_/g, "%5F").replace(/\./g, "%2E").replace(/\!/g, "%21").replace(/\~/g, "%7E").replace(/\*/g, "%2A").replace(/\'/g, "%27").replace(/\(/g, "%28").replace(/\)/g, "%29");
        }

        function execUpdateTransaction() {
            var TrxTicketNumber = $("#MainContent_hd_ticketid").val();
            var TrxResponse = $("#MainContent_ASPxPopupControl5_ASPxCallbackPanel12_ASPxResponse_I").val();
            var TrxStatus = $("#cmbStatusTicket").val();
            var TrxUsername = $("#MainContent_hd_username").val();
            var TrxChannel = $("#MainContent_ASPxPopupControl5_ASPxCallbackPanel12_ASPxChannel_I").val();
            var TrxThreadID = $("#MainContent_TrxGenesysID").val();
            var TrxGenesysID = $("#MainContent_TrxGenesysID").val();
            var TrxEscalasiUnit = $("#cmbTujuanEskalasi").val();
            

            if (TrxResponse === 'Agent Response') {
                alert("Agent Response is empty")
                return false;
            }
            if (confirm("Do you want to process?")) {
                $.ajax({
                    type: "POST",
                    url: "WebServiceTransaction.asmx/Update_TransactionTicket",
                    data: "{ TrxTicketNumber: '" + TrxTicketNumber + "',TrxResponse: '" + encodeData(TrxResponse) + "',TrxStatus: '" + TrxStatus + "',TrxUsername: '" + TrxUsername + "',TrxChannel: '" + TrxChannel + "', TrxThreadID: '" + TrxThreadID + "', TrxGenesysID: '" + TrxGenesysID + "', TrxEscalasiUnit:'" + TrxEscalasiUnit + "', TrxAmount:'0' }",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        var json = JSON.parse(data.d);
                        var i, x = "";
                        var tblTickets = "";

                        for (i = 0; i < json.length; i++) {
                            //alert(json[i].Result)
                            if (json[i].Result === 'True') {
                                alert(json[i].msgSystem)
                                window.location.href = "new_inbox.aspx?status=<%=Request.QueryString("status")%>&action=Edit";
                            } else {
                                alert(json[i].msgSystem)
                                return false;
                            }

                        }
                    },
                    error: function (xmlHttpRequest, textStatus, errorThrown) {
                        console.log(xmlHttpRequest.responseText);
                        console.log(textStatus);
                        console.log(errorThrown);
                    }
                })
            } else {
                return false;
            }
        }

        function execCancelTransaction() {
            popupupdate.Hide();
        }
  
        function loadatt() {
            document.getElementById('MainContent_ASPxPopupControl5_ASPxCallbackPanel12_frameattachment').src = "uploadcontrol.aspx?idticketutama=NS190511001";
        }

        function execEscalation() {
            var TrxUsername = $("#MainContent_hd_username").val();
            var TrxLayer = $("#cmbEscalationToLayer").val();
            var TrxStatus = $("#cmbStatusTicket").val();
            var TrxChannel = $("#MainContent_ASPxPopupControl5_ASPxCallbackPanel12_ASPxChannel_I").val();
            var TrxResponse = $("#MainContent_ASPxPopupControl5_ASPxCallbackPanel12_ASPxCallbackPanel10_TxtEscalationReason_I").val();
            var TrxTicketNumber = $("#MainContent_hd_ticketid").val();
            var TrxThreadID = $("#MainContent_TrxThreadID").val();
            var TrxGenesysID = $("#MainContent_TrxGenesysID").val();
            var TrxEscalasiUnit = $("#cmbTujuanEskalasi").val();

            if (TrxLayer === '0') {
                alert("Escalation to layer is empty")
                return false;
            }
            if (TrxResponse === '') {
                alert("Escalation reason is empty")
                return false;
            }
            if (confirm("Do you want to process?")) {
                $.ajax({
                    type: "POST",
                    url: "WebServiceTransaction.asmx/EscalationTransactionTicket",
                    data: "{ TrxTicketNumber: '" + TrxTicketNumber + "', TrxResponse: '" + encodeData(TrxResponse) + "', TrxLayer: '" + TrxLayer + "', TrxStatus: '" + TrxStatus + "', TrxUsername: '" + TrxUsername + "',TrxChannel: '" + TrxChannel + "', TrxThreadID: '" + TrxThreadID + "', TrxGenesysID: '" + TrxGenesysID + "', TrxEscalasiUnit:'" + TrxEscalasiUnit + "', TrxAmount:'0'}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        var json = JSON.parse(data.d);
                        var i, x = "";
                        var tblTickets = "";

                        for (i = 0; i < json.length; i++) {
                            if (json[i].Result === 'True') {
                                alert("Ticket has been escalation successfully");
                                popupupdate.Hide();
                            } else {
                                alert(json[i].msgSystem);
                                return false;
                            }

                        }
                    },
                    error: function (xmlHttpRequest, textStatus, errorThrown) {
                        console.log(xmlHttpRequest.responseText);
                        console.log(textStatus);
                        console.log(errorThrown);
                    }
                })
            }
            else
                return false;
        }
  
        function ShowUpdateTransaction(text) {
            $("#MainContent_hd_ticketid").val(text);
            var TicketNumber = $("#MainContent_hd_ticketid").val();
            var TrxUserName = $("#MainContent_TrxUserName").val();
            
            $.ajax({
                type: "POST",
                url: "WebServiceTransaction.asmx/Select_Data_TransactionTicket",
                data: "{ filterData: '" + TicketNumber + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {

                    var json = JSON.parse(data.d);
                    var i, x = "";
                    var tblTickets = "";

                    for (i = 0; i < json.length; i++) {
                        GetDetailTransaction(json[i].TrxTicketNumber, json[i].TrxCategoryName);
                        loaddataiframe(json[i].TrxTicketNumber)
                        popupupdate.Show();

                        if ((json[i].TrxStatus) === 'Closed' || (json[i].TrxStatus) === 'Solved') {
                            BTNSubmit.SetVisible(false);
                            BTNCancel.SetVisible(false);
                        } else {
                            if (json[i].TrxReleaseUser == 'No') {

                            } else {

                                if (json[i].TrxTicketPosition != $("#MainContent_TrxLoginTypeAngka").val()) {
                                    BTNSubmit.SetVisible(false);
                                    BTNCancel.SetVisible(false);
                                }
                                else {
                                    if (json[i].TrxTicketPosition == "1") {
                                        if (TrxUserName != json[i].TrxReleaseUser) {
                                            BTNSubmit.SetVisible(false);
                                            BTNCancel.SetVisible(false);
                                        }
                                    }

                                }
                            }
                        }

                        if (json[i].TrxTicketPosition != $("#MainContent_TrxLoginTypeAngka").val()) {
                            BTNSubmit.SetVisible(false);
                            BTNCancel.SetVisible(false);
                        }

                        if ((json[i].TrxCategoryName) === 'Complaint') {
                            $("#divCategoryType").css("display", "block");
                        } else {
                            $("#divCategoryType").css("display", "none");
                        }

                        var TglKejadian = ("" + json[i].TrxTahun + "-" + json[i].TrxBulan + "-" + json[i].TrxHari + "");
                        var DateKejadian = new Date(TglKejadian);
                        BTN_datePengaduan.SetDate(DateKejadian);

                        $("#MainContent_ASPxPopupControl5_ASPxCallbackPanel12_DateofTransaction_I").val(json[i].TrxDateofTransaction);
                        $("#MainContent_ASPxPopupControl5_ASPxCallbackPanel12_cbClaimStatus_I").val(json[i].TrxcbClaimStatus);
                        $("#MainContent_ASPxPopupControl5_ASPxCallbackPanel12_cbBankName_I").val(json[i].TrxcbBankName);
                        $("#MainContent_ASPxPopupControl5_ASPxCallbackPanel12_TxtAmount_I").val(json[i].TrxAmountDecimal);
                        $("#MainContent_ASPxPopupControl5_ASPxCallbackPanel12_BTN_cmbPenyebab_I").val(json[i].TrxPenyebab);
                        $("#MainContent_ASPxPopupControl5_ASPxCallbackPanel12_TxtPenerimaPengaduan_I").val(json[i].TrxPenerimaPengaduan);
                        $("#MainContent_ASPxPopupControl5_ASPxCallbackPanel12_BTN_cmbStatusPelapor_I").val(json[i].TrxStatusPelapor);
                        $("#MainContent_ASPxPopupControl5_ASPxCallbackPanel12_CmbCountry_I").val(json[i].TrxCategoryName);
                        $("#MainContent_ASPxPopupControl5_ASPxCallbackPanel12_ASPxComboBox1_I").val(json[i].TrxLevel1Name);
                        $("#MainContent_ASPxPopupControl5_ASPxCallbackPanel12_cmb_level2_I").val(json[i].TrxLevel2Name);
                        $("#MainContent_ASPxPopupControl5_ASPxCallbackPanel12_CmbCity_I").val(json[i].TrxLevel3Name);
                        $("#MainContent_ASPxPopupControl5_ASPxCallbackPanel12_ASPxDescription_I").val(json[i].TrxDetailComplaint);
                        $("#cmbStatusTicket").val(json[i].TrxStatus);
                        $("#hd_sla").val(json[i].TrxSLA);
                        $("#MainContent_hd_ticketid").val(json[i].TrxTicketNumber);
                        $("#MainContent_ASPxPopupControl5_ASPxCallbackPanel12_cmbskalaprioritas_I").val(json[i].TrxSkalaPrioritas);
                        $("#MainContent_ASPxPopupControl5_ASPxCallbackPanel12_cmbjenisnasabah_I").val(json[i].TrxJenisNasabah);
                        $("#MainContent_ASPxPopupControl5_ASPxCallbackPanel12_TxtNomorRekening_I").val(json[i].TrxNomorRekening);
                        $("#MainContent_ASPxPopupControl5_ASPxCallbackPanel12_cmbsumberinformmasi_I").val(json[i].TrxSumberInformasi);
                        $("#MainContent_ASPxPopupControl5_ASPxCallbackPanel12_ASPxChannel_I").val(json[i].TrxTicketSourceName);
                        $("#MainContent_TrxThreadID").val(json[i].TrxThreadID);
                        $("#MainContent_TrxGenesysID").val(json[i].TrxGenesysID);
                        ASPxCallbackPanel4.PerformCallback();
                        ASPxCallbackPanel10.PerformCallback();

                    }
                },
                error: function (xmlHttpRequest, textStatus, errorThrown) {
                    console.log(xmlHttpRequest.responseText);
                    console.log(textStatus);
                    console.log(errorThrown);
                }
            })
        }

        function ASPxGridView1_SelectionChanged(s, e) {
            s.GetSelectedFieldValues("TicketNumber", GetSelectedFieldValuesCallback);
        }
        function GetSelectedFieldValuesCallback(values) {
            $('#MainContent_TrxTicketNumber').val(values);
        }
 
        function ShowParentChild(values) {
            $("#MainContent_hd_ticketid").val(values);
            $("#dx_LabelTicketNumber").text(values);
            ASPxCallbackPanel1.PerformCallback();
            ASPxPopupControl1.Show();
        }
        function showTransaction() {
            ASPxGridView1.Refresh();
        }
        function showParent() {
            ASPxGridView3.Refresh();
        }
 
        function execParentNumber() {
            var TrxTicketNumberFrom = $("#MainContent_hd_ticketid").val();
            var TrxTicketNumberTo = $("#MainContent_TrxTicketNumber").val();
            var TrxUsername = $("#MainContent_hd_username").val();
            var TrxReason = $("#dxmReason_I").val();
            if (TrxTicketNumberTo === '') {
                alert("Please, Select your data transaction")
                return false;
            }
            if (TrxReason === 'Reason Parent Ticket') {
                alert("Reason parent is empty")
                return false;
            }
            if (confirm("Do you want to process?")) {
                $.ajax({
                    type: "POST",
                    url: "WebServiceTransaction.asmx/ParentChildNumberID",
                    data: "{ TrxTicketNumberTo: '" + TrxTicketNumberTo + "', TrxUserName: '" + TrxUsername + "', TrxTicketNumberFrom: '" + TrxTicketNumberFrom + "', TrxReason: '" + encodeData(TrxReason) + "'}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        var json = JSON.parse(data.d);
                        var i, x = "";
                        var tblTickets = "";

                        for (i = 0; i < json.length; i++) {
                            if (json[i].Result === 'True') {
                                $("#MainContent_TrxTicketNumber").val("");
                                $("#dxmReason_I").val("");
                                alert(json[i].msgSystem);
                                closeReloadPage();
                                //ASPxPopupControl1.Hide();
                            } else {
                                alert(json[i].msgSystem);
                                return false;
                            }

                        }
                    },
                    error: function (xmlHttpRequest, textStatus, errorThrown) {
                        console.log(xmlHttpRequest.responseText);
                        console.log(textStatus);
                        console.log(errorThrown);
                    }
                })
            }
            else
                return false;
        }
  
        function ShowUpdateParentID(v) {
            $("#MainContent_TrxTicketNumberUpdate").val(v);
            var TrxUsername = $("#MainContent_hd_username").val();
            var TrxTicketNumberUpdate = $("#MainContent_TrxTicketNumberUpdate").val();
            if (confirm("Do you want to process?")) {
                $.ajax({
                    type: "POST",
                    url: "WebServiceTransaction.asmx/DeleteParentChildNumberID",
                    data: "{ TrxTicketNumber: '" + TrxTicketNumberUpdate + "', TrxUsername: '" + TrxUsername + "'}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        var json = JSON.parse(data.d);
                        var i, x = "";
                        var tblTickets = "";

                        for (i = 0; i < json.length; i++) {
                            if (json[i].Result === 'True') {
                                alert(json[i].msgSystem);
                                ASPxPopupControl1.Hide();
                            } else {
                                alert(json[i].msgSystem);
                                return false;
                            }

                        }
                    },
                    error: function (xmlHttpRequest, textStatus, errorThrown) {
                        console.log(xmlHttpRequest.responseText);
                        console.log(textStatus);
                        console.log(errorThrown);
                    }
                })
            }
            else {
                return false;
            }
        }

        function updateSpam(value) {
            if (confirm("Do you want to process?")) {
                var test = new FormData();
                $.ajax({
                    type: "POST",
                    url: "AjaxPages/updateSpam.aspx?id=" + value,
                    contentType: false,
                    processData: false,
                    data: test,
                    success: function (response) {
                        //alert("Ticket has been update successfully.");
                        window.location.reload();
                    }
                });
                return true;
            }
            else
                return false;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:HiddenField ID="hd_id" runat="server" />
    <asp:HiddenField ID="hd_ticketid" runat="server" />
    <asp:HiddenField ID="hd_ivcid" runat="server" />
    <asp:HiddenField ID="hd_username" runat="server" />
    <asp:HiddenField ID="TrxThreadID" runat="server" />
    <asp:HiddenField ID="TrxGenesysID" runat="server" />
    <asp:HiddenField ID="TrxLoginTypeAngka" runat="server" />
    <asp:HiddenField ID="TrxUserName" runat="server" />
    <asp:HiddenField ID="TrxTicketNumber" runat="server" />
    <asp:HiddenField ID="TrxTicketNumberUpdate" runat="server" />

    <h4 class="headline">Data To do list 
        <span class="line bg-info"></span>
    </h4>
    <div class="btn-group pull-right" style="margin-top: -40px;">
        <asp:Literal ID="ltrStatus" runat="server"></asp:Literal>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="padding-md" style="margin-left: -35px; margin-right: -35px; margin-top: -20px;">
                <asp:Literal runat="server" ID="kotakStatus"></asp:Literal>
            </div>
        </div>
    </div>
    <div class="row">
        <hr />
        <div class="col-md-12">
            <dx:ASPxGridView ID="ASPxGridView2" ClientInstanceName="ASPxGridView2" Width="100%" runat="server"
                DataSourceID="dsTodolist" KeyFieldName="NumberRow"
                SettingsPager-PageSize="10" Theme="Metropolis" Styles-Header-Font-Bold="true" Font-Size="X-Small" Visible="false">
                <SettingsPager>
                    <AllButton Text="All">
                    </AllButton>
                    <NextPageButton Text="Next &gt;">
                    </NextPageButton>
                    <PrevPageButton Text="&lt; Prev">
                    </PrevPageButton>
                    <PageSizeItemSettings Visible="true" Items="25, 50, 75" ShowAllItem="true" />
                </SettingsPager>
                <SettingsEditing Mode="Inline" />
                <Settings ShowFilterRow="true" ShowGroupPanel="true" ShowHorizontalScrollBar="true" VerticalScrollBarMode="Hidden" />
                <SettingsBehavior ConfirmDelete="true" />
                <Columns>
                    <dx:GridViewDataTextColumn Caption="Action" VisibleIndex="0" CellStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" Width="50px" ToolTip="Preview Data Transaction">
                        <DataItemTemplate>
                            <a href="#" id="TicketID" onclick="ShowUpdateTransaction('<%# Eval("TicketNumber")%>')"><%# Request.QueryString("action")%></a>
                        </DataItemTemplate>
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="Ticket On Layer" FieldName="TicketPosition" HeaderStyle-HorizontalAlign="left" Width="90px" Settings-AutoFilterCondition="Contains"></dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="Ticket Number" FieldName="TicketNumber" HeaderStyle-HorizontalAlign="left" Width="110px" Settings-AutoFilterCondition="Contains"></dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="Customer Name" FieldName="NamePIC" HeaderStyle-HorizontalAlign="left" Width="250px" Settings-AutoFilterCondition="Contains"></dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="Category" FieldName="CategoryName" HeaderStyle-HorizontalAlign="left" Width="200px" Settings-AutoFilterCondition="Contains"></dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="Enquiry Type" FieldName="SubCategory1Name" HeaderStyle-HorizontalAlign="left" Width="200px" Settings-AutoFilterCondition="Contains"></dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="Enquiry Detail" FieldName="SubCategory2Name" HeaderStyle-HorizontalAlign="left" Width="200px" Settings-AutoFilterCondition="Contains"></dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="Reason" FieldName="SubCategory3Name" HeaderStyle-HorizontalAlign="left" Width="300px" Settings-AutoFilterCondition="Contains"></dx:GridViewDataTextColumn>
                    <dx:GridViewDataMemoColumn Caption="Description Complaint" FieldName="DetailComplaint" HeaderStyle-HorizontalAlign="left" Width="250px" Settings-AutoFilterCondition="Contains"></dx:GridViewDataMemoColumn>
                    <dx:GridViewDataTextColumn Caption="SLA" FieldName="SLA" HeaderStyle-HorizontalAlign="left" Width="130px" Settings-AutoFilterCondition="Contains"></dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="Description SLA" FieldName="UsedDaySLAOK" HeaderStyle-HorizontalAlign="left" Width="130px" Settings-AutoFilterCondition="Contains"></dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="Department" FieldName="UnitKerjaNya" HeaderStyle-HorizontalAlign="left" Width="160px" Settings-AutoFilterCondition="Contains"></dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="From Name" FieldName="NAMA_PELAPOR" HeaderStyle-HorizontalAlign="left" Width="160px" Settings-AutoFilterCondition="Contains"></dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="From Phone" FieldName="PHONE_PELAPOR" HeaderStyle-HorizontalAlign="left" Width="160px" Settings-AutoFilterCondition="Contains"></dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="Status" FieldName="Status" HeaderStyle-HorizontalAlign="left" Width="160px" Settings-AutoFilterCondition="Contains"></dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="Created Date" FieldName="datecreate" HeaderStyle-HorizontalAlign="left" Width="130px" Settings-AutoFilterCondition="Contains"></dx:GridViewDataTextColumn>
                    <dx:GridViewDataDateColumn Caption="Last Response Date" FieldName="LastResponseDate" HeaderStyle-HorizontalAlign="left" Width="130px" Settings-AutoFilterCondition="Contains" PropertiesDateEdit-DisplayFormatString="yyyy-MM-dd hh:mm:ss"></dx:GridViewDataDateColumn>
                    <dx:GridViewDataTextColumn Caption="Last Response By" FieldName="LastResponseBy" HeaderStyle-HorizontalAlign="left" Width="160px" Settings-AutoFilterCondition="Contains"></dx:GridViewDataTextColumn>
                    <%-- <dx:GridViewDataTextColumn Caption="Parent Case Number" FieldName="ParentNumberID" HeaderStyle-HorizontalAlign="left" Width="130px" Settings-AutoFilterCondition="Contains"></dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="Action" VisibleIndex="22" CellStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" Width="50px" ToolTip="Parent Child Ticket">
                        <DataItemTemplate>
                            <a href="#" id="ParentID" onclick="ShowParentChild('<%# Eval("TicketNumber")%>')">
                                <asp:Image ImageUrl="img/icon/Text-Edit-icon2.png" ID="img_insert" runat="server" />
                            </a>
                        </DataItemTemplate>
                    </dx:GridViewDataTextColumn> --%>
                </Columns>
            </dx:ASPxGridView>
            <asp:SqlDataSource ID="dsTodolist" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>"></asp:SqlDataSource>


            <%--grid inbox email--%>
            <dx:ASPxGridView ID="GridView_InboxEmail" ClientInstanceName="GridView_InboxEmail" Width="100%" runat="server" DataSourceID="sqlInboxEmail" KeyFieldName="ID"
                SettingsPager-PageSize="10" Theme="MetropolisBlue" Styles-Header-Font-Bold="true" Font-Size="X-Small">
                <SettingsPager>
                    <AllButton Text="All">
                    </AllButton>
                    <NextPageButton Text="Next &gt;">
                    </NextPageButton>
                    <PrevPageButton Text="&lt; Prev">
                    </PrevPageButton>
                    <PageSizeItemSettings Visible="true" Items="10, 15, 20" ShowAllItem="true" />
                </SettingsPager>
                <SettingsEditing Mode="Inline" />
                <Settings ShowFilterRow="true" ShowGroupPanel="true" ShowHorizontalScrollBar="true" />
                <SettingsBehavior ConfirmDelete="true" />
                <Columns>
                    <dx:GridViewDataTextColumn Caption="Action" VisibleIndex="0" CellStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" Width="110px">
                        <DataItemTemplate>
                            <a href="mainframe.aspx?agentid=<%# Session("username")%>&channel=email&genesysid=<%# Eval("CHANNELID")%>&customerid=&account=<%# Eval("CustomerID")%>&accountid=<%# Eval("CustomerID")%>&subject=&phoneChat=&emailaddress=<%# Eval("CustomerID")%>&source=inbound&threadid=-">Ticket</a>&nbsp;
                            |&nbsp;<a href="email_inbox.aspx?ivcid=<%# Eval("CHANNELID")%>&emailid=<%# Eval("EMAIL_ID")%>&from=<%# Eval("CUSTOMERID")%>&to=<%# Eval("CUSTOMERID")%>&status=reply&idpage=<%= Request.QueryString("idpage") %>" id="a_assign">Reply</a>
                            |&nbsp;<a href="#" onclick="if (!updateSpam('<%# Eval("IVC_ID")%>')) return false;">Spam</a>
                        </DataItemTemplate>
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="Email" FieldName="CustomerID" HeaderStyle-HorizontalAlign="left" Width="500px" Settings-AutoFilterCondition="Contains" />
                    <dx:GridViewDataTextColumn Caption="Subject" FieldName="ESUBJECT" HeaderStyle-HorizontalAlign="left" Width="500px" Settings-AutoFilterCondition="Contains" />
                    <dx:GridViewDataDateColumn Caption="Email Date" FieldName="datereceive" HeaderStyle-HorizontalAlign="left" Width="150px" PropertiesDateEdit-DisplayFormatString="yyyy-MM-dd HH:mm:ss" Settings-AutoFilterCondition="Contains" />
                    <dx:GridViewDataDateColumn Caption="Blending Date" FieldName="dateblending" HeaderStyle-HorizontalAlign="left" Width="150px" PropertiesDateEdit-DisplayFormatString="yyyy-MM-dd HH:mm:ss" Settings-AutoFilterCondition="Contains" />
                </Columns>
            </dx:ASPxGridView>
            <asp:SqlDataSource ID="sqlInboxEmail" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>"
                SelectCommand="sp_icc_channel_history" SelectCommandType="StoredProcedure">
                <SelectParameters>
                    <asp:SessionParameter DbType="String" Name="agent"
                        SessionField="datausername" />
                </SelectParameters>
            </asp:SqlDataSource>
        </div>
    </div>


    <dx:ASPxPopupControl ID="ASPxPopupControl5" ClientInstanceName="popupupdate" runat="server" CloseAction="CloseButton" Modal="false"
        closeonescape="true" Height="670px" AllowResize="true" Width="1150px"
        PopupVerticalAlign="WindowCenter" PopupHorizontalAlign="WindowCenter" AllowDragging="True" Theme="SoftOrange" ShowPageScrollbarWhenModal="true" ScrollBars="Vertical"
        ShowFooter="True" HeaderText="Form Update Ticket" FooterText="" AutoUpdatePosition="true" ClientSideEvents-CloseUp="function(s, e) { closeReloadPage(); }">
        <ContentCollection>
            <dx:PopupControlContentControl ID="PopupControlContentControl5" runat="server">
                <dx:ASPxCallbackPanel ID="ASPxCallbackPanel12" runat="server" Width="1100px" ShowLoadingPanel="false" ClientInstanceName="ASPxCallbackPanel12">
                    <PanelCollection>
                        <dx:PanelContent>
                            <div class="panel-tab clearfix">
                                <ul class="tab-bar">
                                    <li class="active"><a href="#dataTicket" data-toggle="tab"><i class="fa fa-file-text"></i>&nbsp;<strong>Data Transaction Ticket</strong></a></li>
                                    <li><a href="#dataInteraction" data-toggle="tab"><i class="fa fa-group"></i>&nbsp;<strong>Data Interaction Ticket</strong></a></li>
                                    <li><a href="#dataEscalation" data-toggle="tab"><i class="fa fa-share-square-o"></i>&nbsp;<strong>Data Escalation Ticket</strong></a></li>
                                    <li><a href="#dataattchment" data-toggle="tab"><i class="fa fa-hdd-o"></i>&nbsp;<strong>Data Attachment Ticket</strong></a></li>
                                </ul>
                            </div>
                            <div class="panel-body">
                                <div class="tab-content">
                                    <div class="tab-pane fade in active" id="dataTicket">
                                        <div class="row">
                                            <div class="col-md-12 col-sm-12">
                                                <div class="row">
                                                    <div class="col-md-3">
                                                        <div class="form-group">
                                                            <label>Date Created</label>
                                                            <dx:ASPxDateEdit AutoPostBack="false" ID="BTN_datePengaduan" ClientInstanceName="BTN_datePengaduan" runat="server" Cursor="not-allowed"
                                                                CssClass="form-control input-sm" Width="100%" EditFormatString="yyyy-MM-dd" DisplayFormatString="yyyy-MM-dd" ReadOnly="true">
                                                            </dx:ASPxDateEdit>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 col-sm-6"></div>
                                                    <div class="col-md-3">
                                                        <label>Channel</label>
                                                        <dx:ASPxComboBox ID="ASPxChannel" ClientInstanceName="ASPxChannel" runat="server" Theme="Metropolis" Height="30px"
                                                            ItemStyle-HoverStyle-BackColor="#0076c4" AutoPostBack="false" Enabled="false" Cursor="not-allowed"
                                                            Width="100%">
                                                            <ItemStyle>
                                                                <HoverStyle BackColor="#0076c4" ForeColor="#ffffff">
                                                                </HoverStyle>
                                                            </ItemStyle>
                                                        </dx:ASPxComboBox>
                                                    </div>
                                                </div>
                                                
                                                <div class="row">
                                                    <div class="col-md-3 col-sm-3">
                                                        <div class="form-group">
                                                            <label>Category</label>
                                                            <dx:ASPxComboBox runat="server" ID="CmbCountry" DropDownStyle="DropDownList" IncrementalFilteringMode="StartsWith" Theme="Metropolis" Height="30px"
                                                                DataSourceID="sql_country" TextField="Name" ReadOnly="true" Enabled="false" ValueField="CategoryID"
                                                                EnableSynchronization="False" Width="100%" Cursor="not-allowed">
                                                                <ClientSideEvents SelectedIndexChanged="function(s, e) { OnCountryChanged(s); }" />
                                                            </dx:ASPxComboBox>
                                                            <asp:SqlDataSource ID="sql_transaction_type" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>"></asp:SqlDataSource>
                                                            <asp:SqlDataSource ID="sql_country" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>"
                                                                SelectCommand="select * from mCategory"></asp:SqlDataSource>
                                                            <asp:SqlDataSource ID="sql_city" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>"
                                                                SelectCommand="select SubCategory3ID, SubName, SLA from mSubCategoryLv3 WHERE CategoryID =@CategoryID">
                                                                <SelectParameters>
                                                                    <asp:Parameter Name="CategoryID" />
                                                                </SelectParameters>
                                                            </asp:SqlDataSource>
                                                        </div>
                                                    </div>
                                                    <!-- /.col -->
                                                    <div class="col-md-3 col-sm-3">
                                                        <div class="form-group">
                                                            <label>Enquiry Type</label>
                                                            <dx:ASPxComboBox runat="server" ID="ASPxComboBox1" ClientInstanceName="ASPxComboBox1" ReadOnly="false" Theme="Metropolis" Height="30px"
                                                                DropDownStyle="DropDown" DataSourceID="sql_ASPxComboBox1" TextField="SubName" Width="100%" Enabled="false"
                                                                ValueField="SubCategory2ID" IncrementalFilteringMode="StartsWith" EnableSynchronization="False" Cursor="not-allowed">
                                                                <ClientSideEvents EndCallback="OnEndCallbackTiga" />
                                                                <ClientSideEvents SelectedIndexChanged="function(s, e) {popupsla(s.GetSelectedItem().texts[0]);}" />
                                                            </dx:ASPxComboBox>
                                                            <asp:SqlDataSource ID="sql_ASPxComboBox1" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>" SelectCommand="select * from mSubCategoryLv2 WHERE SubCategory2ID=@SubCategory2ID">
                                                                <SelectParameters>
                                                                    <asp:Parameter Name="SubCategory2ID" />
                                                                </SelectParameters>
                                                            </asp:SqlDataSource>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3 col-sm-3">
                                                        <div class="form-group">
                                                            <label>Enquiry Detail</label>
                                                            <dx:ASPxComboBox runat="server" ID="cmb_level2" ClientInstanceName="cmb_level2" ReadOnly="false" Theme="Metropolis" Height="30px"
                                                                DropDownStyle="DropDown" DataSourceID="SqlDataSource6" TextField="SubName" Width="100%" Enabled="false"
                                                                ValueField="SubCategory2ID" IncrementalFilteringMode="StartsWith" EnableSynchronization="False" Cursor="not-allowed">
                                                            </dx:ASPxComboBox>
                                                            <asp:SqlDataSource ID="SqlDataSource6" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>" SelectCommand="select * from mSubCategoryLv2 WHERE SubCategory2ID=@SubCategory2ID">
                                                                <SelectParameters>
                                                                    <asp:Parameter Name="SubCategory2ID" />
                                                                </SelectParameters>
                                                            </asp:SqlDataSource>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3 col-sm-3">
                                                        <div class="form-group">
                                                            <label>Reason</label>
                                                            <dx:ASPxComboBox runat="server" ID="CmbCity" ClientInstanceName="cmbCity" Theme="Metropolis" Height="30px"
                                                                DropDownStyle="DropDown" DataSourceID="sql_city" TextField="SubName" Width="100%" Enabled="false"
                                                                ValueField="SubCategory3ID" IncrementalFilteringMode="StartsWith" EnableSynchronization="False" Cursor="not-allowed">
                                                                <ClientSideEvents EndCallback="OnEndCallback" />
                                                                <%--<ClientSideEvents SelectedIndexChanged="function(s, e) {OnSlaChanged(s.GetSelectedItem().texts);}" />--%>
                                                                <ClientSideEvents SelectedIndexChanged="function(s, e) {OnSlaChanged(s.GetSelectedItem().value);}" />
                                                            </dx:ASPxComboBox>
                                                        </div>
                                                    </div>
                                                    <!-- /.col -->
                                                </div>
                                                
                                                <div class="row">
                                                    <div id="DivLoadSLA" name="DivLoadSLA"></div>
                                                    <div id="Div1" style="height: 0px; visibility: hidden" runat="server" visible="false">
                                                        <div class="col-md-3 col-sm-3">
                                                            <div class="form-group">
                                                                <label>Ticket Status</label>

                                                                <dx:ASPxComboBox ID="cmb_status" ClientInstanceName="cmb_status" runat="server" TextField="status" CssClass="form-control input-sm"
                                                                    ValueField="status" ItemStyle-HoverStyle-BackColor="#F37021" DataSourceID="sql_cmb_status" AutoPostBack="false" ReadOnly="false"
                                                                    Width="100%" IncrementalFilteringMode="Contains">

                                                                <%--<dx:ASPxComboBox ID="cmb_status" ClientInstanceName="cmb_status" runat="server" TextField="status" Theme="Metropolis"
                                                                    ValueField="status" ItemStyle-HoverStyle-BackColor="#F37021" DataSourceID="sql_cmb_status"
                                                                    Width="100%" IncrementalFilteringMode="Contains">--%>
                                                                    <%--<Columns>
                                                                        <dx:ListBoxColumn Caption="Status" FieldName="status" />
                                                                    </Columns>--%>
                                                                    <ItemStyle>
                                                                        <HoverStyle BackColor="#0076c4" ForeColor="#ffffff">
                                                                        </HoverStyle>
                                                                    </ItemStyle>
                                                                </dx:ASPxComboBox>
                                                                <asp:SqlDataSource ID="sql_cmb_status" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>" SelectCommand="select * from mstatus order by Urutan ASC"></asp:SqlDataSource>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3 col-sm-3">
                                                            <label>Unit</label>
                                                            <asp:TextBox CssClass="form-control" ID="txt_channel" runat="server" Enabled="false"></asp:TextBox>
                                                        </div>
                                                        <div class="col-md-3 col-sm-3">
                                                            <label>Enginer</label>
                                                            <asp:TextBox CssClass="form-control" ID="txt_update_enginer" runat="server" Enabled="false">                                      
                                                            </asp:TextBox>
                                                        </div>
                                                        <div class="col-md-3 col-sm-3">
                                                            <label>SLA</label>
                                                            <asp:TextBox CssClass="form-control" ID="txt_sla" runat="server" Enabled="false"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-md-6 col-sm-6">
                                                        <div class="form-group">
                                                            <dx:ASPxMemo ID="ASPxDescription" runat="server" Width="100%" Rows="8" Theme="Metropolis" NullText="Detail Complaint" ReadOnly="true" Enabled="false" />
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 col-sm-6">
                                                        <div class="form-group">
                                                            <dx:ASPxMemo ID="ASPxResponse" runat="server" Width="100%" Rows="8" Theme="Metropolis" NullText="Agent Response" />
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="panel panel-info">
                                                    <div class="panel-heading">
                                                        <h4 class="panel-title">
                                                            <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#OptionalDataUpdate">
                                                                Optional Data
                                                                <span class="badge badge-danger pull-right"><i class="fa fa-plus"></i></span>
                                                            </a>
                                                        </h4>
                                                    </div>
                                                    <div id="OptionalDataUpdate" class="panel-collapse collapse" style="height: 0px;">
                                                        <div class="panel-body">
                                                            <div class="row">
                                                                <div class="col-md-3 col-sm-3">
                                                                    <div class="form-group">
                                                                        <label>Priority Scale</label>
                                                                        <dx:ASPxComboBox ID="cmbskalaprioritas" ClientInstanceName="cmbskalaprioritas" runat="server"           CssClass="form-control input-sm"
                                                                            ItemStyle-HoverStyle-BackColor="#EE4D2D" AutoPostBack="false" Theme="Default"
                                                                            Width="100%" ReadOnly="true">
                                                                            <ItemStyle>
                                                                                <HoverStyle BackColor="#EE4D2D" ForeColor="#ffffff">
                                                                                </HoverStyle>
                                                                            </ItemStyle>
                                                                        </dx:ASPxComboBox>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-3 col-sm-3">
                                                                    <div class="form-group">
                                                                        <label>Source of Information / Channel</label>
                                                                        <dx:ASPxComboBox ID="cmbsumberinformmasi" ClientInstanceName="cmbsumberinformmasi" runat="server"
                                                                            CssClass="form-control input-sm" ValueField="SumberInformasi" ItemStyle-HoverStyle-BackColor="#EE4D2D" AutoPostBack="false" Width="100%" ReadOnly="true">
                                                                            <ItemStyle>
                                                                                <HoverStyle BackColor="#EE4D2D" ForeColor="#ffffff">
                                                                                </HoverStyle>
                                                                            </ItemStyle>
                                                                        </dx:ASPxComboBox>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <hr />
                                                <div class="row">
                                                    <div class="col-md-8">
                                                    </div>
                                                    <div class="col-md-2">
                                                        <dx:ASPxButton ID="BTNSubmit" runat="server" Theme="Metropolis" Text="Update" Width="100%" AutoPostBack="false"
                                                            HoverStyle-BackColor="#EE4D2D" Height="30px" ClientInstanceName="BTNSubmit">
                                                            <ClientSideEvents Click="function(s, e) { execUpdateTransaction(); }" />
                                                        </dx:ASPxButton>
                                                    </div>
                                                    <div class="col-md-2">
                                                        <dx:ASPxButton ID="BTNCancel" runat="server" Theme="Metropolis" Text="Cancel" Width="100%" AutoPostBack="false"
                                                            HoverStyle-BackColor="#EE4D2D" Height="30px" ClientInstanceName="BTNCancel">
                                                            <ClientSideEvents Click="function(s, e) { execCancelTransaction(); }" />
                                                        </dx:ASPxButton>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="dataInteraction">
                                        <dx:ASPxCallbackPanel ID="ASPxCallbackPanel4" runat="server" Width="1030px" ShowLoadingPanel="false" ClientInstanceName="ASPxCallbackPanel4">
                                            <PanelCollection>
                                                <dx:PanelContent>
                                                    <dx:ASPxGridView ID="ASPxGridView4" ClientInstanceName="ASPxGridView4" runat="server" KeyFieldName="ID"
                                                        DataSourceID="dsInteraction" Width="1060px" Theme="Metropolis" Styles-Header-Font-Bold="true" Font-Size="X-Small"
                                                        SettingsPager-PageSize="15">
                                                        <SettingsPager>
                                                            <AllButton Text="All">
                                                            </AllButton>
                                                            <NextPageButton Text="Next &gt;">
                                                            </NextPageButton>
                                                            <PrevPageButton Text="&lt; Prev">
                                                            </PrevPageButton>
                                                            <PageSizeItemSettings Visible="true" Items="10, 15, 20" ShowAllItem="true" />
                                                        </SettingsPager>
                                                        <SettingsEditing Mode="Inline" />
                                                        <Settings ShowFilterRow="false" ShowFilterRowMenu="false" ShowGroupPanel="false"
                                                            ShowVerticalScrollBar="false" ShowHorizontalScrollBar="true" />
                                                        <SettingsBehavior ConfirmDelete="true" />
                                                        <Columns>
                                                            <dx:GridViewDataTextColumn Caption="ID" FieldName="ID" ReadOnly="true" Width="50px" Visible="false"
                                                                PropertiesTextEdit-ReadOnlyStyle-BackColor="LightGray">
                                                            </dx:GridViewDataTextColumn>
                                                            <dx:GridViewDataTextColumn Caption="Interaction ID" FieldName="GenesysID" Width="200px"></dx:GridViewDataTextColumn>
                                                            <dx:GridViewDataTextColumn Caption="Thread ID" FieldName="ThreadID" Width="200px"></dx:GridViewDataTextColumn>
                                                            <dx:GridViewDataMemoColumn Caption="Response" FieldName="ResponseComplaint" Width="300px"></dx:GridViewDataMemoColumn>
                                                            <dx:GridViewDataTextColumn Caption="Dispatch To" FieldName="DispatchTicket" Width="100px"></dx:GridViewDataTextColumn>
                                                            <dx:GridViewDataTextColumn Caption="Dispatch To Layer" FieldName="DispatchToLayer" Width="100px"></dx:GridViewDataTextColumn>
                                                            <dx:GridViewDataTextColumn Caption="Dispatch To Dept" FieldName="ORGANIZATION_NAME" Width="100px"></dx:GridViewDataTextColumn>
                                                            <dx:GridViewDataTextColumn Caption="Created by" FieldName="AgentCreate" Width="100px"></dx:GridViewDataTextColumn>
                                                            <dx:GridViewDataTextColumn Caption="Date Create" FieldName="DateCreate" Width="130px"></dx:GridViewDataTextColumn>
                                                            <dx:GridViewDataTextColumn Caption="Status" FieldName="Status" Width="100px"></dx:GridViewDataTextColumn>
                                                            <dx:GridViewDataTextColumn Caption="Type" FieldName="InteractionType" Width="100px"></dx:GridViewDataTextColumn>
                                                            <dx:GridViewDataTextColumn Caption="Document" VisibleIndex="11" CellStyle-HorizontalAlign="Center"
                                                                HeaderStyle-HorizontalAlign="Center" Width="180px">
                                                                <DataItemTemplate>
                                                                    <a target="_blank" href="uConnector/uConnector_Files/HTML/<%# Eval("GenesysID")%>/<%# Eval("GenesysID")%>.html">
                                                                        <%# Eval("GenesysID")%>
                                                                    </a>
                                                                </DataItemTemplate>
                                                            </dx:GridViewDataTextColumn>
                                                        </Columns>
                                                    </dx:ASPxGridView>
                                                    <asp:SqlDataSource ID="dsInteraction" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>"></asp:SqlDataSource>
                                                </dx:PanelContent>
                                            </PanelCollection>
                                        </dx:ASPxCallbackPanel>
                                    </div>
                                    <%-- <div class="tab-pane fade" id="dataDispatch">
                                        <div id="loadFormDispatch" name="loadFormDispatch"></div>
                                    </div>--%>
                                    <div class="tab-pane fade" id="dataattchment" style="height: 400px; overflow: hidden;">
                                        <%--<div id="loadiframe" name="loadiframe"></div>--%>
                                        <iframe id="frameattachment" style="width: 100%; height: 400px; border: none; overflow: hidden;" runat="server"></iframe>
                                    </div>
                                    <div class="tab-pane fade" id="dataEscalation">
                                        <dx:ASPxCallbackPanel ID="ASPxCallbackPanel10" runat="server" Width="100%" ShowLoadingPanel="false" ClientInstanceName="ASPxCallbackPanel10">
                                            <PanelCollection>
                                                <dx:PanelContent>
                                                    <div id="divNotifikasi" runat="server">
                                                        <div class="alert alert-warning" style="width: 100%;">
                                                            <strong>Ticket Number :
                                                                <asp:Label ID="LblTicketNumber" runat="server"></asp:Label></strong> Has been escalation.
                                                        </div>
                                                    </div>
                                                    <div id="divNotifikasiClosed" runat="server">
                                                        <div class="alert alert-warning">
                                                            <strong>Ticket Number :
                                                                <asp:Label ID="LblTicketNumberClosed" runat="server"></asp:Label></strong> Has been closed.
                                                        </div>
                                                    </div>
                                                    <div id="divEscalation" runat="server">
                                                        <div class="row">
                                                            <div class="col-md-4">
                                                                <label>Escalation To Layer</label>
                                                                <select class="Metropolis" id="cmbEscalationToLayer" name="cmbEscalationToLayer" onchange="selectUser()" style="width: 100%; height: 30px;">
                                                                    <option value="0" selected="selected">--Select--</option>
                                                                    <asp:Literal runat="server" ID="ltrEscalationLayer"></asp:Literal>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <br />
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <label>Escalation Reason</label>
                                                                <dx:ASPxMemo ID="TxtEscalationReason" runat="server" Width="100%" Rows="10" Theme="Metropolis" />
                                                            </div>
                                                        </div>
                                                        <br />
                                                        <div class="row">
                                                            <div class="col-md-8"></div>
                                                            <div class="col-md-2">
                                                                <dx:ASPxButton ID="BTN_Release" runat="server" Theme="Metropolis" Text="Submit" Width="100%" AutoPostBack="false"
                                                                    HoverStyle-BackColor="#EE4D2D" Height="30px">
                                                                    <ClientSideEvents Click="function(s, e) { execEscalation(); }" />
                                                                </dx:ASPxButton>
                                                            </div>
                                                            <div class="col-md-2">
                                                                <dx:ASPxButton ID="BTN_Cancel" runat="server" Theme="Metropolis" Text="Cancel" Width="100%" AutoPostBack="false"
                                                                    HoverStyle-BackColor="#EE4D2D" Height="30px">
                                                                    <ClientSideEvents Click="function(s, e) { execCancelTransaction(); }" />
                                                                </dx:ASPxButton>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </dx:PanelContent>
                                            </PanelCollection>
                                        </dx:ASPxCallbackPanel>
                                    </div>
                                </div>
                            </div>
                            <!-- /.row -->
                        </dx:PanelContent>
                    </PanelCollection>
                </dx:ASPxCallbackPanel>
            </dx:PopupControlContentControl>
        </ContentCollection>
    </dx:ASPxPopupControl>
    <dx:ASPxPopupControl ID="ASPxPopupControl1" ClientInstanceName="ASPxPopupControl1" runat="server" CloseAction="CloseButton" Modal="false"
        closeonescape="true" Height="660px" AllowResize="true" Width="1050px"
        PopupVerticalAlign="WindowCenter" PopupHorizontalAlign="WindowCenter" AllowDragging="True" Theme="SoftOrange" ShowPageScrollbarWhenModal="true" ScrollBars="Vertical"
        ShowFooter="True" HeaderText="Form Parent Child Ticket" FooterText="" AutoUpdatePosition="true" ClientSideEvents-CloseUp="function(s, e) { closeReloadPage(); }">
        <ContentCollection>
            <dx:PopupControlContentControl ID="PopupControlContentControl1" runat="server">
                <dx:ASPxCallbackPanel ID="ASPxCallbackPanel1" runat="server" Width="1000px" ShowLoadingPanel="false" ClientInstanceName="ASPxCallbackPanel1">
                    <PanelCollection>
                        <dx:PanelContent>
                            <div class="panel-tab clearfix">
                                <ul class="tab-bar">
                                    <li class="active"><a href="#Tab1" data-toggle="tab" onclick="showTransaction()"><i class="fa fa-file-text"></i>&nbsp;<strong>Data Transaction Ticket</strong>
                                        <span class="badge badge-warning" style="background-color: #FF8800;">
                                            <dx:ASPxLabel ID="dx_LabelTicketNumber" runat="server" Theme="Metropolis" Font-Size="10px" ForeColor="White" ClientIDMode="Static"></dx:ASPxLabel>
                                        </span>
                                    </a>
                                    </li>
                                    <li><a href="#Tab2" data-toggle="tab"><i class="fa fa-cogs"></i>&nbsp;<strong>Data Setting Query Ticket</strong></a></li>
                                    <li><a href="#Tab3" onclick="showParent()" data-toggle="tab"><i class="fa fa-sitemap"></i>&nbsp;<strong>Data Parent Child Ticket</strong></a></li>
                                </ul>
                            </div>
                            <div class="panel-body">
                                <div class="tab-content">
                                    <div class="tab-pane fade in active" id="Tab1">
                                        <div class="row">
                                            <div class="col-md-12" style="margin-left: -10px;">
                                                <dx:ASPxGridView ID="ASPxGridView1" ClientInstanceName="ASPxGridView1" runat="server" Styles-Header-Font-Bold="true" Font-Size="X-Small"
                                                    KeyFieldName="TicketNumber" Width="100%" AutoGenerateColumns="False" Theme="Metropolis" SettingsPager-PageSize="10">
                                                    <SettingsPager>
                                                        <AllButton Text="All">
                                                        </AllButton>
                                                        <NextPageButton Text="Next &gt;">
                                                        </NextPageButton>
                                                        <PrevPageButton Text="&lt; Prev">
                                                        </PrevPageButton>
                                                        <PageSizeItemSettings Visible="true" Items="10, 15, 20" ShowAllItem="true" />
                                                    </SettingsPager>
                                                    <SettingsEditing Mode="Inline" />
                                                    <SettingsCookies Enabled="false" StoreControlWidth="True" />
                                                    <SettingsBehavior EnableRowHotTrack="true" AllowSelectByRowClick="true" ConfirmDelete="true" AllowSelectSingleRowOnly="true" />
                                                    <Settings ShowFilterRow="True" ShowFilterRowMenu="false" ShowGroupPanel="true" ShowVerticalScrollBar="true" ShowHorizontalScrollBar="true" />
                                                    <Columns>
                                                        <dx:GridViewCommandColumn ShowSelectCheckbox="True" VisibleIndex="0" Width="30px" CellStyle-HorizontalAlign="Center"></dx:GridViewCommandColumn>
                                                        <dx:GridViewDataTextColumn FieldName="TicketNumber" Width="150" VisibleIndex="1" Settings-AutoFilterCondition="Contains" />
                                                        <dx:GridViewDataTextColumn FieldName="TicketSourceName" Caption="Channel" Width="150" />
                                                        <dx:GridViewDataTextColumn FieldName="GenesysNumber" Caption="Interaction ID" Width="200" Settings-AutoFilterCondition="Contains" />
                                                        <dx:GridViewDataTextColumn FieldName="ThreadID" Width="150" Settings-AutoFilterCondition="Contains" />
                                                        <dx:GridViewDataTextColumn FieldName="CustomerID" Width="150" Settings-AutoFilterCondition="Contains" />
                                                        <dx:GridViewDataTextColumn FieldName="Name" Width="200" Settings-AutoFilterCondition="Contains" />
                                                        <dx:GridViewDataTextColumn FieldName="NomorRekening" Width="150" Caption="Account Number" Settings-AutoFilterCondition="Contains" />
                                                        <dx:GridViewDataTextColumn FieldName="HP" Width="150" Caption="Phone Number" Settings-AutoFilterCondition="Contains" />
                                                        <dx:GridViewDataTextColumn FieldName="EMAIL" Width="150" Caption="Email Address" Settings-AutoFilterCondition="Contains" />
                                                        <dx:GridViewDataTextColumn FieldName="CIF" Width="150" Settings-AutoFilterCondition="Contains" />
                                                        <dx:GridViewDataTextColumn FieldName="NIK" Width="150" Settings-AutoFilterCondition="Contains" />
                                                        <dx:GridViewDataTextColumn FieldName="AccountInbound" Caption="Account" Width="150" Settings-AutoFilterCondition="Contains" />
                                                        <dx:GridViewDataTextColumn FieldName="SubCategory3Name" Caption="Reason" Width="300" Settings-AutoFilterCondition="Contains" />
                                                        <dx:GridViewDataMemoColumn FieldName="DetailComplaint" Caption="User Issue Remark" Width="300" />
                                                        <dx:GridViewDataTextColumn FieldName="Status" Width="160px" />
                                                        <dx:GridViewDataTextColumn FieldName="UserCreate" Width="150" />
                                                        <dx:GridViewDataTextColumn FieldName="DateCreate" Width="150" />
                                                    </Columns>
                                                    <ClientSideEvents SelectionChanged="ASPxGridView1_SelectionChanged" />
                                                </dx:ASPxGridView>
                                                <br />
                                                <dx:ASPxMemo ID="dxmReason" runat="server" Theme="Metropolis" Height="120" Width="100%" NullText="Reason Parent Ticket" ClientIDMode="Static" placeholder="Reason Parent Child Ticket"></dx:ASPxMemo>
                                                <br />
                                                <div class="row">
                                                    <div class="col-md-8">
                                                    </div>
                                                    <div class="col-md-2">
                                                        <dx:ASPxButton ID="ASPxButton1" runat="server" Theme="Metropolis" Text="Submit" Width="100%" AutoPostBack="false"
                                                            HoverStyle-BackColor="#EE4D2D" Height="30px">
                                                            <ClientSideEvents Click="function(s, e) { execParentNumber(); }" />
                                                        </dx:ASPxButton>
                                                    </div>
                                                    <div class="col-md-2">
                                                        <dx:ASPxButton ID="ASPxButton2" runat="server" Theme="Metropolis" Text="Close" Width="100%" AutoPostBack="false"
                                                            HoverStyle-BackColor="#EE4D2D" Height="30px">
                                                            <%--<ClientSideEvents Click="function(s, e) { execCancelParentNumber(); }" />--%>
                                                            <ClientSideEvents Click="function(s, e) { closeReloadPage(); }" />
                                                        </dx:ASPxButton>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="Tab2">
                                        <div class="row">
                                            <div class="col-md-12" style="margin-left: -10px;">
                                                <dx:ASPxGridView ID="GridView" ClientInstanceName="GridView" Width="100%" runat="server" AutoGenerateColumns="False" DataSourceID="ds_query" KeyFieldName="ID"
                                                    Theme="Metropolis" Styles-Header-Font-Bold="true" Font-Size="X-Small">
                                                    <SettingsPager>
                                                        <AllButton Text="All">
                                                        </AllButton>
                                                        <NextPageButton Text="Next &gt;">
                                                        </NextPageButton>
                                                        <PrevPageButton Text="&lt; Prev">
                                                        </PrevPageButton>
                                                        <PageSizeItemSettings Visible="true" Items="10, 15, 20" ShowAllItem="true" />
                                                    </SettingsPager>
                                                    <SettingsEditing Mode="Inline" />
                                                    <Settings ShowFilterRow="false" ShowFilterRowMenu="false" ShowFilterBar="Hidden" ShowVerticalScrollBar="false"
                                                        ShowGroupPanel="false" />
                                                    <SettingsBehavior ConfirmDelete="true" />
                                                    <Columns>
                                                        <dx:GridViewCommandColumn Caption="Action" HeaderStyle-HorizontalAlign="Center" VisibleIndex="0"
                                                            ButtonType="Image" FixedStyle="Left" Width="30px">
                                                            <EditButton Visible="true">
                                                                <Image ToolTip="Edit" Url="img/Icon/Text-Edit-icon2.png" />
                                                            </EditButton>
                                                            <NewButton Visible="true">
                                                                <Image ToolTip="New" Url="img/Icon/Apps-text-editor-icon2.png" />
                                                            </NewButton>
                                                            <DeleteButton Visible="false">
                                                                <Image ToolTip="Delete" Url="img/Icon/Actions-edit-clear-icon2.png" />
                                                            </DeleteButton>
                                                            <CancelButton Visible="true">
                                                                <Image ToolTip="Cancel" Url="img/icon/cancel1.png">
                                                                </Image>
                                                            </CancelButton>
                                                            <UpdateButton Visible="true">
                                                                <Image ToolTip="Update" Url="img/icon/Updated1.png" />
                                                            </UpdateButton>
                                                        </dx:GridViewCommandColumn>
                                                        <dx:GridViewDataTextColumn FieldName="ID" ReadOnly="True" Visible="false" VisibleIndex="1">
                                                            <EditFormSettings Visible="False" />
                                                        </dx:GridViewDataTextColumn>
                                                        <dx:GridViewDataComboBoxColumn Caption="Day" FieldName="Day" VisibleIndex="2">
                                                            <PropertiesComboBox>
                                                                <Items>
                                                                    <dx:ListEditItem Text="5" Value="5" />
                                                                    <dx:ListEditItem Text="10" Value="10" />
                                                                    <dx:ListEditItem Text="15" Value="15" />
                                                                    <dx:ListEditItem Text="20" Value="20" />
                                                                    <dx:ListEditItem Text="25" Value="25" />
                                                                    <dx:ListEditItem Text="30" Value="30" />
                                                                </Items>
                                                            </PropertiesComboBox>
                                                        </dx:GridViewDataComboBoxColumn>
                                                        <dx:GridViewDataDateColumn FieldName="FilterDate" Caption="Start Filter Date" VisibleIndex="3" Width="150px"></dx:GridViewDataDateColumn>
                                                    </Columns>
                                                    <Settings ShowGroupPanel="True" />
                                                </dx:ASPxGridView>
                                                <asp:SqlDataSource ID="ds_query" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>"
                                                    DeleteCommand="DELETE FROM [Temp_SettingHiStoryTransaction] WHERE [ID] = @ID"
                                                    SelectCommand="select * from Temp_SettingHiStoryTransaction where CreatedBy=@username and Type='ParentChild'"
                                                    UpdateCommand="UPDATE [Temp_SettingHiStoryTransaction] SET [Day] = @Day, [FilterDate] = @FilterDate, Type='ParentChild', [CreatedBy]=@username WHERE [ID] = @ID">
                                                    <SelectParameters>
                                                        <asp:Parameter Name="username" Type="String" />
                                                    </SelectParameters>
                                                    <DeleteParameters>
                                                        <asp:Parameter Name="ID" Type="Int32" />
                                                    </DeleteParameters>
                                                    <InsertParameters>
                                                        <asp:Parameter Name="Day" Type="String" />
                                                        <asp:Parameter Name="FilterDate" Type="String" />
                                                        <asp:Parameter Name="username" Type="String" />
                                                    </InsertParameters>
                                                    <UpdateParameters>
                                                        <asp:Parameter Name="Day" Type="String" />
                                                        <asp:Parameter Name="FilterDate" Type="String" />
                                                        <asp:Parameter Name="username" Type="String" />
                                                    </UpdateParameters>
                                                </asp:SqlDataSource>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="Tab3">
                                        <div class="row">
                                            <div class="col-md-12" style="margin-left: -10px;">
                                                <dx:ASPxGridView ID="ASPxGridView3" ClientInstanceName="ASPxGridView3" runat="server" Styles-Header-Font-Bold="true" Font-Size="X-Small"
                                                    KeyFieldName="ParentNumberID" Width="100%" AutoGenerateColumns="False" Theme="Metropolis" SettingsPager-PageSize="10"
                                                    OnCustomColumnGroup="ASPxGridView3_CustomColumnGroup" OnCustomGroupDisplayText="ASPxGridView3_CustomGroupDisplayText"
                                                    OnCustomColumnSort="ASPxGridView3_CustomColumnSort">
                                                    <SettingsPager>
                                                        <AllButton Text="All">
                                                        </AllButton>
                                                        <NextPageButton Text="Next &gt;">
                                                        </NextPageButton>
                                                        <PrevPageButton Text="&lt; Prev">
                                                        </PrevPageButton>
                                                        <PageSizeItemSettings Visible="true" Items="10, 15, 20" ShowAllItem="true" />
                                                    </SettingsPager>
                                                    <SettingsEditing Mode="Inline" />
                                                    <SettingsBehavior EnableRowHotTrack="true" AllowSelectByRowClick="true" ConfirmDelete="true" />
                                                    <Settings ShowFilterRow="false" ShowFilterRowMenu="false" ShowGroupPanel="true" ShowVerticalScrollBar="false" ShowHorizontalScrollBar="true" />
                                                    <Columns>
                                                        <dx:GridViewDataTextColumn Caption="Action" VisibleIndex="0" CellStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" Width="50px">
                                                            <DataItemTemplate>
                                                                <a href="#" onclick="ShowUpdateParentID('<%# Eval("TicketNumber")%>')">
                                                                    <asp:Image ImageUrl="img/icon/Actions-edit-clear-icon2.png" ID="Image1" runat="server" />
                                                                </a>
                                                            </DataItemTemplate>
                                                        </dx:GridViewDataTextColumn>
                                                        <dx:GridViewDataTextColumn FieldName="ParentNumberID" Caption="Parent Case Number" Width="150" GroupIndex="1" />
                                                        <dx:GridViewDataTextColumn FieldName="TicketNumber" Width="150" />
                                                        <dx:GridViewDataTextColumn FieldName="TicketSourceName" Caption="Channel" Width="150" />
                                                        <dx:GridViewDataTextColumn FieldName="GenesysID" Caption="Interaction ID" Width="200" />
                                                        <dx:GridViewDataTextColumn FieldName="ThreadID" Width="150" />
                                                        <dx:GridViewDataTextColumn FieldName="NIK" Caption="Customer ID" Width="150" />
                                                        <dx:GridViewDataTextColumn FieldName="Name" Width="200" />
                                                        <dx:GridViewDataTextColumn FieldName="NomorRekening" Width="150" Caption="Account Number" />
                                                        <dx:GridViewDataTextColumn FieldName="HP" Width="150" Caption="Phone Number" />
                                                        <dx:GridViewDataTextColumn FieldName="Email" Width="150" Caption="Email Address" />
                                                        <dx:GridViewDataTextColumn FieldName="CIF" Width="150" />
                                                        <dx:GridViewDataTextColumn FieldName="NoKTP" Width="150" />
                                                        <dx:GridViewDataTextColumn FieldName="SubCategory3Name" Caption="Reason" Width="300" />
                                                        <dx:GridViewDataMemoColumn FieldName="DetailComplaint" Caption="User Issue Remark" Width="300" />
                                                        <dx:GridViewDataMemoColumn FieldName="ResponseComplaint" Caption="Agent Response" Width="300" />
                                                        <dx:GridViewDataTextColumn FieldName="ParentReason" Caption="Parent Child Reason" Width="300" />
                                                        <dx:GridViewDataTextColumn FieldName="StatusInteraction" Caption="Status" Width="160px" />
                                                        <dx:GridViewDataTextColumn FieldName="UserCreate" Caption="Created Ticket By" Width="150" />
                                                        <dx:GridViewDataTextColumn FieldName="DateCreate" Caption="Created Date Ticket" Width="150" />
                                                        <dx:GridViewDataTextColumn FieldName="AgentCreate" Caption="Created Interaction By" Width="150" />
                                                        <dx:GridViewDataTextColumn FieldName="DateInteraction" Caption="Created Date Interaction" Width="150" />
                                                        <dx:GridViewDataTextColumn FieldName="ParentNumberCreated" Caption="Created Ticket Parent By" Width="150" />
                                                        <dx:GridViewDataTextColumn FieldName="ParentNumberDate" Caption="Created Date Parent" Width="150" />
                                                    </Columns>
                                                    <SettingsBehavior AutoExpandAllGroups="true" />
                                                    <Settings ShowGroupedColumns="True" />
                                                </dx:ASPxGridView>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </dx:PanelContent>
                    </PanelCollection>
                </dx:ASPxCallbackPanel>
            </dx:PopupControlContentControl>
        </ContentCollection>
    </dx:ASPxPopupControl>
    <dx:ASPxPopupControl ID="ASPxPopupControl2" ClientInstanceName="ASPxPopupControl2" runat="server" CloseAction="CloseButton" Modal="false"
        closeonescape="true" Height="150px" AllowResize="true" Width="250px"
        PopupVerticalAlign="WindowCenter" PopupHorizontalAlign="WindowCenter" AllowDragging="True" Theme="SoftOrange" ShowPageScrollbarWhenModal="true" ScrollBars="None"
        ShowFooter="True" HeaderText="Form Update Amount" FooterText="" AutoUpdatePosition="true">
        <ContentCollection>
            <dx:PopupControlContentControl ID="PopupControlContentControl2" runat="server">
                <dx:ASPxTextBox ID="TxtInputAmount" runat="server" Width="100%" Height="30px">
                    <ClientSideEvents TextChanged="price_InitAndKeyUp" ValueChanged="price_InitAndKeyUp" />
                </dx:ASPxTextBox>
            </dx:PopupControlContentControl>
        </ContentCollection>
    </dx:ASPxPopupControl>
</asp:Content>
