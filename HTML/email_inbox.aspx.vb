﻿Imports System.IO
Imports System.Data.SqlClient
Imports DevExpress.Web.ASPxHtmlEditor
'Imports System
'Imports System.Data
'Imports System.Drawing
'Imports System.Drawing.Drawing2D
Public Class email_inbox
    Inherits System.Web.UI.Page

    Dim loq As New cls_globe
    Dim connection As New ClsConn
    Dim sqldr As SqlDataReader
    Dim com As SqlCommand
    Dim query As String = String.Empty
    Dim con As New SqlConnection(ConfigurationManager.ConnectionStrings("DefaultConnection").ConnectionString)
    Dim EmailForm As String = ConfigurationManager.AppSettings("EmailForm")
    Dim emailid As String = String.Empty
    Dim generateemailid As String = String.Empty
    Dim sqlcom, comm As SqlCommand
    Dim sqlcon As New SqlConnection(ConfigurationManager.ConnectionStrings("DefaultConnection").ConnectionString)
    Dim connectionString As String = ConfigurationManager.ConnectionStrings("DefaultConnection").ConnectionString
    Dim connectionemail As SqlConnection

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("LoginType") = "layer1" Or Session("LoginType") = "layer3" Then
            sql_inbox.SelectCommand = "select * from v_emailin where AGENT='" & Session("username") & "' order by email_date desc"
        Else
            sql_inbox.SelectCommand = "select * from v_emailin order by email_date desc"
        End If

        If Request.QueryString("status") <> "" Then
            Status.Text = Request.QueryString("status").Substring(0, 1).ToUpper() + Request.QueryString("status").Substring(1)
        Else
            Status.Text = "Inbox"
        End If

        If Request.QueryString("status") = "compose" Then
            div_send.Visible = True
            ASPxGridView_Inbox.Visible = False
            ltr_attchment.Visible = False
            iframe_body.Visible = False
            btn_send.Visible = True
            btn_cancel.Visible = True
            div_attachment.Visible = False
            txt_body.Visible = False
            ASPxGridView_Queing.Visible = False
            ASPxGridView_Revert.Visible = False
            btn_revert.Visible = False
        ElseIf Request.QueryString("status") = "reply" Then
            Detail_Email()
            div_send.Visible = True
            ASPxGridView_Inbox.Visible = False
            ltr_attchment.Visible = True
            iframe_body.Visible = True
            btn_send.Visible = True
            btn_cancel.Visible = True
            txt_body.Visible = False
            ASPxGridView_Queing.Visible = False
            ASPxGridView_Revert.Visible = False
            btn_revert.Visible = False
        ElseIf Request.QueryString("status") = "detail" Then
            Detail_Email()
            div_send.Visible = True
            ASPxGridView_Inbox.Visible = False
            ltr_attchment.Visible = True
            iframe_body.Visible = True
            btn_send.Visible = False
            btn_cancel.Visible = False
            txt_body.Visible = False
            ASPxHtmlEditor1.Visible = False
            ASPxGridView_Queing.Visible = False
            ASPxGridView_Revert.Visible = False
            btn_revert.Visible = False
        ElseIf Request.QueryString("status") = "queing" Then
            div_send.Visible = False
            ASPxGridView_Inbox.Visible = False
            ltr_attchment.Visible = False
            iframe_body.Visible = False
            btn_send.Visible = False
            btn_cancel.Visible = False
            txt_body.Visible = False
            ASPxHtmlEditor1.Visible = False
            ASPxGridView_Queing.Visible = True
            ASPxGridView_Revert.Visible = False
            btn_revert.Visible = False
        ElseIf Request.QueryString("status") = "revert" Then
            div_send.Visible = False
            ASPxGridView_Inbox.Visible = False
            ltr_attchment.Visible = False
            iframe_body.Visible = False
            btn_send.Visible = False
            btn_cancel.Visible = False
            txt_body.Visible = False
            ASPxHtmlEditor1.Visible = False
            ASPxGridView_Queing.Visible = False
            ASPxGridView_Revert.Visible = True
            btn_revert.Visible = True
        Else
            div_send.Visible = False
            ASPxGridView_Queing.Visible = False
            ASPxGridView_Revert.Visible = False
            btn_revert.Visible = False
        End If
    End Sub

    Private Sub Detail_Email()
        Dim strSql As String = String.Empty
        strSql = "select * from v_emailin where IVC_ID='" & Request.QueryString("ivcid") & "'"
        sqldr = connection.ExecuteReader(strSql)
        Try
            If sqldr.HasRows() Then
                sqldr.Read()
                txt_to.Text = sqldr("EFROM").ToString
                txt_cc.Text = sqldr("ECC").ToString
                txt_subject.Text = sqldr("ESUBJECT").ToString
                emailid = sqldr("EMAIL_ID").ToString
            Else
            End If
            sqldr.Close()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
        Dim directoryPath As String = Server.MapPath(String.Format("~/HTML/attachment/receive/" + EmailForm + "/" & ReplaceSpecialLetterEmailid(emailid) & "/" & "file.html"))
        If Not File.Exists(directoryPath) Then
            ClientScript.RegisterStartupScript(Me.GetType(), "alert", "alert('email empty.');", True)
        Else
            ' file ada
            iframe_body.Src = "attachment/receive/" & EmailForm & "/" & ReplaceSpecialLetterEmailid(emailid) & "/file.html"
        End If

        Dim dataChart As String = String.Empty
        Dim strdata As String = "SELECT replace(URL,' ','%20') as url FROM icc_email_in_detail WHERE EMAIL_ID='" & Request.QueryString("emailid") & "'"
        Try
            sqldr = connection.ExecuteReader(strdata)
            If sqldr.HasRows() Then
                While sqldr.Read()
                    dataChart &= "<tr><td style='font-size: x-small;text-align:left;'><a href=attachment/receive/" & sqldr("URL").ToString & " target='_blank'>" & sqldr("URL").ToString & "</a></td></tr>"
                End While
                sqldr.Close()
            Else
                div_attachment.Visible = False
            End If
            ltr_attchment.Text = dataChart
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try

        btn_send.Visible = False
        btn_cancel.Visible = False
    End Sub

    Private Sub btn_send_ServerClick(sender As Object, e As EventArgs) Handles btn_send.ServerClick
        Dim filename As String = String.Empty
        Dim FolderName As String = DateTime.Now.ToString("yyyyMMddhhmmssfff")
        If Request.QueryString("status") = "compose" Then
            generateemailid = FolderName
        ElseIf Request.QueryString("status") = "reply" Then
            generateemailid = emailid
            Update_Email_IN(Request.QueryString("ivcid"))
        End If
        If uploadreply.HasFile = True Then
            If Request.QueryString("status") = "compose" Then
                Dim directoryPath As String = Server.MapPath(String.Format("~/HTML/attachment/receive/" + EmailForm + "/" & "/{0}/", FolderName.Trim()))
                If Not Directory.Exists(directoryPath) Then
                    Directory.CreateDirectory(directoryPath)
                Else
                    ' Folder Sudah Ada
                    ClientScript.RegisterStartupScript(Me.GetType(), "alert", "alert('Directory already exists.');", True)
                End If
            Else

            End If

            Dim Time As String = DateTime.Now.ToString("MMddhhmmssfff")
            filename = Path.GetFileName(uploadreply.FileName)
            Dim folderPath As String = Server.MapPath("~/HTML/attachment/receive/" + EmailForm + "/" + ReplaceSpecialLetterEmailid(generateemailid) + "/" + filename)
            Try
                uploadreply.SaveAs(folderPath)
            Catch ex As Exception
                Response.Write(DirectCast("", String))
            End Try

            Dim insertattchment As String = "insert into ICC_EMAIL_IN_DETAIL (EMAIL_ID, URL) VALUES ('" & generateemailid & "','" & EmailForm & "\" & ReplaceSpecialLetterEmailid(generateemailid) & "\" & filename & "')"
            com = New SqlCommand(insertattchment, con)
            Try
                con.Open()
                com.ExecuteNonQuery()
                con.Close()
            Catch ex As Exception
                Response.Write(DirectCast("", String))
            End Try
            loq.writedata(Session("username"), "email sending attachment", "Proses kirim attachment", insertattchment, "Channel Email Send Attachment email_inbox.aspx")

        Else

            filename = ""
            If Request.QueryString("status") = "compose" Then
                Dim directoryPath As String = Server.MapPath(String.Format("~/HTML/attachment/receive/" + EmailForm + "/" & "/{0}/", FolderName.Trim()))
                If Not Directory.Exists(directoryPath) Then
                    Directory.CreateDirectory(directoryPath)
                Else
                    ' Folder Sudah Ada
                    ClientScript.RegisterStartupScript(Me.GetType(), "alert", "alert('Directory already exists.');", True)
                End If
            Else

            End If
        End If

        Using mStream As MemoryStream = New MemoryStream()
            ASPxHtmlEditor1.Export(HtmlEditorExportFormat.Txt, mStream)
            Dim plainText As String = System.Text.Encoding.UTF8.GetString(mStream.ToArray())
            ASPxMemo1.Text = plainText
        End Using

        connectionemail = New SqlConnection(connectionString)
        comm = New SqlCommand()
        comm.Connection = connectionemail
        comm.CommandType = CommandType.StoredProcedure
        comm.CommandText = "sp_insert_send_email"
        comm.Parameters.Add("@emailid", Data.SqlDbType.VarChar).Value = generateemailid
        comm.Parameters.Add("@efrom", Data.SqlDbType.VarChar).Value = EmailForm
        comm.Parameters.Add("@eto", Data.SqlDbType.VarChar).Value = txt_to.Text
        comm.Parameters.Add("@ecc", Data.SqlDbType.VarChar).Value = txt_cc.Text
        comm.Parameters.Add("@esubject", Data.SqlDbType.VarChar).Value = txt_subject.Text
        comm.Parameters.Add("@bodytext", Data.SqlDbType.VarChar).Value = ASPxMemo1.Text
        comm.Parameters.Add("@bodyhtml", Data.SqlDbType.VarChar).Value = ASPxHtmlEditor1.Html
        comm.Parameters.Add("@path", Data.SqlDbType.VarChar).Value = filename
        comm.Parameters.Add("@agent", Data.SqlDbType.VarChar).Value = Session("username")
        comm.Parameters.Add("@jenisemail", Data.SqlDbType.VarChar).Value = Request.QueryString("status")
        comm.Parameters.Add("@jenisemailinternal", Data.SqlDbType.VarChar).Value = Session("lvluser")
        comm.Parameters.Add("@attachmentid", Data.SqlDbType.VarChar).Value = FolderName
        Try
            connectionemail.Open()
            sqldr = comm.ExecuteReader()
            connectionemail.Close()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try

        ASPxHtmlEditor1.Html = ""
        Response.Redirect("email_inbox.aspx?status=send&idpage=1010")
    End Sub

    Private Sub btn_cancelCompose_ServerClick(sender As Object, e As EventArgs) Handles btn_cancel.ServerClick
        Response.Redirect("email_inbox.aspx?status=inbox&idpage=1010")
    End Sub

    Private Sub ASPxGridView_Queing_Load(sender As Object, e As EventArgs) Handles ASPxGridView_Queing.Load
        SqlData_Queing.SelectCommand = "select * from v_EmailQueing order by Email_Date desc"
    End Sub

    Private Sub ASPxGridView_Revert_Load(sender As Object, e As EventArgs) Handles ASPxGridView_Revert.Load
        If Request.QueryString("email_id") <> "" Then
            query = "update ICC_EMAIL_IN set agent='', CNT='0' where EMAIL_ID='" & Request.QueryString("email_id") & "'"
            Try
                connection.ExecuteReader(query)
                Response.Redirect("email_inbox.aspx?status=revert&idpage=1010")
            Catch ex As Exception
                Response.Write(ex.Message)
            End Try
        End If
        SqlData_Revert.SelectCommand = "select * from v_EmailRevert order by Email_Date desc"
    End Sub

    Private Sub btn_revert_ServerClick(sender As Object, e As EventArgs) Handles btn_revert.ServerClick
        query = "exec SP_EMAIL_REVERT"
        Try
            connection.ExecuteReader(query)
            Response.Redirect("email_inbox.aspx?status=revert&idpage=1010")
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub



    Function Update_Email_IN(ByVal ivcid As String)
        Dim strsql As String = "update ICC_CHANNEL_HISTORY set Status='1' where channelid='" & ivcid & "'"
        'loq.writedata(Session("UserName"), "Update To do list", "Proses update channel history", strsql, "update ICC_CHANNEL_HISTORY")
        com = New SqlCommand(strsql, con)
        Try
            con.Open()
            com.ExecuteNonQuery()
            con.Close()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try


        Dim strsqldata As String = "update icc_email_in set flag='1' where ivc_id='" & ivcid & "'"
        'loq.writedata(Session("UserName"), "Update channel email in", "Proses update channel email in", strsqldata, "update icc_email_in")
        com = New SqlCommand(strsqldata, con)
        Try
            con.Open()
            com.ExecuteNonQuery()
            con.Close()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Function

    Public Function ReplaceSpecialLetterEmailid(ByVal str)
        Dim TmpStr As String
        TmpStr = str
        TmpStr = Replace(TmpStr, "ICC-", "")
        TmpStr = Replace(TmpStr, "'", "")
        TmpStr = Replace(TmpStr, ";", "")
        ReplaceSpecialLetterEmailid = TmpStr
    End Function

End Class