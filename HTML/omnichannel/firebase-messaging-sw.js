importScripts("https://www.gstatic.com/firebasejs/8.10.0/firebase-app.js");
importScripts("https://www.gstatic.com/firebasejs/8.10.0/firebase-messaging.js");

// Initialize the Firebase app in the service worker by passing in the
const firebaseConfig = {
    apiKey: "AIzaSyDG3DVEILnwTYdo1RPuCfaXRHoNRSyXnEM",
    authDomain: "web-chat-a7180.firebaseapp.com",
    projectId: "web-chat-a7180",
    storageBucket: "web-chat-a7180.appspot.com",
    messagingSenderId: "492304206438",
    appId: "1:492304206438:web:0dd178e41155a8133efba7",
};

firebase.initializeApp(firebaseConfig);

// Retrieve an instance of Firebase Messaging so that it can handle background
const messaging = firebase.messaging();

messaging.setBackgroundMessageHandler(function (payload) {
    console.log(
        "[firebase-messaging-sw.js] Received background message ",
        payload,
    );
    
    // Customize notification here
    const notificationTitle = payload.notification.title;
    const notificationOptions = {
        body: payload.notification.body,
        icon: "public/assets/img/mendawai.png",
    };

    return self.registration.showNotification(
        notificationTitle,
        notificationOptions,
    );
});