'use strict';

// from >>> config.js
// const url_api = "http://localhost:3001" 
// pcConfig

let localVideo = document.querySelector('#localVideo');
let remoteVideo = document.querySelector('#remoteVideo');
const callPanel = document.querySelector('#call-panel');
// const btnCall = document.querySelector('#btn-call');

let otherUser;
let remoteRTCMessage;
let iceCandidatesFromCaller = [];
let peerConnection;
let remoteStream;
let localStream;
let callInProgress = false;

// btnCall.addEventListener("click", () => {
//     callPanel.classList.add('d-block');
//     console.log(callPanel);
// });

// Set up audio and video regardless of what devices are present.
let sdpConstraints = {
    offerToReceiveAudio: true,
    offerToReceiveVideo: true
};


// let socket;
function connectSocket(socket) {
    localStorage.removeItem('ActiveCall'); //clear

    socket.on('newCall', data => {
        console.log(data);
        otherUser = data.caller;
        remoteRTCMessage = data.rtcMessage;
        localStorage.setItem('ActiveCall', JSON.stringify(data));

        Swal.fire({
            title: 'Incomming Call',
            icon: 'info',
            showCancelButton: true,
            confirmButtonText: 'Answer',
        }).then((result) => {
            if (result.isConfirmed) {
                answer();
            }
            else {
                hangup();
            }
        })
    })

    socket.on('callAnswered', data => {
        //when other accept our call
        remoteRTCMessage = data.rtcMessage
        peerConnection.setRemoteDescription(new RTCSessionDescription(remoteRTCMessage));
        console.log("Call Started. They Answered");
        callProgress()
    })

    socket.on("return-hangup", data => {
        console.log("Call hangup");
        hangup();
    });

    socket.on('ICEcandidate', data => {
        console.log("GOT ICE candidate");
        let message = data.rtcMessage
        let candidate = new RTCIceCandidate({
            sdpMLineIndex: message.label,
            candidate: message.candidate
        });

        if (peerConnection) {
            console.log("ICE candidate Added");
            peerConnection.addIceCandidate(candidate);
        } else {
            console.log("ICE candidate Pushed");
            iceCandidatesFromCaller.push(candidate);
        }

    })

}


//event from html
async function call() {
    const ActiveCustomer = localStorage.getItem('ActiveCustomer');
    const data = JSON.parse(ActiveCustomer);

    if (data) {
        document.getElementById("callName").innerHTML = 'On call : ' + data.name;
        otherUser = data.name;
        await beReady()
            .then(bool => {
                processCall(data.name)
            })
    } else {
        Swal.fire({
            title: 'Call Failed',
            text: 'Please select callee name.',
            icon: 'warning',
            confirmButtonText: 'Try Again',
        })
    }
}

//event from html
function answer() {
    //do the event firing
    callPanel.classList.add('d-block');
    beReady()
        .then(bool => {
            processAccept();
        })
}

function sendCall(data) {
    //to send a call
    console.log("Send Call");
    socket.emit("call", data);
}


function answerCall(data) {
    //to answer a call
    socket.emit("answerCall", data);
    callProgress();
}

function sendICEcandidate(data) {
    //send only if we have caller, else no need to
    console.log("Send ICE candidate");
    socket.emit("ICEcandidate", data)
}

async function beReady() {
    return await navigator.mediaDevices.getUserMedia({
        audio: true,
        video: true
    })
        .then(stream => {
            localStream = stream;
            localVideo.srcObject = stream;

            return createConnectionAndAddStream()
        })
        .catch(function (e) {
            alert('getUserMedia() error: ' + e.name);
        });
}

function createConnectionAndAddStream() {
    createPeerConnection();
    peerConnection.addStream(localStream);
    return true;
}

function processCall(userName) {
    peerConnection.createOffer((sessionDescription) => {
        peerConnection.setLocalDescription(sessionDescription);
        sendCall({
            username: userName,
            rtcMessage: sessionDescription
        })
    }, (error) => {
        console.log("Error");
    });
}

function processAccept() {
    peerConnection.setRemoteDescription(new RTCSessionDescription(remoteRTCMessage));
    peerConnection.createAnswer((sessionDescription) => {
        peerConnection.setLocalDescription(sessionDescription);

        if (iceCandidatesFromCaller.length > 0) {
            //I am having issues with call not being processed in real world (internet, not local)
            //so I will push iceCandidates I received after the call arrived, push it and, once we accept
            //add it as ice candidate
            //if the offer rtc message contains all thes ICE candidates we can ingore this.
            for (let i = 0; i < iceCandidatesFromCaller.length; i++) {
                let candidate = iceCandidatesFromCaller[i];
                console.log("ICE candidate Added From queue");
                try {
                    peerConnection.addIceCandidate(candidate).then(done => {
                        console.log(done);
                    }).catch(error => {
                        console.log(error);
                    })
                } catch (error) {
                    console.log(error);
                }
            }
            iceCandidatesFromCaller = [];
            console.log("ICE candidate queue cleared");
        } else {
            console.log("NO Ice candidate in queue");
        }

        answerCall({
            caller: otherUser,
            rtcMessage: sessionDescription
        })

    }, (error) => {
        console.log("Error");
    })
}

/////////////////////////////////////////////////////////

function createPeerConnection() {
    try {
        peerConnection = new RTCPeerConnection(pcConfig);
        // peerConnection = new RTCPeerConnection();
        peerConnection.onicecandidate = handleIceCandidate;
        peerConnection.onaddstream = handleRemoteStreamAdded;
        peerConnection.onremovestream = handleRemoteStreamRemoved;
        console.log('Created RTCPeerConnnection');
        return;
    } catch (e) {
        console.log('Failed to create PeerConnection, exception: ' + e.message);
        alert('Cannot create RTCPeerConnection object.');
        return;
    }
}

function handleIceCandidate(event) {
    // console.log('icecandidate event: ', event);
    if (event.candidate) {
        console.log("Local ICE candidate");
        // console.log(event.candidate.candidate);

        sendICEcandidate({
            username: otherUser,
            rtcMessage: {
                label: event.candidate.sdpMLineIndex,
                id: event.candidate.sdpMid,
                candidate: event.candidate.candidate
            }
        })

    } else {
        console.log('End of candidates.');
    }
}

function handleRemoteStreamAdded(event) {
    console.log('Remote stream added.');
    remoteStream = event.stream;
    remoteVideo.srcObject = remoteStream;
}

function handleRemoteStreamRemoved(event) {
    console.log('Remote stream removed. Event: ', event);
    remoteVideo.srcObject = null;
    localVideo.srcObject = null;
}

window.onbeforeunload = function () {
    if (callInProgress) {
        stop();
    }
};


async function stop() {
    localStream.getTracks().forEach(track => track.stop());
    callInProgress = false;
    if (peerConnection) {
        peerConnection.close();
    }
    peerConnection = null;
    remoteVideo.srcObject = null;
    localVideo.srcObject = null;
    otherUser = null;

}

async function hangup() {
    console.log('Hanging up.');
    const data = JSON.parse(localStorage.getItem('ActiveCall'));
    // callPanel.classList.remove('d-block');

    socket.emit("hangup", {
        call_id: data?.call_id,
        customer_id: data?.customer_id,
        username: otherUser,
        rtcMessage: remoteRTCMessage
    });
    await stop();
    await CreateTicket(data);
    await getCallHistory();
}

function callProgress() {
    callInProgress = true;
}

async function CreateTicket(value) {
    try {
        console.log(value);
        window.open(`../mainframe.aspx?agentid=${value.agent}&channel=call&genesysid=${value.call_id}&customerid=${value.customer_id}&account=${value.email}&accountid=${value.customer_id}&subject=&phoneChat=&emailaddress=${value.email}&source=inbound&threadid=-`);
    } catch (error) {
        console.log(error);
    }
}

async function getCallHistory() {
    const ActiveCustomer = localStorage.getItem('ActiveCustomer');
    const data = JSON.parse(ActiveCustomer);
    if (data) {
        const response = await fetch(`${url_api}/omnichannel/list_call`, {
            method: "POST",
            headers: { "Content-Type": "application/json" },
            body: JSON.stringify({
                "agent": data.agent_handle,
                "customer_id": data.customer_id,
            }),
        });
        let result = await response.json();
        let row = result.data;

        let html = '';
        if (result.status === 200) {
            for (let i = 0; i < row.length; i++) {
                html += `<div class="card p-2 border mb-2">
                    <div class="d-flex align-items-center">
                        <div class="flex-shrink-0 avatar-xs ms-1 me-3">
                            <div class="avatar-title bg-soft-primary text-primary rounded-circle">
                                <i class="bx bxs-phone-call"></i>
                            </div>
                        </div>
                        <div class="flex-grow-1 overflow-hidden">
                            <h5 class="font-size-14 text-truncate mb-1">${row[i].Email}</h5>
                            <p class="text-muted font-size-13 mb-0">${row[i].TALK_TIME}</p>
                        </div>
                        <div class="flex-shrink-0 ms-3">
                            <div class="d-flex gap-2">
                                <div>
                                    <a href="#" class="text-muted px-1" title="Info Detail">
                                        <i class="bx bxs-info-circle"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>`;
            }
            document.getElementById('list-call-history').innerHTML = html;
        }
    }

}
// getCallHistory();
