const soundMessage = new Audio('omnichannel/assets/sound/MESSAGE.mp3');
const websocket = io(url_api);
const AgentName = document.getElementById("TrxUserName").value;
const TotalNotif = document.getElementById("TotalNotif");
const ListNotification = document.getElementById("ListNotification");
console.log(AgentName)

websocket.auth = {
    username: AgentName,
    flag_to: 'agent',
}
websocket.connect();
websocket.on('connect', function () {
    console.info(new Date().toLocaleString() + ' : id = ' + websocket.id);
    AskPermission();
    LoadListNotification();
});
websocket.on('return-inbox-email', async function (data) {
    data.channel = 'Email';
    data.name = data.efrom;
    data.message = data.esubject;
    WebNotification(data);
    ShowNotification(data.efrom, data.esubject);
    LoadListNotification();
});

websocket.on('send-message-customer', async function (data) {
    // push from blending
    WebNotification(data);
    ShowNotification(data.name, data.message);
    LoadListNotification();
});

websocket.on('return-message-customer', async function (data) {
    WebNotification(data);
    ShowNotification(data.name, data.message);
    LoadListNotification();
});

websocket.on('return-message-whatsapp', async function (data) {
    WebNotification(data);
    ShowNotification(data.name, data.message);
    LoadListNotification();
});

websocket.on('return-directmessage-twitter', async function (data) {
    WebNotification(data);
    ShowNotification(data.name, data.message);
    LoadListNotification();
});


function AskPermission() {
    try {
        Notification.requestPermission().then((permission) => {
            console.log(`Notification permission : ${permission}`);
        });
    }
    catch (e) {
        return false;
    }
    return true;
}

function ShowNotification(Title, Body) {
    function PushNotification() {
        let options = {
            body: `${Body}`,
            icon: 'assets/img/mendawai.png',
            tag: Body
        }
        const notif = new Notification(Title, options);
        notif.onclick = function (event) {
            event.preventDefault();
            // location.href = `${DomainUrl}/inhealth/HTML/TrxShowTicket.aspx?ticketid=${Title}`;
        }
        soundMessage.play();
    }


    if (Notification.permission === 'granted') {
        PushNotification();
    } else if (Notification.permission !== 'denied') {
        Notification.requestPermission().then((permission) => {
            if (permission === 'granted') {
                PushNotification();
            }
        });
    }
}

function WebNotification(data) {
    Swal.fire({
        title: `${data.name} - ${data.channel}`,
        text: data.message,
        icon: 'info',
        position: 'top-end',
        showConfirmButton: false,
        timer: 3000,
        toast: true,
        timerProgressBar: true
    });
}

async function LoadListNotification(){
    try {
        const res = await fetch(`${url_api}/omnichannel/notification?agent_name=${AgentName}`, {
            method: 'GET',
            headers: {
                "Content-Type" : "application/json"
            }
        });
        const json = await res.json();
        const row = json.data;

        let html = "";
            TotalNotif.innerHTML = ""; //clear 
            TotalNotif.innerHTML = row.length; 
            // <img src="${icon}" alt="User Avatar">
            html += `<li><a>You have <span class="badge badge-info">${row.length}</span> unread messages.</a></li>`;

        if(row.length > 0){
            for (i = 0; i < row.length; i++) {
                html += `<li>
                    <a class="clearfix" href="chat_live.aspx?idpage=2027">
                        <div class="detail">
                            <strong>${row[i].name} - <span class="label label-default">${row[i].channel}</span></strong>
                            <p class="no-margin text-nowrap" style="width:150px;">${row[i].message}</p>
                            <small class="text-muted"><i class="fa fa-check text-success"></i> ${row[i].date_create}</small>
                        </div>
                    </a>	
                </li>`;
            }
            ListNotification.innerHTML = ""; //clear the tbody
            ListNotification.innerHTML += html;
        }
    } catch (error) {
        console.log(error);
    }

}

