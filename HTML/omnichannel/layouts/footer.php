<!-- JAVASCRIPT -->
<script src="assets/js/bootstrap.bundle.min.js"></script>
    <script src="assets/js/simplebar.min.js"></script>
    <script src="assets/js/waves.min.js"></script>
    <!-- glightbox js -->
    <script src="assets/js/glightbox.min.js"></script>
    <!-- Swiper JS -->
    <script src="assets/js/swiper-bundle.min.js"></script>
    <!-- fg-emoji-picker JS -->
    <script src="assets/js/fgEmojiPicker.js"></script>
    <script src="assets/sweetalert/sweetalert2.all.min.js"></script>
    <!-- page init -->
    <script src="assets/js/app.js"></script>
    <script src="assets/js/init.js"></script>

    <!-- scripts -->
    <script src="http://localhost:3001/socket.io/socket.io.js"></script>
    <!-- <script src="http://app.mendawai.com:3500/socket.io/socket.io.js"></script> -->
    <script src="scripts/config.js"></script>
    <!-- <script src="scripts/notification.js"></script> -->
    <script src="scripts/call_webrtc.js"></script>
    <script src="scripts/main.js"></script>

    <!-- <script src="https://www.gstatic.com/firebasejs/8.10.0/firebase-app.js"></script> -->
    <!-- <script src="https://www.gstatic.com/firebasejs/8.10.0/firebase-messaging.js"></script> -->

</body>

</html>