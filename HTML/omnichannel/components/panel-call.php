<div class="user-profile-sidebar" id="call-panel">
    <div class="p-3 border-bottom">
        <div class="user-chat-nav p-2">
            <div class="d-flex w-100">
                <div class="flex-grow-1">
                    <button type="button" class="btn nav-btn d-block d-lg-block call-panel-show">
                        <i class="bx bx-x"></i>
                    </button>
                </div>
                <h5 class="flex-shrink-1 font-size-11 text-uppercase text-muted mt-3 mr-4" id="callName"></h5>
                <div class="flex-shrink-0">
                    <div class="dropdown">
                        <button class="btn nav-btn dropdown-toggle" type="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class='bx bx-dots-vertical-rounded'></i>
                        </button>
                        <div class="dropdown-menu dropdown-menu-end">
                            <a class="dropdown-item d-flex justify-content-between align-items-center d-lg-none user-profile-show" href="#">View Profile <i class="bx bx-user text-muted"></i></a>
                            <a class="dropdown-item d-flex justify-content-between align-items-center" href="#">Archive <i class="bx bx-archive text-muted"></i></a>
                            <a class="dropdown-item d-flex justify-content-between align-items-center" href="#">Muted <i class="bx bx-microphone-off text-muted"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="user-profile-img">
            <video id="localVideo" class="rounded" autoplay muted playsinline style="position: absolute;z-index:1; top: 0; left: 0;width: 100px; height:100px; border: 1px solid"></video>
            <video id="remoteVideo" class="profile-img rounded" autoplay playsinline style="border: 1px solid"></video>
        </div>
    </div>
    <!-- End profile user -->

    <!-- Start user-profile-desc -->
    <div class="text-center border-bottom">
        <div class="row">
            <div class="col-sm col-4">
                <div class="mb-4">
                    <button type="button" class="btn avatar-sm p-0" onclick="call()">
                        <span class="avatar-title rounded bg-light text-body">
                            <i class="bx bxs-phone-call"></i>
                        </span>
                    </button>
                    <h5 class="font-size-11 text-uppercase text-muted mt-2">Call</h5>
                </div>
            </div>
            <div class="col-sm col-4">
                <div class="mb-4">
                    <button type="button" class="btn avatar-sm p-0" onclick="hangup()">
                        <span class="avatar-title rounded bg-light text-body">
                            <i class="bx bx-phone-off"></i>
                        </span>
                    </button>
                    <h5 class="font-size-11 text-uppercase text-muted mt-2">Hang Up</h5>
                </div>
            </div>
        </div>
    </div>

    <div class="p-4 user-profile-desc" data-simplebar>
        <div class="simplebar-wrapper" style="margin: -24px;">
            <div class="simplebar-height-auto-observer-wrapper">
                <div class="simplebar-height-auto-observer"></div>
            </div>
            <div class="simplebar-mask">
                <div class="simplebar-offset" style="right: 0px; bottom: 0px;">
                    <div class="simplebar-content-wrapper" style="height: 100%; overflow: hidden scroll;">
                        <div class="simplebar-content" style="padding: 24px;">
                            <div>
                                <div>
                                    <h5 class="font-size-11 text-muted text-uppercase mb-3">Call History</h5>
                                </div>

                                <div id="list-call-history">
                                    <!-- <div class="card p-2 border mb-2">
                                        <div class="d-flex align-items-center">
                                            <div class="flex-shrink-0 avatar-xs ms-1 me-3">
                                                <div class="avatar-title bg-soft-primary text-primary rounded-circle">
                                                    <i class="bx bxs-phone-call"></i>
                                                </div>
                                            </div>
                                            <div class="flex-grow-1 overflow-hidden">
                                                <h5 class="font-size-14 text-truncate mb-1">design-phase-1-approved.pdf</h5>
                                                <p class="text-muted font-size-13 mb-0">12.5 MB</p>
                                            </div>

                                            <div class="flex-shrink-0 ms-3">
                                                <div class="d-flex gap-2">
                                                    <div>
                                                        <a href="#" class="text-muted px-1">
                                                            <i class="bx bxs-download"></i>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div> -->

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="simplebar-placeholder" style="width: auto; height: 1096px;"></div>
        </div>
        <div class="simplebar-track simplebar-horizontal" style="visibility: hidden;">
            <div class="simplebar-scrollbar" style="width: 0px; display: none;"></div>
        </div>
        <div class="simplebar-track simplebar-vertical" style="visibility: visible;">
            <div class="simplebar-scrollbar" style="height: 74px; display: block; transform: translate3d(0px, 184px, 0px);">
            </div>
        </div>
    </div>
</div>