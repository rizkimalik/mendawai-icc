<div class="user-profile-sidebar" id="user-profile">
    <div class="p-4 user-profile-desc" data-simplebar>
        <div class="mt-auto pb-4">
            <div class="d-flex align-items-end">
                <div class="flex-grow-1">
                    <h5 class="user-name mb-1 text-truncate"><span id="customer_name"></span></h5>
                    <p class="font-size-14 text-truncate mb-0"><i class="bx bxs-circle font-size-10 text-success me-1 ms-0"></i> online - ID:<span id="chat_id"></span></p>
                </div>
                <div class="flex-shrink-0">
                    <!-- <button type="button" class="btn btn-sm btn-soft-primary">Edit</button> -->
                    <button type="button" class="btn btn-sm btn-soft-danger user-profile-show">Close</button>
                </div>
            </div>
        </div>
        <div class="pb-2">
            <h5 class="font-size-11 text-uppercase mb-2">Info Detail:</h5>
            <div class="mb-3">
                <p class="text-muted font-size-14 mb-1">Customer ID</p>
                <h5 class="font-size-14 mb-0"><span id="customer_id"></span></h5>
            </div>
            <div class="mb-3">
                <p class="text-muted font-size-14 mb-1">Identity User</p>
                <h5 class="font-size-14"><span id="user_id"></span></h5>
            </div>
            <div class="mb-3">
                <p class="text-muted font-size-14 mb-1">Channel</p>
                <h5 class="font-size-14 mb-0"><span id="channel"></span></h5>
            </div>
            <div class="mb-3">
                <p class="text-muted font-size-14 mb-1">Page Name</p>
                <h5 class="font-size-14"><span id="page_name"></span></h5>
            </div>
        </div>

        <hr class="my-4">
        <div>
            <h5 class="font-size-11 text-muted text-uppercase mb-3">Attached Files</h5>
            <div>
                <!-- <div class="card p-2 border mb-2">
                    <div class="d-flex align-items-center">
                        <div class="flex-shrink-0 avatar-xs ms-1 me-3">
                            <div class="avatar-title bg-soft-primary text-primary rounded-circle">
                                <i class="bx bx-file"></i>
                            </div>
                        </div>
                        <div class="flex-grow-1 overflow-hidden">
                            <h5 class="font-size-14 text-truncate mb-1">design-phase-1-approved.pdf</h5>
                            <p class="text-muted font-size-13 mb-0">12.5 MB</p>
                        </div>

                        <div class="flex-shrink-0 ms-3">
                            <div class="d-flex gap-2">
                                <div>
                                    <a href="#" class="text-muted px-1">
                                        <i class="bx bxs-download"></i>
                                    </a>
                                </div>
                                <div class="dropdown">
                                    <a class="dropdown-toggle text-muted px-1" href="#" role="button"
                                        data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <i class="bx bx-dots-horizontal-rounded"></i>
                                    </a>
                                    <div class="dropdown-menu dropdown-menu-end">
                                        <a class="dropdown-item d-flex align-items-center justify-content-between"
                                            href="#">Delete <i class="bx bx-trash ms-2 text-muted"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> -->
            </div>
        </div>
    </div>
</div>