﻿Imports System
Imports System.Web.UI
Imports Microsoft.VisualBasic
Imports System.IO
Imports System.Data
Imports System.Data.SqlClient
Imports DevExpress.Web.ASPxUploadControl
Imports DevExpress.Web.ASPxClasses.Internal
Imports DevExpress.Web.ASPxGridView
Imports Microsoft.VisualBasic.Logging
Imports DevExpress.Web.ASPxEditors
Public Class ticket_create
    Inherits System.Web.UI.Page

    Dim execute, strExecute, Proses As New ClsConn
    Dim sqldr, readPhone, readAccount, readPhatHTML, readGroupID As SqlDataReader
    Dim data As String
    Dim sqlConn As New SqlConnection(ConfigurationManager.ConnectionStrings("DefaultConnection").ConnectionString)
    Dim sqlComm As SqlCommand
    Dim PathTransactionTicket As String = ConfigurationManager.AppSettings("PathTransactionTicket")
    Dim strLogTime As String = DateTime.Now.ToString("yyyy")
    Dim strSql As String = String.Empty
    Dim _ClassFunction As New WebServiceTransaction
    Dim strDocument As String = ConfigurationManager.AppSettings("UconConvert")

    Private Sub mainframe_Load(sender As Object, e As EventArgs) Handles Me.Load

        If Me.IsPostBack Then
            TabName.Value = Request.Form(TabName.UniqueID)
            Response.Write(TabName.Value)
        End If

        ThreadCategory()
        EscalationCheck()
        'ThreadInsertTransaction(Request.QueryString("channel"), Request.QueryString("threadid"), Request.QueryString("genesysid"), Request.QueryString("account"), Request.QueryString("accountid"), Request.QueryString("agentid"), Request.QueryString("subject"), Request.QueryString("PhoneChat"))
        Dim valCustomerid As String = Request.QueryString("customerid")
        If Request.QueryString("source") = "thread" Then
            If valCustomerid = "" Then
                ThreadCustomerData(Request.QueryString("account"), "inbound")
            Else
                ThreadCustomerData(Request.QueryString("customerid"), "thread")
            End If
        ElseIf valCustomerid <> "" Then
            ThreadCustomerData(Request.QueryString("customerid"), "thread")
        Else
            ThreadCustomerData(Request.QueryString("account"), "inbound")
        End If
        ThreadReload()
        TrxUserName.Value = Session("username")
        _settingQueryTransaction()
        'If (Session("ROLE") = "Layer 1" Or Session("ROLE") = "Layer 2" Or Session("ROLE") = "Layer 2 TL") Then
        If (Session("ROLE") = "Layer 1" Or Session("ROLE") = "Layer 2") Then
            AddTicket.Visible = True
        Else
            AddTicket.Visible = False
        End If

    End Sub
    Private Sub ThreadCategory()
        litTrx.Text &= ""
        Dim Data, str As String
        Data = "<font style='background : orange;'></font>"
        str = "select * from mcategory where NA='Y'"
        Try
            sqlComm = New SqlCommand(str, sqlConn)
            sqlConn.Open()
            sqldr = sqlComm.ExecuteReader()
            While sqldr.Read()
                Data &= "<option value='" & sqldr("CategoryID").ToString & "' >" & sqldr("Name").ToString & "</option>"
            End While
            sqldr.Close()
            sqlConn.Close()
            litTrx.Text = Data
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
        TxtPenerimaPengaduan.Text = Session("username")
        hd_username.Value = Session("username")
    End Sub
    Private Sub ThreadGenarate()
        TxtGenesysNumberID.Text = New Random().Next(1000000, 9999999)  'Request.QueryString("i")
        TxtThreadID.Text = New Random().Next(100000, 999999) 'Request.QueryString("t")
    End Sub
    Private Sub ThreadReload()
        TxtType.Text = Request.QueryString("channel")
        TxtGenesysNumberID.Text = Request.QueryString("genesysid")
        TxtThreadID.Text = Request.QueryString("threadid")
        TxtAccount.Text = Request.QueryString("account")
        TxtAccountID.Text = Request.QueryString("accountid")
        TxtSubject.Text = Request.QueryString("subject")
        TxtThreadTicket.Text = GetValueThreadTicketID(Request.QueryString("genesysid"), Session("username"))
    End Sub
    Private Sub EscalationCheck()
        litEscalationNew.Text &= ""
        Dim Data, str As String
        Dim TrxLayer As String = String.Empty
        If Session("LoginTypeAngka") = "2" Then
            TrxLayer = "7"
        Else
            TrxLayer = Session("LoginTypeAngka")
        End If
        str = "exec Temp_SP_Workflow '" & TrxLayer & "'"
        Try
            sqlComm = New SqlCommand(str, sqlConn)
            sqlConn.Open()
            sqldr = sqlComm.ExecuteReader()
            While sqldr.Read()
                Data &= " <option value='" & sqldr("LayerUser").ToString & "' >Layer " & sqldr("LayerUser").ToString & "</option>"
            End While
            sqldr.Close()
            sqlConn.Close()
            litEscalationNew.Text = Data
            _ClassFunction.LogSuccess(strLogTime, str)
        Catch ex As Exception
            _ClassFunction.LogError(strLogTime, ex, str)
            Response.Write(ex.Message)
        End Try
    End Sub
    Function EscalationTransaction(ByVal TicketNumber As String)
        Dim statusTicket As String = String.Empty
        Dim dataPosition As String = String.Empty
        Dim labelDispatch As String = String.Empty
        Dim strdata As String = String.Empty
        Dim LayerCreateBy As String = String.Empty
        Try
            sqlConn.Open()
            strdata = "select * from tTicket where TicketNumber='" & TicketNumber & "'"
            sqlComm = New SqlCommand(strdata, sqlConn)
            sqldr = sqlComm.ExecuteReader()
            If sqldr.HasRows Then
                sqldr.Read()
                statusTicket = sqldr("Status").ToString
                dataPosition = sqldr("TicketPosition").ToString
                LayerCreateBy = sqldr("LayerCreateBy").ToString
            End If
            sqldr.Close()
            sqlConn.Close()
            _ClassFunction.LogSuccess(strLogTime, strdata)
        Catch ex As Exception
            _ClassFunction.LogError(strLogTime, ex, strdata)
            Response.Write(ex.Message)
        End Try

        Dim TrxLayer As String = String.Empty
        If Session("LoginTypeAngka") = "2" Then
            If LayerCreateBy = Session("LoginTypeAngka") Then
                TrxLayer = "7"
            Else
                TrxLayer = Session("LoginTypeAngka")
            End If
        Else
            TrxLayer = Session("LoginTypeAngka")
        End If
        Dim strSql As String = String.Empty
        Dim strTemp As String = String.Empty
        ltrEscalationLayer.Text &= ""
        strSql = "exec Temp_SP_Workflow '" & TrxLayer & "'"
        Try
            sqldr = Proses.ExecuteReader(strSql)
            While sqldr.Read()
                strTemp &= " <option value='" & sqldr("LayerUser").ToString & "' >Layer " & sqldr("LayerUser").ToString & "</option>"
            End While
            sqldr.Close()
            ltrEscalationLayer.Text = strTemp
            _ClassFunction.LogSuccess(strLogTime, strSql)
        Catch ex As Exception
            _ClassFunction.LogError(strLogTime, ex, strSql)
            Response.Write(ex.Message)
        End Try

        If Session("LoginTypeAngka") = dataPosition Then
            If (statusTicket = "Closed" Or statusTicket = "Solved") Then
                divEscalation.Visible = False
                divNotifikasi.Visible = False
                divNotifikasiClosed.Visible = True
            Else
                divNotifikasi.Visible = False
                divNotifikasiClosed.Visible = False
                divEscalation.Visible = True
            End If
        Else
            If (statusTicket = "Closed" Or statusTicket = "Solved") Then
                divEscalation.Visible = False
                divNotifikasi.Visible = False
                divNotifikasiClosed.Visible = True
            Else
                divEscalation.Visible = False
                divNotifikasiClosed.Visible = False
                divNotifikasi.Visible = True
            End If
        End If
        LblTicketNumber.Text = TicketNumber
        LblTicketNumberClosed.Text = TicketNumber
    End Function
    Function ThreadCustomerData(ByVal _ValueAccount As String, ByVal _ValueSource As String)
        If _ValueSource = "thread" Then
            ThreadCustomerReadCustomerID(_ValueAccount)
        Else
            If ThreadCustomerCountingAccount(_ValueAccount) = True Then
                ThreadCustomerRead(_ValueAccount)
            Else
                If Request.QueryString("n") = "1" Then
                    ASPxPopupControl4.ShowOnPageLoad = False
                Else
                    TrxAction.Value = "insert"
                    ASPxPopupControl4.ShowOnPageLoad = True
                End If
                If Request.QueryString("channel") = "voice" Or Request.QueryString("channel") = "Voice" Then
                    TxtCustomerPhone.Text = _ValueAccount
                ElseIf Request.QueryString("channel") = "email" Or Request.QueryString("channel") = "Email" Then
                    TxtCustomerEmail.Text = _ValueAccount
                ElseIf Request.QueryString("channel") = "chat" Or Request.QueryString("channel") = "Chat" Then
                    TxtCustomerEmail.Text = _ValueAccount
                Else
                    If Request.QueryString("phoneChat") <> "" Then
                        TxtCustomerEmail.Text = Request.QueryString("emailaddress")
                        TxtCustomerPhone.Text = _ValueAccount
                    Else
                        TxtCustomerEmail.Text = _ValueAccount
                        TxtCustomerPhone.Text = Request.QueryString("phoneChat")
                    End If
                End If
                TrxAction.Value = "insert"
            End If
        End If
    End Function
    Function ThreadCustomerRead(ByVal _Value As String)
        Dim _strGroupID As String = String.Empty
        strSql = "SELECT COUNT (*) AS DATA FROM mCustomerChannel WHERE ValueChannel='" & _Value & "'"
        sqldr = strExecute.ExecuteReader(strSql)
        Try
            If sqldr.HasRows() Then
                sqldr.Read()
                If (sqldr("DATA").ToString > "1") Then
                    ASPxPopupControl5.ShowOnPageLoad = True
                Else
                    strSql = "SELECT mCustomer.* FROM mCustomer left outer join mCustomerChannel on mCustomer.CustomerID = mCustomerChannel.CustomerID where mCustomerChannel.ValueChannel='" & _Value & "'"
                    sqldr = strExecute.ExecuteReader(strSql)
                    Try
                        If sqldr.HasRows() Then
                            sqldr.Read()
                            If sqldr("GroupID").ToString <> "0" Then
                                _strGroupID = "select top 1 * from mCustomer where GroupID='" & sqldr("GroupID").ToString & "' order by DateCreateCustomer Asc"
                                readGroupID = strExecute.ExecuteReader(_strGroupID)
                                If readGroupID.HasRows() Then
                                    readGroupID.Read()
                                    hd_customerid.Value = readGroupID("CustomerID").ToString
                                    hd_Customer_Detail.Value = readGroupID("CustomerID").ToString
                                    TxtName.Text = readGroupID("Name").ToString
                                    TxtPhone.Text = readGroupID("HP").ToString
                                    TxtEmail.Text = readGroupID("Email").ToString
                                    cmbGender.Value = readGroupID("JenisKelamin").ToString
                                    dtBirth.Date = Convert.ToDateTime(readGroupID("Birth").ToString)
                                    TxtCIF.Text = readGroupID("CIF").ToString
                                    TxtNIK.Text = readGroupID("NIK").ToString
                                    TxtAddress.Text = readGroupID("Alamat").ToString
                                    ThreadCustomerAccountNumber(readGroupID("CustomerID").ToString)
                                    InputChannel(readGroupID("CustomerID").ToString)
                                Else

                                End If
                                readGroupID.Close()
                                _ClassFunction.LogSuccess(strLogTime, _strGroupID)
                            Else
                                hd_customerid.Value = sqldr("CustomerID").ToString
                                hd_Customer_Detail.Value = sqldr("CustomerID").ToString
                                TxtName.Text = sqldr("Name").ToString
                                TxtPhone.Text = sqldr("HP").ToString
                                TxtEmail.Text = sqldr("Email").ToString
                                cmbGender.Value = sqldr("JenisKelamin").ToString
                                dtBirth.Date = Convert.ToDateTime(sqldr("Birth").ToString)
                                TxtCIF.Text = sqldr("CIF").ToString
                                TxtNIK.Text = sqldr("NIK").ToString
                                TxtAddress.Text = sqldr("Alamat").ToString
                                ThreadCustomerAccountNumber(sqldr("CustomerID").ToString)
                                InputChannel(sqldr("CustomerID").ToString)
                            End If

                        Else

                        End If
                        sqldr.Close()
                        _ClassFunction.LogSuccess(strLogTime, strSql)
                    Catch ex As Exception
                        _ClassFunction.LogError(strLogTime, ex, strSql)
                        Response.Write(ex.Message)
                    End Try
                End If
            Else
            End If
            sqldr.Close()
            _ClassFunction.LogSuccess(strLogTime, strSql)
        Catch ex As Exception
            _ClassFunction.LogError(strLogTime, ex, strSql)
            Response.Write(ex.Message)
        End Try
    End Function
    Function ThreadCustomerReadCustomerID(ByVal _Value As String)
        Dim _strGroupID As String = String.Empty
        strSql = "select * from mCustomer where CustomerID='" & _Value & "'"
        sqldr = strExecute.ExecuteReader(strSql)
        Try
            If sqldr.HasRows() Then
                sqldr.Read()
                If sqldr("GroupID").ToString <> "0" Then
                    _strGroupID = "select top 1 * from mCustomer where GroupID='" & sqldr("GroupID").ToString & "' order by DateCreateCustomer Asc"
                    readGroupID = strExecute.ExecuteReader(_strGroupID)
                    If readGroupID.HasRows() Then
                        readGroupID.Read()
                        hd_customerid.Value = readGroupID("CustomerID").ToString
                        hd_Customer_Detail.Value = readGroupID("CustomerID").ToString
                        TxtName.Text = readGroupID("Name").ToString
                        TxtPhone.Text = readGroupID("HP").ToString
                        TxtEmail.Text = readGroupID("Email").ToString
                        cmbGender.Value = readGroupID("JenisKelamin").ToString
                        dtBirth.Date = Convert.ToDateTime(readGroupID("Birth").ToString)
                        TxtCIF.Text = readGroupID("CIF").ToString
                        TxtNIK.Text = readGroupID("NIK").ToString
                        TxtAddress.Text = readGroupID("Alamat").ToString
                        ThreadCustomerAccountNumber(readGroupID("CustomerID").ToString)
                        InputChannel(readGroupID("CustomerID").ToString)
                    Else

                    End If
                    readGroupID.Close()
                    _ClassFunction.LogSuccess(strLogTime, _strGroupID)
                Else
                    hd_customerid.Value = sqldr("CustomerID").ToString
                    hd_Customer_Detail.Value = sqldr("CustomerID").ToString
                    TxtName.Text = sqldr("Name").ToString
                    TxtPhone.Text = sqldr("HP").ToString
                    TxtEmail.Text = sqldr("Email").ToString
                    cmbGender.Value = sqldr("JenisKelamin").ToString
                    dtBirth.Date = Convert.ToDateTime(sqldr("Birth").ToString)
                    TxtCIF.Text = sqldr("CIF").ToString
                    TxtNIK.Text = sqldr("NIK").ToString
                    TxtAddress.Text = sqldr("Alamat").ToString
                    ThreadCustomerAccountNumber(sqldr("CustomerID").ToString)
                    InputChannel(sqldr("CustomerID").ToString)
                End If
            Else
            End If
            sqldr.Close()
            _ClassFunction.LogSuccess(strLogTime, strSql)
        Catch ex As Exception
            _ClassFunction.LogError(strLogTime, ex, strSql)
            Response.Write(ex.Message)
        End Try
    End Function
    Function ThreadCustomerAccountNumber(ByVal CustomerID As String)
        dsAccountNumber.SelectCommand = "select * from BTN_NomorRekening where CustomerID='" & CustomerID & "'"
    End Function
    Function ThreadCustomerCountingAccount(ByVal Account As String)
        strSql = "SELECT COUNT (*) AS DATA FROM mCustomerChannel WHERE ValueChannel='" & Account & "'"
        sqldr = strExecute.ExecuteReader(strSql)
        Try
            If sqldr.HasRows() Then
                sqldr.Read()
                If (sqldr("DATA").ToString = "0" Or sqldr("DATA").ToString = "") Then
                    _ClassFunction.LogSuccess(strLogTime, strSql)
                    Return False
                Else
                    _ClassFunction.LogSuccess(strLogTime, strSql)
                    Return True
                End If
            Else
            End If
            sqldr.Close()
        Catch ex As Exception
            _ClassFunction.LogError(strLogTime, ex, strSql)
            Response.Write(ex.Message)
        End Try
    End Function
    Function ThreadInsertTransaction(ByVal MediaType As String, ByVal ThreadID As String, ByVal GenesysNumber As String, ByVal Account As String, ByVal AccountContactID As String, ByVal AgentID As String, ByVal Subject As String, ByVal PhoneChat As String)
        Dim strTime As String = DateTime.Now.ToString("yyyyMMddhhmmss")
        If ThreadCountingGenesys(Request.QueryString("genesysid")) = False Then
            If Request.QueryString("n") = "1" Then

            Else
                If (ThreadCustomerCountingAccount(Account)) = True Then
                    Try
                        strSql = "INSERT INTO TR_THREAD (ValueThread, ThreadID, GenesysNumber, Account, AccountContactID, AgentID, Subject, PhoneChat, CustomerID) VALUES('" & MediaType & "','" & ThreadID & "','" & GenesysNumber & "','" & Account & "','" & AccountContactID & "','" & AgentID & "','" & Subject & "','" & PhoneChat & "', '" & GetValueCustomerID(Account) & "')"
                        execute.ExecuteNonQuery(strSql)
                        strExecute.LogSuccess(strLogTime, "Insert Tabel TR_THREAD - " & strSql)
                    Catch ex As Exception
                        strExecute.LogError(strLogTime, ex, "Insert Tabel TR_THREAD - " & strSql)
                    End Try
                Else
                    Try
                        strSql = "INSERT INTO TR_THREAD (ValueThread, ThreadID, GenesysNumber, Account, AccountContactID, AgentID, Subject, PhoneChat) VALUES('" & MediaType & "','" & ThreadID & "','" & GenesysNumber & "','" & Account & "','" & AccountContactID & "','" & AgentID & "','" & Subject & "','" & PhoneChat & "')"
                        execute.ExecuteNonQuery(strSql)
                        strExecute.LogSuccess(strLogTime, "Insert Tabel TR_THREAD - " & strSql)
                    Catch ex As Exception
                        strExecute.LogError(strLogTime, ex, "Insert Tabel TR_THREAD - " & strSql)
                    End Try
                End If
            End If
        Else
            ThreadReload()
        End If
    End Function
    Function GetValueCustomerID(ByVal _Value As String)
        Try
            strSql = "select CustomerID from mCustomerChannel WHERE ValueChannel='" & _Value & "'"
            sqldr = strExecute.ExecuteReader(strSql)
            If sqldr.HasRows() Then
                sqldr.Read()
                Return sqldr("CustomerID").ToString
            Else
                Return ""
            End If
            sqldr.Close()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try

    End Function
    Function ThreadCountingGenesys(ByVal GenesysID As String)
        strSql = "SELECT COUNT (*) AS DATA FROM TR_THREAD WHERE GenesysNumber='" & GenesysID & "'"
        sqldr = strExecute.ExecuteReader(strSql)
        Try
            If sqldr.HasRows() Then
                sqldr.Read()
                If (sqldr("DATA").ToString = "0" Or sqldr("DATA").ToString = "") Then
                    Return False
                Else
                    Return True
                End If
            Else
            End If
            sqldr.Close()
            _ClassFunction.LogSuccess(strLogTime, strSql)
        Catch ex As Exception
            _ClassFunction.LogError(strLogTime, ex, strSql)
            Response.Write(ex.Message)
        End Try
    End Function
    Function InputChannel(ByVal CustomerID As String)
        dsTambahChannelSyncronise.SelectParameters("customerid").DefaultValue = CustomerID
    End Function
    Private Sub ASPxGridView1_RowDeleting(sender As Object, e As DevExpress.Web.Data.ASPxDataDeletingEventArgs) Handles ASPxGridView1.RowDeleting
        Dim _idTransaction As String = e.Keys("ID")
        Dim _NomorRekening As String = e.Values("NomorRekening")
        Dim _strScript As String = String.Empty
        Dim _strScriptloq As String = String.Empty
        _strScript = "delete from BTN_NomorRekening where ID='" & _idTransaction & "'"
        Try
            _strScriptloq = _strScript & " - Delete Account Number : " & _NomorRekening
            strExecute.LogSuccess(strLogTime, _strScriptloq)
            dsAccountNumber.DeleteCommand = _strScript
        Catch ex As Exception
            strExecute.LogError(strLogTime, ex, _strScriptloq)
        End Try
    End Sub
    Private Sub ASPxGridView1_RowInserting(sender As Object, e As DevExpress.Web.Data.ASPxDataInsertingEventArgs) Handles ASPxGridView1.RowInserting
        Dim _strScript As String = String.Empty
        Dim _AccountNumber As String = e.NewValues("NomorRekening")
        Dim substAccountNumber As Integer = _AccountNumber.Length
        If (ValidasiAccountNumberLength(substAccountNumber)) = False Then
            e.Cancel = True
            Throw New Exception(_AccountNumber & " Format account number is not valid")
        Else
            strSql = "select NomorRekening  from BTN_NomorRekening WHERE NomorRekening='" & _AccountNumber & "'"
            sqldr = strExecute.ExecuteReader(strSql)
            If sqldr.HasRows() Then
                sqldr.Read()
                e.Cancel = True
                Throw New Exception(_AccountNumber & " already exists")
            Else
                e.Cancel = False
                _strScript = "insert into BTN_NomorRekening(CustomerID, NomorRekening, Usercreate) VALUES ('" & hd_customerid.Value & "', '" & _AccountNumber & "', '" & Session("username") & "')"
                strExecute.LogSuccess(strLogTime, "Insert Account Number - " & _strScript)
                dsAccountNumber.InsertCommand = _strScript
            End If
            sqldr.Close()
        End If
    End Sub
    Private Sub ASPxGridView1_RowUpdating(sender As Object, e As DevExpress.Web.Data.ASPxDataUpdatingEventArgs) Handles ASPxGridView1.RowUpdating
        Dim _strScript As String = String.Empty
        Dim _idTransaction As String = e.Keys("ID")
        Dim _AccountNumber As String = e.NewValues("NomorRekening")
        Dim substAccountNumber As Integer = _AccountNumber.Length
        If (ValidasiAccountNumberLength(substAccountNumber)) = False Then
            e.Cancel = True
            Throw New Exception(_AccountNumber & " Format account number is not valid")
        Else
            strSql = "select CustomerID, NomorRekening from BTN_NomorRekening WHERE NomorRekening='" & _AccountNumber & "'"
            sqldr = strExecute.ExecuteReader(strSql)
            If sqldr.HasRows() Then
                sqldr.Read()
                If hd_customerid.Value <> sqldr("CustomerID").ToString() Then
                    e.Cancel = True
                    Throw New Exception(_AccountNumber & " already exists")
                Else
                    e.Cancel = False
                    _strScript = "update BTN_NomorRekening set NomorRekening='" & _AccountNumber & "', UserUpdate='" & Session("username") & "', DateUpdate=GETDATE() where ID='" & _idTransaction & "'"
                    strExecute.LogSuccess(strLogTime, strSql)
                    strExecute.LogSuccess(strLogTime, "Update Account Number - " & _strScript)
                    dsAccountNumber.UpdateCommand = _strScript
                End If
            Else
                _strScript = "update BTN_NomorRekening set NomorRekening='" & _AccountNumber & "', UserUpdate='" & Session("username") & "', DateUpdate=GETDATE() where ID='" & _idTransaction & "'"
                strExecute.LogSuccess(strLogTime, strSql)
                strExecute.LogSuccess(strLogTime, "Update Account Number - " & _strScript)
                dsAccountNumber.UpdateCommand = _strScript
            End If
            sqldr.Close()
        End If
    End Sub
    'Private Sub ASPxGridView1_load(sender As Object, e As EventArgs) Handles ASPxGridView1.Load
    '    ThreadCustomerAccountNumber(hd_customerid.Value)
    'End Sub

    'Private Sub btnViewCustomer_Click(sender As Object, e As EventArgs) Handles btnViewCustomer.ServerClick
    '    ''noted dari ASPxGridView7_Load
    '    Label1.Text = "NewDataCust"
    '    'NewCustData()
    '    'tabShow.Visible = True
    '    ''End noted dari ASPxGridView7_Load

    'End Sub

    Private Sub btnBankAccountNo_Click(sender As Object, e As EventArgs) Handles btnBankAccountNo.ServerClick
        ''noted dari ASPxGridView1_load

        ThreadCustomerAccountNumber(hd_customerid.Value)

        ASPxGridView1.Visible = True
        ASPxGridView12.Visible = True

    End Sub

    'Private Sub btnHisTransaction_Click(sender As Object, e As EventArgs) Handles btnHisTransaction.ServerClick
    '    ''====noted dari ASPxGridView2_load=====
    '    Label3.Text = "NewDataCust3"
    '    NewCustData3()

    'End Sub

    'Private Sub btnDataCust_Click(sender As Object, e As EventArgs) Handles btnDataCust.ServerClick
    '    ''====call ASPxGridView5_Load=====
    '    Label2.Text = "NewDataCust2"
    '    'NewCustData2()
    'End Sub

    Private Sub ASPxGridView2_load(sender As Object, e As EventArgs) Handles ASPxGridView2.Load
        NewCustData3()
    End Sub

    Private Sub ASPxGridView3_Load(sender As Object, e As EventArgs) Handles ASPxGridView3.Load
        '==== Event ketika save ticket kemudian data di load kembali
        Try
            Dim strSql As New T_TransactionLoad_DBMLDataContext
            strSql.CommandTimeout = 480
            ASPxGridView3.DataSource = strSql.L_TransactionLoad("" & hd_customerid.Value & "", "" & Session("username") & "", "" & TxtGenesysNumberID.Text & "").ToList()
            ASPxGridView3.DataBind()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Private Sub ASPxGridView4_Load(sender As Object, e As EventArgs) Handles ASPxGridView4.Load
        Dim strCommands As String = "exec SP_Temp_Interaction '" & hd_ticketid.Value & "'"
        dsInteraction.SelectCommand = strCommands
    End Sub

    Sub NewCustData3()

        'ASPxGridView2.Visible = True
        Dim strAccountNumber As String = String.Empty
        If hd_NomorRekening.Value = "" Then
            strAccountNumber = "0"
        Else
            strAccountNumber = hd_NomorRekening.Value
        End If
        '==== Event ketika preview Data customer kemudian data ticket di load
        Try
            Dim strSql As New T_TransactionHistory_DBMLDataContext
            strSql.CommandTimeout = 480
            ASPxGridView2.DataSource = strSql.L_TransactionHistory("" & hd_customerid.Value & "", "" & strAccountNumber & "").ToList()
            ASPxGridView2.DataBind()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try

        'Dim strNewCust3 As String = "NewDataCust3"

        'If Label3.Text = strNewCust3 Then
        '    ASPxGridView2.Visible = True
        '    Dim strAccountNumber As String = String.Empty
        '    If hd_NomorRekening.Value = "" Then
        '        strAccountNumber = "0"
        '    Else
        '        strAccountNumber = hd_NomorRekening.Value
        '    End If
        '    '==== Event ketika preview Data customer kemudian data ticket di load
        '    Try
        '        Dim strSql As New T_TransactionHistory_DBMLDataContext
        '        strSql.CommandTimeout = 480
        '        ASPxGridView2.DataSource = strSql.L_TransactionHistory("" & hd_customerid.Value & "", "" & strAccountNumber & "").ToList()
        '        ASPxGridView2.DataBind()
        '    Catch ex As Exception
        '        Response.Write(ex.Message)
        '    End Try
        'End If

    End Sub

    'Sub NewCustData2()
    '    Dim strNewCust2 As String = "NewDataCust2"

    '    If Label2.Text = strNewCust2 Then
    '        ASPxGridView5.Visible = True
    '        Try
    '            Dim strSql As New T_DataCustomerDataContext
    '            strSql.CommandTimeout = 480
    '            ASPxGridView5.DataSource = strSql.L_DataCustomer().ToList()
    '            ASPxGridView5.DataBind()
    '        Catch ex As Exception
    '            Response.Write(ex.Message)
    '        End Try
    '    End If

    'End Sub

    'Sub NewCustData()
    '    Dim strNewCust As String = "NewDataCust"

    '    If Label1.Text = strNewCust Then
    '        ASPxGridView7.Visible = True
    '        Try
    '            Dim strSql As New CustomerChannel_DBMLDataContext
    '            strSql.CommandTimeout = 480
    '            ASPxGridView7.DataSource = strSql.SP_Temp_MultipleCustomerChannel().ToList()
    '            ASPxGridView7.DataBind()
    '        Catch ex As Exception
    '            Response.Write(ex.Message)
    '        End Try
    '    End If
    '    If Me.IsPostBack Then
    '        TabName.Value = Request.Form(TabName.UniqueID)
    '    End If
    'End Sub

    'Private Sub ASPxGridView5_Load(sender As Object, e As EventArgs) Handles ASPxGridView5.Load
    '    NewCustData2()
    'End Sub

    'Private Sub ASPxGridView7_Load(sender As Object, e As EventArgs) Handles ASPxGridView7.Load
    '    NewCustData()
    'End Sub

    Protected Sub ASPxGridView10_BeforePerformDataSelect(sender As Object, e As EventArgs)
        Session("customerid") = (TryCast(sender, ASPxGridView)).GetMasterRowKeyValue()
        dsTambahAccountNumber.SelectParameters("customerid").DefaultValue = Session("customerid")
        dsTambahChannel.SelectParameters("customerid").DefaultValue = Session("customerid")
    End Sub
    Protected Sub ASPxGridView10_RowUpdating(sender As Object, e As DevExpress.Web.Data.ASPxDataUpdatingEventArgs)
        If Session("customerid") <> "" Then
            Dim _strScript As String = String.Empty
            Dim _idTransaction As String = e.Keys("ID")
            Dim _AccountNumber As String = e.NewValues("NomorRekening")
            Dim substAccountNumber As Integer = _AccountNumber.Length
            If (ValidasiAccountNumberLength(substAccountNumber)) = False Then
                e.Cancel = True
                Throw New Exception(_AccountNumber & " Format account number is not valid")
            Else
                strSql = "select CustomerID, NomorRekening from BTN_NomorRekening WHERE NomorRekening='" & _AccountNumber & "'"
                sqldr = strExecute.ExecuteReader(strSql)
                If sqldr.HasRows() Then
                    sqldr.Read()
                    If Session("customerid") <> sqldr("CustomerID").ToString() Then
                        e.Cancel = True
                        Throw New Exception(_AccountNumber & " already exists")
                    Else
                        e.Cancel = False
                        _strScript = "update BTN_NomorRekening set NomorRekening='" & _AccountNumber & "', UserUpdate='" & Session("username") & "', DateUpdate=GETDATE() where ID='" & _idTransaction & "'"
                        strExecute.LogSuccess(strLogTime, strSql)
                        strExecute.LogSuccess(strLogTime, _strScript)
                        dsTambahAccountNumber.UpdateCommand = _strScript
                    End If
                Else
                    _strScript = "update BTN_NomorRekening set NomorRekening='" & _AccountNumber & "', UserUpdate='" & Session("username") & "', DateUpdate=GETDATE() where ID='" & _idTransaction & "'"
                    strExecute.LogSuccess(strLogTime, strSql)
                    strExecute.LogSuccess(strLogTime, _strScript)
                    dsTambahAccountNumber.UpdateCommand = _strScript
                End If
                sqldr.Close()
            End If
        Else
            e.Cancel = True
            Throw New Exception("Customer is empty")
        End If
    End Sub
    Protected Sub ASPxGridView10_RowInserting(sender As Object, e As DevExpress.Web.Data.ASPxDataInsertingEventArgs)
        If Session("customerid") <> "" Then
            Dim _strScript As String = String.Empty
            Dim _AccountNumber As String = e.NewValues("NomorRekening")
            Dim substAccountNumber As Integer = _AccountNumber.Length
            If (ValidasiAccountNumberLength(substAccountNumber)) = False Then
                e.Cancel = True
                Throw New Exception(_AccountNumber & " Format account number is not valid")
            Else
                strSql = "select NomorRekening  from BTN_NomorRekening WHERE NomorRekening='" & _AccountNumber & "'"
                sqldr = strExecute.ExecuteReader(strSql)
                If sqldr.HasRows() Then
                    sqldr.Read()
                    e.Cancel = True
                    Throw New Exception(_AccountNumber & " already exists")
                Else
                    e.Cancel = False
                    _strScript = "insert into BTN_NomorRekening(CustomerID, NomorRekening, Usercreate) VALUES ('" & Session("customerid") & "', '" & _AccountNumber & "', '" & Session("username") & "')"
                    strExecute.LogSuccess(strLogTime, _strScript)
                    dsTambahAccountNumber.InsertCommand = _strScript
                End If
                sqldr.Close()
            End If
        Else
            e.Cancel = True
            Throw New Exception("Customer is empty")
        End If
    End Sub
    Protected Sub ASPxGridView10_RowDeleting(sender As Object, e As DevExpress.Web.Data.ASPxDataDeletingEventArgs)
        If Session("customerid") <> "" Then
            Dim _idTransaction As String = e.Keys("ID")
            Dim _NomorRekening As String = e.Values("NomorRekening")
            Dim _strScript As String = String.Empty
            Dim _strScriptloq As String = String.Empty
            _strScript = "delete from BTN_NomorRekening where ID='" & _idTransaction & "'"
            Try
                _strScriptloq = _strScript & " - Account Number : " & _NomorRekening
                strExecute.LogSuccess(strLogTime, _strScriptloq)
                dsTambahAccountNumber.DeleteCommand = _strScript
            Catch ex As Exception
                strExecute.LogError(strLogTime, ex, _strScriptloq)
            End Try
        Else
            e.Cancel = True
            Throw New Exception("Customer is empty")
        End If
    End Sub
    Protected Sub ASPxGridView11_RowInserting(sender As Object, e As DevExpress.Web.Data.ASPxDataInsertingEventArgs)
        If Session("customerid") <> "" Then
            Dim _strScript As String = String.Empty
            Dim ValueChannel As String = e.NewValues("ValueChannel")
            Dim FlagChannel As String = e.NewValues("FlagChannel")
            Dim substValueChannel As Integer = ValueChannel.Length
            If FlagChannel = "Email" Then
                If IsValidEmailFormat(ValueChannel) = True Then
                    strSql = "select CustomerID, ValueChannel  from mCustomerChannel WHERE ValueChannel='" & ValueChannel & "'"
                    sqldr = strExecute.ExecuteReader(strSql)
                    If sqldr.HasRows() Then
                        sqldr.Read()
                        e.Cancel = True
                        Throw New Exception(ValueChannel & " already exists")
                    Else
                        e.Cancel = False
                        _strScript = "insert into mCustomerChannel (CustomerID, ValueChannel, FlagChannel, UserCreate) values('" & Session("customerid") & "', '" & ValueChannel & "', '" & FlagChannel & "', '" & Session("username") & "')"
                        'strExecute.LogSuccess(strLogTime, _strScript)
                        dsTambahChannel.InsertCommand = _strScript
                    End If
                    sqldr.Close()
                Else
                    e.Cancel = True
                    Throw New Exception(ValueChannel & " Format email address is not valid")
                End If
            ElseIf FlagChannel = "Phone" Then
                If (ValidasiPhoneLength(substValueChannel)) = True Then
                    strSql = "select CustomerID, ValueChannel  from mCustomerChannel WHERE ValueChannel='" & ValueChannel & "'"
                    sqldr = strExecute.ExecuteReader(strSql)
                    If sqldr.HasRows() Then
                        sqldr.Read()
                        e.Cancel = True
                        Throw New Exception(ValueChannel & " already exists")
                    Else
                        e.Cancel = False
                        _strScript = "insert into mCustomerChannel (CustomerID, ValueChannel, FlagChannel, UserCreate) values('" & Session("customerid") & "', '" & ValueChannel & "', '" & FlagChannel & "', '" & Session("username") & "')"
                        'strExecute.LogSuccess(strLogTime, _strScript)
                        dsTambahChannel.InsertCommand = _strScript
                    End If
                    sqldr.Close()
                Else
                    e.Cancel = True
                    Throw New Exception(ValueChannel & " Format phone number is not valid")
                End If
            Else
                e.Cancel = True
                Throw New Exception("Type is not empty")
            End If
        Else
            e.Cancel = True
            Throw New Exception("Customer is empty")
        End If
    End Sub
    Protected Sub ASPxGridView11_RowUpdating(sender As Object, e As DevExpress.Web.Data.ASPxDataUpdatingEventArgs)
        If Session("customerid") <> "" Then
            Dim _strScript As String = String.Empty
            Dim _idTransaction As String = e.Keys("ID")
            Dim ValueChannel As String = e.NewValues("ValueChannel")
            Dim FlagChannel As String = e.NewValues("FlagChannel")
            Dim substValueChannel As Integer = ValueChannel.Length
            If FlagChannel = "Email" Then
                If IsValidEmailFormat(ValueChannel) = True Then
                    strSql = "select CustomerID, ValueChannel  from mCustomerChannel WHERE ValueChannel='" & ValueChannel & "'"
                    sqldr = strExecute.ExecuteReader(strSql)
                    If sqldr.HasRows() Then
                        sqldr.Read()
                        If Session("customerid") <> sqldr("CustomerID").ToString() Then
                            e.Cancel = True
                            Throw New Exception(ValueChannel & " already exists")
                        Else
                            e.Cancel = False
                            _strScript = "update mCustomerChannel set ValueChannel='" & ValueChannel & "', FlagChannel='" & FlagChannel & "', UserUpdate='" & Session("username") & "', DateUpdate=GETDATE() where ID='" & _idTransaction & "'"
                            'strExecute.LogSuccess(strLogTime, strSql)
                            'strExecute.LogSuccess(strLogTime, _strScript)
                            dsTambahChannel.UpdateCommand = _strScript
                        End If
                    Else
                        _strScript = "update mCustomerChannel set ValueChannel='" & ValueChannel & "', FlagChannel='" & FlagChannel & "', UserUpdate='" & Session("username") & "', DateUpdate=GETDATE() where ID='" & _idTransaction & "'"
                        'strExecute.LogSuccess(strLogTime, strSql)
                        'strExecute.LogSuccess(strLogTime, _strScript)
                        dsTambahChannel.UpdateCommand = _strScript
                    End If
                    sqldr.Close()
                Else
                    e.Cancel = True
                    Throw New Exception(ValueChannel & " Format email address is not valid")
                End If
            ElseIf FlagChannel = "Phone" Then
                If (ValidasiPhoneLength(substValueChannel)) = True Then
                    strSql = "select CustomerID, ValueChannel  from mCustomerChannel WHERE ValueChannel='" & ValueChannel & "'"
                    sqldr = strExecute.ExecuteReader(strSql)
                    If sqldr.HasRows() Then
                        sqldr.Read()
                        If Session("customerid") <> sqldr("CustomerID").ToString() Then
                            e.Cancel = True
                            Throw New Exception(ValueChannel & " already exists")
                        Else
                            e.Cancel = False
                            _strScript = "update mCustomerChannel set ValueChannel='" & ValueChannel & "', FlagChannel='" & FlagChannel & "', UserUpdate='" & Session("username") & "', DateUpdate=GETDATE() where ID='" & _idTransaction & "'"
                            'strExecute.LogSuccess(strLogTime, strSql)
                            'strExecute.LogSuccess(strLogTime, _strScript)
                            dsTambahChannel.UpdateCommand = _strScript
                        End If
                    Else
                        _strScript = "update mCustomerChannel set ValueChannel='" & ValueChannel & "', FlagChannel='" & FlagChannel & "', UserUpdate='" & Session("username") & "', DateUpdate=GETDATE() where ID='" & _idTransaction & "'"
                        'strExecute.LogSuccess(strLogTime, strSql)
                        'strExecute.LogSuccess(strLogTime, _strScript)
                        dsTambahChannel.UpdateCommand = _strScript
                    End If
                    sqldr.Close()
                Else
                    e.Cancel = True
                    Throw New Exception(ValueChannel & " Format phone number is not valid")
                End If
            Else
                e.Cancel = True
                Throw New Exception("Type is not empty")
            End If
        Else
            e.Cancel = True
            Throw New Exception("Customer is empty")
        End If
    End Sub
    Protected Sub ASPxGridView11_RowDeleting(sender As Object, e As DevExpress.Web.Data.ASPxDataDeletingEventArgs)
        If Session("customerid") <> "" Then
            Dim _idTransaction As String = e.Keys("ID")
            Dim _ValueChannel As String = e.Values("ValueChannel")
            Dim _strScript As String = String.Empty
            Dim _strScriptloq As String = String.Empty
            _strScript = "delete from mCustomerChannel where ID='" & _idTransaction & "'"
            Try
                _strScriptloq = _strScript & " - Channel : " & _ValueChannel
                'strExecute.LogSuccess(strLogTime, _strScriptloq)
                dsTambahChannel.DeleteCommand = _strScript
            Catch ex As Exception
                'strExecute.LogError(strLogTime, ex, _strScriptloq)
            End Try
        Else
            e.Cancel = True
            Throw New Exception("Customer is empty")
        End If
    End Sub
    Private Sub ASPxGridView12_RowDeleting(sender As Object, e As DevExpress.Web.Data.ASPxDataDeletingEventArgs) Handles ASPxGridView12.RowDeleting
        Dim _idTransaction As String = e.Keys("ID")
        Dim _ValueChannel As String = e.Values("ValueChannel")
        Dim _strScript As String = String.Empty
        Dim _strScriptloq As String = String.Empty
        _strScript = "delete from mCustomerChannel where ID='" & _idTransaction & "'"
        Try
            _strScriptloq = _strScript & " - Channel : " & _ValueChannel
            'strExecute.LogSuccess(strLogTime, _strScriptloq)
            dsTambahChannelSyncronise.DeleteCommand = _strScript
        Catch ex As Exception
            'strExecute.LogError(strLogTime, ex, _strScriptloq)
        End Try
    End Sub
    Private Sub ASPxGridView12_RowInserting(sender As Object, e As DevExpress.Web.Data.ASPxDataInsertingEventArgs) Handles ASPxGridView12.RowInserting
        If hd_customerid.Value <> "" Then
            Dim _strScript As String = String.Empty
            Dim ValueChannel As String = e.NewValues("ValueChannel")
            Dim FlagChannel As String = e.NewValues("FlagChannel")
            Dim substValueChannel As Integer = ValueChannel.Length
            If FlagChannel = "Email" Then
                If IsValidEmailFormat(ValueChannel) = True Then
                    strSql = "select CustomerID, ValueChannel  from mCustomerChannel WHERE ValueChannel='" & ValueChannel & "'"
                    sqldr = strExecute.ExecuteReader(strSql)
                    If sqldr.HasRows() Then
                        sqldr.Read()
                        e.Cancel = True
                        Throw New Exception(ValueChannel & " already exists")
                    Else
                        e.Cancel = False
                        _strScript = "insert into mCustomerChannel (CustomerID, ValueChannel, FlagChannel, UserCreate) values('" & hd_customerid.Value & "', '" & ValueChannel & "', '" & FlagChannel & "', '" & Session("username") & "')"
                        'strExecute.LogSuccess(strLogTime, _strScript)
                        dsTambahChannelSyncronise.InsertCommand = _strScript
                    End If
                    sqldr.Close()
                Else
                    e.Cancel = True
                    Throw New Exception(ValueChannel & " Format email address is not valid")
                End If
            ElseIf FlagChannel = "Phone" Then
                If (ValidasiPhoneLength(substValueChannel)) = True Then
                    strSql = "select CustomerID, ValueChannel  from mCustomerChannel WHERE ValueChannel='" & ValueChannel & "'"
                    sqldr = strExecute.ExecuteReader(strSql)
                    If sqldr.HasRows() Then
                        sqldr.Read()
                        e.Cancel = True
                        Throw New Exception(ValueChannel & " already exists")
                    Else
                        e.Cancel = False
                        _strScript = "insert into mCustomerChannel (CustomerID, ValueChannel, FlagChannel, UserCreate) values('" & hd_customerid.Value & "', '" & ValueChannel & "', '" & FlagChannel & "', '" & Session("username") & "')"
                        'strExecute.LogSuccess(strLogTime, _strScript)
                        dsTambahChannelSyncronise.InsertCommand = _strScript
                    End If
                    sqldr.Close()
                Else
                    e.Cancel = True
                    Throw New Exception(ValueChannel & " Format phone number is not valid")
                End If
            Else
                e.Cancel = True
                Throw New Exception("Type is not empty")
            End If
        Else
            e.Cancel = True
            Throw New Exception("Customer is empty")
        End If
    End Sub
    Private Sub ASPxGridView12_RowUpdating(sender As Object, e As DevExpress.Web.Data.ASPxDataUpdatingEventArgs) Handles ASPxGridView12.RowUpdating
        If hd_customerid.Value <> "" Then
            Dim _strScript As String = String.Empty
            Dim _idTransaction As String = e.Keys("ID")
            Dim ValueChannel As String = e.NewValues("ValueChannel")
            Dim FlagChannel As String = e.NewValues("FlagChannel")
            Dim substValueChannel As Integer = ValueChannel.Length
            If FlagChannel = "Email" Then
                If IsValidEmailFormat(ValueChannel) = True Then
                    strSql = "select CustomerID, ValueChannel  from mCustomerChannel WHERE ValueChannel='" & ValueChannel & "'"
                    sqldr = strExecute.ExecuteReader(strSql)
                    If sqldr.HasRows() Then
                        sqldr.Read()
                        If hd_customerid.Value <> sqldr("CustomerID").ToString() Then
                            e.Cancel = True
                            Throw New Exception(ValueChannel & " already exists")
                        Else
                            e.Cancel = False
                            _strScript = "update mCustomerChannel set ValueChannel='" & ValueChannel & "', FlagChannel='" & FlagChannel & "', UserUpdate='" & Session("username") & "', DateUpdate=GETDATE() where ID='" & _idTransaction & "'"
                            'strExecute.LogSuccess(strLogTime, strSql)
                            'strExecute.LogSuccess(strLogTime, _strScript)
                            dsTambahChannelSyncronise.UpdateCommand = _strScript
                        End If
                    Else
                        _strScript = "update mCustomerChannel set ValueChannel='" & ValueChannel & "', FlagChannel='" & FlagChannel & "', UserUpdate='" & Session("username") & "', DateUpdate=GETDATE() where ID='" & _idTransaction & "'"
                        'strExecute.LogSuccess(strLogTime, strSql)
                        'strExecute.LogSuccess(strLogTime, _strScript)
                        dsTambahChannelSyncronise.UpdateCommand = _strScript
                    End If
                    sqldr.Close()
                Else
                    e.Cancel = True
                    Throw New Exception(ValueChannel & " Format email address is not valid")
                End If
            ElseIf FlagChannel = "Phone" Then
                If (ValidasiPhoneLength(substValueChannel)) = True Then
                    strSql = "select CustomerID, ValueChannel  from mCustomerChannel WHERE ValueChannel='" & ValueChannel & "'"
                    sqldr = strExecute.ExecuteReader(strSql)
                    If sqldr.HasRows() Then
                        sqldr.Read()
                        If hd_customerid.Value <> sqldr("CustomerID").ToString() Then
                            e.Cancel = True
                            Throw New Exception(ValueChannel & " already exists")
                        Else
                            e.Cancel = False
                            _strScript = "update mCustomerChannel set ValueChannel='" & ValueChannel & "', FlagChannel='" & FlagChannel & "', UserUpdate='" & Session("username") & "', DateUpdate=GETDATE() where ID='" & _idTransaction & "'"
                            'strExecute.LogSuccess(strLogTime, strSql)
                            'strExecute.LogSuccess(strLogTime, _strScript)
                            dsTambahChannelSyncronise.UpdateCommand = _strScript
                        End If
                    Else
                        _strScript = "update mCustomerChannel set ValueChannel='" & ValueChannel & "', FlagChannel='" & FlagChannel & "', UserUpdate='" & Session("username") & "', DateUpdate=GETDATE() where ID='" & _idTransaction & "'"
                        'strExecute.LogSuccess(strLogTime, strSql)
                        'strExecute.LogSuccess(strLogTime, _strScript)
                        dsTambahChannelSyncronise.UpdateCommand = _strScript
                    End If
                    sqldr.Close()
                Else
                    e.Cancel = True
                    Throw New Exception(ValueChannel & " Format phone number is not valid")
                End If
            Else
                e.Cancel = True
                Throw New Exception("Type is not empty")
            End If
        Else
            e.Cancel = True
            Throw New Exception("Customer is empty")
        End If
    End Sub
    Private Sub ASPxCallbackPanel1_Callback(sender As Object, e As DevExpress.Web.ASPxClasses.CallbackEventArgsBase) Handles ASPxCallbackPanel1.Callback
        ' Select Data Transaction Ticket
        ASPxGridView3.Visible = False
    End Sub
    Private Sub ASPxCallbackPanel3_Callback(sender As Object, e As DevExpress.Web.ASPxClasses.CallbackEventArgsBase) Handles ASPxCallbackPanel3.Callback
        ASPxGridView3.Visible = True
        ASPxButton1.Visible = False
        ASPxButton3.Visible = True

        '=== Event ketika save ticket kemudian data di load kembali
        Try
            Dim strSql As New T_TransactionLoad_DBMLDataContext
            strSql.CommandTimeout = 480
            ASPxGridView3.DataSource = strSql.L_TransactionLoad("" & hd_customerid.Value & "", "" & Session("username") & "", "" & TxtGenesysNumberID.Text & "").ToList()
            ASPxGridView3.DataBind()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Private Sub ASPxCallbackPanel4_Callback(sender As Object, e As DevExpress.Web.ASPxClasses.CallbackEventArgsBase) Handles ASPxCallbackPanel4.Callback
        Dim strSql As New T_DataInteractionDataContext
        strSql.CommandTimeout = 480
        ASPxGridView4.DataSource = strSql.SP_Temp_Interaction("" & hd_ticketid.Value & "").ToList()
        ASPxGridView4.DataBind()
    End Sub
    Private Sub ASPxCallbackPanel5_Callback(sender As Object, e As DevExpress.Web.ASPxClasses.CallbackEventArgsBase) Handles ASPxCallbackPanel5.Callback
        ThreadCustomerAccountNumber(hd_customerid.Value)
    End Sub
    Private Sub ASPxCallbackPanel6_Callback(sender As Object, e As DevExpress.Web.ASPxClasses.CallbackEventArgsBase) Handles ASPxCallbackPanel6.Callback
        Dim strAccountNumber As String = String.Empty
        If hd_NomorRekening.Value = "" Then
            strAccountNumber = "0"
        Else
            strAccountNumber = hd_NomorRekening.Value
        End If
        '=== Event ketika preview Data customer kemudian data ticket di load
        Try
            Dim strSql As New T_TransactionHistory_DBMLDataContext
            strSql.CommandTimeout = 480
            ASPxGridView2.DataSource = strSql.L_TransactionHistory("" & hd_customerid.Value & "", "" & strAccountNumber & "").ToList()
            ASPxGridView2.DataBind()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Private Sub ASPxCallbackPanel7_Callback(sender As Object, e As DevExpress.Web.ASPxClasses.CallbackEventArgsBase) Handles ASPxCallbackPanel7.Callback
        'Dim url As String = "" & strDocument & "/" & TxtGenesysNumberID.Text & "/" & TxtGenesysNumberID.Text & ".html"
        'Dim directoryPath As String = Server.MapPath(String.Format(url))
        'If Not File.Exists(directoryPath) Then
        '    ClientScript.RegisterStartupScript(Me.GetType(), "alert", "alert('Document Not Found');", True)
        'Else
        '    ' file ada
        '    IFrameEmail.Src = url
        '    'IFrameEmail.Src = "uConnector_Files/HTML/000002/000002.html"
        'End If
        RedirectPhatHTML()
    End Sub
    Private Sub ASPxCallbackPanel10_Callback(sender As Object, e As DevExpress.Web.ASPxClasses.CallbackEventArgsBase) Handles ASPxCallbackPanel10.Callback
        EscalationTransaction(hd_ticketid.Value)
    End Sub
    Function ThreadCustomerCountingAccountID(ByVal AccountID As String)
        strSql = "SELECT COUNT (*) AS DATA FROM mCustomer WHERE AccountID='" & AccountID & "'"
        sqldr = strExecute.ExecuteReader(strSql)
        Try
            If sqldr.HasRows() Then
                sqldr.Read()
                If (sqldr("DATA").ToString = "0" Or sqldr("DATA").ToString = "") Then
                    _ClassFunction.LogSuccess(strLogTime, strSql)
                    Return False
                Else
                    _ClassFunction.LogSuccess(strLogTime, strSql)
                    Return True
                End If
            Else
            End If
            sqldr.Close()
        Catch ex As Exception
            _ClassFunction.LogError(strLogTime, ex, strSql)
            Response.Write(ex.Message)
        End Try
        Return True
    End Function
    Function IsValidEmailFormat(ByVal s As String) As Boolean
        Try
            Return Regex.IsMatch(s, "^([0-9a-zA-Z]([-\.\w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-\w]*[0-9a-zA-Z]\.)+[a-zA-Z]{2,9})$*([;])*")
        Catch
            Return False
        End Try
        Return True
    End Function
    Function ValidasiPhoneLength(ByVal _Value As Integer)
        Dim _strPhone As String = "select PhoneLength from Temp_SettingParameter"
        readPhone = strExecute.ExecuteReader(_strPhone)
        If readPhone.HasRows() Then
            readPhone.Read()
            If _Value > readPhone("PhoneLength") Then
                Return True
            Else
                Return False
            End If
        Else
        End If
        readPhone.Close()
        Return True
    End Function
    Function ValidasiAccountNumberLength(ByVal _Value As Integer)
        Dim _strAccount As String = "select AcountNumberLength from Temp_SettingParameter"
        readAccount = strExecute.ExecuteReader(_strAccount)
        If readAccount.HasRows() Then
            readAccount.Read()
            If _Value <> readAccount("AcountNumberLength") Then
                Return False
            Else
                Return True
            End If
        Else
        End If
        readAccount.Close()
        Return True
    End Function
    Private Sub RedirectPhatHTML()
        Dim _strPhatHTML As String = "select PathHTML from Temp_SettingParameter"
        readPhatHTML = strExecute.ExecuteReader(_strPhatHTML)
        If readPhatHTML.HasRows() Then
            readPhatHTML.Read()
            Dim url As String = "" & readPhatHTML("PathHTML").ToString & "/" & TxtGenesysNumberID.Text & "/" & TxtGenesysNumberID.Text & ".html"
            Dim directoryPath As String = Server.MapPath(String.Format(url))
            If Not File.Exists(directoryPath) Then
                'ClientScript.RegisterStartupScript(Me.GetType(), "alert", "alert('Document Not Found');", True)
                divDocument.Visible = True
                IFrameEmail.Visible = False
                If TxtType.Text = "voice" Then
                    lblframe.Text = "Channel is Voice, Document does not exist"
                Else
                    lblframe.Text = "Document Not Found"
                End If
            Else
                IFrameEmail.Src = url
            End If
        Else
        End If
        readPhatHTML.Close()
    End Sub
    Protected Sub ASPxGridView8_CustomColumnGroup(sender As Object, e As CustomColumnSortEventArgs)
        CompareColumnValues(e)
    End Sub
    Private Sub CompareColumnValues(ByVal e As DevExpress.Web.ASPxGridView.CustomColumnSortEventArgs)
        If e.Column.FieldName = "ParentNumberID" Then
            e.Handled = True
        End If
    End Sub
    Protected Sub ASPxGridView8_CustomGroupDisplayText(sender As Object, e As ASPxGridViewColumnDisplayTextEventArgs)
        If e.Column.FieldName = "ParentNumberID" Then
            'e.DisplayText = "Data : "
        End If
    End Sub
    Protected Sub ASPxGridView8_CustomColumnSort(sender As Object, e As CustomColumnSortEventArgs)
        CompareColumnValues(e)
    End Sub
    Private Sub ASPxGridView8_Load(sender As Object, e As EventArgs) Handles ASPxGridView3.Load
        'Dim _strSql As String = String.Empty
        'Dim _ParentNumberID As String = String.Empty
        '_strSql = "SELECT ParentNumberID FROM tTicket WHERE TicketNumber='" & hd_ticketid.Value & "'"
        'sqldr = Proses.ExecuteReader(_strSql)
        'Try
        '    If sqldr.HasRows() Then
        '        sqldr.Read()
        '        _ParentNumberID = sqldr("ParentNumberID").ToString
        '    Else
        '    End If
        '    sqldr.Close()
        'Catch ex As Exception
        '    Response.Write(ex.Message)
        'End Try
        ''DsParentNumber.SelectParameters("ParentNumberID").DefaultValue = _ParentNumberID
        CType(ASPxGridView8.Columns("ParentNumberID"), GridViewDataTextColumn).GroupBy()
        Try
            Dim strSql As New ParentNumberDBMLDataContext
            strSql.CommandTimeout = 480
            ASPxGridView8.DataSource = strSql.SP_Temp_ParentChildTicketDetail(hd_ticketid.Value).ToList()
            ASPxGridView8.DataBind()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Private Sub ASPxGridView6_Load(sender As Object, e As EventArgs) Handles ASPxGridView6.Load
        Try
            Dim strSql As New hiStoryTransactionDBMLDataContext
            strSql.CommandTimeout = 480
            ASPxGridView6.DataSource = strSql.SP_Temp_hiStoryTransaction(Session("username"), "ParentChild").ToList()
            ASPxGridView6.DataBind()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Private Sub _settingQueryTransaction()
        ds_query.SelectParameters("username").DefaultValue = Session("username")
        ds_query.InsertParameters("username").DefaultValue = Session("username")
        ds_query.UpdateParameters("username").DefaultValue = Session("username")
    End Sub
    Private Sub ASPxCallbackPanel9_Callback(sender As Object, e As DevExpress.Web.ASPxClasses.CallbackEventArgsBase) Handles ASPxCallbackPanel9.Callback
        CType(ASPxGridView8.Columns("ParentNumberID"), GridViewDataTextColumn).GroupBy()
        Try
            Dim strSql As New ParentNumberDBMLDataContext
            strSql.CommandTimeout = 480
            ASPxGridView8.DataSource = strSql.SP_Temp_ParentChildTicketDetail(hd_ticketid.Value).ToList()
            ASPxGridView8.DataBind()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Private Sub GridView_RowInserting(sender As Object, e As DevExpress.Web.Data.ASPxDataInsertingEventArgs) Handles GridView.RowInserting
        Dim _strScript As String = String.Empty
        strSql = "select * from Temp_SettingHiStoryTransaction WHERE CreatedBy='" & Session("username") & "' and Type='ParentChild'"
        sqldr = strExecute.ExecuteReader(strSql)
        If sqldr.HasRows() Then
            sqldr.Read()
            e.Cancel = True
            Throw New Exception("For user " & Session("username") & " to setting query data transaksi already exists, please update data existing")
        Else
            e.Cancel = False
            _strScript = "insert into Temp_SettingHiStoryTransaction([Day], [FilterDate], [CreatedBy], [Type]) VALUES (@Day, @FilterDate, '" & Session("username") & "', 'ParentChild')"
            strExecute.LogSuccess(strLogTime, _strScript)
            ds_query.InsertCommand = _strScript
        End If
        sqldr.Close()
    End Sub
    Function GetValueThreadTicketID(ByVal _Value1 As String, ByVal _Value2 As String)
        Dim _strSql As String
        _strSql = "Select ThreadTicket from Temp_ThreadTicketID WHERE GenesysNumber='" & _Value1 & "' And Agent='" & _Value2 & "'"
        sqldr = Proses.ExecuteReader(_strSql)
        Try
            If sqldr.HasRows() Then
                sqldr.Read()
                _ClassFunction.LogSuccess(strLogTime, _strSql)
                Return sqldr("ThreadTicket").ToString
            Else
                _ClassFunction.LogSuccess(strLogTime, "Mainframe GetValueThreadTicketID - " & _strSql)
            End If
            sqldr.Close()
        Catch ex As Exception
            _ClassFunction.LogError(strLogTime, ex, "Mainframe GetValueThreadTicketID - " & _strSql)
        End Try
    End Function
End Class