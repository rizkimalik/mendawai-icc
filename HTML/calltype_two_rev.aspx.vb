﻿Imports System
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports DevExpress.Web.ASPxEditors
Imports DevExpress.Web.ASPxGridView
Imports DevExpress.Web.ASPxGridLookup
Imports DevExpress.Web.ASPxClasses
Public Class calltype_two_rev
    Inherits System.Web.UI.Page

    Dim Com As SqlCommand
    Dim Dr As SqlDataReader
    Dim SelTicket, SelTicket1, Categori As String
    Dim ConnectionTest As New SqlConnection(ConfigurationManager.ConnectionStrings("DefaultConnection").ConnectionString)
    Dim Connection As New SqlConnection(ConfigurationManager.ConnectionStrings("DefaultConnection").ConnectionString)
    Dim Comm, sqlcom As SqlCommand
    Dim con As New SqlConnection(ConfigurationManager.ConnectionStrings("DefaultConnection").ConnectionString)

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        dsSubject.SelectCommand = "select b.CategoryID,b.IDHandling, a.Name as JenisTransaksi,b.SubName as SubjectTable,c.SubName as UnitKerja,b.NA,b.ID from mCategory a left outer join mSubCategoryLv2 b on a.CategoryID = b.CategoryID left outer join mSubCategoryLv1 as c on b.SubCategory1ID=c.SubCategory1ID where c.SubName <> ''"
        dsmCategory.SelectCommand = "select * from mCategory Where NA='Y'"
    End Sub

    ''COmbo Box Trigger
    Private Sub cmbCombo2_OnCallback(ByVal source As Object, ByVal e As CallbackEventArgsBase)
        FillComboUnitKerja(TryCast(source, ASPxComboBox), e.Parameter, dsmSubCategoryLv1)
    End Sub
    Protected Sub FillComboUnitKerja(ByVal cmb As ASPxComboBox, ByVal id As String, ByVal source As SqlDataSource)
        cmb.Items.Clear()
        ' trap null selection
        If String.IsNullOrEmpty(id) Then
            Return
        End If

        ' get the values
        source.SelectParameters(0).DefaultValue = id
        Dim view As DataView = CType(source.Select(DataSourceSelectArguments.Empty), DataView)
        For Each row As DataRowView In view
            cmb.Items.Add(row(3).ToString(), row(2))
        Next row
    End Sub
    Protected Sub InitializeComboUnitKerja(ByVal e As ASPxGridViewEditorEventArgs, ByVal parentComboName As String, ByVal source As SqlDataSource, ByVal callBackHandler As CallbackEventHandlerBase)

        Dim id As String = String.Empty
        If (Not ASPxGridView1.IsNewRowEditing) Then
            Dim val As Object = ASPxGridView1.GetRowValuesByKeyValue(e.KeyValue, parentComboName)
            If (val Is Nothing OrElse val Is DBNull.Value) Then
                id = Nothing
            Else
                id = val.ToString()
            End If
        End If
        Dim combo As ASPxComboBox = TryCast(e.Editor, ASPxComboBox)
        If combo IsNot Nothing Then
            ' unbind combo
            combo.DataSourceID = Nothing
            FillComboUnitKerja(combo, id, source)
            AddHandler combo.Callback, callBackHandler
        End If
        Return
    End Sub
    Private Sub ASPxGridView1_CellEditorInitialize(sender As Object, e As ASPxGridViewEditorEventArgs) Handles ASPxGridView1.CellEditorInitialize
        Dim SelectData, val1, val2 As String
        val1 = ""
        val2 = ""
        dsmSubCategoryLv1.SelectCommand = "select * from mSubCategoryLv1 Where categoryid=@categoryid and NA='Y'"
        Select Case e.Column.FieldName
            'Case "JenisTransaksi"
            '    InitializeCombo(e, "CategoryID", dsmCategory, AddressOf cmbCombo2_OnCallback)
            Case "UnitKerja"
                InitializeComboUnitKerja(e, "CategoryID", dsmSubCategoryLv1, AddressOf cmbCombo2_OnCallback)
            Case Else
        End Select
        'If ASPxGridView1.GetRowValues(e.VisibleIndex, "JenisTransaksi") <> "" Then
        '    SelectData = "select * from mSubCategoryLv2 where ID='" & ASPxGridView1.GetRowValues(e.VisibleIndex, "ID") & "'"
        '    Com = New SqlCommand(SelectData, ConnectionTest)
        '    ConnectionTest.Open()
        '    Dr = Com.ExecuteReader()
        '    If Dr.Read() Then
        '        val1 = Dr("CategoryID")
        '        val2 = Dr("SubCategory1ID")
        '    End If
        '    Dr.Close()
        '    ConnectionTest.Close()

        '    If ASPxGridView1.IsEditing Then
        '        If e.Column.FieldName = "JenisTransaksi" Then
        '            e.Editor.Value = val1
        '        ElseIf e.Column.FieldName = "UnitKerja" Then
        '            e.Editor.Value = val2
        '        End If
        '    End If
        'Else
        '    dsSubject.SelectCommand = "select b.IDHandling, a.Name as JenisTransaksi,b.SubName as UnitKerja,c.SubName as SubjectTable,c.NA,c.ID from mCategory a left outer join mSubCategoryLv2 b on a.CategoryID = b.CategoryID left outer join mSubCategoryLv1 c on b.SubCategory1ID=c.SubCategory1ID where c.SubName <> ''"
        '    dsmCategory.SelectCommand = "select top 10 * from mCategory Where NA='Y'"
        '    dsmSubCategoryLv1.SelectCommand = "select * from mSubCategoryLv1 Where categoryid=@categoryid and NA='Y'"
        'End If

        'If ASPxGridView1.IsNewRowEditing Then
        '    If e.Column.FieldName = "NA" Then
        '        e.Editor.Value = "Y"
        '    End If
        'Else
        '    SelectData = "select * from mSubCategoryLv2 where ID='" & ASPxGridView1.GetRowValues(e.VisibleIndex, "ID") & "'"
        '    Com = New SqlCommand(SelectData, ConnectionTest)
        '    ConnectionTest.Open()
        '    Dr = Com.ExecuteReader()
        '    If Dr.Read() Then
        '        val1 = Dr("NA")
        '    End If
        '    Dr.Close()
        '    ConnectionTest.Close()

        '    If e.Column.FieldName = "NA" Then
        '        e.Editor.Value = val1
        '    End If
        'End If
        'Select Case e.Column.FieldName
        '    'Case "JenisTransaksi"
        '    '    InitializeCombo(e, "CategoryID", dsmCategory, AddressOf cmbCombo2_OnCallback)
        '    Case "UnitKerja"
        '        InitializeComboUnitKerja(e, "CategoryID", dsmSubCategoryLv1, AddressOf cmbCombo2_OnCallback)
        '    Case Else
        'End Select
        'If ASPxGridView1.IsNewRowEditing Then
        '    If e.Column.FieldName = "NA" Then
        '        e.Editor.Value = "Y"
        '    End If
        'Else
        'End If
    End Sub
    ''End

    Private Sub ASPxGridView1_RowInserting(sender As Object, e As DevExpress.Web.Data.ASPxDataInsertingEventArgs) Handles ASPxGridView1.RowInserting
        Dim cekJenisTransaksi As String
        If e.NewValues("JenisTransaksi") = Nothing Then
            cekJenisTransaksi = ""
        Else
            cekJenisTransaksi = e.NewValues("JenisTransaksi").ToString()
        End If
        Throw New Exception(cekJenisTransaksi & " already exists")
    End Sub
End Class