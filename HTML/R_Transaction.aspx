﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/HTML/Ticket.Master" CodeBehind="R_Transaction.aspx.vb" Inherits="ICC.R_Transaction" %>

<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxCallbackPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxGridView.Export" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="FeaturedContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div>
        <h4 class="headline">Report All Data
			<span class="line bg-warning"></span>
        </h4>
        <br />
        <dx:ASPxLabel Visible="false" runat="server" ID="sqlOutput"></dx:ASPxLabel>
    </div>
    <div class="row" style="margin-bottom: -15px;">
        <div class="col-sm-2">
            <label>Start Date</label>
            <dx:ASPxDateEdit ID="dt_strdate" runat="server" CssClass="form-control input-sm" Width="100%" EditFormatString="yyyy-MM-dd" DisplayFormatString="yyyy-MM-dd">
                <ValidationSettings ErrorTextPosition="Bottom" ErrorDisplayMode="ImageWithText" ValidationGroup="SMLvalidationGroup">
                    <RequiredField IsRequired="true" ErrorText="Must be filled" />
                </ValidationSettings>
            </dx:ASPxDateEdit>
        </div>
        <div class="col-sm-2">
            <label>End Date</label>
            <dx:ASPxDateEdit ID="dt_endate" runat="server" CssClass="form-control input-sm" Width="100%" EditFormatString="yyyy-MM-dd" DisplayFormatString="yyyy-MM-dd">
                <ValidationSettings ErrorTextPosition="Bottom" ErrorDisplayMode="ImageWithText" ValidationGroup="SMLvalidationGroup">
                    <RequiredField IsRequired="true" ErrorText="Must be filled" />
                </ValidationSettings>
            </dx:ASPxDateEdit>
        </div>
        <div class="col-sm-2" style="margin-top: 5px;">
            <br />
            <dx:ASPxButton ID="btn_Submit" runat="server" Theme="Metropolis" AutoPostBack="False" Text="Submit" ValidationGroup="SMLvalidationGroup"
                HoverStyle-BackColor="#EE4D2D" Height="30px" Width="100%">
            </dx:ASPxButton>
        </div>
    </div>
    <hr />
    <div class="row">
        <div class="col-sm-12">
            <div>
                <dx:ASPxGridView ID="ASPxGridView1" ClientInstanceName="ASPxGridView1" runat="server" Font-Size="X-Small" DataSourceID="LinqDataSource1"
                    AutoGenerateColumns="False" Theme="Metropolis" Width="100%" Styles-Header-Font-Bold="true" SettingsPager-PageSize="10">
                    <SettingsPager>
                        <AllButton Text="All">
                        </AllButton>
                        <NextPageButton Text="Next &gt;">
                        </NextPageButton>
                        <PrevPageButton Text="&lt; Prev">
                        </PrevPageButton>
                        <PageSizeItemSettings Visible="true" Items="15, 20, 25" ShowAllItem="true" />
                    </SettingsPager>
                    <Settings ShowFilterRow="true" ShowFilterRowMenu="false" ShowGroupPanel="true" ShowFilterBar="Hidden" EnableFilterControlPopupMenuScrolling="true"
                        ShowVerticalScrollBar="false" ShowFooter="false" ShowHorizontalScrollBar="true" />
                    <Columns>
                        <%--<dx:GridViewDataTextColumn Caption="No" FieldName="NomorTrx" Width="40px"></dx:GridViewDataTextColumn>--%>
                        <dx:GridViewDataTextColumn Caption="Interaction ID" FieldName="GenesysID" Width="200px" Settings-AutoFilterCondition="Contains"></dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="Thread ID" FieldName="ThreadID" Width="150px" Settings-AutoFilterCondition="Contains"></dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="Thread Ticket" FieldName="ThreadTicket" Width="150px" Settings-AutoFilterCondition="Contains"></dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="Ticket Number" FieldName="TicketNumber" Width="150px" Settings-AutoFilterCondition="Contains"></dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="Account" FieldName="AccountInbound" Width="150px" Settings-AutoFilterCondition="Contains"></dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="Account ID" FieldName="AccountID" Width="150px" Settings-AutoFilterCondition="Contains"></dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="Customer ID" FieldName="NIK" Width="150px" Settings-AutoFilterCondition="Contains"></dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="CIF Number" FieldName="CIF" Width="150px" Settings-AutoFilterCondition="Contains"></dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="Category" FieldName="CategoryName" Width="150px" Settings-AutoFilterCondition="Contains"></dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="Enquiry Type" FieldName="Level1" Width="150px" Settings-AutoFilterCondition="Contains"></dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="Enquiry Detail" FieldName="Level2" Width="250px" Settings-AutoFilterCondition="Contains"></dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="Reason" FieldName="Level3" Width="300px" Settings-AutoFilterCondition="Contains"></dx:GridViewDataTextColumn>
                        <%--<dx:GridViewDataTextColumn Caption="User Issue Remark" FieldName="Description" Width="300px" Settings-AutoFilterCondition="Contains"></dx:GridViewDataTextColumn>--%>
                        <dx:GridViewDataMemoColumn Caption="User Issue Remark" FieldName="Description" Width="300px" Settings-AutoFilterCondition="Contains"></dx:GridViewDataMemoColumn>
                         <dx:GridViewDataMemoColumn Caption="Agent Response" FieldName="ResponComplaint" Width="300px" Settings-AutoFilterCondition="Contains"></dx:GridViewDataMemoColumn>
                        <%--<dx:GridViewDataTextColumn Caption="Agent Response" FieldName="ResponComplaint" Width="300px" Settings-AutoFilterCondition="Contains"></dx:GridViewDataTextColumn>--%>
                        <dx:GridViewDataTextColumn Caption="Bank Product Type" FieldName="StrPenyebab" HeaderStyle-HorizontalAlign="left" Width="150px" Settings-AutoFilterCondition="Contains"></dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="Priority Scale" FieldName="SkalaPrioritas" HeaderStyle-HorizontalAlign="left" Width="150px"></dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="Source Information" FieldName="SumberInformasi" Width="150px" Settings-AutoFilterCondition="Contains"></dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="User Status Pelapor" FieldName="StrStatusPelapor" Width="150px" Settings-AutoFilterCondition="Contains"></dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="User Status" FieldName="JenisNasabah" HeaderStyle-HorizontalAlign="left" Width="150px" Settings-AutoFilterCondition="Contains"></dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="SLA" FieldName="SLA" HeaderStyle-HorizontalAlign="left" Width="150px" Settings-AutoFilterCondition="Contains"></dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="Name" FieldName="CustomerName" Width="150px" Settings-AutoFilterCondition="Contains"></dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="Email Address" FieldName="Email" Width="150px" Settings-AutoFilterCondition="Contains"></dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="Phone Number" FieldName="HP" Width="150px" Settings-AutoFilterCondition="Contains"></dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="VIP User" FieldName="CusStatus" Width="150px" Settings-AutoFilterCondition="Contains"></dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="Account Number" FieldName="NomorRekening" HeaderStyle-HorizontalAlign="left" Width="150px" Settings-AutoFilterCondition="Contains"></dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="Amount" FieldName="Amount" Width="150px" Settings-AutoFilterCondition="Contains"  PropertiesTextEdit-DisplayFormatString="{0:n2}"></dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="Address" FieldName="Alamat" Width="250px" Settings-AutoFilterCondition="Contains"></dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="Channel" FieldName="TicketSourceName" Width="150px" Settings-AutoFilterCondition="Contains"></dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="Ticket Status" FieldName="TicketStatus" Width="160px" Settings-AutoFilterCondition="Contains"></dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="Created By" FieldName="CreatedBy" Width="150px" Settings-AutoFilterCondition="Contains"></dx:GridViewDataTextColumn>
                        <dx:GridViewDataDateColumn Caption="Created Date" FieldName="CreatedDate" Width="150px" Settings-AutoFilterCondition="Contains" PropertiesDateEdit-DisplayFormatString="yyyy-MM-dd hh:mm:ss"></dx:GridViewDataDateColumn>
                        <dx:GridViewDataTextColumn Caption="Solved By" FieldName="UserSolved" Width="150px" Settings-AutoFilterCondition="Contains"></dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="Solved Date" FieldName="DateSolvedDisplay" Width="150px" Settings-AutoFilterCondition="Contains"></dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="Closed By" FieldName="ClosedBy" Width="150px" Settings-AutoFilterCondition="Contains"></dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="Closed Date" FieldName="DateClosedDisplay" Width="150px" Settings-AutoFilterCondition="Contains"></dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="Last Response By" FieldName="LastResponseBy" Width="150px" Settings-AutoFilterCondition="Contains"></dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="Last Response Date" FieldName="LastResponseDate" Width="150px" Settings-AutoFilterCondition="Contains"></dx:GridViewDataTextColumn>
                    </Columns>
                </dx:ASPxGridView>
            </div>
        </div>
    </div>
    <hr />
    <div class="row">
        <div class="col-sm-2">
            <asp:DropDownList runat="server" ID="ddList" Height="30" CssClass="form-control input-sm">
                <asp:ListItem Value="xlsx" Text="Excel" />
                <asp:ListItem Value="xls" Text="Excel 97-2003" />
                <asp:ListItem Value="pdf" Text="PDF" />
                <asp:ListItem Value="rtf" Text="RTF" />
                <asp:ListItem Value="csv" Text="CSV" />
            </asp:DropDownList>
        </div>
        <div class="col-sm-2">
            <dx:ASPxButton ID="btn_Export" runat="server" Text="Export" Theme="Metropolis" ValidationGroup="SMLvalidationGroup"
                HoverStyle-BackColor="#EE4D2D" Height="30px" Width="100%">
            </dx:ASPxButton>
        </div>
    </div>
    <hr />
    <dx:ASPxGridViewExporter ID="ASPxGridViewExporter1" runat="server" GridViewID="ASPxGridView1"></dx:ASPxGridViewExporter>
     <asp:LinqDataSource ID="LinqDataSource1" runat="server" ContextTypeName="ICC.R_Transaction_View_DBMLDataContext"
        EntityTypeName="" TableName="V_TransactionS" Where="CreatedDate >= @StartTanggalFilter And CreatedDate <= @EndTanggalFilter" OrderBy="CreatedDate ASC">
        <WhereParameters>
            <asp:SessionParameter Name="StartTanggalFilter" SessionField="StartTanggalFilter" Type="DateTime"/>
            <asp:SessionParameter Name="EndTanggalFilter" SessionField="EndTanggalFilter" Type="DateTime"/>
        </WhereParameters>
    </asp:LinqDataSource>
</asp:Content>
