﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/HTML/Ticket.Master" CodeBehind="docHTML.aspx.vb" Inherits="ICC.docHTML" %>

<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxFileManager" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="FeaturedContent" runat="server">
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <div class="row">
        <div class="col-md-12">
            <h4 class="headline">Document HTML
			<span class="line bg-info"></span>
            </h4>
            <dx:ASPxFileManager ID="fileManager" runat="server" Width="100%">
                <Settings RootFolder="~/HTML/uConnector/uConnector_Files" ThumbnailFolder="~/tmp/Thumbnails"
                    AllowedFileExtensions=".doc,.docx,.xls,.xlsx,.jpg,.pdf,.html"
                    InitialFolder="DocumentBC" />
                <SettingsFileList View="Details">
                    <DetailsViewSettings AllowColumnResize="true" AllowColumnDragDrop="true" AllowColumnSort="true" ShowHeaderFilterButton="true" />
                </SettingsFileList>
                <%--<SettingsToolbar ShowDownloadButton="true" />--%>
                <%--<SettingsEditing AllowCreate="true" />--%>
            </dx:ASPxFileManager>
        </div>
    </div>
</asp:Content>
