﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" CodeBehind="uploadcontrol.aspx.vb" Inherits="ICC.uploadcontrol" %>

<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxRoundPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxUploadControl" TagPrefix="dx" %>

<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript">
    function deleteattachment(id) {
        if (confirm("Do you want to process?")) {
            var test = new FormData();
            $.ajax({
                type: "POST",
                url: "AjaxPages/selectattchment.aspx?id=" + id,
                contentType: false,
                processData: false,
                data: test,
                success: function (response) {
                    alert(response);
                    var idticket = '<%=Request.QueryString("idticketutama")%>';
                    window.location.href = "uploadcontrol.aspx?idticketutama=" + idticket + ""
                }
            });

            return true;
        }
        else
            return false;
    }
</script>
<!-- Bootstrap core CSS -->
<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
<script type="text/javascript">
    // <![CDATA[
    var fieldSeparator = "|";
    function FileUploadStart() {
        document.getElementById("uploadedListFiles").innerHTML = "";
    }
    function FileUploaded(s, e) {
        if (e.isValid) {
            //var linkFile = document.createElement("a");
            //var indexSeparator = e.callbackData.indexOf(fieldSeparator);
            //var fileName = e.callbackData.substring(0, indexSeparator);
            //var pictureUrl = e.callbackData.substring(indexSeparator + fieldSeparator.length);
            //var date = new Date();
            //var imgSrc = "~/attachment/sending/" + pictureUrl + "?dx=" + date.getTime();
            //linkFile.innerHTML = fileName;
            //linkFile.setAttribute("href", imgSrc);
            //linkFile.setAttribute("target", "_blank");
            //var container = document.getElementById("uploadedListFiles");
            //container.appendChild(linkFile);
            //container.appendChild(document.createElement("br"));
            //loadDataTicket();
            var idticket = '<%=Request.QueryString("idticketutama")%>';
            window.location.href = "uploadcontrol.aspx?idticketutama=" + idticket + ""
        }
    }
    // ]]>
</script>

<form runat="server" style="width: 1060px; height: 380px; overflow: hidden;">
    <%--<a href="#" onclick="loadDataTicket()">test</a>--%>
    <br />
    <div id="divFileUpload" runat="server">
        <label style="color: red; font-size: x-small;">* Maks 3 Mb</label>
        <dx:ASPxUploadControl ID="UploadControl" runat="server" ShowAddRemoveButtons="false" Theme="Metropolis" BrowseButtonStyle-Font-Size="11px" ButtonStyle-Font-Size="11px"
            Width="100%" ShowUploadButton="True" AddUploadButtonsHorizontalPosition="left" Height="32px"
            ShowProgressPanel="True" ClientInstanceName="UploadControl" OnFileUploadComplete="UploadControl_FileUploadComplete"
            FileInputCount="1" UploadMode="Advanced">
            <ValidationSettings MaxFileSize="3194304" ShowErrors="true" MaxFileSizeErrorText="file cannot be more than 3 MB" ErrorStyle-Font-Size="Small"
                AllowedFileExtensions=".jpg,.jpeg,.jpe,.gif,.bmp,.png,.xls,.xlsx,.pdf,.PNG,.JPG,.JPEG,.GIF,.BMP,.doc,.docx,.rar,.zip">
            </ValidationSettings>
            <ClientSideEvents FileUploadComplete="function(s, e) { FileUploaded(s, e) }" FileUploadStart="function(s, e) { FileUploadStart(); }" />
        </dx:ASPxUploadControl>
    </div>
    <br />
    <div id="divtable" runat="server" visible="false">
        <dx:ASPxGridView ID="ASPxGridView4" ClientInstanceName="ASPxGridView4" Width="1160px" runat="server" DataSourceID="ds_attchment" KeyFieldName="ID"
            SettingsPager-PageSize="5" Theme="Metropolis" Styles-Header-Font-Bold="true" Font-Size="X-Small">
            <SettingsPager>
                <AllButton Text="All">
                </AllButton>
                <NextPageButton Text="Next &gt;">
                </NextPageButton>
                <PrevPageButton Text="&lt; Prev">
                </PrevPageButton>
                <PageSizeItemSettings Visible="true" Items="10, 15, 20" ShowAllItem="true" />
            </SettingsPager>
            <SettingsEditing Mode="Inline" />
            <Settings ShowFilterRow="false" ShowGroupPanel="false" ShowHorizontalScrollBar="true" />
            <SettingsBehavior ConfirmDelete="true" />
            <Columns>
                <dx:GridViewCommandColumn Caption="Action" HeaderStyle-HorizontalAlign="Center" VisibleIndex="0"
                    ButtonType="Image" FixedStyle="Left" Width="40px">
                    <DeleteButton Visible="true">
                        <Image ToolTip="Delete" Url="img/icon/Actions-edit-clear-icon2.png" />
                    </DeleteButton>
                </dx:GridViewCommandColumn>
                <%--<dx:GridViewDataTextColumn Caption="Ticket Number" FieldName="TicketNumber" HeaderStyle-HorizontalAlign="left" Width="150px"></dx:GridViewDataTextColumn>--%>
                <dx:GridViewDataTextColumn Caption="Filename" FieldName="Path" HeaderStyle-HorizontalAlign="left" Width="500px"></dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="User Create" FieldName="Usercreate" HeaderStyle-HorizontalAlign="left" Width="150px"></dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn Caption="Date" FieldName="Datecreate" HeaderStyle-HorizontalAlign="left" Width="150px"></dx:GridViewDataTextColumn>
                <dx:GridViewDataColumn Caption="Action" CellStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" Width="40px">
                    <DataItemTemplate>
                        <a href="<%# Eval("Path")%>" target="_blank">Download</a>
                    </DataItemTemplate>
                </dx:GridViewDataColumn>
            </Columns>
        </dx:ASPxGridView>
    </div>
    <div id="uploadedListFiles" style="height: 20px; font-family: Arial;">
    </div>
    <asp:sqldatasource id="ds_attchment" runat="server" connectionstring="<%$ ConnectionStrings:DefaultConnection %>" deletecommand="delete from BTN_Trx_Attchment where ID=@ID"></asp:sqldatasource>
    <%--<div style="visibility: hidden;">
        <dx:ASPxRoundPanel ID="ASPxRoundPanel1" runat="server" Width="550px" ClientInstanceName="RoundPanel"
            HeaderText="Uploaded files (jpeg, gif)" Height="20%">
            <PanelCollection>
                <dx:PanelContent ID="PanelContent1" runat="server">
                    
                </dx:PanelContent>
            </PanelCollection>
        </dx:ASPxRoundPanel>
    </div>--%>
</form>
