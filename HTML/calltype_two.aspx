﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/HTML/Ticket.Master" CodeBehind="calltype_two.aspx.vb" Inherits="ICC.calltype_two" %>

<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxGridLookup" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxCallbackPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxHtmlEditor.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxHtmlEditor" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="FeaturedContent" runat="server">
    <script type="text/javascript">
        function OnJenisTransaksiChange(cmbParent) {
            var comboValue = cmbParent.GetSelectedItem().value;
            if (comboValue)
                ASPxGridView1.GetEditor("UnitKerja").PerformCallback(comboValue.toString());
            //alert(comboValue.toString());
        }
    </script>
    <script>
        var isUpdated = false;
        function OnBeginCallback(s, e) {
            if (e.command == "UPDATEEDIT")
                isUpdated = true;
        }
        function OnEndCallback(s, e) {
            if (isUpdated) {
                //do something
                //alert("READY!");
                isUpdated = false;
                test.SetText("");
            }
        }
        function OnGridFocusedRowChanged() {
            var grid = ASPxGridLookup1.GetGridView();
            grid.GetRowValues(grid.GetFocusedRowIndex(), 'ORGANIZATION_ID', OnGetRowValues);
        }
        function OnGetRowValues(values) {
            var temp = test.GetText();
            temp = (temp == null || temp == "") ? "" : temp + ',';
            test.SetText(temp + values);
            //alert(temp + values);
        }
        function OnGridFocusedRowChangedList() {
            var aa;
            for (var i = 0; i < ASPxListBox1.GetItemCount() ; i++) {
                //items.push(ASPxListBox1.GetSelectedItem(i));
                var item = ASPxListBox1.GetSelectedItem(i);
                if (item.selected === true) {

                    aa += item.value + ',';
                    break;
                } else {

                }
            }
        }
        function Clear() {
            ASPxGridLookup1.GetGridView().UnselectAllRowsOnPage(); ASPxGridLookup1.HideDropDown();
        }
    </script>
    <script>
        function OnStartEditing(s, e) {
            if (e.focusedColumn.fieldName === "TagIDs") {
                gl.GetGridView().UnselectAllRowsOnPage();
                gl.SetValue(e.rowValues[e.focusedColumn.index].value);
                prevColumnIndex = e.focusedColumn.index;
            }

        }
        function OnEndEditing(s, e) {
            if (prevColumnIndex == null) return;
            e.rowValues[prevColumnIndex].value = gl.GetGridView().GetSelectedKeysOnPage();
            e.rowValues[prevColumnIndex].text = gl.GetText();
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <h4 class="headline">Data Enquiry Detail
			<span class="line bg-warning"></span>
    </h4>
    <dx:ASPxGridView ID="ASPxGridView1" ClientInstanceName="ASPxGridView1" Width="100%" runat="server" DataSourceID="dsSubject"
        KeyFieldName="ID" SettingsPager-PageSize="15" Theme="Metropolis" Styles-Header-Font-Bold="true" Font-Size="X-Small">
        <SettingsPager>
            <AllButton Text="All">
            </AllButton>
            <NextPageButton Text="Next &gt;">
            </NextPageButton>
            <PrevPageButton Text="&lt; Prev">
            </PrevPageButton>
            <PageSizeItemSettings Visible="true" Items="25, 50, 75" ShowAllItem="true" />
        </SettingsPager>
        <SettingsEditing Mode="inline" />
        <Settings ShowFilterRow="true" ShowFilterRowMenu="false" ShowGroupPanel="true" />
        <SettingsBehavior ConfirmDelete="true" />
        <Columns>
            <dx:GridViewCommandColumn Caption="Action" HeaderStyle-HorizontalAlign="Center" VisibleIndex="0"
                ButtonType="Image" FixedStyle="Left" Width="100px">
                <EditButton Visible="True">
                    <Image ToolTip="Edit" Url="img/icon/Text-Edit-icon2.png" />
                </EditButton>
                <NewButton Visible="True">
                    <Image ToolTip="New" Url="img/icon/Apps-text-editor-icon2.png" />
                </NewButton>
                <DeleteButton Visible="false">
                    <Image ToolTip="Delete" Url="img/icon/Actions-edit-clear-icon2.png" />
                </DeleteButton>
                <CancelButton>
                    <Image ToolTip="Cancel" Url="img/icon/cancel1.png">
                    </Image>
                </CancelButton>
                <UpdateButton>
                    <Image ToolTip="Update" Url="img/icon/Updated1.png" />
                </UpdateButton>
            </dx:GridViewCommandColumn>
            <dx:GridViewDataTextColumn Caption="ID" FieldName="ID" Visible="false"
                Width="10px" HeaderStyle-HorizontalAlign="Center">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataComboBoxColumn FieldName="JenisTransaksi" Caption="Category" Settings-AutoFilterCondition="Contains" Width="250px" Settings-FilterMode="DisplayText">
                <PropertiesComboBox IncrementalFilteringMode="Contains" TextFormatString="{1}" TextField="Name" ValueField="CategoryID" DataSourceID="dsmCategory">
                    <Columns>
                        <dx:ListBoxColumn Caption="ID" FieldName="CategoryID" Width="80px" />
                        <dx:ListBoxColumn Caption="Jenis Transaksi" FieldName="Name" Width="150px" />
                    </Columns>
                    <ClientSideEvents SelectedIndexChanged="function(s, e) { OnJenisTransaksiChange(s); }"></ClientSideEvents>
                </PropertiesComboBox>
            </dx:GridViewDataComboBoxColumn>
            <dx:GridViewDataComboBoxColumn FieldName="UnitKerja" Caption="Enquiry Type" Settings-FilterMode="DisplayText" Settings-AutoFilterCondition="Contains" Width="250px">
                <PropertiesComboBox IncrementalFilteringMode="Contains" TextFormatString="{1}" TextField="SubName" ValueField="SubCategory1ID" DataSourceID="dsmSubCategoryLv1">
                </PropertiesComboBox>
            </dx:GridViewDataComboBoxColumn>
            <dx:GridViewDataTextColumn Caption="Enquiry Detail" FieldName="SubjectTable" Settings-FilterMode="DisplayText" Settings-AutoFilterCondition="Contains" Width="250px">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataComboBoxColumn Caption="Status" FieldName="NA" HeaderStyle-HorizontalAlign="Center"
                Width="100px">
                <PropertiesComboBox>
                    <Items>
                        <dx:ListEditItem Text="Active" Value="Y" />
                        <dx:ListEditItem Text="In Active" Value="N" />
                    </Items>
                </PropertiesComboBox>
            </dx:GridViewDataComboBoxColumn>
        </Columns>
        <%--<ClientSideEvents BatchEditStartEditing="OnStartEditing" BatchEditEndEditing="OnEndEditing" />--%>
        <ClientSideEvents BeginCallback="OnBeginCallback" EndCallback="OnEndCallback" />
    </dx:ASPxGridView>

    <asp:SqlDataSource ID="sqlDataSource3" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>" SelectCommand="select * from v_MasterReportLabel"></asp:SqlDataSource>
    <asp:SqlDataSource ID="sqlDataSource4" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>" SelectCommand="select * from mSubCategoryLv2"></asp:SqlDataSource>
    <asp:SqlDataSource ID="dsSubject" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>"></asp:SqlDataSource>
    <asp:SqlDataSource ID="dsmCategory" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>"></asp:SqlDataSource>
    <asp:SqlDataSource ID="dsmSubCategoryLv1" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>"
        SelectCommand="select * from mSubCategoryLv1 Where NA='Y' and CategoryID=@CategoryID">
        <SelectParameters>
            <asp:Parameter DefaultValue="0" Name="CategoryID" Type="String" />
        </SelectParameters>
    </asp:SqlDataSource>
    <div style="visibility: hidden; height: 0px;">
        <dx:ASPxTextBox ID="test" runat="server" ClientInstanceName="test" />
    </div>
</asp:Content>
