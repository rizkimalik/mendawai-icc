﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/HTML/Ticket.Master" CodeBehind="Temp_ExtendPending.aspx.vb" Inherits="ICC.Temp_ExtendPending" %>

<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxCallbackPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxHtmlEditor.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxHtmlEditor" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="FeaturedContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <h4 class="headline">Data Pending Auto Closed
			<span class="line bg-warning"></span>
    </h4>
    <dx:ASPxGridView ID="ASPxGridView1" ClientInstanceName="ASPxGridView1" Width="100%" runat="server" AutoGenerateColumns="False" DataSourceID="ds_query" KeyFieldName="ID"
        Theme="Metropolis" Styles-Header-Font-Bold="true" Font-Size="X-Small">
        <SettingsPager>
            <AllButton Text="All">
            </AllButton>
            <NextPageButton Text="Next &gt;">
            </NextPageButton>
            <PrevPageButton Text="&lt; Prev">
            </PrevPageButton>
            <PageSizeItemSettings Visible="true" Items="10, 15, 20" ShowAllItem="true" />
        </SettingsPager>
        <SettingsEditing Mode="Inline" />
        <Settings ShowFilterRow="true" ShowFilterRowMenu="false" ShowFilterBar="Hidden" ShowVerticalScrollBar="false"
            ShowGroupPanel="false" />
        <SettingsBehavior ConfirmDelete="true" />
        <Columns>
            <dx:GridViewCommandColumn Caption="Action" HeaderStyle-HorizontalAlign="Center" VisibleIndex="0"
                ButtonType="Image" FixedStyle="Left" Width="100px">
                <EditButton Visible="True">
                    <Image ToolTip="Edit" Url="img/icon/Text-Edit-icon2.png" />
                </EditButton>
                <NewButton Visible="True">
                    <Image ToolTip="New" Url="img/icon/Apps-text-editor-icon2.png" />
                </NewButton>
                <DeleteButton Visible="false">
                    <Image ToolTip="Delete" Url="img/icon/Actions-edit-clear-icon2.png" />
                </DeleteButton>
                <CancelButton>
                    <Image ToolTip="Cancel" Url="img/icon/cancel1.png">
                    </Image>
                </CancelButton>
                <UpdateButton>
                    <Image ToolTip="Update" Url="img/icon/Updated1.png" />
                </UpdateButton>
            </dx:GridViewCommandColumn>
            <dx:GridViewDataTextColumn FieldName="ID" ReadOnly="True" Visible="false" VisibleIndex="1">
                <EditFormSettings Visible="False" />
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataComboBoxColumn FieldName="CategoryID" Caption="Category" Settings-AutoFilterCondition="Contains" Width="130px" Settings-FilterMode="DisplayText">
                <PropertiesComboBox IncrementalFilteringMode="Contains" TextFormatString="{1}" TextField="Name" ValueField="CategoryID" DataSourceID="ds_Category">
                    <Columns>
                        <dx:ListBoxColumn Caption="ID" FieldName="CategoryID" Width="80px" />
                        <dx:ListBoxColumn Caption="Category" FieldName="Name" Width="150px" />
                    </Columns>
                </PropertiesComboBox>
            </dx:GridViewDataComboBoxColumn>
            <dx:GridViewDataComboBoxColumn FieldName="StatusID" Caption="Status" Settings-AutoFilterCondition="Contains" Settings-FilterMode="DisplayText">
                <PropertiesComboBox IncrementalFilteringMode="Contains" TextFormatString="{1}" TextField="status" ValueField="ID" DataSourceID="ds_Status">
                    <Columns>
                        <dx:ListBoxColumn Caption="ID" FieldName="ID" Width="80px" />
                        <dx:ListBoxColumn Caption="Status" FieldName="status" Width="150px" />
                    </Columns>
                </PropertiesComboBox>
            </dx:GridViewDataComboBoxColumn>
            <%--<dx:GridViewDataTextColumn FieldName="NameStatus" Caption="Status Type"></dx:GridViewDataTextColumn>--%>
            <dx:GridViewDataTextColumn FieldName="SLA" Caption="SLA" Width="130px" CellStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"></dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="UserCreate" ReadOnly="true" Caption="Created By" Width="130px"></dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn FieldName="DateCreate" ReadOnly="true" Caption="Created Date" Width="130px"></dx:GridViewDataTextColumn>
            <dx:GridViewDataComboBoxColumn Caption="State" FieldName="NA" Width="50px">
                <PropertiesComboBox>
                    <Items>
                        <dx:ListEditItem Text="Active" Value="Y" />
                        <dx:ListEditItem Text="In Active" Value="N" />
                    </Items>
                </PropertiesComboBox>
            </dx:GridViewDataComboBoxColumn>
        </Columns>
        <Settings ShowGroupPanel="True" />
    </dx:ASPxGridView>
    <asp:SqlDataSource ID="ds_Category" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>" SelectCommand="select * from mCategory"></asp:SqlDataSource>
    <asp:SqlDataSource ID="ds_Status" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>" SelectCommand="select * from mStatus"></asp:SqlDataSource>
    <asp:SqlDataSource ID="ds_query" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>"
        DeleteCommand="DELETE FROM [Temp_RFC_ExtendSubStatus] WHERE [ID] = @ID"
        InsertCommand="INSERT INTO [Temp_RFC_ExtendSubStatus] ([CategoryID], [StatusID], [SLA], [NA], [UserCreate]) VALUES (@CategoryID, @StatusID, @SLA, @NA, @username)"
        SelectCommand="SELECT * FROM Temp_RFC_ExtendSubStatus"
        UpdateCommand="UPDATE [Temp_RFC_ExtendSubStatus] SET [CategoryID] = @CategoryID, [StatusID] = @StatusID, [SLA] = @SLA, [NA]=@NA, [UserCreate]=@username WHERE [ID] = @ID">
        <DeleteParameters>
            <asp:Parameter Name="ID" Type="Int32" />
        </DeleteParameters>
        <InsertParameters>
            <asp:Parameter Name="CategoryID" Type="String" />
            <asp:Parameter Name="StatusID" Type="String" />
            <%--<asp:Parameter Name="NameStatus" Type="String" />--%>
            <asp:Parameter Name="SLA" Type="String" />
            <asp:Parameter Name="NA" Type="String" />
            <asp:Parameter Name="username" Type="String" />
        </InsertParameters>
        <UpdateParameters>
            <asp:Parameter Name="CategoryID" Type="String" />
            <asp:Parameter Name="StatusID" Type="String" />
            <%--<asp:Parameter Name="NameStatus" Type="String" />--%>
            <asp:Parameter Name="SLA" Type="String" />
            <asp:Parameter Name="NA" Type="String" />
            <asp:Parameter Name="username" Type="String" />
        </UpdateParameters>
    </asp:SqlDataSource>
</asp:Content>
