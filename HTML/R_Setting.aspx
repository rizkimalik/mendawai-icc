﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/HTML/Ticket.Master" CodeBehind="R_Setting.aspx.vb" Inherits="ICC.R_Setting" %>

<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxCallbackPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxHtmlEditor.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxHtmlEditor" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="FeaturedContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <h4 class="headline">Setting Export Data Reporting 
			<span class="line bg-warning"></span>
    </h4>
    <dx:ASPxGridView ID="ASPxGridView1" ClientInstanceName="ASPxGridView1" Width="100%" runat="server" AutoGenerateColumns="False" DataSourceID="ds_query" KeyFieldName="ID"
        Theme="Metropolis" Styles-Header-Font-Bold="true" Font-Size="X-Small">
        <SettingsPager>
            <AllButton Text="All">
            </AllButton>
            <NextPageButton Text="Next &gt;">
            </NextPageButton>
            <PrevPageButton Text="&lt; Prev">
            </PrevPageButton>
            <PageSizeItemSettings Visible="true" Items="10, 15, 20" ShowAllItem="true" />
        </SettingsPager>
        <SettingsEditing Mode="Inline" />
        <Settings ShowFilterRow="true" ShowFilterRowMenu="false" ShowFilterBar="Hidden" ShowVerticalScrollBar="false"
            ShowGroupPanel="false" />
        <SettingsBehavior ConfirmDelete="true" />
        <Columns>
            <dx:GridViewCommandColumn Caption="Action" HeaderStyle-HorizontalAlign="Center" VisibleIndex="0"
                ButtonType="Image" FixedStyle="Left" Width="70px">
                <EditButton Visible="true">
                    <Image ToolTip="Edit" Url="img/Icon/Text-Edit-icon2.png" />
                </EditButton>
                <NewButton Visible="false">
                    <Image ToolTip="New" Url="img/Icon/Apps-text-editor-icon2.png" />
                </NewButton>
                <DeleteButton Visible="false">
                    <Image ToolTip="Delete" Url="img/Icon/Actions-edit-clear-icon2.png" />
                </DeleteButton>
                <CancelButton Visible="true">
                    <Image ToolTip="Cancel" Url="img/icon/cancel1.png">
                    </Image>
                </CancelButton>
                <UpdateButton Visible="true">
                    <Image ToolTip="Update" Url="img/icon/Updated1.png" />
                </UpdateButton>
            </dx:GridViewCommandColumn>
            <dx:GridViewDataTextColumn FieldName="ID" ReadOnly="True" Visible="false" VisibleIndex="1">
                <EditFormSettings Visible="False" />
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataComboBoxColumn Caption="Day" FieldName="Value" VisibleIndex="3">
                <PropertiesComboBox>
                    <Items>
                        <dx:ListEditItem Text="-30" Value="-30" />
                        <dx:ListEditItem Text="-60" Value="-60" />
                        <dx:ListEditItem Text="-90" Value="-90" />
                        <dx:ListEditItem Text="-120" Value="-120" />
                        <dx:ListEditItem Text="-150" Value="-150" />
                        <dx:ListEditItem Text="-180" Value="-180" />
                    </Items>
                </PropertiesComboBox>
            </dx:GridViewDataComboBoxColumn>
        </Columns>
        <Settings ShowGroupPanel="True" />
    </dx:ASPxGridView>
    <asp:SqlDataSource ID="ds_query" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>"
        DeleteCommand="DELETE FROM [TR_Data_Setting] WHERE [ID] = @ID"
        InsertCommand="INSERT INTO [TR_Data_Setting] ([Value], [CreatedBy]) VALUES (@Value, @username)"
        SelectCommand="SELECT * FROM TR_Data_Setting Where Type='Report'"
        UpdateCommand="UPDATE [TR_Data_Setting] SET [Value] = @Value, [CreatedBy]=@username WHERE [ID] = @ID">
        <DeleteParameters>
            <asp:Parameter Name="ID" Type="Int32" />
        </DeleteParameters>
        <InsertParameters>
            <asp:Parameter Name="Value" Type="String" />
            <asp:Parameter Name="username" Type="String" />
        </InsertParameters>
        <UpdateParameters>
            <asp:Parameter Name="Value" Type="String" />
            <asp:Parameter Name="username" Type="String" />
        </UpdateParameters>
    </asp:SqlDataSource>
</asp:Content>
