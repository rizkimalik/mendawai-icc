﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/HTML/Ticket.Master"
    CodeBehind="dash_sosialmedia.aspx.vb" Inherits="ICC.dash_sosialmedia" %>

    <asp:Content ID="Content1" ContentPlaceHolderID="FeaturedContent" runat="server">
        <script src="js/echarts.min.js"></script>
        <script>
            const URL_API = '../API/dash_socialmedia.php';
            async function DashTotalChannel() {
                try {
                    const res = await fetch(URL_API + '?action=total_channel', {
                        method: 'GET',
                        headers: {
                            "Content-Type": "application/json"
                        }
                    });
                    const obj = await res.json();
                    const data = obj.data;
                    // console.log(obj)

                    let html = '';
                    let total = 0;
                    let queing = 0;
                    if (obj.status === 200) {
                        for (let i = 0; i < data.length; i++) {
                            html += `<div class="col-md-3 col-sm-4">
                                <div class="panel panel-default panel-stat2">
                                    <div class="panel-body">
                                        <span class="stat-icon avatar"> 
											<img src="img/channel/${(data[i].ChannelName).toLowerCase()}.png" class="img-circle" alt="Channel"> 
										</span>
                                        <div class="pull-right text-right">
                                            <h2 class="m-top-none"><span>${data[i].Total}</span></h2>
                                            <h5>${data[i].ChannelName}</h5>
                                            <i class="fa fa-clock-o fa-lg"></i><span class="m-left-xs">${data[i].Queing} Queing</span>
                                        </div>

                                    </div>
                                </div>
                            </div>`;
                            total += data[i].Total;
                            queing += data[i].Queing;
                        }
                        html += `<div class="col-md-3 col-sm-4">
                            <div class="panel panel-default panel-stat2">
                                <div class="panel-body">
                                    <span class="stat-icon avatar"> 
                                        <img src="img/channel/all_channel.png" class="img-circle" alt="Channel"> 
                                    </span>
                                    <div class="pull-right text-right">
                                        <h2 class="m-top-none"><span>${total}</span></h2>
                                        <h5>All Channel</h5>
                                        <i class="fa fa-clock-o fa-lg"></i><span class="m-left-xs">${queing} Queing</span>
                                    </div>
                                </div>
                            </div>
                        </div>`;
                        document.getElementById('DashTotalChannel').innerHTML = html;
                    }
                } catch (error) {
                    console.log(error)
                }
            }

            async function DashAgentOnline() {
                try {
                    const res = await fetch(URL_API + '?action=agent_online', {
                        method: 'GET',
                        headers: {
                            "Content-Type": "application/json"
                        }
                    });
                    const obj = await res.json();
                    const data = obj.data;
                    // console.log(obj)

                    let html = '';
                    if (obj.status === 200) {
                        for (let i = 0; i < data.length; i++) {
                            html += `<div class="panel-group" id="accordion-${data[i].username}">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion-${data[i].username}" href="#collapse-${data[i].username}">
                                                <i class="fa fa-user"></i> ${data[i].username}
                                                <span class="pull-right">
                                                    Handle
                                                    <span class="badge badge-success"> ${data[i].total_handle}</span>
                                                </span>
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapse-${data[i].username}" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <ul class="list-group">
                                                <li class="list-group-item clearfix inbox-item">
                                                    <span class="from">Max Chat</span>
                                                    <span class="inline-block pull-right">
                                                        <span class="label label-info">${data[i].max_chat}</span>
                                                    </span>
                                                </li>
                                                <li class="list-group-item clearfix inbox-item">
                                                    <span class="from">Max Whatsapp</span>
                                                    <span class="inline-block pull-right">
                                                        <span class="label label-info">${data[i].max_whatsapp}</span>
                                                    </span>
                                                </li>
                                                <li class="list-group-item clearfix inbox-item">
                                                    <span class="from">Max Email</span>
                                                    <span class="inline-block pull-right">
                                                        <span class="label label-info">${data[i].max_email}</span>
                                                    </span>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>`;
                        }
                        document.getElementById('DashAgentOnline').innerHTML = html;
                    }
                } catch (error) {
                    console.log(error)
                }
            }

            async function DashTop10Chat() {
                try {
                    const res = await fetch(URL_API + '?action=top10_chat', {
                        method: 'GET',
                        headers: {
                            "Content-Type": "application/json"
                        }
                    });
                    const obj = await res.json();
                    const data = obj.data;
                    // console.log(obj)

                    let html = '';
                    if (obj.status === 200) {
                        for (let i = 0; i < data.length; i++) {
                            html += `<tr>
										<td>${data[i].customer_id}</td>
										<td>${data[i].user_id}</td>
										<td>${data[i].name}</td>
										<td>${data[i].message}</td>
										<td><span class="badge badge-info">${data[i].channel}</span></td>
										<td>${data[i].date_create}</td>
										<td>${data[i].agent_handle}</td>
									</tr>`;
                        }
                        document.getElementById('DashGridTop10').innerHTML = "";
                        document.getElementById('DashGridTop10').innerHTML = html;
                    }
                } catch (error) {
                    console.log(error)
                }
            }

            async function DashGrafikChannel() {
                try {
                    const chartDom = document.getElementById('DashTotalChart');
                    const DashTotalChart = echarts.init(chartDom);
                    const option = {
                        legend: {},
                        tooltip: {
                            trigger: 'axis',
                            axisPointer: {
                                type: 'shadow'
                            }
                        },
                        xAxis: {
                            type: 'category',
                            data: []
                        },
                        yAxis: {},
                        series: []
                    }

                    const res = await fetch(URL_API + '?action=grafik_channel', {
                        method: 'GET',
                        headers: {
                            "Content-Type": "application/json"
                        }
                    });
                    const obj = await res.json();
                    const data = obj.data;
                    // console.log(obj)

                    if (obj.status === 200) {
                        let series_chat = [];
                        let series_whatsapp = [];
                        let series_email = [];
                        let series_call = [];
                        let series_facebook = [];
                        let series_twitter = [];
                        let series_instagram = [];

                        for (let i = 0; i < data.length; i++) {
                            option.xAxis.data.push(data[i].Time);

                            series_chat.push(data[i].Chat);
                            series_whatsapp.push(data[i].Whatsapp);
                            series_email.push(data[i].Email);
                            series_call.push(data[i].Call);
                            series_facebook.push(data[i].Facebook);
                            series_twitter.push(data[i].Twitter);
                            series_instagram.push(data[i].Instagram);
                        }

                        option.series.push({
                            name: 'Chat',
                            type: 'bar',
                            data: series_chat,
                        }, {
                            name: 'Whatsapp',
                            type: 'bar',
                            data: series_whatsapp,
                        }, {
                            name: 'Email',
                            type: 'bar',
                            data: series_email,
                        }, {
                            name: 'Call',
                            type: 'bar',
                            data: series_call,
                        }, {
                            name: 'Facebook',
                            type: 'bar',
                            data: series_facebook,
                        }, {
                            name: 'Twitter',
                            type: 'bar',
                            data: series_twitter,
                        }, {
                            name: 'Instagram',
                            type: 'bar',
                            data: series_instagram,
                        });

                        option && DashTotalChart.setOption(option);
                    }
                } catch (error) {
                    console.log(error)
                }

            }

            function RefreshLoadData() {
                DashTotalChannel();
                DashAgentOnline();
                DashTop10Chat();
                DashGrafikChannel();
            }
            RefreshLoadData();

            setInterval(() => {
                RefreshLoadData();
            }, 10000);
        </script>
    </asp:Content>
    <asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
        <div class="main-header clearfix">
            <div class="page-title">
                <h3 class="no-margin">Dashboard Sosial Media</h3>
            </div>
            <ul class="page-stats">
                <li>
                    <a href="#" onclick="RefreshLoadData()" class="refresh-widget" data-toggle="tooltip"
                        data-placement="bottom" title="Refresh Data" data-original-title="Refresh"><i
                            class="fa fa-refresh"></i> refresh</a>
                </li>
            </ul>
        </div>
        <!-- /main-header -->

        <div class="row">
            <div id="DashTotalChannel"></div>
            <div class="loading-overlay">
                <i class="loading-icon fa fa-refresh fa-spin fa-lg"></i>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-8">
                <div class="panel panel-default">
                    <div class="panel-heading clearfix">
                        <span class="pull-left"><i class="fa fa-bar-chart-o fa-lg"></i>Data Graph Today</span>
                        <ul class="tool-bar">
                            <li>
                                <a href="#" onclick="DashGrafikChannel()" class="refresh-widget" data-toggle="tooltip"
                                    data-placement="bottom" title="" data-original-title="Refresh"><i
                                        class="fa fa-refresh"></i></a>
                            </li>
                        </ul>
                    </div>
                    <div class="panel-body p-0" style="height: 350px;">
                        <div id="DashTotalChart" style="height: 100%;width: 100%;"></div>
                        <div class="loading-overlay">
                            <i class="loading-icon fa fa-refresh fa-spin fa-lg"></i>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-4">
                <div class="panel panel-default">
                    <div class="panel-heading clearfix">
                        <span class="pull-left"><i class="fa fa-bar-chart-o fa-lg"></i> Agent Online</span>
                        <ul class="tool-bar">
                            <li>
                                <a href="#" onclick="DashAgentOnline()" class="refresh-widget" data-toggle="tooltip"
                                    data-placement="bottom" title="" data-original-title="Refresh"><i
                                        class="fa fa-refresh"></i></a>
                            </li>
                        </ul>
                    </div>
                    <div class="panel-body" style="height: 350px; overflow:auto;">
                        <div id="DashAgentOnline"></div>
                    </div>
                    <div class="loading-overlay">
                        <i class="loading-icon fa fa-refresh fa-spin fa-lg"></i>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default table-responsive">
                    <div class="panel-heading clearfix">
                        <span class="pull-left"><i class="fa fa-bar-chart-o fa-lg"></i>Top 10 Chat</span>
                        <ul class="tool-bar">
                            <li>
                                <a href="#" onclick="DashTop10Chat()" class="refresh-widget" data-toggle="tooltip"
                                    data-placement="bottom" title="" data-original-title="Refresh"><i
                                        class="fa fa-refresh"></i></a>
                            </li>
                        </ul>
                    </div>
                    <div class="panel-body">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>CustomerID</th>
                                    <th>Value</th>
                                    <th>Name</th>
                                    <th>Message</th>
                                    <th>Channel</th>
                                    <th>Datetime</th>
                                    <th>Agent Handle</th>
                                </tr>
                            </thead>
                            <tbody id="DashGridTop10"></tbody>
                        </table>
                        <div class="loading-overlay">
                            <i class="loading-icon fa fa-refresh fa-spin fa-lg"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>


    </asp:Content>