﻿Imports System.IO
Imports System.Data
Imports System.Data.SqlClient
Public Class addsla
    Inherits System.Web.UI.Page

    Dim com As SqlCommand
    Dim con As New SqlConnection(ConfigurationManager.ConnectionStrings("DefaultConnection").ConnectionString)
    Dim sqldr As SqlDataReader
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Dim CommandText As String = "SP_update_SLA_Ticket"
            Dim strConnString As String = ConfigurationManager.ConnectionStrings("DefaultConnection").ConnectionString
            Dim con As New SqlConnection(strConnString)
            Dim cmd As New SqlCommand()
            cmd.CommandType = CommandType.StoredProcedure
            cmd.CommandText = CommandText
            cmd.Parameters.Add("@idticket", SqlDbType.VarChar).Value = Request.QueryString("id")
            cmd.Parameters.Add("@sla", SqlDbType.VarChar).Value = Request.QueryString("sla")
            cmd.Parameters.Add("@slasebelumnya", SqlDbType.VarChar).Value = Request.QueryString("slasebelumnya")
            cmd.Parameters.Add("@description", SqlDbType.VarChar).Value = Request.QueryString("description")
            cmd.Parameters.Add("@usercreate", SqlDbType.VarChar).Value = Session("username")
            cmd.Connection = con
            Try
                con.Open()
                Dim count As Object = cmd.ExecuteNonQuery()
            Catch ex As Exception
                Response.Write(ex.Message)
            Finally
                con.Close()
                con.Dispose()
            End Try
            Response.Write("Update SLA success")
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
End Class