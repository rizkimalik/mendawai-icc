﻿Imports System
Imports System.Data
Imports System.Data.SqlClient
Public Class insertticket
    Inherits System.Web.UI.Page

    Dim comm, com, sqlcom As SqlCommand
    Dim sqlcon As New SqlConnection(ConfigurationManager.ConnectionStrings("DefaultConnection").ConnectionString)
    Dim con As New SqlConnection(ConfigurationManager.ConnectionStrings("DefaultConnection").ConnectionString)
    Dim sql As String = String.Empty
    Dim sqldr, read As SqlDataReader
    Dim execute As New ClsConn
    Dim categoryid As String = String.Empty
    Dim subcategory1id As String = String.Empty
    Dim subcategory1name As String = String.Empty
    Dim subcategory2name As String = String.Empty
    Dim subcategory3name As String = String.Empty
    Dim subcategory2id As String = String.Empty
    Dim subcategory3id As String = String.Empty
    Dim errorcode As String = String.Empty
    Dim tanggalCloseSetelahDiCek As System.DateTime
    Dim vtahun As String = String.Empty
    Dim vbulan As String = String.Empty
    Dim vhari As String = String.Empty
    Dim vjam As String = String.Empty
    Dim vmenit As String = String.Empty
    Dim vdetik As String = String.Empty
    Dim vdateclose As String = String.Empty
    Dim koneksi As New ClsConn
    Dim strTicket As String = String.Empty
    Dim connection As SqlConnection
    Dim connectionString As String = ConfigurationManager.ConnectionStrings("DefaultConnection").ConnectionString
    Dim dataChart As String = String.Empty
    Dim customerid As String = String.Empty
    Dim nik As String = String.Empty
    Dim customer As String = String.Empty
    Dim email As String = String.Empty
    Dim phone As String = String.Empty
    Dim loq As New cls_globe
    Dim JamBatasanTicket As String = ConfigurationManager.AppSettings("JamBatasanTicket")
    Dim ConnectionTest As New SqlConnection(ConfigurationManager.ConnectionStrings("DefaultConnection").ConnectionString)
    Dim Com1 As SqlCommand
    Dim EmailForm As String = ConfigurationManager.AppSettings("EmailForm")
    Dim recCount As Integer
    Dim connString As String = ConfigurationManager.ConnectionStrings("DefaultConnection").ConnectionString

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Request.QueryString("source") = "Chat" Then
            f_customer_chat(Request.QueryString("chatid"))
        End If
        insert()
    End Sub

    Function f_customer_chat(ByVal chatid As String)
        sql = "select a.customerid, a.Name, a.HP, a.Email from mcustomer a left outer join view_tchat b on a.customerid = b.customerid where b.Chatid='" & chatid & "'"
            sqldr = koneksi.ExecuteReader(sql)
            Try
                If sqldr.HasRows() Then
                    sqldr.Read()
                    nik = sqldr("customerid").ToString
                    customer = sqldr("Name").ToString
                    email = sqldr("Email").ToString
                    phone = sqldr("HP").ToString
                Else

                End If
                sqldr.Close()
            Catch ex As Exception
                Response.Write(ex.Message)
            End Try
    End Function

    Private Sub insert()
        Dim idticket As String = String.Empty
        Dim datecreate As String = DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss tt ")
        sql = "select a.IDKamus, a.CategoryID, d.Name, c.SubCategory1ID, c.SubName as subname1, b.SubCategory2ID, b.SubName as subname2, a.SubCategory3ID, a.SubName as subname3 from mSubCategoryLv3 a " & _
              "right join mSubCategoryLv2 b on a.SubCategory2ID = b.SubCategory2ID " & _
              "right join mSubCategoryLv1 c on a.SubCategory1ID = c.SubCategory1ID " & _
              "right join mCategory d on a.CategoryID = d.CategoryID " & _
              "where d.CategoryID='" & Request.QueryString("type") & "' and a.SubCategory3ID='" & Request.QueryString("leveltiga") & "'"
        sqldr = execute.ExecuteReader(sql)
        Try
            If sqldr.HasRows() Then
                sqldr.Read()
                categoryid = sqldr("Name").ToString
                'subcategory1id = sqldr("SubCategory1ID").ToString
                subcategory1name = sqldr("subname1").ToString
                subcategory2name = sqldr("subname2").ToString
                subcategory3name = sqldr("subname3").ToString
                errorcode = sqldr("IDKamus").ToString
            Else
            End If
            sqldr.Close()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
        'Dim YearID As System.DateTime
        'Dim vyearfull As String = DateTime.Now.ToString("yyyyMMddhhmmss")

        Dim vyearfull As String = DateTime.Now.ToString("yyyy")
        Dim vyear As String = DateTime.Now.ToString("yy")


        Dim NumberDigit As String = String.Empty
        strTicket = "select  NumberDigit = Right(10000001 + COUNT(ID) + 0, 7)  " & _
                    "from tTicket where Datepart(YEAR ,DateCreate)= " & vyearfull & ""
        sqldr = execute.ExecuteReader(strTicket)
        Try
            If sqldr.HasRows() Then
                sqldr.Read()
                NumberDigit = sqldr("NumberDigit").ToString
            Else
            End If
            sqldr.Close()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
        Dim TicketNumber As String = Session("channel_code") & "" & vyear & "" & NumberDigit
        Dim posisi As String = String.Empty
        If Request.QueryString("enginer") <> "None" Then
            posisi = "2"
        Else
            If Session("LoginType") = "layer1" Then
                posisi = "2"
            Else
                posisi = "1"
            End If

        End If
        Dim count As Integer
        connection = New SqlConnection(connectionString)
        comm = New SqlCommand()
        comm.Connection = connection
        comm.CommandType = CommandType.StoredProcedure
        comm.CommandText = "sp_insert_data_ticket"
        If Request.QueryString("source") = "Chat" Then
            comm.Parameters.Add("@nik", Data.SqlDbType.VarChar).Value = nik
            comm.Parameters.Add("@namapelapor", Data.SqlDbType.VarChar).Value = customer
            comm.Parameters.Add("@emailpelapor", Data.SqlDbType.VarChar).Value = email
            comm.Parameters.Add("@phonepelapor", Data.SqlDbType.VarChar).Value = phone
            comm.Parameters.Add("@alamatpelapor", Data.SqlDbType.VarChar).Value = "kosong"
            comm.Parameters.Add("@emcid", Data.SqlDbType.VarChar).Value = nik
            comm.Parameters.Add("@account", Data.SqlDbType.VarChar).Value = "0"
            If Request.QueryString("nomorrekening") = "" Then
                comm.Parameters.Add("@nomorrekening", Data.SqlDbType.VarChar).Value = "0"
            Else
                comm.Parameters.Add("@nomorrekening", Data.SqlDbType.VarChar).Value = Request.QueryString("nomorrekening")
            End If
            insert_email_chat(Request.QueryString("chatid"), email, TicketNumber, Request.QueryString("sendemail"))
        Else
            comm.Parameters.Add("@nik", Data.SqlDbType.VarChar).Value = Request.QueryString("nik")
            comm.Parameters.Add("@namapelapor", Data.SqlDbType.VarChar).Value = Request.QueryString("namapelapor")
            comm.Parameters.Add("@emailpelapor", Data.SqlDbType.VarChar).Value = Request.QueryString("emailpelapor")
            comm.Parameters.Add("@phonepelapor", Data.SqlDbType.VarChar).Value = Request.QueryString("phonepelapor")
            comm.Parameters.Add("@alamatpelapor", Data.SqlDbType.VarChar).Value = Request.QueryString("alamatpelapor")
            comm.Parameters.Add("@emcid", Data.SqlDbType.VarChar).Value = Request.QueryString("nik")
            comm.Parameters.Add("@account", Data.SqlDbType.VarChar).Value = Request.QueryString("account")
            comm.Parameters.Add("@nomorrekening", Data.SqlDbType.VarChar).Value = Request.QueryString("nomorrekening")
        End If
        comm.Parameters.Add("@ticketnumber", Data.SqlDbType.VarChar).Value = TicketNumber
        comm.Parameters.Add("@channelcode", Data.SqlDbType.VarChar).Value = Session("channel_code")
        'comm.Parameters.Add("@unitid", Data.SqlDbType.VarChar).Value = Request.QueryString("unitid")
        comm.Parameters.Add("@sourcename", Data.SqlDbType.VarChar).Value = Request.QueryString("source")
        'comm.Parameters.Add("@complaintlevel", Data.SqlDbType.VarChar).Value = Request.QueryString("priority")
        comm.Parameters.Add("@categoryid", Data.SqlDbType.VarChar).Value = Request.QueryString("type")
        comm.Parameters.Add("@categoryname", Data.SqlDbType.VarChar).Value = categoryid
        comm.Parameters.Add("@subcategory1id", Data.SqlDbType.VarChar).Value = Request.QueryString("leveldua")
        comm.Parameters.Add("@subcategory1name", Data.SqlDbType.VarChar).Value = subcategory1name
        comm.Parameters.Add("@subcategory2id", Data.SqlDbType.VarChar).Value = Request.QueryString("subcategory2id")
        comm.Parameters.Add("@subcategory2name", Data.SqlDbType.VarChar).Value = subcategory2name
        comm.Parameters.Add("@subcategory3id", Data.SqlDbType.VarChar).Value = Request.QueryString("leveltiga")
        comm.Parameters.Add("@subcategory3name", Data.SqlDbType.VarChar).Value = subcategory3name
        comm.Parameters.Add("@detailcomplaint", Data.SqlDbType.VarChar).Value = Request.QueryString("description")
        comm.Parameters.Add("@responsecomplaint", Data.SqlDbType.VarChar).Value = Request.QueryString("response")
        comm.Parameters.Add("@sla", Data.SqlDbType.VarChar).Value = Request.QueryString("sla")
        'comm.Parameters.Add("@severity", Data.SqlDbType.VarChar).Value = Request.QueryString("severity")
        comm.Parameters.Add("@status", Data.SqlDbType.VarChar).Value = Request.QueryString("status")
        comm.Parameters.Add("@usercreate", Data.SqlDbType.VarChar).Value = Session("username")
        'comm.Parameters.Add("@channelid", Data.SqlDbType.VarChar).Value = Request.QueryString("channelid")
        comm.Parameters.Add("@divisi", Data.SqlDbType.VarChar).Value = Request.QueryString("iddepartment")
        comm.Parameters.Add("@enginer", Data.SqlDbType.VarChar).Value = Request.QueryString("enginer")
        comm.Parameters.Add("@position", Data.SqlDbType.VarChar).Value = posisi
        '--DataBTN Enhance Form
        comm.Parameters.Add("@tglkejadian", Data.SqlDbType.VarChar).Value = Request.QueryString("tglkejadian")
        comm.Parameters.Add("@idpenyebab", Data.SqlDbType.VarChar).Value = Request.QueryString("idpenyebab")
        comm.Parameters.Add("@strpenyebab", Data.SqlDbType.VarChar).Value = Request.QueryString("strpenyebab")
        comm.Parameters.Add("@strpenerima", Data.SqlDbType.VarChar).Value = Request.QueryString("strpenerima")
        comm.Parameters.Add("@idstatuspelapor", Data.SqlDbType.VarChar).Value = Request.QueryString("idstatuspelapor")
        comm.Parameters.Add("@strstatuspelapor", Data.SqlDbType.VarChar).Value = Request.QueryString("strstatuspelapor")
        comm.Parameters.Add("@lokasiPengaduan", Data.SqlDbType.VarChar).Value = Session("lokasiPengaduan")
        comm.Parameters.Add("@skalaprioritas", Data.SqlDbType.VarChar).Value = Request.QueryString("skalaprioritas")
        comm.Parameters.Add("@jenisnasabah", Data.SqlDbType.VarChar).Value = Request.QueryString("jenisnasabah")
        comm.Parameters.Add("@category3id", Data.SqlDbType.VarChar).Value = Request.QueryString("category3id")
        comm.Parameters.Add("@sumberinformasi", Data.SqlDbType.VarChar).Value = Request.QueryString("sumberinformasi")
        If Request.QueryString("sendemail") = "1" Then
            comm.Parameters.Add("@kirimemail", Data.SqlDbType.VarChar).Value = "YES"
        Else
            comm.Parameters.Add("@kirimemail", Data.SqlDbType.VarChar).Value = "NO"
        End If
        Try
            connection.Open()
            sqldr = comm.ExecuteReader()
            sqldr.Read()
            idticket = sqldr(0)
            'customerid = sqldr(1)`
            sqldr.Close()
            connection.Close()
            'Restu 07-06-2019 
            'ini di komen, karena ticket number yg di btn_trx_attachment jadi ke isi insert data success 
            'Response.Write("Insert data success")

            'Harusnya ini balikannya ticket number
            Response.Write(idticket)
            insert_email_atasan(idticket)
            If Request.QueryString("source") = "Phone" Then
                'inbound_channel_history(Request.QueryString("ivcid"), idticket)
                updateiccchannelhistory(Request.QueryString("account"), idticket)
            ElseIf Request.QueryString("source") = "Email" Then
                updateiccchannelhistory(Request.QueryString("account"), idticket)
            Else
            End If
            loq.writedata(Session("UserName"), "Insert", "insert data ticket", idticket, Session("username"))
            loq.LogSys(Session("UserName"), "Insert", "insert data ticket", idticket, "insertticket.aspx")
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Function insert_email_atasan(ByVal ticket As String)
        Try
            Using conn As New SqlConnection(connString)
                conn.Open()
                Dim cmd As SqlCommand = New SqlCommand("Select COUNT (ID) as LDAPCount from ICC_LDAP_Setting WHERE EmailAtasan='Y'", conn)
                recCount = cmd.ExecuteScalar()
            End Using
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
        If recCount <> 0 Then
            Dim complaint, level1, level2, level3, description, name, unit, channel As String
            Dim strvalue As String = String.Empty
            strvalue = "SELECT a.CategoryName, a.SubCategory1Name, a.SubCategory2Name, a.SubCategory3Name, a.DetailComplaint, a.channelid, a.Divisi, b.Name FROM tTicket a left outer join mCustomer b on a.nik = b.customerid where a.TicketNumber='" & ticket & "'"
            sqldr = execute.ExecuteReader(strvalue)
            Try
                If sqldr.HasRows() Then
                    sqldr.Read()
                    complaint = sqldr("CategoryName").ToString
                    level1 = sqldr("SubCategory1Name").ToString
                    level2 = sqldr("SubCategory2Name").ToString
                    level3 = sqldr("SubCategory3Name").ToString
                    description = sqldr("DetailComplaint").ToString
                    name = sqldr("Name").ToString
                    channel = sqldr("channelid").ToString
                Else
                End If
                sqldr.Close()
            Catch ex As Exception
                Response.Write(ex.Message)
            End Try

            Dim RequestTittle As String = "Data Transaksi Dengan Ticket Number " & ticket & ""
            Dim Body As String = "<hr noshade='noshade' style='background-color:red; height: 5px;'/>" &
                                    "<p>Dear Bapak/Ibu,</p>" &
                                    "<br/>" &
                                    "<br/>" &
                                    "<p>" & Session("username") & " telah membuat ticket di sistem dengan detail sebagai berikut:</p>" &
                                    "<p style='margin-top:-5px;'>Category : " & complaint & "</p>" &
                                    "<p style='margin-top:-5px;'>Group Kategori Produk : " & level1 & "</p>" &
                                    "<p style='margin-top:-5px;'>Kategori Produk : " & level2 & "</p>" &
                                    "<p style='margin-top:-5px;'>Kategori Permasalahan : " & level3 & "</p>" &
                                    "<p style='margin-top:-5px;'>Description : " & description & "</p>" &
                                    "<p style='margin-top:-5px;'>Customer Name : " & name & "</p>" &
                                    "<br/>" &
                                    "<br />" &
                                    "<p>Mohon bantuannya untuk ditindaklanjuti</p>" &
                                    "<p style='margin-top:-5px;'>Terima kasih atas perhatiannya</p>" &
                                    "<br/>" &
                                    "<br />" &
                                    "<br />" &
                                    "<p>Salam</p>" &
                                    "<br/>" &
                                  "<hr noshade='noshade' style='background-color:red; height: 5px;'/>" &
                                  "<p style='text-align:center;'><font size='2'>BTN Customer Care</font></p>"
            Dim EmailAddress As String = String.Empty
            Dim str As String = String.Empty
            str = "select EMAIL_ADDRESS from msuser where LEVELUSER='Supervisor'"
            sqldr = execute.ExecuteReader(str)
            Try
                While sqldr.Read()
                    Dim insertdata As String = "insert into ICC_EMAIL_OUT (DIRECTION, EFROM, ETO, ESUBJECT, EBODY_TEXT, EBODY_HTML, Email_Date, TicketNumber, JENIS_EMAIL_INTERNAL, AGENT) VALUES ('out', '" & EmailForm & "', '" & sqldr("EMAIL_ADDRESS").ToString & "', '" & RequestTittle & "', '" & ReplaceSpecialLetter(Body) & "', '" & ReplaceSpecialLetter(Body) & "', GETDATE(), '" & ticket & "', 'email supervisor', '" & Session("username") & "')"
                    com = New SqlCommand(insertdata, con)
                    Try
                        con.Open()
                        com.ExecuteNonQuery()
                        con.Close()
                    Catch ex As Exception
                        Response.Write(DirectCast("", String))
                    End Try
                End While
                sqldr.Close()
                loq.writedata(Session("UserName"), "Insert", "insert email atasan", ticket, Session("username"))
                loq.LogSys(Session("UserName"), "Insert", "insert email atasan", ticket, "insertticket.aspx")
            Catch ex As Exception
                Response.Write(ex.Message)
            End Try
        Else

        End If       
    End Function

    Function insert_email_chat(ByVal chatid As String, ByVal email As String, ByVal Ticket As String, ByVal sendemail As String)
        Dim emailid As String = DateTime.Now.ToString("yyyyMMddhhmmssfff")
        If sendemail = "1" Then
            Try
                sqlcon.Open()
                Dim strdata As String = "SELECT * FROM View_Tchat WHERE ChatID='" & chatid & "' and Pesan <> '' order by TrxChatID asc"
                com = New SqlCommand(strdata, sqlcon)
                sqldr = com.ExecuteReader()
                dataChart &= "<table style='width:100%;'><tr style='font-size: x-small;text-align:left;'><td style='width:20%;'>Nama</td><td style='width:70%;'>Pesan</td><td style='width:10%;'>Tanggal</td></tr>"
                While sqldr.Read()
                    dataChart &= "<tr><td style='font-size: x-small;text-align:left;width:20%;'>" & sqldr("Nama").ToString & "</td><td style='font-size: x-small;text-align:left;width:70%;'>" & sqldr("Pesan").ToString & "</td><td style='font-size: x-small;text-align:left;width:10%;'>" & sqldr("DateCreate").ToString & "</td></tr>"
                End While
                sqldr.Close()
                sqlcon.Close()
                dataChart &= "</table>"
                dataChart = dataChart.Substring(0, dataChart.Length)
                'dataChart = dataChart & ""
            Catch ex As Exception
                Response.Write(ex.Message)
            End Try
            Dim RequestTittle As String = "Data Transaksi Dengan Ticket Number " & Request.QueryString("idticket") & ""
            Dim Body As String = "<hr noshade='noshade' style='background-color:#3c8dbc; height: 5px;'/>" &
                                    "<p>Yth. Bapak/Ibu,</p>" &
                                    "<br/>" &
                                    "<br/>" &
                                    "<br/>" &
                                    "<p>Terima kasih telah menghubungi layanan sahabat BTN.</p>" &
                                    "<br/>" &
                                    "<p>Laporan/Permintaan/Pertanyaan Bapak/Ibu telah kami terima dan terdaftar dengan nomor pelaporan " & Request.QueryString("idticket") & "</p>" &
                                    "<br/>" &
                                    "<p>Berikut hasil percakapan Bapak/Ibu dengan Customer Service Officer kami</p>" &
                                    "<br/>" &
                                    "<p>" & dataChart & "</p>" &
                                    "<p>Salam Hangat, </p>" &
                                    "<br/>" &
                                    "<p>SAHABAT BTN</p>" &
                                    "<br />" &
                                    "<br/>" &
                                    "<p>PT Bank Tabungan Negara (Persero) Tbk Menara BTN</p>" &
                                    "<p>Jl Gajah Mada No.1 Jakarta 10130</p>" &
                                    "<p>Layanan Contact Center : 1500-286</p>" &
                                    "<p>Layanan Live Chat 24 Jam : btnproperti.co.id</p>" &
                                    "<p>Email : btncontactcenter@btn.co.id</p>" &
                                    "<p>Website : www.btn.co.id</p>" &
                                    "<p>Kantor Cabang BTN : Senin-Jumat, Jam operasional 08:00 - 15:30 WIB</p>" &
                                    "<hr noshade='noshade' style='background-color:#3c8dbc; height: 5px;'/>"

            Dim insertdata As String = "insert into ICC_EMAIL_OUT (EMAIL_ID, DIRECTION, EFROM, ETO, ESUBJECT, EBODY_TEXT, EBODY_HTML, Email_Date, TicketNumber, JENIS_EMAIL_INTERNAL, AGENT) VALUES ('ICC-" & emailid & "','out', '" & EmailForm & "', '" & email & "', '" & RequestTittle & "', '" & Body & "', '" & Body & "', GETDATE(), '" & Ticket & "', 'email chat', '" & Session("username") & "')"
            com = New SqlCommand(insertdata, con)
            Try
                con.Open()
                com.ExecuteNonQuery()
                con.Close()
                loq.writedata(Session("UserName"), "Insert", "insert email chat", Ticket, Session("username"))
                loq.LogSys(Session("UserName"), "Insert", "insert email chat", Ticket, "insertticket.aspx")
            Catch ex As Exception
                Response.Write(DirectCast("", String))
            End Try
        Else

        End If

        Dim updatechat As String = "update tchat_end set ticketnumber='" & Ticket & "' where chatid='" & chatid & "'"
        com = New SqlCommand(updatechat, con)
        Try
            con.Open()
            com.ExecuteNonQuery()
            con.Close()
            loq.writedata(Session("UserName"), "Update", "Update Ticket Number TChat", Ticket, Session("username"))
            loq.LogSys(Session("UserName"), "Update", "Update Ticket Number TChat", Ticket, "insertticket.aspx")
        Catch ex As Exception
            Response.Write(DirectCast("", String))
        End Try
        Dim updateiccchat As String = "UPDATE ICC_CHANNEL_HISTORY SET TICKETNUMBER='" & Ticket & "' WHERE CHATID='" & chatid & "'"
        com = New SqlCommand(updateiccchat, con)
        Try
            con.Open()
            com.ExecuteNonQuery()
            con.Close()
            loq.writedata(Session("UserName"), "Update", "Update Ticket Number TChat", Ticket, Session("username"))
            loq.LogSys(Session("UserName"), "Update", "Update Ticket Number TChat", Ticket, "insertticket.aspx")
        Catch ex As Exception
            Response.Write(DirectCast("", String))
        End Try
    End Function

    Public Function ReplaceSpecialLetter(ByVal str)
        Dim TmpStr As String
        TmpStr = str
        TmpStr = Replace(TmpStr, "'", "")
        ReplaceSpecialLetter = TmpStr
    End Function

    Function updateiccchannelhistory(ByVal account As String, ByVal ticket As String)
        Dim updateiccchat As String
        If Request.QueryString("source") = "Email" Then
            updateiccchat = "UPDATE ICC_CHANNEL_HISTORY SET TICKETNUMBER='" & ticket & "', FLAGPHONE='Y', STATUS='1' WHERE CustomerID='" & account & "' And ChannelID='" & Request.QueryString("ivcid") & "'"
            com = New SqlCommand(updateiccchat, con)
            Try
                con.Open()
                com.ExecuteNonQuery()
                con.Close()
                loq.writedata(Session("UserName"), "Update", "Update ICC_CHANNEL_HISTORY " & account & "", ticket, Session("username"))
                loq.LogSys(Session("UserName"), "Update", "Update ICC_CHANNEL_HISTORY " & account & "", ticket, "insertticket.aspx")
            Catch ex As Exception
                Response.Write(DirectCast("", String))
            End Try

            Dim strsqldata As String = "update icc_email_in set flag='1', TICKETNUMBER='" & ticket & "' where ivc_id='" & Request.QueryString("ivcid") & "'"
            com = New SqlCommand(strsqldata, con)
            Try
                con.Open()
                com.ExecuteNonQuery()
                con.Close()
            Catch ex As Exception
                Response.Write(DirectCast("", String))
            End Try
        Else
            updateiccchat = "UPDATE ICC_CHANNEL_HISTORY SET TICKETNUMBER='" & ticket & "', FLAGPHONE='Y' WHERE CustomerID='" & account & "' And FLAGPHONE='N'"
            com = New SqlCommand(updateiccchat, con)
            Try
                con.Open()
                com.ExecuteNonQuery()
                con.Close()
                loq.writedata(Session("UserName"), "Update", "Update ICC_CHANNEL_HISTORY " & account & "", ticket, Session("username"))
                loq.LogSys(Session("UserName"), "Update", "Update ICC_CHANNEL_HISTORY " & account & "", ticket, "insertticket.aspx")
            Catch ex As Exception
                Response.Write(DirectCast("", String))
            End Try
        End If
        
    End Function

    Function inbound_channel_history(ByVal phone As String, ByVal idticket As String)
        Try
            Dim insertdata As String = "insert into ICC_CHANNEL_HISTORY (CHANNELTYPE, DATED, AGENT, CHANNELDESC, CustomerID, STATUS, DIRECTION, FLAGNOTIF, TICKETNUMBER) " & _
                                       "VALUES ('INBOUND', GETDATE(), '" & Session("username") & "', 'ICC_CALL_HISTORY_IN', '" & phone & "', '1', '1', '1', '" & idticket & "')"
            com = New SqlCommand(insertdata, con)
            Try
                con.Open()
                com.ExecuteNonQuery()
                con.Close()
            Catch ex As Exception
                Response.Write(DirectCast("", String))
            End Try
            loq.writedata(Session("UserName"), "insert", "insert channel inbound", idticket, Session("username"))
            loq.LogSys(Session("UserName"), "insert", "insert channel inbound", idticket, "insertticket.aspx")
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Function

End Class