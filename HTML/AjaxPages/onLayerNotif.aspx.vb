﻿Imports System.Globalization
Imports System.Threading

Public Class onLayerNotif
    Inherits System.Web.UI.Page
    Dim clsglobe As New cls_globe
    Dim strsql, tmp, t1, t2, t3 As String
    Dim tbldata As DataTable

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Session("dbName") = "master"
        If Session("username") <> "" Then
            Dim curCulture As CultureInfo = Thread.CurrentThread.CurrentCulture
            Dim tInfo As TextInfo = curCulture.TextInfo()
            Dim title As String = "converted"

            'tInfo.ToTitleCase(tInfo.ToLower("SMS"))
            'Response.Write(tmp)
            'Exit Sub
            Try
                strsql = "select * from ICC_CHANNEL_HISTORY where agent='" & Session("username") & "' and DIRECTION=0 and FLAGNOTIF=0 order by IVC_ID ASC"
                tbldata = clsglobe.ExecuteQuery(strsql)
                If tbldata.Rows.Count <> 0 Then
                    t1 = "<a href='detilsosmed.aspx?hid=" & tbldata.Rows(0).Item("IVC_ID") & "&id=" & tbldata.Rows(0).Item("CHANNELID") & "&ket=" & tInfo.ToTitleCase(tInfo.ToLower(tbldata.Rows(0).Item("CHANNELTYPE"))) & "'>" & tbldata.Rows(0).Item("CHANNELTYPE") & "</a>"
                    t2 = "<a href='detilsosmed.aspx?hid=" & tbldata.Rows(0).Item("IVC_ID") & "&id=" & tbldata.Rows(0).Item("CHANNELID") & "&ket=" & tInfo.ToTitleCase(tInfo.ToLower(tbldata.Rows(0).Item("CHANNELTYPE"))) & "'>" & tbldata.Rows(0).Item("CustomerID") & "</a>"
                    t3 = tbldata.Rows(0).Item("NOTE") & " <a href='detilsosmed.aspx?hid=" & tbldata.Rows(0).Item("IVC_ID") & "&id=" & tbldata.Rows(0).Item("CHANNELID") & "&ket=" & tInfo.ToTitleCase(tInfo.ToLower(tbldata.Rows(0).Item("CHANNELTYPE"))) & "'>Detail.</a>"
                    tmp = t1 & "|" & t2 & "|" & t3
                    'tmp = tbldata.Rows(0).Item("CHANNELTYPE").ToString & "|" & tbldata.Rows(0).Item("CustomerID") & "|" & tbldata.Rows(0).Item("NOTE")
                    Response.Write(tmp)
                    strsql = "update ICC_CHANNEL_HISTORY set FLAGNOTIF=1 where IVC_ID='" & tbldata.Rows(0).Item("IVC_ID") & "'"
                    clsglobe.ExecuteNonQuery(strsql)
                    Exit Sub
                End If
                strsql = "select * from txcalllist where OnNotif=0"
                tbldata = clsglobe.ExecuteQuery(strsql)
                If tbldata.Rows.Count <> 0 Then
                    t1 = "<a href='OutboundCall.aspx?menu=CALLBACK&id=" & tbldata.Rows(0).Item("ID") & "'>CALLBACK</a>"
                    t2 = "<a href='OutboundCall.aspx?menu=CALLBACK&id=" & tbldata.Rows(0).Item("ID") & "'>" & tbldata.Rows(0).Item("NoPolis") & "</a>"
                    t3 = tbldata.Rows(0).Item("Tanggal") & " <a href='OutboundCall.aspx?menu=CALLBACK&id=" & tbldata.Rows(0).Item("ID") & "'>Detail.</a>"
                    tmp = t1 & "|" & t2 & "|" & t3
                    Response.Write(tmp)
                    strsql = "update txcalllist set OnNotif=1 where ID='" & tbldata.Rows(0).Item("ID") & "'"
                    clsglobe.ExecuteNonQuery(strsql)
                    Exit Sub
                End If
                'cek Call Appointment
                strsql = "select a.*,b.fullname from Appointment a inner join ICC_CALLLIST b on a.idcalllist=b.id where a.notif=0 order by a.dated"
                tbldata = clsglobe.ExecuteQuery(strsql)
                If tbldata.Rows.Count <> 0 Then
                    If tbldata.Rows(0).Item("username") = Session("username") Then
                        If Now.ToString("MMM dd yyyy hh:mm tt") = CDate(tbldata.Rows(0).Item("dated")).AddMinutes(-5).ToString("MMM dd yyyy hh:mm tt") Then
                            t1 = "<a href='OutboundCall.aspx?menu=Appointment&id=" & tbldata.Rows(0).Item("ID") & "'>Appointment</a>"
                            t2 = "<a href='OutboundCall.aspx?menu=Appointment&id=" & tbldata.Rows(0).Item("ID") & "'>" & tbldata.Rows(0).Item("fullname") & "</a>"
                            t3 = tbldata.Rows(0).Item("dated") & "<br/>" & tbldata.Rows(0).Item("NoAppointment") & " <a href='OutboundCall.aspx?menu=Appointment&id=" & tbldata.Rows(0).Item("ID") & "'>Detail.</a>"
                            tmp = t1 & "|" & t2 & "|" & t3
                            Response.Write(tmp)
                            strsql = "update Appointment set notif=1 where id='" & tbldata.Rows(0).Item("ID") & "'"
                            clsglobe.ExecuteNonQuery(strsql)
                            Exit Sub
                        End If
                    End If
                End If
                Response.Write("0")
            Catch ex As Exception
                Response.Write("0")
            End Try
        Else
            Response.Write("0")
        End If
    End Sub
End Class