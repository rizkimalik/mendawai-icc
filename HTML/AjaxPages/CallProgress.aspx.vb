﻿Public Class CallProgress
    Inherits System.Web.UI.Page
    Dim clsglobe As New cls_globe
    Dim strsql, tmpStr, strsqlCus As String
    Dim tbldata, tbldataCus As DataTable
    Dim campaign As String
    Dim StartCall, EndCall As DateTime

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Session("dbName") = "master"
        If Request.QueryString("data") = "Appointment" And Request.QueryString("status") = "read" Then
            strsql = "select * from Appointment a inner join ICC_CALLLIST b on a.idcalllist=b.id inner join ICC_CALL_HISTORY_OUT c on a.idcalllist=c.CustomerID " & _
                "where a.id='" & Request.QueryString("idData") & "' and c.CALL_RESULT<>'' order by c.CALL_END"
            tbldata = clsglobe.ExecuteQuery(strsql)
            tmpStr = Nothing
            For i = 0 To tbldata.Rows.Count - 1
                tmpStr &= CDate(tbldata.Rows(i).Item("CALL_END")).ToString("MMM dd yyyy hh:mm:ss tt") & vbCrLf
                tmpStr &= tbldata.Rows(i).Item("Note") & vbCrLf
            Next
            If tbldata.Rows.Count <> 0 Then
                Response.Write(CDate(tbldata.Rows(0).Item("dated")).ToString("MMM dd yyyy hh:mm:ss tt") & "|" & tbldata.Rows(0).Item("NoAppointment").ToString & "|" & tmpStr & "|" & tbldata.Rows(0).Item("CALL_RESULT").ToString & "|" & tbldata.Rows(0).Item("CALL_REMARK").ToString)
            Else
                Response.Write("END")
            End If

            Exit Sub
        End If
        campaign = Request.QueryString("campaign")
        If Request.QueryString("startCall") = Nothing Then
            Response.Write("END")
            Exit Sub
        End If
        StartCall = CDate(Request.QueryString("startCall"))
        If CDate(Request.QueryString("endCall")) = Nothing Then
            EndCall = CDate(Session("EndCall"))
        Else
            EndCall = CDate(Request.QueryString("endCall"))
        End If

        If Session("BanyakCall") = Nothing Then
            If Request.QueryString("Nodial") <> Nothing Then
                Session("BanyakCall") &= Request.QueryString("Nodial") & "|"
            End If
            If Request.QueryString("Nodial2") <> Nothing Then
                Session("BanyakCall") &= Request.QueryString("Nodial2") & "|"
            End If
            If Request.QueryString("Nodial3") <> Nothing Then
                Session("BanyakCall") &= Request.QueryString("Nodial3")
            End If
            Session("JmlCall") = Session("BanyakCall").split("|")
            If Session("JmlCall")(1) = Nothing Then
                Session("BanyakCall") = Replace(Session("BanyakCall"), "|", "")
                Session("JmlCall") = Session("BanyakCall").split("|")
            End If
        End If

        If Request.QueryString("statusCall") = "connected" Then
            If (Request.QueryString("callresutl") = "FOLLOWUP" Or Request.QueryString("callresutl") = "CALLBACK") And Request.QueryString("StatusApp") = "read" Then
                strsql = "insert into Appointment(username,idcalllist,status,notif,dated,NoAppointment,capaign) values " & _
                    "('" & Session("username") & "','" & Request.QueryString("idTable") & "','0'," & _
                    "'0','" & Request.QueryString("de_Appointment") & "','" & Request.QueryString("NoAppointment") & "','" & campaign & "')"
                clsglobe.ExecuteNonQuery(strsql)
                'campaign = "Appointment"
            ElseIf (Request.QueryString("callresutl") = "FOLLOWUP" Or Request.QueryString("callresutl") = "CALLBACK") And Request.QueryString("StatusApp") = "save" Then
                strsql = "update Appointment set status=1 where id='" & Request.QueryString("idTable") & "' and username='" & Trim(Session("username")) & "'"

                clsglobe.ExecuteNonQuery(strsql)
            End If
            Session("BanyakCall") = Nothing
            strsql = "update ICC_CALLLIST set FLAG='Y' where id=" & Request.QueryString("idTable")
            clsglobe.ExecuteNonQuery(strsql)
            If campaign = "Appointment" Then
                strsql = "select * from Appointment where id=" & Request.QueryString("idTable")
                tbldata = clsglobe.ExecuteQuery(strsql)
                campaign = tbldata.Rows(0).Item("capaign").ToString
                strsql = "insert into ICC_CALL_HISTORY_OUT(CALL_START,CALL_END,TALK_TIME,DEVICE_ID, AGENT, TELP_NUMBER, CampHead,Camptype,Note,CustomerID,CALL_RESULT, CALL_REMARK, premi, CALL_STATUS) values " & _
            "('" & StartCall & "','" & EndCall & "',DATEDIFF(SECOND,'" & StartCall & "','" & EndCall & "'),'" & Session("NoExt") & "','" & Session("username") & "','" & Request.QueryString("CallNo") & "'," & _
            "'" & campaign & "','Appointment','" & Request.QueryString("CallNote") & "','" & tbldata.Rows(0).Item("idcalllist").ToString & "','" & Request.QueryString("callresutl") & "'," & _
            "'" & Request.QueryString("callremark") & "','" & Request.QueryString("premi") & "','" & Request.QueryString("statusCall") & "')"
            Else
                strsql = "insert into ICC_CALL_HISTORY_OUT(CALL_START,CALL_END,TALK_TIME,DEVICE_ID, AGENT, TELP_NUMBER, CampHead,Note,CustomerID,CALL_RESULT, CALL_REMARK, premi, CALL_STATUS) values " & _
            "('" & StartCall & "','" & EndCall & "',DATEDIFF(SECOND,'" & StartCall & "','" & EndCall & "'),'" & Session("NoExt") & "','" & Session("username") & "','" & Request.QueryString("CallNo") & "'," & _
            "'" & campaign & "','" & Request.QueryString("CallNote") & "','" & Request.QueryString("idTable") & "','" & Request.QueryString("callresutl") & "'," & _
            "'" & Request.QueryString("callremark") & "','" & Request.QueryString("premi") & "','" & Request.QueryString("statusCall") & "')"
            End If
            
            clsglobe.ExecuteNonQuery(strsql)

            strsql = "select top 1 * from ICC_CALLLIST where FLAG<>'Y' and campaign='" & Request.QueryString("campaign") & "' and agent='" & Trim(Session("username")) & "' order by CALL_STATUSID"
            tbldata = clsglobe.ExecuteQuery(strsql)
            If tbldata.Rows.Count = Nothing Then
                Response.Write("END")
            Else
                Response.Write(tbldata.Rows(0).Item("id") & "|" & tbldata.Rows(0).Item("recordnr") & "|" & tbldata.Rows(0).Item("fullname") & "|" & tbldata.Rows(0).Item("gender") & "|" & tbldata.Rows(0).Item("dob") _
                               & "|" & tbldata.Rows(0).Item("age") & "|" & tbldata.Rows(0).Item("telp_home") & "|" & tbldata.Rows(0).Item("telp_office") & "|" & tbldata.Rows(0).Item("telp_ext") & "|" & tbldata.Rows(0).Item("telp_mobile") _
                               & "|" & tbldata.Rows(0).Item("city") & "|" & tbldata.Rows(0).Item("kota_ktr") & "|" & tbldata.Rows(0).Item("segmentasi_wilayah") & "|" & tbldata.Rows(0).Item("vendorcode") & "|" & tbldata.Rows(0).Item("batchcode") & "|" & tbldata.Rows(0).Item("campaign"))
            End If
        Else
            If Session("idTable") <> Request.QueryString("idTable") Then
                Session("CALL_STATUSID") = CInt(Session("CALL_STATUSID")) + 1
                If Session("CALL_STATUSID") = 1 Then
                    strsql = "update ICC_CALLLIST set CALL_STATUSID=CALL_STATUSID+1 where ID=" & Request.QueryString("idTable")
                    clsglobe.ExecuteNonQuery(strsql)
                    Session("idTable") = Request.QueryString("idTable")
                End If
            End If
            If (Request.QueryString("callresutl") = "FOLLOWUP" Or Request.QueryString("callresutl") = "CALLBACK") And Request.QueryString("StatusApp") = "read" Then
                strsql = "insert into Appointment(username,idcalllist,status,notif,dated,NoAppointment,capaign) values " & _
                    "('" & Session("username") & "','" & Request.QueryString("idTable") & "','0'," & _
                    "'0','" & Request.QueryString("de_Appointment") & "','" & Request.QueryString("NoAppointment") & "','" & campaign & "')"
                clsglobe.ExecuteNonQuery(strsql)
                'campaign = "Appointment"
            End If
            If campaign = "Appointment" Then
                strsql = "select * from Appointment where id=" & Request.QueryString("idTable")
                tbldata = clsglobe.ExecuteQuery(strsql)
                campaign = tbldata.Rows(0).Item("capaign").ToString
                strsql = "insert into ICC_CALL_HISTORY_OUT(CALL_START,CALL_END,TALK_TIME,DEVICE_ID, AGENT, TELP_NUMBER, CampHead,Camptype,Note,CustomerID,CALL_RESULT, CALL_REMARK, premi, CALL_STATUS) values " & _
            "('" & StartCall & "','" & EndCall & "',DATEDIFF(SECOND,'" & StartCall & "','" & EndCall & "'),'" & Session("NoExt") & "','" & Session("username") & "','" & Request.QueryString("CallNo") & "'," & _
            "'" & campaign & "','Appointment','" & Request.QueryString("CallNote") & "','" & tbldata.Rows(0).Item("idcalllist").ToString & "','" & Request.QueryString("callresutl") & "'," & _
            "'" & Request.QueryString("callremark") & "','" & Request.QueryString("premi") & "','" & Request.QueryString("statusCall") & "')"
            Else
                strsql = "insert into ICC_CALL_HISTORY_OUT(CALL_START,CALL_END,TALK_TIME,DEVICE_ID, AGENT, TELP_NUMBER, CampHead,Note,CustomerID,CALL_RESULT, CALL_REMARK, premi, CALL_STATUS) values " & _
            "('" & StartCall & "','" & EndCall & "',DATEDIFF(SECOND,'" & StartCall & "','" & EndCall & "'),'" & Session("NoExt") & "','" & Session("username") & "','" & Request.QueryString("CallNo") & "'," & _
            "'" & campaign & "','" & Request.QueryString("CallNote") & "','" & Request.QueryString("idTable") & "','" & Request.QueryString("callresutl") & "'," & _
            "'" & Request.QueryString("callremark") & "','" & Request.QueryString("premi") & "','" & Request.QueryString("statusCall") & "')"
            End If
            clsglobe.ExecuteNonQuery(strsql)

            Session("Next") = Session("Next") + 1
            'Session("Next") = Session("JmlCall").length
            If Session("BanyakCall") <> Nothing And Session("JmlCall").length >= Session("Next") Then
                If Session("JmlCall").length = Session("Next") Then
                    Session("BanyakCall") = Nothing
                    Session("Next") = Nothing
                    Session("JmlCall") = Nothing
                    Session("CALL_STATUSID") = 0
                    strsql = "select top 1 * from ICC_CALLLIST where FLAG<>'Y' and campaign='" & Request.QueryString("campaign") & "' and agent='" & Trim(Session("username")) & "' order by CALL_STATUSID"
                    tbldata = clsglobe.ExecuteQuery(strsql)
                    If tbldata.Rows.Count = Nothing Then
                        Response.Write("END")
                    Else
                        Response.Write(tbldata.Rows(0).Item("id") & "|" & tbldata.Rows(0).Item("recordnr") & "|" & tbldata.Rows(0).Item("fullname") & "|" & tbldata.Rows(0).Item("gender") & "|" & tbldata.Rows(0).Item("dob") _
                               & "|" & tbldata.Rows(0).Item("age") & "|" & tbldata.Rows(0).Item("telp_home") & "|" & tbldata.Rows(0).Item("telp_office") & "|" & tbldata.Rows(0).Item("telp_ext") & "|" & tbldata.Rows(0).Item("telp_mobile") _
                               & "|" & tbldata.Rows(0).Item("city") & "|" & tbldata.Rows(0).Item("kota_ktr") & "|" & tbldata.Rows(0).Item("segmentasi_wilayah") & "|" & tbldata.Rows(0).Item("vendorcode") & "|" & tbldata.Rows(0).Item("batchcode") & "|" & tbldata.Rows(0).Item("campaign"))
                    End If
                Else
                    Response.Write(Request.QueryString("campaign") & "|Next|" & Session("JmlCall")(Session("Next")))
                End If
            End If
        End If
    End Sub

End Class