﻿Imports System
Imports System.Data
Imports System.Data.SqlClient
Imports System.IO
Imports System.Drawing
Imports System.Drawing.Drawing2D
Imports DevExpress.Web.ASPxHtmlEditor
Public Class email_send
    Inherits System.Web.UI.Page

    Dim connection As New ClsConn
    Dim sqldr As SqlDataReader
    Dim com As SqlCommand
    Dim con As New SqlConnection(ConfigurationManager.ConnectionStrings("DefaultConnection").ConnectionString)
    Dim EmailForm As String = ConfigurationManager.AppSettings("EmailForm")
    Dim sqlcom, comm As SqlCommand
    Dim sqlcon As New SqlConnection(ConfigurationManager.ConnectionStrings("DefaultConnection").ConnectionString)
    Dim connectionString As String = ConfigurationManager.ConnectionStrings("DefaultConnection").ConnectionString
    Dim connectionemail As SqlConnection
    Dim loq As New cls_globe

    Private Sub Page_Init(sender As Object, e As EventArgs) Handles Me.Init
        If Session("LoginType") = "layer1" Or Session("LoginType") = "layer3" Then
            sql_send.SelectCommand = "select *, ICC_EMAIL_SENT.SentDate from v_emailOUT left outer join ICC_EMAIL_SENT on v_emailOUT.IVC_ID = ICC_EMAIL_SENT.IVC_ID where v_emailOUT.AGENT='" & Session("username") & "' order by v_emailOUT.email_date desc"
        Else
            sql_send.SelectCommand = "select *, ICC_EMAIL_SENT.SentDate from v_emailOUT left outer join ICC_EMAIL_SENT on v_emailOUT.IVC_ID = ICC_EMAIL_SENT.IVC_ID order by email_date desc"
        End If
        If Request.QueryString("status") = "compose" Then
            div_send.Visible = True
            ASPxGridView_Send.Visible = False
            ltr_attchment.Visible = False
            iframe_body.Visible = False
            btn_send.Visible = True
            btn_cancel.Visible = True
            div_attachment.Visible = False
            txt_body.Visible = False
        ElseIf Request.QueryString("status") = "reply" Then
            Detail_Email()
            div_send.Visible = True
            ASPxGridView_Send.Visible = False
            ltr_attchment.Visible = True
            iframe_body.Visible = True
            btn_send.Visible = True
            btn_cancel.Visible = True
            txt_body.Visible = False
            btn_update.Visible = True
            btn_send.Visible = False
        ElseIf Request.QueryString("status") = "detail" Then
            Detail_Email()
            div_send.Visible = True
            ASPxGridView_Send.Visible = False
            ltr_attchment.Visible = True
            iframe_body.Visible = True
            btn_send.Visible = False
            btn_cancel.Visible = False
            txt_body.Visible = False
        Else
            div_send.Visible = False
        End If
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub

    Private Sub Detail_Email()
        Dim path As String = String.Empty
        Dim ATTACHMENT_ID As String = String.Empty
        Dim ivcid As String = String.Empty
        Dim emailid As String = String.Empty
        Dim strSql As String = String.Empty
        strSql = "select *, ICC_EMAIL_SENT.SentDate from v_emailOUT left outer join ICC_EMAIL_SENT on v_emailOUT.IVC_ID = ICC_EMAIL_SENT.IVC_ID where v_emailOUT.IVC_ID='" & Request.QueryString("ivcid") & "'"
        sqldr = connection.ExecuteReader(strSql)
        Try
            If sqldr.HasRows() Then
                sqldr.Read()
                txtto.Text = sqldr("ETO").ToString
                txtcc.Text = sqldr("ECC").ToString
                txtsubject.Text = sqldr("ESUBJECT").ToString
                If Request.QueryString("status") = "reply" Then
                    ASPxHtmlEditor1.Html = sqldr("EBODY_HTML").ToString
                Else

                End If
                path = sqldr("path").ToString
                emailid = sqldr("EMAIL_ID").ToString
                ivcid = sqldr("IVC_ID").ToString
            Else
            End If
            sqldr.Close()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
        Dim directoryPath As String = Server.MapPath(String.Format("~/HTML/attachment/receive/" + EmailForm + "/" & ReplaceSpecialLetterEmailid(emailid) & "/" & "file.html"))
        If Not File.Exists(directoryPath) Then
            ClientScript.RegisterStartupScript(Me.GetType(), "alert", "alert('email empty.');", True)
        Else
            ' file ada
            iframe_body.Src = "attachment/receive/" & EmailForm & "/" & ReplaceSpecialLetterEmailid(emailid) & "/file.html"
        End If
        'If emailid <> "" Then
        '    iframe_body.Src = "attachment/receive/" & EmailForm & "/" & ReplaceSpecialLetterEmailid(emailid) & "/file.html"
        'Else
        '    iframe_body.Src = "attachment/error404.html"
        'End If

        Dim dataChart As String = String.Empty
        Dim strdata As String = "SELECT replace(URL,' ','%20') as url FROM icc_email_in_detail WHERE EMAIL_ID='" & Request.QueryString("emailid") & "'"
        Try
            sqldr = connection.ExecuteReader(strdata)
            If sqldr.HasRows() Then
                While sqldr.Read()
                    dataChart &= "<tr><td style='font-size: x-small;text-align:left;'><a href=attachment/receive/" & sqldr("URL").ToString & " target='_blank'>" & sqldr("URL").ToString & "</a></td></tr>"
                End While
                ltr_attchment.Text = dataChart
            Else
                div_attachment.Visible = False
            End If
            sqldr.Close()

        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Private Sub btn_send_ServerClick(sender As Object, e As EventArgs) Handles btn_send.ServerClick
        Dim filename As String = String.Empty
        Dim FolderName As String = "ICC-" & DateTime.Now.ToString("yyyyMMddHHmmssfff")
        If fu_EmailAttach.HasFile = True Then
            Dim directoryPath As String = Server.MapPath(String.Format("~/HTML/attachment/receive/" & EmailForm & "/" & "/{0}/", ReplaceSpecialLetterEmailid(FolderName).Trim()))
            If Not Directory.Exists(directoryPath) Then
                Directory.CreateDirectory(directoryPath)
            Else
                ' Folder Sudah Ada
                'ClientScript.RegisterStartupScript(Me.GetType(), "alert", "alert('Directory already exists.');", True)
            End If

            Dim Time As String = DateTime.Now.ToString("yyyyMMddHHmmssfff")
            filename = Path.GetFileName(fu_EmailAttach.FileName)
            Dim folderPath As String = Server.MapPath("~/HTML/attachment/receive/" & EmailForm & "/" & ReplaceSpecialLetterEmailid(FolderName) & "/" + filename)
            Try
                fu_EmailAttach.SaveAs(folderPath)
            Catch ex As Exception
                Response.Write(DirectCast("", String))
            End Try

            Dim insertattchment As String = "insert into ICC_EMAIL_IN_DETAIL (EMAIL_ID, URL) VALUES ('" & FolderName & "','" & EmailForm & "\" & ReplaceSpecialLetterEmailid(FolderName) & "\" & filename & "')"
            loq.writedata(Session("UserName"), "email sending attachment", "Proses kirim attachment", insertattchment, "Channel Email Send Attachment send_email.aspx")
            com = New SqlCommand(insertattchment, con)
            Try
                con.Open()
                com.ExecuteNonQuery()
                con.Close()
            Catch ex As Exception
                Response.Write(DirectCast("", String))
            End Try
        Else
            filename = ""
        End If

        Using mStream As MemoryStream = New MemoryStream()
            ASPxHtmlEditor1.Export(HtmlEditorExportFormat.Txt, mStream)
            Dim plainText As String = System.Text.Encoding.UTF8.GetString(mStream.ToArray())
            ASPxMemo1.Text = plainText
        End Using

        Dim insertdata As String = "insert into ICC_EMAIL_OUT (EMAIL_ID, DIRECTION, EFROM, ETO, ECC, ESUBJECT, EBODY_TEXT, EBODY_HTML, Email_Date, Path, agent, JENIS_EMAIL, JENIS_EMAIL_INTERNAL, ATTACHMENT_ID) " &
                                   "VALUES ('" & FolderName & "','out', '" & EmailForm & "', '" & txtto.Text & "', '" & txtcc.Text & "', '" & txtsubject.Text & "', '" & ASPxMemo1.Text & "', '" & ASPxHtmlEditor1.Html & "', GETDATE(), '" & filename & "','" & Session("username") & "', 'compose', '" & Session("lvluser") & "','" & ReplaceSpecialLetterEmailid(FolderName) & "')"
        'loq.writedata(Session("username"), "email sending", "Proses kirim email keluar", insertdata, "channel email send send_email.aspx")
        com = New SqlCommand(insertdata, con)
        Try
            con.Open()
            com.ExecuteNonQuery()
            con.Close()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try

        connectionemail = New SqlConnection(connectionString)
        comm = New SqlCommand()
        comm.Connection = connectionemail
        comm.CommandType = CommandType.StoredProcedure
        comm.CommandText = "sp_insert_send_email"
        comm.Parameters.Add("@emailid", Data.SqlDbType.VarChar).Value = FolderName
        comm.Parameters.Add("@efrom", Data.SqlDbType.VarChar).Value = EmailForm
        comm.Parameters.Add("@eto", Data.SqlDbType.VarChar).Value = txtto.Text
        comm.Parameters.Add("@ecc", Data.SqlDbType.VarChar).Value = txtcc.Text
        comm.Parameters.Add("@esubject", Data.SqlDbType.VarChar).Value = txtsubject.Text
        comm.Parameters.Add("@bodytext", Data.SqlDbType.VarChar).Value = ASPxMemo1.Text
        comm.Parameters.Add("@bodyhtml", Data.SqlDbType.VarChar).Value = ASPxHtmlEditor1.Html
        comm.Parameters.Add("@path", Data.SqlDbType.VarChar).Value = filename
        comm.Parameters.Add("@agent", Data.SqlDbType.VarChar).Value = Session("username")
        comm.Parameters.Add("@jenisemail", Data.SqlDbType.VarChar).Value = "compose"
        comm.Parameters.Add("@jenisemailinternal", Data.SqlDbType.VarChar).Value = Session("lvluser")
        comm.Parameters.Add("@attachmentid", Data.SqlDbType.VarChar).Value = ReplaceSpecialLetterEmailid(FolderName)
        Try
            connectionemail.Open()
            sqldr = comm.ExecuteReader()
            connectionemail.Close()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try

        ASPxHtmlEditor1.Html = ""
        Response.Redirect("email_send.aspx?status=send&idpage=2027")
    End Sub

    Private Sub updateAlert()
        Dim updateActivity As String = "update user1 set Activity='N'"
        Try
            com = New SqlCommand(updateActivity, con)
            con.Open()
            com.ExecuteNonQuery()
            con.Close()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
        Dim IdupdateActivity As String = "update user1 set Activity='Y' where MenuID='" & Request.QueryString("idpage") & "'"
        Try
            com = New SqlCommand(IdupdateActivity, con)
            con.Open()
            com.ExecuteNonQuery()
            con.Close()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Private Sub btn_cancel_ServerClick(sender As Object, e As EventArgs) Handles btn_cancel.ServerClick
        Response.Redirect("email_send.aspx?status=inbox&idpage=2027")
    End Sub

    Public Function ReplaceSpecialLetterEmailid(ByVal str)
        Dim TmpStr As String
        TmpStr = str
        TmpStr = Replace(TmpStr, "ICC-", "")
        TmpStr = Replace(TmpStr, ";", "")
        ReplaceSpecialLetterEmailid = TmpStr
    End Function

    Private Sub btn_update_ServerClick(sender As Object, e As EventArgs) Handles btn_update.ServerClick
        Dim filename As String = String.Empty
        Dim FolderName As String = DateTime.Now.ToString("yyyyMMddHHmmsstt")
        If fu_EmailAttach.HasFile = True Then
            Dim directoryPath As String = Server.MapPath(String.Format("~/HTML/attachment/receive/" & EmailForm & "/" & "/{0}/", ReplaceSpecialLetterEmailid(FolderName).Trim()))
            If Not Directory.Exists(directoryPath) Then
                Directory.CreateDirectory(directoryPath)
            Else
                ' Folder Sudah Ada
                'ClientScript.RegisterStartupScript(Me.GetType(), "alert", "alert('Directory already exists.');", True)
            End If

            Dim Time As String = DateTime.Now.ToString("yyyyMMddHHmmssttfff")
            filename = Path.GetFileName(fu_EmailAttach.FileName)
            Dim folderPath As String = Server.MapPath("~/HTML/attachment/receive/" & EmailForm & "/" & ReplaceSpecialLetterEmailid(FolderName) & "/" + filename)
            Try
                fu_EmailAttach.SaveAs(folderPath)
            Catch ex As Exception
                Response.Write(DirectCast("", String))
            End Try

            Dim insertattchment As String = "insert into ICC_EMAIL_IN_DETAIL (EMAIL_ID, URL) VALUES ('" & FolderName & "','" & EmailForm & "\" & ReplaceSpecialLetterEmailid(FolderName) & "\" & filename & "')"
            com = New SqlCommand(insertattchment, con)
            Try
                con.Open()
                com.ExecuteNonQuery()
                con.Close()
            Catch ex As Exception
                Response.Write(DirectCast("", String))
            End Try
        Else
            filename = ""
        End If

        Using mStream As MemoryStream = New MemoryStream()
            ASPxHtmlEditor1.Export(HtmlEditorExportFormat.Txt, mStream)
            Dim plainText As String = System.Text.Encoding.UTF8.GetString(mStream.ToArray())
            ASPxMemo1.Text = plainText
        End Using

        Dim updatedate As String = "update ICC_EMAIL_OUT set ETO='" & txtto.Text & "', ECC='" & txtcc.Text & "', ESUBJECT='" & txtsubject.Text & "', EBODY_TEXT='" & ASPxMemo1.Text & "', EBODY_HTML='" & ASPxHtmlEditor1.Html & "', Email_Date=getdate() where ivc_id='" & Request.QueryString("ivcid") & "'"
        com = New SqlCommand(updatedate, con)
        Try
            con.Open()
            com.ExecuteNonQuery()
            con.Close()
        Catch ex As Exception
            Response.Write(DirectCast("", String))
        End Try
    End Sub
End Class