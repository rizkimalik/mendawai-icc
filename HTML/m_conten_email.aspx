﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/HTML/Ticket.Master" CodeBehind="m_conten_email.aspx.vb" Inherits="ICC.m_conten_email" %>

<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxCallbackPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxHtmlEditor.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxHtmlEditor" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxSpellChecker.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxSpellChecker" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="FeaturedContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <h4 class="headline">Setting Notifikasi Email Content  
			<span class="line bg-warning"></span>
    </h4>

    

    <dx:ASPxGridView ID="ASPxGridView_Email" ClientInstanceName="ASPxGridView" runat="server" KeyFieldName="ID"
        DataSourceID="ds_Query" Width="100%" Theme="Metropolis" Styles-Header-Font-Bold="true" Font-Size="X-Small">
        <SettingsPager>
            <AllButton Text="All">
            </AllButton>
            <NextPageButton Text="Next &gt;">
            </NextPageButton>
            <PrevPageButton Text="&lt; Prev">
            </PrevPageButton>
            <PageSizeItemSettings Visible="true" Items="10, 15, 20" ShowAllItem="true" />
        </SettingsPager>
        <SettingsEditing Mode="EditForm" />
        <Settings ShowFilterRow="true" ShowFilterRowMenu="false" ShowGroupPanel="true"
            ShowVerticalScrollBar="false" ShowHorizontalScrollBar="false" />
        <SettingsBehavior ConfirmDelete="true" />
        <Templates>
            <EditForm>
                Subject : <dx:ASPxTextBox runat="server" ID="txtSubject" Text='<%#Eval("Subject")%>' Width="450px"></dx:ASPxTextBox>
                <br /><br />
                Header Email : 
                <dx:ASPxHtmlEditor Settings-AllowDesignView="true" Html='<%#Eval("Header_HTML")%>' Settings-AllowHtmlView="true" ID="html_editor_Body" Width="100%" Height="250px" runat="server">
                          
                </dx:ASPxHtmlEditor>
                <br /><br />
                Footer Email : 
                <dx:ASPxHtmlEditor Settings-AllowDesignView="true" Html='<%#Eval("Footer_HTML")%>' Settings-AllowHtmlView="true" ID="html_footer" Width="100%" Height="250px" runat="server">
                          
                </dx:ASPxHtmlEditor>
                <br /><br />
                <div style="text-align: right; padding: 2px 2px 2px 2px">
                    <dx:ASPxGridViewTemplateReplacement ID="UpdateButton" ReplacementType="EditFormUpdateButton"
                        runat="server">
                    </dx:ASPxGridViewTemplateReplacement>
                    <dx:ASPxGridViewTemplateReplacement ID="CancelButton" ReplacementType="EditFormCancelButton"
                        runat="server">
                    </dx:ASPxGridViewTemplateReplacement>
                </div>
            </EditForm>
        </Templates>
        <Columns>
            <dx:GridViewCommandColumn Caption="Action" HeaderStyle-HorizontalAlign="Center" VisibleIndex="0"
                ButtonType="Image" FixedStyle="Left" Width="80px">
                <EditButton Visible="True">
                    <Image ToolTip="Edit" Url="img/icon/Text-Edit-icon2.png" />
                </EditButton>
                <NewButton Visible="false">
                    <Image ToolTip="New" Url="img/icon/Apps-text-editor-icon2.png" />
                </NewButton>
                <DeleteButton Visible="false">
                    <Image ToolTip="Delete" Url="img/icon/Actions-edit-clear-icon2.png" />
                </DeleteButton>
                <CancelButton>
                    <Image ToolTip="Cancel" Url="img/icon/cancel1.png">
                    </Image>
                </CancelButton>
                <UpdateButton>
                    <Image ToolTip="Update" Url="img/icon/Updated1.png" />
                </UpdateButton>
            </dx:GridViewCommandColumn>
            <dx:GridViewDataTextColumn Caption="ID" FieldName="ID" ReadOnly="true" Visible="false"></dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="Subject" FieldName="Subject" Width="300px"></dx:GridViewDataTextColumn>
            <dx:GridViewDataMemoColumn Caption="Header" FieldName="Header_HTML" PropertiesMemoEdit-Height="150">
                 <PropertiesMemoEdit EncodeHtml ="false" ></PropertiesMemoEdit>
            </dx:GridViewDataMemoColumn>
           <%-- <dx:GridViewDataMemoColumn Caption="Body" FieldName="Body"></dx:GridViewDataMemoColumn>--%>
            <dx:GridViewDataMemoColumn Caption="Footer" FieldName="Footer_HTML" PropertiesMemoEdit-Height="150">
              <PropertiesMemoEdit EncodeHtml ="false" ></PropertiesMemoEdit>
            </dx:GridViewDataMemoColumn>
            <dx:GridViewDataTextColumn Caption="Type" FieldName="Type" ReadOnly="true" Width="100px"></dx:GridViewDataTextColumn>
        </Columns>
    </dx:ASPxGridView>
    <asp:SqlDataSource ID="ds_Query" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>" SelectCommand="select * from TR_TemplateNotifikasiEmail"
       UpdateCommand="update TR_TemplateNotifikasiEmail set Header_HTML=@Header_HTML,Footer_HTML=@Footer_HTML,Subject=@Subject where ID=@ID" ></asp:SqlDataSource>
</asp:Content>
