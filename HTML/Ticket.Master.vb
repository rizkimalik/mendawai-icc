﻿Imports System
Imports System.Data
Imports System.Data.SqlClient
Imports DevExpress.Web.ASPxGridView
Public Class Ticket
    Inherits System.Web.UI.MasterPage

    Dim sqldr, dr, sqldtr, sqldra As SqlDataReader
    Dim tbldata, tbldata2, tbldata3, tbldataM As DataTable
    Dim Proses As New ClsConn
    Dim sql As String
    Dim VTicket, VTwitter, VFacebook, VEmail, VFax, Vchat, VSms As String
    Dim sqlCon As New SqlConnection(ConfigurationManager.ConnectionStrings("DefaultConnection").ConnectionString)
    Dim sqlConnection As New SqlConnection(ConfigurationManager.ConnectionStrings("DefaultConnection").ConnectionString)
    Dim sqlConn, connections As New SqlConnection(ConfigurationManager.ConnectionStrings("DefaultConnection").ConnectionString)
    Dim Com, sqlcom, SqlComman, comm As SqlCommand
    Dim url, cssParent, cssSubMenuParent, cssSumMenuTreeParent As String
    Dim MenuID, SubMenuID, SubMenuTreeID As Integer
    Dim userprofile, strsql As String
    Dim litChannel As String
    Dim imgAgent As String
    Dim con As New ClsConn
    Dim DescAUX As String = String.Empty
    Dim addParam As String = String.Empty
    Dim target As String = String.Empty

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim VariabelCookiesUsername As HttpCookie = HttpContext.Current.Request.Cookies("CookiesUserName")
        Dim TrxCookiesUserName As String = If(VariabelCookiesUsername IsNot Nothing, VariabelCookiesUsername.Value.Split("="c)(1), "undefined")

        If TrxCookiesUserName = "undefined" Then
            Response.Redirect("~/Logout.aspx")
        End If

        txt_username.Text = Session("username")
        TrxUserName.Value = Session("username")
        SessionID.Value = Session("SessionID")
        Session("dbName") = "master"

        Try
            sql = "update msUser set SessionID='" & Session("SessionID") & "' where username='" & Session("username") & "'"
            Proses.ExecuteReader(sql)
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try


        Try
            sql = "select DescAUX from msUser where username='" & Session("username") & "'"
            sqldr = con.ExecuteReader(sql)
            If sqldr.HasRows() Then
                sqldr.Read()
                DescAUX = sqldr("DescAUX").ToString
            Else
            End If
            sqldr.Close()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try


        If (Session("LoginTypeAngka") = "1" Or Session("LoginTypeAngka") = "2") Then
            If Session("NameKaryawan") = "" Then
                lbl_user_login.Text = Session("username") & " - " & DescAUX
            Else
                lbl_user_login.Text = Session("NameKaryawan") & " - " & Session("NamaGrup") & " - " & DescAUX
            End If
        ElseIf Session("LoginTypeAngka") = "3" Then
            If Session("NameKaryawan") = "" Then
                lbl_user_login.Text = Session("username") & " - " & DescAUX
            Else
                lbl_user_login.Text = Session("NameKaryawan") & " - " & Session("Org") & " - " & DescAUX
            End If
        ElseIf Session("LoginTypeAngka") = "4" Then
            If Session("NameKaryawan") = "" Then
                lbl_user_login.Text = Session("username") & " - " & DescAUX
            Else
                lbl_user_login.Text = Session("NameKaryawan") & " - " & Session("NamaGrup") & " - " & DescAUX
            End If
        ElseIf Session("LoginTypeAngka") = "5" Then
            If Session("NameKaryawan") = "" Then
                lbl_user_login.Text = Session("username") & ""
            Else
                lbl_user_login.Text = Session("NameKaryawan") & ""
            End If
        Else
            If Session("NameKaryawan") = "" Then
                lbl_user_login.Text = Session("username") & " - " & DescAUX
            Else
                lbl_user_login.Text = Session("NameKaryawan") & " - " & Session("Org") & " - " & DescAUX
            End If
        End If

        'count_dispatch()
        User_Menu()
        If Session("path") = "" Then
            imgAgent = "img/user.jpg"
        Else
            imgAgent = Session("path")
        End If
        userprofile = "<i class='fa a-user'></i>" & Session("username") & ""
        ltr_agent.Text = userprofile
        If Request.QueryString("idpage") <> "1014" Then
            Session("rbuser") = ""
        Else
        End If
        detailagent()
    End Sub
    Sub User_Menu()
        Dim display As String = ""
        Dim Menu As String = ""
        Dim SubMenu As String = ""
        Dim SubMenuTree As String = ""
        Dim DSubMenu As String = ""
        Dim Url As String = ""
        Dim Css As String = ""
        Dim IconCheck As String = ""
        Dim ActiveMenu As String = ""
        Dim ActiveSubmenu As String = ""

        sql = "select user1.Number, user4.MenuID, MenuName, user1.Url, user1.Icon, user1.DivID, user1.Activity from user4 left outer join User1 " & _
              "on user4.MenuID=user1.MenuID " & _
              "where user4.leveluserid='" & Session("LEVELUSERID") & "' " & _
              "group by MenuName, user4.MenuID, Number, user1.url, user1.Icon, user1.DivID, user1.Activity " & _
              "order by user1.number asc"
        sqlcom = New SqlCommand(sql, connections)
        connections.Open()
        sqldr = sqlcom.ExecuteReader()
        While sqldr.Read
            If sqldr("Url").ToString <> "" Then
                Css = ""
            Else
                Css = "openable open"
            End If

            If Request.QueryString("idpage") = sqldr("MenuID").ToString Then
                IconCheck = "<span class='badge badge-warning' style='background-color:#f37021;'><i class='fa fa-check'></i></span>"
                ActiveMenu = "active"
                display = "block"
            Else
                ActiveMenu = ""
                IconCheck = ""
                display = "none"
            End If
            Menu &= "<ul><li id='" & sqldr("DivID") & "' class='" & Css & " " & ActiveMenu & "' runat='server'>" &
                    "<a href='" & sqldr("Url").ToString & "'>" &
                    "<span class='menu-icon'>" &
                    "<i class='" & sqldr("Icon") & "'></i>" &
                    "</span>" &
                    "<span class='text'>" & sqldr("MenuName").ToString & "" &
                    "</span>" &
                    "" & IconCheck & "" &
                    "<span class='menu-hover'></span>" &
                    "</a>"
            If sqldr("MenuID").ToString <> "" Then
                SubMenu = "SELECT distinct user4.MenuID, user4.SubMenuID, user2.SubMenuName, User2.Url, user2.Param, user2.Activity, " & _
                          "User2.DivID FROM USER4 INNER JOIN User2 ON USER4.SubMenuID = USER2.SubMenuID " & _
                          "INNER JOIN User1 ON USER4.MenuID = USER1.MenuID " & _
                          "WHERE USER4.leveluserid='" & Session("LEVELUSERID") & "' AND USER4.MenuID='" & sqldr("MenuID") & "' order by menuid asc"
                Com = New SqlCommand(SubMenu, sqlCon)
                sqlCon.Open()
                dr = Com.ExecuteReader()
                Menu &= "<ul class='submenu' style='display:" & display & ";'>"
                While dr.Read()
                    If Request.QueryString("idtable") = dr("SubMenuID").ToString Then
                        ActiveSubmenu = "active"
                    Else
                        ActiveSubmenu = ""
                    End If

                    If dr("Param").ToString = "Y" Then
                        If Session("LoginType") = "Supervisor" Then
                            addParam = dr("Url") & "?id=" & Session("orgSupervisor") & "&lvlUser=" & ReplaceSpecialLetter(Session("lvluser")) & "&org=" & Session("unitkerjaagent") & "&layerID=" & Session("LoginTypeAngka") & ""
                        ElseIf Session("LoginType") = "Layer 1" Then
                            addParam = dr("Url") & "?id=" & Session("username") & "&lvlUser=" & ReplaceSpecialLetter(Session("lvluser")) & "&org=" & Session("unitkerjaagent") & "&layerID=" & Session("LoginTypeAngka") & ""
                        ElseIf Session("LoginType") = "Layer 2" Then
                            addParam = dr("Url") & "?id=" & Session("username") & "&lvlUser=" & ReplaceSpecialLetter(Session("lvluser")) & "&org=" & Session("unitkerjaagent") & "&layerID=" & Session("LoginTypeAngka") & ""
                        ElseIf Session("LoginType") = "Layer 3" Then
                            addParam = dr("Url") & "?id=" & Session("username") & "&lvlUser=" & ReplaceSpecialLetter(Session("lvluser")) & "&org=" & Session("organization") & "&layerID=" & Session("LoginTypeAngka") & ""
                        Else
                            addParam = dr("Url") & "?id=" & Session("username") & "&lvlUser=" & ReplaceSpecialLetter(Session("lvluser")) & "&org=" & Session("unitkerjaagent") & "&layerID=" & Session("LoginTypeAngka") & ""
                        End If
                        target = "target='_blank'"
                    ElseIf dr("Param").ToString = "X" Then
                        addParam = dr("Url") & "?userName=" & Session("username") & ""
                        target = "target='_blank'"
                    Else
                        addParam = dr("Url") & "?idpage=" & dr("MenuID") & "&idtable=" & dr("SubMenuID") & ""
                        target = ""
                    End If

                    Menu &= "<li id='" & dr("DivID") & "' class='" & ActiveSubmenu & "' runat='server'>" &
                             "<a href=" & addParam & " " & target & ">" &
                             "<span class='submenu-label'>" & dr("SubMenuName") & "</span></a>"

                    If dr("SubMenuID").ToString <> "" Then
                        SubMenuTree = "select distinct user4.UserID, user4.MenuID, user1.MenuName, user2.SubMenuID, user2.SubMenuName, user3.SubMenuIDTree, " & _
                                        "user3.MenuTreeName, User3.Url, User3.DivID " & _
                                        "from user4 left outer join " & _
                                        "user3 on.user3.SubMenuID=user4.SubMenuID left outer join user2 on.user4.SubMenuID = user2.SubMenuID " & _
                                        "left outer join User1 on user4.MenuID = user1.MenuID " & _
                                        "where user3.SubMenuID='" & dr("SubMenuID").ToString & "' and user4.leveluserid='" & Session("LEVELUSERID") & "' "
                        SqlComman = New SqlCommand(SubMenuTree, sqlConnection)
                        sqlConnection.Open()
                        sqldtr = SqlComman.ExecuteReader()
                        Menu &= "<ul class='submenu third-level'>"
                        While sqldtr.Read()
                            Menu &= "<li id=" & sqldtr("DivID") & " runat='server'>" &
                                    "<a href=" & sqldtr("Url") & "><span class='submenu-label'>" & sqldtr("MenuTreeName") & "</span></a>"
                        End While
                        sqldtr.Close()
                        sqlConnection.Close()
                        Menu &= "</li></ul>"
                    Else

                    End If
                End While
                dr.Close()
                sqlCon.Close()
                Menu &= "</li></ul></li></ul>"
            Else

            End If
        End While
        sqldr.Close()
        connections.Close()
        ltr_Menu.Text = Menu
        'ltr_SubMenu.Text = DSubMenu
    End Sub
    Private Sub filelogout_ServerClick(sender As Object, e As EventArgs) Handles filelogout.ServerClick
        Dim TrxCookiesUserName As HttpCookie = Request.Cookies("CookiesUserName")
        TrxCookiesUserName.Expires = DateTime.Now.AddDays(-1)
        Response.Cookies.Add(TrxCookiesUserName)

        Dim updatelogout As String = "UPDATE MSUSER SET LOGIN='0' WHERE USERNAME = '" & Session("username") & "'"
        Try
            Com = New SqlCommand(updatelogout, sqlCon)
            sqlCon.Open()
            Com.ExecuteNonQuery()
            sqlCon.Close()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try

        Dim ilogin As String = "INSERT INTO ICC_LOG_IN (USERID,ACTIVITY_DATE,AUX_DESCRIPTION) VALUES('" & Session("username") & "',GETDATE(),'LOGOUT')"
        sqlcom = New SqlCommand(ilogin, sqlCon)
        Try
            sqlCon.Open()
            sqlcom.ExecuteNonQuery()
            sqlCon.Close()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
        Response.Redirect("~/login.aspx")
    End Sub
    Private Sub ASPxCallbackPanel11_Callback(sender As Object, e As DevExpress.Web.ASPxClasses.CallbackEventArgsBase) Handles ASPxCallbackPanel11.Callback
        txt_new_password.Enabled = True
        txt_re_password.Enabled = True
    End Sub
    Private Sub detailagent()
        sql_organization.SelectCommand = "SELECT ORGANIZATION_ID, ORGANIZATION_NAME, description FROM mOrganization"
        sql_level_user.SelectCommand = "SELECT * FROM MLEVELUSER where LevelUserID <> '319'"
        sql_karyawan.SelectCommand = "SELECT * FROM MKARYAWAN"
        sql_user.SelectCommand = "SELECT USERID, USERNAME, MSUSER.PASSWORD, NAME, LEVELUSER, ORGANIZATION, MAX_CHAT, MAX_EMAIL, mOrganization.ORGANIZATION_NAME,mOrganization.ORGANIZATION_ID, MSUSER.EMAIL, MSUSER.OUTBOUND, MSUSER.CHAT, MSUSER.INBOUND, MSUSER.EMAIL_ADDRESS, BTN_Campaign.Namacampaign FROM MSUSER LEFT OUTER JOIN mOrganization ON msUser.ORGANIZATION= mOrganization.ORGANIZATION_ID LEFT OUTER JOIN BTN_Campaign on msuser.Group_campaign = BTN_Campaign.ID where USERNAME='" & Session("username") & "'"
    End Sub
    Protected Sub TicketNumber_DataSelect(ByVal sender As Object, ByVal e As EventArgs)
        Session("username") = (TryCast(sender, ASPxGridView)).GetMasterRowKeyValue()
        sql_menu.SelectCommand = "select DISTINCT USER4.MenuID, USER1.MenuName from user4 LEFT OUTER JOIN User1 ON.USER4.MenuID = USER1.MenuID where USER4.UserID='" & Session("username") & "'"
        Dim lvlID As String
        Dim strSql As String = "select mLevelUser.LevelUserID from msUser inner join mLevelUser on msUser.LEVELUSER = mLevelUser.Description where msUser.USERNAME='" & Session("username") & "'"
        sqlcom = New SqlCommand(strSql, sqlCon)
        Try
            sqlCon.Open()
            sqldr = sqlcom.ExecuteReader
            sqldr.Read()
            lvlID = sqldr("LevelUserID").ToString
            sqldr.Close()
            sqlCon.Close()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Protected Sub GridList_DataSelect(ByVal sender As Object, ByVal e As EventArgs)
        Session("MenuID") = (TryCast(sender, ASPxGridView)).GetMasterRowKeyValue()
        sql_subMenu_dr.SelectCommand = "SELECT * FROM USER2 WHERE MENUID='" & Session("MenuID") & "'"
        sql_sub_menu.SelectCommand = "select DISTINCT user2.MenuID, USER4.SubMenuID, USER2.SubMenuName from user4 LEFT OUTER JOIN User2 ON.USER4.SubMenuID = USER2.SubMenuID where USER4.UserID='" & Session("username") & "' And USER2.MenuID='" & Session("MenuID") & "'"
    End Sub
    Protected Sub gv_menu_tree_DataSelect(ByVal sender As Object, ByVal e As EventArgs)
        Session("SubMenuID") = (TryCast(sender, ASPxGridView)).GetMasterRowKeyValue()
        sql_menu_tree.SelectCommand = "select DISTINCT USER3.SubMenuIDTree, USER3.MenuTreeName from user4 LEFT OUTER JOIN USER3 ON.USER4.MenuIDTree = USER3.SubMenuIDTree where USER3.SubMenuID='" & Session("SubMenuID") & "'"
        sql_User3.SelectCommand = "Select * from user3 where SubMenuID='" & Session("SubMenuID") & "'"

        Dim lvlID As String
        Dim strSql As String = "select LevelUserID from user4 where UserID='" & Session("username") & "'"
        sqlcom = New SqlCommand(strSql, sqlCon)
        Try
            sqlCon.Open()
            sqldr = sqlcom.ExecuteReader
            sqldr.Read()
            lvlID = sqldr("LevelUserID").ToString
            sqldr.Close()
            sqlCon.Close()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Public Function ReplaceSpecialLetter(ByVal str)
        Dim TmpStr As String
        TmpStr = str
        TmpStr = Replace(TmpStr, " ", "")
        ReplaceSpecialLetter = TmpStr
    End Function
End Class