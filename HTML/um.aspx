﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/HTML/Ticket.Master" CodeBehind="um.aspx.vb" Inherits="ICC.um" %>

<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxCallbackPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxHtmlEditor.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxHtmlEditor" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="FeaturedContent" runat="server">
    <script>
        function showUser(checked) {
            if (checked) {
                document.getElementById("MainContent_hdleveluser").value = "1";
                ASPxCallbackPanel3.PerformCallback();
            } else {
                document.getElementById("MainContent_hdleveluser").value = "0";
                ASPxCallbackPanel3.PerformCallback();
            }
        };
        function click() {
            alert("test")
        }
    </script>
    <script>
        function OnEndCallback() {
            if (ASPxGridView1.cpInsertNote != "") {
                alert(ASPxGridView1.cpInsertNote);
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:HiddenField ID="hdleveluser" runat="server" />
    <h4 class="headline">User Setting Previledge
			<span class="line bg-warning"></span>
    </h4>

    <div class="padding-md" style="margin-top: -20px;">
        <div class="row">
            <div class="col-md-10 col-sm-10" style="margin-left: -15px;">
                <dx:ASPxRadioButtonList ID="ASPxRadioButtonList1" runat="server" AutoPostBack="true" RepeatLayout="OrderedList"
                    OnSelectedIndexChanged="ASPxRadioButtonList1_SelectedIndexChanged"
                    ValueType="System.String" ClientInstanceName="ASPxRadioButtonList1" Theme="SoftOrange">
                    <Items>
                        <dx:ListEditItem Text="Agent Atau Cabang" Value="Agent" />
                        <dx:ListEditItem Text="Supervisor" Value="Supervisor" />
                        <dx:ListEditItem Text="Unit Kerja Permasalahan" Value="CaseUnit" />
                        <dx:ListEditItem Text="Administrator" Value="Administrator" />
                    </Items>
                    <ClientSideEvents Init="" />
                </dx:ASPxRadioButtonList>
                <asp:Label ID="Label2" runat="server" Text="Label" Visible="false"></asp:Label>
            </div>
            <div class="col-md-2 col-sm-2">
                <div class="btn-group pull-right" style="margin-right: -32px;">
                    <a class="btn btn-danger btn-xs" id="kembali" runat="server" style="background-color:#EE4D2D; border-color:#EE4D2D;"><i class="fa fa-refresh"></i>&nbsp;Refresh</a>
                </div>
            </div>
        </div>
        <br />
        <div class="row">
            <div class="col-md-12 col-sm-12" style="margin-left:-15px; font-weight:400;">
                <asp:Label ID="Lblnotif" runat="server" ForeColor="Red" Font-Size="Small"></asp:Label>
            </div>
        </div>
        <div class="row">
            <div>
                <dx:ASPxGridView ID="ASPxGridView1" ClientInstanceName="ASPxGridView1" runat="server" Theme="Metropolis" Styles-Header-Font-Bold="true" Font-Size="X-Small"
                    DataSourceID="sql_user" KeyFieldName="USERNAME" Width="100%">
                    <SettingsPager>
                        <AllButton Text="All">
                        </AllButton>
                        <NextPageButton Text="Next &gt;">
                        </NextPageButton>
                        <PrevPageButton Text="&lt; Prev">
                        </PrevPageButton>
                        <PageSizeItemSettings Visible="true" Items="10, 15, 20" ShowAllItem="true" />
                    </SettingsPager>
                    <SettingsPager PageSize="10" />
                    <SettingsEditing Mode="Inline" />
                    <Settings ShowFilterRow="true" ShowGroupPanel="false" ShowHorizontalScrollBar="TRUE" />
                    <SettingsBehavior ConfirmDelete="true" />
                    <Columns>
                        <dx:GridViewCommandColumn Caption="Action" ButtonType="Image" Width="150px" HeaderStyle-HorizontalAlign="Center" VisibleIndex="0">
                            <NewButton Visible="true">
                                <Image Url="~/Images/icon/Apps-text-editor-icon2.png"></Image>
                            </NewButton>
                            <DeleteButton Visible="false">
                                <Image Url="~/Images/icon/Actions-edit-clear-icon2.png"></Image>
                            </DeleteButton>
                            <EditButton Visible="True">
                                <Image ToolTip="Edit" Url="img/Icon/Text-Edit-icon2.png" />
                            </EditButton>
                            <CancelButton Visible="true">
                                <Image ToolTip="Cancel" Url="~/Images/icon/cancel1.png">
                                </Image>
                            </CancelButton>
                            <UpdateButton Visible="true">
                                <Image ToolTip="Update" Url="~/Images/icon/Updated1.png" />
                            </UpdateButton>
                        </dx:GridViewCommandColumn>
                        <dx:GridViewDataColumn Caption="Username" FieldName="USERNAME" ReadOnly="false" Settings-AutoFilterCondition="Contains" Width="200px" />
                        <dx:GridViewDataColumn Caption="Name" FieldName="NAME" Settings-AutoFilterCondition="Contains" Width="200px" />
                        <dx:GridViewDataComboBoxColumn Caption="Level user" FieldName="LEVELUSER" HeaderStyle-HorizontalAlign="left" Width="200px">
                            <PropertiesComboBox TextField="Description" ValueField="Description" EnableSynchronization="False"
                                TextFormatString="{1}" IncrementalFilteringMode="Contains" DataSourceID="sql_level_user">
                                <Columns>
                                    <dx:ListBoxColumn Caption="ID" FieldName="LevelUserID" Width="50px" />
                                    <dx:ListBoxColumn Caption="Level User" FieldName="Description" Width="200px" />
                                </Columns>
                            </PropertiesComboBox>
                        </dx:GridViewDataComboBoxColumn>
                        <dx:GridViewDataColumn Caption="Password" FieldName="PASSWORD" Visible="false" Settings-AutoFilterCondition="Contains" Width="200px" />
                        <dx:GridViewDataComboBoxColumn Caption="NIK" Visible="false" FieldName="NIK" HeaderStyle-HorizontalAlign="left" Width="200px">
                            <PropertiesComboBox TextField="Name" ValueField="NIK" EnableSynchronization="False"
                                TextFormatString="{0}" IncrementalFilteringMode="Contains" DataSourceID="sql_karyawan">
                                <Columns>
                                    <dx:ListBoxColumn Caption="NIK" FieldName="NIK" Width="70px" />
                                    <dx:ListBoxColumn Caption="Name" FieldName="Name" Width="200px" />
                                    <dx:ListBoxColumn Caption="Email" FieldName="Email" Width="200px" />
                                </Columns>
                            </PropertiesComboBox>
                        </dx:GridViewDataComboBoxColumn>
                        <dx:GridViewDataComboBoxColumn Caption="Unit Kerja Permasalahan" FieldName="ORGANIZATION_NAME"
                            HeaderStyle-HorizontalAlign="left" Width="200px">
                            <PropertiesComboBox TextField="ORGANIZATION_NAME" ValueField="ORGANIZATION_NAME" EnableSynchronization="False"
                                TextFormatString="{1}" IncrementalFilteringMode="Contains" DataSourceID="sql_organization">
                                <Columns>
                                    <dx:ListBoxColumn Caption="ID" FieldName="ORGANIZATION_ID" Width="50px" />
                                    <dx:ListBoxColumn Caption="Department" FieldName="ORGANIZATION_NAME" Width="250px" />
                                </Columns>
                            </PropertiesComboBox>
                        </dx:GridViewDataComboBoxColumn>
                        <dx:GridViewDataComboBoxColumn Caption="Unit Kerja Agent"
                            FieldName="NamaGrup" HeaderStyle-HorizontalAlign="left" Width="200px">
                            <PropertiesComboBox TextField="NamaGroup" ValueField="NamaGrup" EnableSynchronization="False"
                                TextFormatString="{0}" IncrementalFilteringMode="Contains" DataSourceID="sql_unit_agent">
                                <Columns>
                                    <dx:ListBoxColumn Caption="Nama" FieldName="NamaGrup" Width="250px" />
                                </Columns>
                            </PropertiesComboBox>
                        </dx:GridViewDataComboBoxColumn>
                        <dx:GridViewDataColumn Caption="Email Address" FieldName="EMAIL_ADDRESS" HeaderStyle-HorizontalAlign="Center" Settings-AutoFilterCondition="Contains" Width="400px" />
                        <dx:GridViewDataColumn Caption="Inbound" FieldName="INBOUND" HeaderStyle-HorizontalAlign="Center" Settings-AutoFilterCondition="Contains" />
                        <dx:GridViewDataColumn Caption="Email" FieldName="EMAIL" HeaderStyle-HorizontalAlign="Center" Settings-AutoFilterCondition="Contains" />
                        <%--<dx:GridViewDataColumn Caption="Outbound" FieldName="OUTBOUND" HeaderStyle-HorizontalAlign="Center" Settings-AutoFilterCondition="Contains" />
                        <dx:GridViewDataColumn Caption="Chat" FieldName="CHAT" HeaderStyle-HorizontalAlign="Center" Settings-AutoFilterCondition="Contains" />
                        <dx:GridViewDataColumn Caption="Sosmed" FieldName="SOSMED" HeaderStyle-HorizontalAlign="Center" Settings-AutoFilterCondition="Contains" />
                        <dx:GridViewDataColumn Caption="Max Chat" FieldName="MAX_CHAT" HeaderStyle-HorizontalAlign="Center" Settings-AutoFilterCondition="Contains" />
                        <dx:GridViewDataColumn Caption="Max Email" FieldName="MAX_EMAIL" HeaderStyle-HorizontalAlign="Center" Settings-AutoFilterCondition="Contains" />
                        <dx:GridViewDataColumn Caption="Max Outbound" FieldName="MAX_OUTBOUND" HeaderStyle-HorizontalAlign="Center" Settings-AutoFilterCondition="Contains" />
                        <dx:GridViewDataComboBoxColumn Caption="Group Campaign" HeaderStyle-HorizontalAlign="Center" Settings-AutoFilterCondition="Contains"
                            FieldName="Namacampaign" Width="250px">
                            <PropertiesComboBox TextField="Namacampaign" ValueField="Namacampaign" EnableSynchronization="False"
                                TextFormatString="{0}" IncrementalFilteringMode="Contains" DataSourceID="sql_campaign">
                                <Columns>
                                    <dx:ListBoxColumn Caption="Group Campaign" FieldName="Namacampaign" Width="250px" />
                                </Columns>
                            </PropertiesComboBox>
                        </dx:GridViewDataComboBoxColumn>--%>
                        <dx:GridViewDataComboBoxColumn Caption="Status" FieldName="NA" Width="100px">
                            <PropertiesComboBox>
                                <Items>
                                    <dx:ListEditItem Text="Active" Value="Y" />
                                    <dx:ListEditItem Text="In Active" Value="N" />
                                </Items>
                            </PropertiesComboBox>
                        </dx:GridViewDataComboBoxColumn>
                        <%--<dx:GridViewDataColumn FieldName="SMS" HeaderStyle-HorizontalAlign="Center" Settings-AutoFilterCondition="Contains" />
                        <dx:GridViewDataColumn FieldName="FACEBOOK" HeaderStyle-HorizontalAlign="Center" Settings-AutoFilterCondition="Contains" />
                        <dx:GridViewDataColumn FieldName="TWITTER" HeaderStyle-HorizontalAlign="Center" Settings-AutoFilterCondition="Contains" />--%>
                    </Columns>
                    <Templates>
                        <DetailRow>
                            <dx:ASPxGridView ID="grid" Width="100%" runat="server" DataSourceID="sql_menu" KeyFieldName="MenuID" ClientInstanceName="grid"
                                SettingsBehavior-AllowFocusedRow="true" OnBeforePerformDataSelect="TicketNumber_DataSelect" Theme="Metropolis" Styles-Header-Font-Bold="true" Font-Size="X-Small">
                                <SettingsPager>
                                    <AllButton Text="All">
                                    </AllButton>
                                    <NextPageButton Text="Next &gt;">
                                    </NextPageButton>
                                    <PrevPageButton Text="&lt; Prev">
                                    </PrevPageButton>
                                </SettingsPager>
                                <SettingsPager PageSize="10" />
                                <SettingsEditing Mode="Inline" />
                                <Settings ShowGroupPanel="true" ShowHorizontalScrollBar="false" ShowFilterRow="true" />
                                <SettingsBehavior ConfirmDelete="true" />
                                <Columns>
                                    <dx:GridViewCommandColumn Caption="Action" ButtonType="Image" Width="150px" HeaderStyle-HorizontalAlign="Center">
                                        <NewButton Visible="true">
                                            <Image Url="~/Images/icon/Apps-text-editor-icon2.png"></Image>
                                        </NewButton>
                                        <DeleteButton Visible="true">
                                            <Image Url="~/Images/icon/Actions-edit-clear-icon2.png"></Image>
                                        </DeleteButton>
                                        <CancelButton>
                                            <Image ToolTip="Cancel" Url="~/Images/icon/cancel1.png">
                                            </Image>
                                        </CancelButton>
                                        <UpdateButton>
                                            <Image ToolTip="Update" Url="~/Images/icon/Updated1.png" />
                                        </UpdateButton>
                                    </dx:GridViewCommandColumn>
                                    <dx:GridViewDataTextColumn Caption="ID" FieldName="MenuID" Settings-AutoFilterCondition="Contains"
                                        CellStyle-HorizontalAlign="left" HeaderStyle-HorizontalAlign="left" Width="20%">
                                    </dx:GridViewDataTextColumn>
                                    <dx:GridViewDataComboBoxColumn Caption="Menu" FieldName="MenuName" HeaderStyle-HorizontalAlign="left" Width="80%">
                                        <PropertiesComboBox TextField="MenuName" ValueField="MenuID" EnableSynchronization="False"
                                            TextFormatString="{0}" IncrementalFilteringMode="Contains" DataSourceID="sql_user_satu">
                                            <Columns>
                                                <dx:ListBoxColumn Caption="ID" FieldName="MenuID" Width="50px" />
                                                <dx:ListBoxColumn Caption="Menu" FieldName="MenuName" Width="200px" />
                                            </Columns>
                                        </PropertiesComboBox>
                                    </dx:GridViewDataComboBoxColumn>
                                </Columns>
                                <Templates>
                                    <DetailRow>
                                        <dx:ASPxGridView ID="GridList" Width="100%" runat="server" DataSourceID="sql_sub_menu"
                                            KeyFieldName="SubMenuID" OnBeforePerformDataSelect="GridList_DataSelect" Theme="Metropolis" Styles-Header-Font-Bold="true" Font-Size="X-Small">
                                            <SettingsEditing Mode="Inline" />
                                            <Settings VerticalScrollBarStyle="Standard" ShowGroupPanel="true" ShowHorizontalScrollBar="false" ShowFilterRow="true" />
                                            <SettingsBehavior ConfirmDelete="true" />
                                            <Columns>
                                                <dx:GridViewCommandColumn Caption="Action" ButtonType="Image" Width="150px" HeaderStyle-HorizontalAlign="Center">
                                                    <NewButton Visible="true">
                                                        <Image Url="~/Images/icon/Apps-text-editor-icon2.png"></Image>
                                                    </NewButton>
                                                    <DeleteButton Visible="true">
                                                        <Image Url="~/Images/icon/Actions-edit-clear-icon2.png"></Image>
                                                    </DeleteButton>
                                                    <CancelButton>
                                                        <Image ToolTip="Cancel" Url="~/Images/icon/cancel1.png">
                                                        </Image>
                                                    </CancelButton>
                                                    <UpdateButton>
                                                        <Image ToolTip="Update" Url="~/Images/icon/Updated1.png" />
                                                    </UpdateButton>
                                                </dx:GridViewCommandColumn>
                                                <dx:GridViewDataTextColumn Caption="ID" FieldName="SubMenuID" Settings-AutoFilterCondition="Contains"
                                                    Width="50px" CellStyle-HorizontalAlign="left" HeaderStyle-HorizontalAlign="left">
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataComboBoxColumn Caption="Sub Menu" FieldName="SubMenuName" HeaderStyle-HorizontalAlign="left">
                                                    <PropertiesComboBox TextField="SubMenuName" ValueField="SubMenuID" EnableSynchronization="False"
                                                        TextFormatString="{0}" IncrementalFilteringMode="Contains" DataSourceID="sql_subMenu_dr">
                                                        <Columns>
                                                            <dx:ListBoxColumn Caption="ID" FieldName="SubMenuID" Width="50px" />
                                                            <dx:ListBoxColumn Caption="Sub Menu" FieldName="SubMenuName" Width="200px" />
                                                        </Columns>
                                                    </PropertiesComboBox>
                                                </dx:GridViewDataComboBoxColumn>
                                            </Columns>

                                            <Templates>
                                                <DetailRow>
                                                    <dx:ASPxGridView ID="gv_menu_tree" runat="server" Width="100%" DataSourceID="sql_menu_tree"
                                                        KeyFieldName="SubMenuIDTree" OnBeforePerformDataSelect="gv_menu_tree_DataSelect" Theme="SoftOrange" Styles-Header-Font-Bold="true" Font-Size="X-Small">
                                                        <SettingsEditing Mode="Inline" />
                                                        <SettingsBehavior ConfirmDelete="true" />
                                                        <Settings VerticalScrollBarStyle="Standard" ShowGroupPanel="true" ShowHorizontalScrollBar="false" ShowFilterRow="true" />
                                                        <Columns>
                                                            <dx:GridViewCommandColumn Caption="Action" ButtonType="Image" Width="150px" HeaderStyle-HorizontalAlign="Center">
                                                                <NewButton Visible="true">
                                                                    <Image Url="~/Images/icon/Apps-text-editor-icon2.png"></Image>
                                                                </NewButton>
                                                                <DeleteButton Visible="true">
                                                                    <Image Url="~/Images/icon/Actions-edit-clear-icon2.png"></Image>
                                                                </DeleteButton>
                                                                <CancelButton>
                                                                    <Image ToolTip="Cancel" Url="~/Images/icon/cancel1.png">
                                                                    </Image>
                                                                </CancelButton>
                                                                <UpdateButton>
                                                                    <Image ToolTip="Update" Url="~/Images/icon/Updated1.png" />
                                                                </UpdateButton>
                                                            </dx:GridViewCommandColumn>
                                                            <dx:GridViewDataTextColumn Caption="ID" FieldName="SubMenuIDTree" Settings-AutoFilterCondition="Contains"
                                                                Width="50px" CellStyle-HorizontalAlign="left" HeaderStyle-HorizontalAlign="left">
                                                            </dx:GridViewDataTextColumn>
                                                            <dx:GridViewDataComboBoxColumn Caption="Menu Tree" FieldName="MenuTreeName" HeaderStyle-HorizontalAlign="left">
                                                                <PropertiesComboBox TextField="MenuTreeName" ValueField="SubMenuIDTree" EnableSynchronization="False"
                                                                    TextFormatString="{0}" IncrementalFilteringMode="Contains" DataSourceID="sql_User3">
                                                                    <Columns>
                                                                        <dx:ListBoxColumn Caption="ID" FieldName="SubMenuIDTree" Width="50px" />
                                                                        <dx:ListBoxColumn Caption="Menu Tree" FieldName="MenuTreeName" Width="200px" />
                                                                    </Columns>
                                                                </PropertiesComboBox>
                                                            </dx:GridViewDataComboBoxColumn>
                                                        </Columns>
                                                    </dx:ASPxGridView>
                                                </DetailRow>
                                            </Templates>
                                            <SettingsDetail ShowDetailRow="true" />
                                            <Settings ShowFooter="True" />
                                        </dx:ASPxGridView>
                                    </DetailRow>
                                </Templates>
                                <SettingsDetail ShowDetailRow="true" />
                                <Settings ShowFooter="True" />
                            </dx:ASPxGridView>
                        </DetailRow>
                    </Templates>
                    <SettingsDetail ShowDetailRow="true" />
                    <Settings ShowFooter="True" />
                </dx:ASPxGridView>
                <asp:SqlDataSource ID="sql_campaign" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>" SelectCommand="select * from btn_campaign"></asp:SqlDataSource>
                <asp:SqlDataSource ID="sql_menu" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>"></asp:SqlDataSource>
                <asp:SqlDataSource ID="sql_sub_menu" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>"></asp:SqlDataSource>
                <asp:SqlDataSource ID="sql_user" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>"></asp:SqlDataSource>
                <asp:SqlDataSource ID="sql_user_satu" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>"
                    SelectCommand="SELECT * FROM User1"></asp:SqlDataSource>
                <asp:SqlDataSource ID="sql_subMenu_dr" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>"></asp:SqlDataSource>
                <asp:SqlDataSource ID="sql_menu_tree" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>"></asp:SqlDataSource>
                <asp:SqlDataSource ID="sql_User3" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>"></asp:SqlDataSource>
                <asp:SqlDataSource ID="sql_level_user" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>"></asp:SqlDataSource>
                <asp:SqlDataSource ID="sql_organization" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>"></asp:SqlDataSource>
                <asp:SqlDataSource ID="sql_karyawan" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>"></asp:SqlDataSource>
                <asp:SqlDataSource ID="sql_unit_agent" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>" SelectCommand="SELECT * FROM Dept"></asp:SqlDataSource>
            </div>
        </div>

    </div>
</asp:Content>
