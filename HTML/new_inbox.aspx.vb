﻿Imports System.Data.SqlClient
Imports DevExpress.Web.ASPxGridView

Public Class new_inbox
    Inherits System.Web.UI.Page

    Dim sqlDr, sqlDr1, sqlDtr, sqlDra, readAccount, datainbox As SqlDataReader
    Dim Proses As New ClsConn
    Dim Execute As New ClsConn
    Dim strExecute As New ClsConn
    Dim SetTicketNumber As String
    Dim sql, sqlEnhanceBTPN As String
    Dim con, sqlcon, sqlConnect, conInbox As New SqlConnection(ConfigurationManager.ConnectionStrings("DefaultConnection").ConnectionString)
    Dim com, com1, sqlcom, sqlcomTo, sqlComInbox As New SqlCommand
    Dim str, AppTicket As String
    Dim strSql As String
    Dim strLogTime As String = DateTime.Now.ToString("yyyy")
    Public Property TicketNumber As Object

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'kotakStatus.Text = TampilDinamisKotakStatus(Session("UserName"), Session("LoginTypeAngka"), Session("_LoginState"))  
        If Not Page.IsPostBack Then
            kotakStatus.Text = TampilDinamisKotakStatusRFC(Session("UserName"), Session("LoginTypeAngka"), Session("_LoginState"))
            Session("datausername") = Session("username")
            UpdateAlert()
            StatusClick()
            hd_username.Value = Session("username")
            TrxLoginTypeAngka.Value = Session("LoginTypeAngka")
            'dataSourceGridList(Request.QueryString("status"), Session("LoginTypeAngka"), Session("organization"), Session("UserName"))
            TrxUserName.Value = Session("username")
            ds_query.SelectParameters("username").DefaultValue = Session("username")
            ds_query.InsertParameters("username").DefaultValue = Session("username")
            ds_query.UpdateParameters("username").DefaultValue = Session("username")
        End If

    End Sub
    Private Sub dataSourceGridList(ByVal statusData As String, ByVal LoginTypeAngka As String, ByVal Divisi As String, ByVal UserName As String)
        dsTodolist.SelectCommand = "exec NEW_Sp_Open '" & UserName & "','" & statusData & "','" & LoginTypeAngka & "','" & Divisi & "'"
    End Sub
    Private Sub StatusClick()
        Dim ltrStatusnya As String = "<span class='badge badge-info' style='margin-bottom: 5px;background-color:#EE4D2D;'><a href='new_inbox.aspx?idpage=1012&status=open' style='color:white;'>Data Ticket " & Request.QueryString("status") & "</a></span>"
        ltrStatus.Text = ltrStatusnya
    End Sub
    Private Sub UpdateAlert()
        Dim updateActivity As String = "update user1 set Activity='N'"
        Try
            sqlcom = New SqlCommand(updateActivity, con)
            con.Open()
            sqlcom.ExecuteNonQuery()
            con.Close()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
        Dim IdupdateActivity As String = "update user1 set Activity='Y' where MenuID='" & Request.QueryString("idpage") & "'"
        Try
            sqlcom = New SqlCommand(IdupdateActivity, con)
            con.Open()
            sqlcom.ExecuteNonQuery()
            con.Close()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Private Sub ReadonlyCustomer()
        CmbCountry.ReadOnly = True
        ASPxComboBox1.ReadOnly = True
        CmbCity.ReadOnly = True
        ASPxDescription.ReadOnly = True
        txt_channel.ReadOnly = True
        txt_sla.ReadOnly = True
    End Sub   
    Function TampilDinamisKotakStatus(ByVal Username As String, ByVal LoginTypeAngka As String, ByVal LoginState As String)
        Dim strStatusData As String = String.Empty
        Dim queryStatusData As String = String.Empty
        Dim action As String = String.Empty

        queryStatusData = "exec SP_TempKotakTodolist '" & Username & "','" & LoginTypeAngka & "','" & LoginState & "'"
        sqlcomTo = New SqlCommand(queryStatusData, sqlConnect)
        sqlcomTo.CommandTimeout = 480
        Try
            sqlConnect.Open()
            sqlDtr = sqlcomTo.ExecuteReader
            If sqlDtr.HasRows Then
                While sqlDtr.Read
                    If (sqlDtr("StatusData") = "Closed" Or sqlDtr("StatusData") = "Solved" Or sqlDtr("NA") = "Active") Then
                        action = "Detail"
                    Else
                        action = "Edit"
                    End If
                    If sqlDtr("JumlahData") = 0 Then
                        strStatusData &= "<a href='new_inbox.aspx?status=" & sqlDtr("StatusData") & "&action=" & action & "' data-toggle='modal' id='modal_ticket_open' runat='server'> " & _
                                            "<div class='" & Session("_statusColumn") & "'> " & _
                                                "<div class='panel panel-default panel-stat2 bg-info'> " & _
                                                    "<div class='panel-body'> " & _
                                                        "<span class='stat-icon'> " & _
                                                            "<i class='fa fa-warning'></i> " & _
                                                        "</span> " & _
                                                        "<div class='pull-right text-right'> " & _
                                                            "<div class='value'> " & _
                                                                "0" & _
                                                            "</div> " & _
                                                            "<div class='title'> " & _
                                                                sqlDtr("StatusData") & _
                                                            "</div> " & _
                                                        "</div> " & _
                                                    "</div> " & _
                                                "</div> " & _
                                            "</div> " & _
                                        "</a>"
                    Else
                        strStatusData &= "<a href='new_inbox.aspx?status=" & sqlDtr("StatusData") & "&action=" & action & "' data-toggle='modal' id='modal_ticket_open' runat='server'> " & _
                                           "<div class='" & Session("_statusColumn") & "'> " & _
                                               "<div class='panel panel-default panel-stat2 bg-success'> " & _
                                                   "<div class='panel-body'> " & _
                                                       "<span class='stat-icon'> " & _
                                                           "<i class='fa fa-folder-open-o'></i> " & _
                                                       "</span> " & _
                                                       "<div class='pull-right text-right'> " & _
                                                           "<div class='value'> " & _
                                                               sqlDtr("JumlahData") & _
                                                           "</div> " & _
                                                           "<div class='title'> " & _
                                                               sqlDtr("StatusData") & _
                                                           "</div> " & _
                                                       "</div> " & _
                                                   "</div> " & _
                                               "</div> " & _
                                           "</div> " & _
                                       "</a>"
                    End If
                End While
            Else
            End If
            sqlDtr.Close()
            sqlConnect.Close()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try

        'Response.Write(queryStatusDataTrx)
        Return strStatusData
    End Function
    Function EscalationTransaction(ByVal TicketNumber As String)
        Dim statusTicket As String = String.Empty
        Dim dataPosition As String = String.Empty
        Dim labelDispatch As String = String.Empty
        Dim LayerCreateBy As String = String.Empty
        Try
            sqlcon.Open()
            Dim strdata As String = "select * from tTicket where TicketNumber='" & TicketNumber & "'"
            com = New SqlCommand(strdata, sqlcon)
            sqlDr = com.ExecuteReader()
            If sqlDr.HasRows Then
                sqlDr.Read()
                statusTicket = sqlDr("Status").ToString
                dataPosition = sqlDr("TicketPosition").ToString
                LayerCreateBy = sqlDr("LayerCreateBy").ToString
            End If
            sqlDr.Close()
            sqlcon.Close()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try

        Dim TrxLayer As String = String.Empty
        If Session("LoginTypeAngka") = "2" Then
            If LayerCreateBy = Session("LoginTypeAngka") Then
                TrxLayer = "7"
            Else
                TrxLayer = Session("LoginTypeAngka")
            End If
        Else
            TrxLayer = Session("LoginTypeAngka")
        End If

        Dim str As String = String.Empty
        Dim strTemp As String = String.Empty
        ltrEscalationLayer.Text &= ""
        str = "exec Temp_SP_Workflow '" & TrxLayer & "'"
        Try
            sqlDr = Proses.ExecuteReader(str)
            While sqlDr.Read()
                strTemp &= " <option value='" & sqlDr("LayerUser").ToString & "' >Layer " & sqlDr("LayerUser").ToString & "</option>"
            End While
            sqlDr.Close()
            ltrEscalationLayer.Text = strTemp
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try

        If Session("LoginTypeAngka") = dataPosition Then
            If (statusTicket = "Closed" Or statusTicket = "Solved") Then
                divEscalation.Visible = False
                divNotifikasi.Visible = False
                divNotifikasiClosed.Visible = True
            Else
                divNotifikasi.Visible = False
                divNotifikasiClosed.Visible = False
                divEscalation.Visible = True
            End If
        Else
            If (statusTicket = "Closed" Or statusTicket = "Solved") Then
                divEscalation.Visible = False
                divNotifikasi.Visible = False
                divNotifikasiClosed.Visible = True
            Else
                divEscalation.Visible = False
                divNotifikasiClosed.Visible = False
                divNotifikasi.Visible = True
            End If
        End If
        LblTicketNumber.Text = TicketNumber
        LblTicketNumberClosed.Text = TicketNumber
    End Function
    Private Sub ASPxGridView4_Load(sender As Object, e As EventArgs) Handles ASPxGridView4.Load
        Dim strCommands As String = "exec SP_Temp_Interaction '" & hd_ticketid.Value & "'"
        dsInteraction.SelectCommand = strCommands
    End Sub
    Private Sub ASPxCallbackPanel4_Callback(sender As Object, e As DevExpress.Web.ASPxClasses.CallbackEventArgsBase) Handles ASPxCallbackPanel4.Callback
        Dim strCommands As String = "exec SP_Temp_Interaction '" & hd_ticketid.Value & "'"
        dsInteraction.SelectCommand = strCommands
    End Sub
    Private Sub ASPxCallbackPanel10_Callback(sender As Object, e As DevExpress.Web.ASPxClasses.CallbackEventArgsBase) Handles ASPxCallbackPanel10.Callback
        EscalationTransaction(hd_ticketid.Value)
    End Sub
    Private Sub ASPxCallbackPanel12_Callback(sender As Object, e As DevExpress.Web.ASPxClasses.CallbackEventArgsBase) Handles ASPxCallbackPanel12.Callback
        ReadonlyCustomer()
        sql_cmb_status.SelectCommand = "select * from mStatus"
        sql = "select * from tTicket where ID='" & hd_id.Value & "'"
        Try
            sqlDr = Execute.ExecuteReader(sql)
            If sqlDr.HasRows Then
                sqlDr.Read()
                SetTicketNumber = sqlDr("TicketNumber").ToString
                CmbCountry.Value = sqlDr("CategoryID").ToString
                ASPxComboBox1.Text = sqlDr("SubCategory2Name").ToString
                CmbCity.Text = sqlDr("SubCategory3Name").ToString
                ASPxDescription.Text = sqlDr("TicketNumber").ToString
                cmb_status.Text = sqlDr("Status").ToString
                txt_channel.Text = sqlDr("channelid").ToString
                txt_sla.Text = sqlDr("SLA").ToString
                DateofTransaction.Value = sqlDr("DateTransaction").ToString
                'DateofTransaction.Value = String.Format("{0:yyyy-MM-dd }", sqlDr("DateTransaction"))
                cbClaimStatus.Value = sqlDr("ClaimStatus").ToString
                cbBankName.Value = sqlDr("BankName").ToString
            End If
            sqlDr.Close()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
        sqlEnhanceBTPN = "select * from BTN_trxTticket where TicketNumber='" & SetTicketNumber & "'"
        Try
            sqlDr = Execute.ExecuteReader(sqlEnhanceBTPN)
            If sqlDr.HasRows Then
                sqlDr.Read()
                BTN_datePengaduan.Value = sqlDr("TglKejadian").ToString
                BTN_cmbPenyebab.Value = sqlDr("IdPenyebab").ToString
                TxtPenerimaPengaduan.Text = sqlDr("StrPenerima").ToString
                BTN_cmbStatusPelapor.Text = sqlDr("StrStatusPelapor").ToString

            End If
            sqlDr.Close()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Private Sub dsTodolist_Selecting(sender As Object, e As SqlDataSourceSelectingEventArgs) Handles dsTodolist.Selecting
        e.Command.CommandTimeout = 480
    End Sub
    Private Sub dsInteraction_Selecting(sender As Object, e As SqlDataSourceSelectingEventArgs) Handles dsInteraction.Selecting
        e.Command.CommandTimeout = 480
    End Sub
    Function TampilDinamisKotakStatusRFC(ByVal Username As String, ByVal LoginTypeAngka As String, ByVal LoginState As String)
        Dim strStatusData As String = String.Empty
        Dim icon As String
        Dim queryStatusData As String = "exec SP_TempKotakTodolist '" & Username & "','" & LoginTypeAngka & "','" & LoginState & "'"
        sqlcomTo = New SqlCommand(queryStatusData, sqlConnect)
        sqlcomTo.CommandTimeout = 480

        Dim queryInbox As String = "select count(*) as Inbox from icc_email_in where agent='" & Username & "' and CNT='2' and TicketNumber='' AND JENIS_EMAIL is null"
        sqlComInbox = New SqlCommand(queryInbox, conInbox)

        Try
            conInbox.Open()
            datainbox = sqlComInbox.ExecuteReader
            If datainbox.HasRows And LoginTypeAngka = "1" Then
                While datainbox.Read
                    If datainbox("Inbox") = 0 Then
                        strStatusData &= "<a href='new_inbox.aspx?idpage=1012&status=Inbox'> " &
                                        "<div class='" & Session("_statusColumn") & "'>" &
                                        "<div class='panel-stat3 bg-info'>" &
                                        "<h1 class='m-top-none' id='userCount'>0</h1>" &
                                        "<h5>Inbox Email</h5> " &
                                        "<i class='fa fa-arrow-circle-o-up fa-lg'></i><span class='m-left-xs'>Click to detail transaction</span>" &
                                        "<div class='stat-icon'>" &
                                            "<i class='fa fa-envelop'></i>" &
                                        "</div>" &
                                        "<div class='loading-overlay'>" &
                                            "<i class='loading-icon fa fa-refresh fa-spin fa-lg'></i>" &
                                        "</div>" &
                                        "</div>" &
                                    "</div>" &
                                "</a>"
                    Else
                        strStatusData &= "<a href='new_inbox.aspx?idpage=1012&status=Inbox'> " &
                                    "<div class='" & Session("_statusColumn") & "'>" &
                                        "<div class='panel-stat3 bg-success'>" &
                                        "<h1 class='m-top-none' id='userCount'> " & datainbox("Inbox") & "</h1>" &
                                        "<h5>Inbox Email</h5> " &
                                        "<i class='fa fa-arrow-circle-o-up fa-lg'></i><span class='m-left-xs'>Click to detail transaction</span>" &
                                        "<div class='stat-icon'>" &
                                            "<i class='fa fa-envelop'></i>" &
                                        "</div>" &
                                        "<div class='loading-overlay'>" &
                                            "<i class='loading-icon fa fa-refresh fa-spin fa-lg'></i>" &
                                        "</div>" &
                                        "</div>" &
                                    "</div>" &
                                "</a>"
                    End If
                End While
            End If

            sqlConnect.Open()
            sqlDtr = sqlcomTo.ExecuteReader
            If sqlDtr.HasRows Then
                While sqlDtr.Read
                    Dim action As String

                    If (sqlDtr("StatusData") = "Closed" Or sqlDtr("StatusData") = "Solved" Or sqlDtr("NA") = "Active") Then
                        action = "Detail"
                        icon = "fa fa-thumbs-up fa-3x"
                    Else
                        If sqlDtr("StatusData") = "Open" Then
                            icon = "fa fa-folder-open-o fa-3x"
                        Else
                            icon = "fa fa-warning fa-3x"
                        End If
                        action = "Edit"
                    End If
                    If sqlDtr("JumlahData") = 0 Then
                        strStatusData &= "<a href='new_inbox.aspx?status=" & sqlDtr("StatusData") & "&action=" & action & "' data-toggle='modal' id='modal_ticket_open' runat='server'> " &
                                             "<div class='" & Session("_statusColumn") & "'>" &
                                              "<div class='panel-stat3 bg-info'>" &
                                                "<h1 class='m-top-none' id='userCount'>0</h1>" &
                                                "<h5>" & sqlDtr("StatusData") & "</h5> " &
                                                "<i class='fa fa-arrow-circle-o-up fa-lg'></i><span class='m-left-xs'>Click to detail transaction</span>" &
                                                "<div class='stat-icon'>" &
                                                    "<i class='" & icon & "'></i>" &
                                                "</div>" &
                                                "<div class='loading-overlay'>" &
                                                    "<i class='loading-icon fa fa-refresh fa-spin fa-lg'></i>" &
                                                "</div>" &
                                              "</div>" &
                                            "</div>" &
                                        "</a>"
                    Else
                        strStatusData &= "<a href='new_inbox.aspx?status=" & sqlDtr("StatusData") & "&action=" & action & "' data-toggle='modal' id='modal_ticket_open' runat='server'> " &
                                            "<div class='" & Session("_statusColumn") & "'>" &
                                             "<div class='panel-stat3 bg-success'>" &
                                               "<h1 class='m-top-none' id='userCount'> " & sqlDtr("JumlahData") & "</h1>" &
                                               "<h5>" & sqlDtr("StatusData") & "</h5> " &
                                               "<i class='fa fa-arrow-circle-o-up fa-lg'></i><span class='m-left-xs'>Click to detail transaction</span>" &
                                               "<div class='stat-icon'>" &
                                                   "<i class='" & icon & "'></i>" &
                                               "</div>" &
                                               "<div class='loading-overlay'>" &
                                                   "<i class='loading-icon fa fa-refresh fa-spin fa-lg'></i>" &
                                               "</div>" &
                                             "</div>" &
                                           "</div>" &
                                       "</a>"
                    End If
                End While
            Else
            End If
            sqlDtr.Close()
            sqlConnect.Close()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try

        'Response.Write(queryStatusDataTrx)
        Return strStatusData
    End Function
    Private Sub ASPxGridView1_Load(sender As Object, e As EventArgs) Handles ASPxGridView1.Load
        Try
            Dim strSql As New hiStoryTransactionDBMLDataContext
            strSql.CommandTimeout = 480
            ASPxGridView1.DataSource = strSql.SP_Temp_hiStoryTransaction(Session("username"), "ParentChild").ToList()
            ASPxGridView1.DataBind()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Protected Sub ASPxGridView3_CustomColumnGroup(sender As Object, e As CustomColumnSortEventArgs)
        CompareColumnValues(e)
    End Sub
    Private Sub CompareColumnValues(ByVal e As DevExpress.Web.ASPxGridView.CustomColumnSortEventArgs)
        If e.Column.FieldName = "ParentNumberID" Then
            e.Handled = True
        End If
    End Sub
    Protected Sub ASPxGridView3_CustomGroupDisplayText(sender As Object, e As ASPxGridViewColumnDisplayTextEventArgs)
        If e.Column.FieldName = "ParentNumberID" Then
            'e.DisplayText = "Data : "
        End If
    End Sub
    Protected Sub ASPxGridView3_CustomColumnSort(sender As Object, e As CustomColumnSortEventArgs)
        CompareColumnValues(e)
    End Sub
    Private Sub ASPxGridView3_Load(sender As Object, e As EventArgs) Handles ASPxGridView3.Load
        CType(ASPxGridView3.Columns("ParentNumberID"), GridViewDataTextColumn).GroupBy()
        Try
            Dim strSql As New ParentNumberDBMLDataContext
            strSql.CommandTimeout = 480
            ASPxGridView3.DataSource = strSql.SP_Temp_ParentChildTicketDetail(hd_ticketid.Value).ToList()
            ASPxGridView3.DataBind()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Private Sub GridView_RowInserting(sender As Object, e As DevExpress.Web.Data.ASPxDataInsertingEventArgs) Handles GridView.RowInserting
        Dim _strScript As String = String.Empty
        strSql = "select * from Temp_SettingHiStoryTransaction WHERE CreatedBy='" & Session("username") & "' and Type='ParentChild'"
        sqlDr = strExecute.ExecuteReader(strSql)
        If sqlDr.HasRows() Then
            sqlDr.Read()
            e.Cancel = True
            Throw New Exception("For user " & Session("username") & " to setting query data transaksi already exists, please update data existing")
        Else
            e.Cancel = False
            _strScript = "insert into Temp_SettingHiStoryTransaction([Day], [FilterDate], [CreatedBy], [Type]) VALUES (@Day, @FilterDate, '" & Session("username") & "', 'ParentChild')"
            strExecute.LogSuccess(strLogTime, _strScript)
            ds_query.InsertCommand = _strScript
        End If
        sqlDr.Close()
    End Sub
    Private Sub ASPxGridView2_Init(sender As Object, e As EventArgs) Handles ASPxGridView2.Init
        Dim getIDPage As String = Request.QueryString("idpage")
        Dim strPage As String = "1012"
        'Response.Write(getIDPage)

        If strPage <> getIDPage Then
            ASPxGridView2.Visible = True
            GridView_InboxEmail.Visible = False
            dataSourceGridList(Request.QueryString("status"), Session("LoginTypeAngka"), Session("organization"), Session("UserName"))
        Else
            ASPxGridView2.Visible = False

            If Session("LoginTypeAngka") = "1" Then
                GridView_InboxEmail.Visible = True
            Else
                GridView_InboxEmail.Visible = False
            End If
        End If

    End Sub

End Class