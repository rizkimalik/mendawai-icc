function gup( name, url ) {
    if (!url) url = location.href;
    name = name.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
    var regexS = "[\\?&]"+name+"=([^&#]*)";
    var regex = new RegExp( regexS );
    var results = regex.exec( url );
    return results == null ? null : results[1];
}
const UserName = gup('id', location.search);
const UserLevel = gup('lvlUser', location.search);
const UserOrg = gup('org', location.search);
const LayerID = gup('layerID', location.search);
// console.log(window.location.hostname);
const url = '../WebServiceTransaction.asmx/New_DashboardSection';

//CHART AGENT CREATE	
const DashLine = Highcharts.chart('chart_agent_create', {
	chart: {
		type: 'spline',
		zoomType: 'x'
	},
	title: {
		text: ''
	},
	subtitle: {
		text: ''
	},
	xAxis: {
		title: {
			text: ''
		},
		type: 'datetime',
		labels: {
			overflow: 'justify'
		}
	},
	yAxis: {
		title: {
			text: ''
		},
	},
	legend: {
		enabled: true,
		layout: 'vertical',
		align: 'right',
		verticalAlign: 'top',
		x: -10,
		y: 10,
		floating: true,
		borderWidth: 0,
		backgroundColor: 'transparent',
		shadow: false
	},
	tooltip: {
		headerFormat: '<b>{series.name}</b><br/>',
		pointFormat: '{point.name} : {point.y}',
		useHTML: true,
		crosshairs: false,
	},
	plotOptions: {
		spline: {
			marker: {
				enable: false
			}
		},
	},
	credits:{
		enabled : false
	},
	series: []
	// series: [{
		// name: '',
		// data: []
	// }]
	/* series: [{
        name: 'John',
        data: [5, 3, 4, 7, 2]
    }, {
        name: 'Jane',
        data: [2, 2, 3, 2, 1]
    }, {
        name: 'Joe',
        data: [3, 4, 4, 2, 5]
    }] */
	
});

//CHART SUMMARY CREATE
const DashBar = Highcharts.chart('chart_summary_create', {
	chart: {
		type: 'column'
	},
	title: {
		text: ''
	},
	xAxis: {
		type: 'category',
		labels: {
            style: {
                fontSize: '9px',
            }
        }
	},
	yAxis: {
		min: 0,
		title: {
			text: ''
		}
	},
	tooltip: {
		headerFormat: '<center><b>{series.name}</b></center>',
		pointFormat: 'Total : {point.y}',
		useHTML: true,
		crosshairs: false,
	
	},
	plotOptions: {
		column: {
            dataLabels: {
                enabled: false
            },
        },
		series: {
            stacking: 'normal'
        }
	},
	legend: {
		enabled: true,
		layout: 'vertical',
		align: 'right',
		verticalAlign: 'top',
		x: -10,
		y: 10,
		floating: true,
		borderWidth: 0,
		backgroundColor: 'transparent',
		shadow: false
	},
	credits:{
		enabled : false
	},
	series: []
	// series: [{
		// name: '',
		// data: []
	// }] 
});


async function DashTotalTicket(){
	
	const config = {
		method: 'POST',
		headers: {
			"Content-Type" : "application/json"
		},
		body: JSON.stringify({
			Data1:"DashTotalTicket",
			Data2:"Administrator",
			Data3:UserName,
			Data4:UserOrg,
			Data5:"ADMIN",
			Data6:LayerID
		})
	}
	
	try {
		const res = await fetch(url, config);
		const obj = await res.json();
		const data = JSON.parse(obj.d);
		// console.log(obj);

		if(res.ok){
			let html = "";
			let icon = "";
			let background = "";
			for (i = 0; i < data.length; i++) {

				if(data[i].StatusData == "Open"){
					icon = 'fa-folder-open-o';
					background = 'bg-success';
				}
				else if(data[i].StatusData == "Progress"){
					icon = 'fa-spinner';
					background = 'bg-info';
				}
				else if(data[i].StatusData == "Pending"){
					icon = 'fa-warning';
					background = 'bg-warning';
				}
				else if(data[i].StatusData == "Solved"){
					icon = 'fa-check';
					background = 'bg-primary';
				} 
				else if(data[i].StatusData == "Closed"){
					icon = 'fa-thumbs-up';
					background = 'bg-danger';
				} 

				html += `<div class="col-md-3">
					<div class="panel panel-default panel-stat2 ${background}">
						<div class="panel-body">
							<span class="stat-icon">
								<i class="fa ${icon}"></i>
							</span>
							<div class="pull-right text-right">
								<div class="value">${data[i].JumlahData}</div>
								<div class="title">Ticket ${data[i].StatusData}</div>
							</div>
						</div>
					</div>
				</div>`;
				
			}
			$("#TotalTicket").html(html);
		}
	} 
	catch (error) {
		console.log(error);	
	}

}
DashTotalTicket();

async function DashLineTicket(){		
	if (DashLine) {
		const x_axis1 = DashLine.xAxis[0];
		const config = {
			method: 'POST',
			headers: {
				"Content-Type" : "application/json"
			},
			body: JSON.stringify({
				Data1:"DashLineTicket",
				Data2:"Administrator",
				Data3:UserName,
				Data4:UserOrg,
				Data5:"ADMIN",
				Data6:LayerID
			})
		}

		try {
			const res = await fetch(url,config);
			const obj = await res.json();
			const data = JSON.parse(obj.d);
			const category = (Object.keys(data[0])).slice(2);
			console.log(data);

			if (res.ok) {
				let JamNya = [],
				Complaint = [],
				Feedback = [],
				Information = [],
				Request = [],
				SecurityAndOthers = [];
				
				let total_agent = 0;
				let colors = ["#00a0dc","#8d6cab","#dd5143","#e68523","#57bfc1","#edb220","#dc4b89","#69a62a","#046293","#66418c"];
				
				for(var i = DashLine.series.length -1; i > -1; i--) {
					DashLine.series[i].remove();
				}
				
				for(let i=0; i < data.length; i++){
					JamNya.push(data[i].JamNya);
					Complaint.push(data[i].Complaint);
					Feedback.push(data[i].Feedback);
					Information.push(data[i].Information);
					Request.push(data[i].Request);
					// SecurityAndOthers.push(data[i].SecurityAndOthers);

					total_agent += (parseInt(data[i].Complaint) + parseInt(data[i].Feedback) + parseInt(data[i].Information) + parseInt(data[i].Request));
				}

				x_axis1.setCategories(JamNya);
				for (let i = 0; i < category.length; i++) {
					let total = "";
					if (category[i] == "Complaint") {
						total = Complaint;
					}
					else if (category[i] == "Feedback") {
						total = Feedback;
					}
					else if (category[i] == "Information") {
						total = Information;
					}
					else if (category[i] == "Request") {
						total = Request;
					}

					DashLine.addSeries({
						name: category[i],
						color : colors[i],
						data: total
					});
				}
				
				DashLine.redraw();
				$("#total_agent").html(total_agent);
			}
		} 
		catch (error) {
			console.log(error);	
		}
	}
}
DashLineTicket();

async function DashBarTicket(){
	const config = {
		method: 'POST',
		headers: {
			"Content-Type" : "application/json"
		},
		body: JSON.stringify({
			Data1:"DashBarTicket",
			Data2:"Administrator",
			Data3:UserName,
			Data4:UserOrg,
			Data5:"ADMIN",
			Data6:LayerID
		})
	}
	
	try {
		const res = await fetch(url, config);
		const obj = await res.json();
		const data = JSON.parse(obj.d);
		// console.log(obj);

		if(res.ok){
			var total_summary = 0;
			for(var i = DashBar.series.length -1; i > -1; i--) {
				DashBar.series[i].remove();
			}
			
			for(var i=0; i < data.length; i++){
				var Ticket = data[i].Ticket;
				var Jumlah = parseInt( data[i].Jumlah );
				var colors = ["#00a0dc","#8d6cab","#dd5143","#e68523","#57bfc1","#edb220","#dc4b89","#69a62a","#046293","#66418c"];
				total_summary+=Jumlah;
				
				DashBar.addSeries({
					name: Ticket,
					data: [{
						name: Ticket, 
						y: Jumlah
					}],
					color: colors[i]
				});	
			}
			DashBar.redraw();	
			$("#total_summary").html(total_summary);
			
		} 
	} 
	catch (error) {
		console.log(error);	
	}

}
DashBarTicket();

async function DashTableTicket(){
	const config = {
		method: 'POST',
		headers: {
			"Content-Type" : "application/json"
		},
		body: JSON.stringify({
			Data1:"DashTableTicket",
			Data2:"Administrator",
			Data3:UserName,
			Data4:UserOrg,
			Data5:"ADMIN",
			Data6:LayerID
		})
	}
	
	try {
		const res = await fetch(url, config);
		const obj = await res.json();
		const data = JSON.parse(obj.d);
		// console.log(data);

		if(res.ok){
			$('#table_agent_create').DataTable( {
				"data": data,
				"destroy": true,
				"width": "100%",
				"columns" : [
					{ "data" : "TicketNumber" },
					{ "data" : "CategoryName" },
					{ "data" : "SubCategory1Name" },
					{ "data" : "SubCategory2Name" },
					{ "data" : "SubCategory3Name" },
					{ "data" : "SLA" },
					{ "data" : "Status" },
					{ "data" : "UserCreate" },
					{ "data" : "DateCreates" },
				]
			});
		}
	} 
	catch (error) {
		console.log(error);	
	}

}
DashTableTicket();

// INTERVAL LOAD DATA CHART
$('#btn-refresh').click(function () {
	DashTotalTicket();	
	DashLineTicket();
	DashBarTicket();
	DashTableTicket();
});
/* setInterval(function () {
	DashTotalTicket();	
	DashLineTicket();
	DashBarTicket();
	DashTableTicket();
}, 5000); //End Interval */



