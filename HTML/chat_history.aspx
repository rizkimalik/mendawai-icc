﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/HTML/Ticket.Master" CodeBehind="chat_history.aspx.vb" Inherits="ICC.chat_history" %>

<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxCallbackPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxSpellChecker.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxSpellChecker" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="FeaturedContent" runat="server">
    <script>
        function showConversationChat() {
            popupConversationChat.Show();
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <h4 class="headline">Data Chat History
        <span class="line bg-danger" style="margin-top: 15px;"></span>
    </h4>
    <dx:ASPxGridView ID="ASPxGridView_ChatHistory" ClientInstanceName="ASPxGridView_ChatHistory" Width="100%" runat="server" DataSourceID="sql_chat_history" KeyFieldName="ID"
        SettingsPager-PageSize="10" Theme="MetropolisBlue">
        <SettingsPager>
            <AllButton Text="All">
            </AllButton>
            <NextPageButton Text="Next &gt;">
            </NextPageButton>
            <PrevPageButton Text="&lt; Prev">
            </PrevPageButton>
            <PageSizeItemSettings Visible="true" Items="10, 15, 20" ShowAllItem="true" />
        </SettingsPager>
        <SettingsEditing Mode="Inline" />
        <Settings ShowFilterRow="true" ShowGroupPanel="true" ShowHorizontalScrollBar="true" />
        <SettingsBehavior ConfirmDelete="true" />
        <Columns>
            <dx:GridViewDataTextColumn Caption="Action" VisibleIndex="0" CellStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" Width="10%">
                <DataItemTemplate>
                    <a href="?chatid=<%# Eval("chat_id")%>&customerid=<%# Eval("customer_id")%>">
                        <asp:Image ImageUrl="img/icon/clone.png" ID="Image2" runat="server" ToolTip="Detail Chat" />
                    </a>
                </DataItemTemplate>
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="ChatID" FieldName="chat_id" VisibleIndex="1" HeaderStyle-HorizontalAlign="left" Width="20%">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="Name" FieldName="name" VisibleIndex="2" HeaderStyle-HorizontalAlign="left" Width="20%">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="CustomerID" FieldName="customer_id" VisibleIndex="2" HeaderStyle-HorizontalAlign="left" Width="20%">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="Email" FieldName="email" VisibleIndex="2" HeaderStyle-HorizontalAlign="left" Width="15%">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="Start Chat" FieldName="start_chat" VisibleIndex="4" HeaderStyle-HorizontalAlign="left" Width="20%">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="End Chat" FieldName="end_chat" VisibleIndex="4" HeaderStyle-HorizontalAlign="left" Width="20%">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="Agent Handle" FieldName="agent_handle" VisibleIndex="2" HeaderStyle-HorizontalAlign="left" Width="15%">
            </dx:GridViewDataTextColumn>
        </Columns>
    </dx:ASPxGridView>
    <asp:SqlDataSource ID="sql_chat_history" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>"></asp:SqlDataSource>

    <dx:ASPxPopupControl ID="popupConversationChat" ClientInstanceName="popupConversationChat" runat="server" CloseAction="CloseButton" Modal="false"
        closeonescape="true" Height="600px" AllowResize="true" Width="1090px"
        PopupVerticalAlign="WindowCenter" PopupHorizontalAlign="WindowCenter" AllowDragging="True" Theme="SoftOrange" ShowPageScrollbarWhenModal="true" ScrollBars="None"
        ShowFooter="True" HeaderText="Conversation Chat" FooterText="" AutoUpdatePosition="true">
        <ContentCollection>
            <dx:PopupControlContentControl ID="PopupControlContentControl5" runat="server">
                <dx:ASPxCallbackPanel ID="ASPxCallbackPanel1" runat="server" Width="1105px" ShowLoadingPanel="false" ClientInstanceName="ASPxCallbackPanel1">
                    <PanelCollection>
                        <dx:PanelContent>
                            <h1>panel</h1>
                        </dx:PanelContent>
                    </PanelCollection>
                </dx:ASPxCallbackPanel>
            </dx:PopupControlContentControl>
        </ContentCollection>
    </dx:ASPxPopupControl>
</asp:Content>

