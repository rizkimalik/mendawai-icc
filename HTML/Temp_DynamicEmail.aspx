﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/HTML/Ticket.Master" CodeBehind="Temp_DynamicEmail.aspx.vb" Inherits="ICC.Temp_DynamicEmail" %>

<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxCallbackPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxHtmlEditor.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxHtmlEditor" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxGridView.Export" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="FeaturedContent" runat="server">
    <script type="text/javascript">
        var lastCountry = null;
        function OnCountryChanged(cmbPerusahaan) {

            if (cmbDirektorat.InCallback())
                lastCountry = cmbPerusahaan.GetValue().toString();
            else
                cmbDirektorat.PerformCallback(cmbPerusahaan.GetValue().toString());

            lastCountry = cmbPerusahaan.GetValue().toString();
            //alert(cmbCountry.GetValue().toString());
            document.getElementById('foo').style.cssText = 'display: inline;';
        }
        function OnEndCallback(s, e) {
            //alert(lastCountry);

            if (lastCountry) {
                cmbDirektorat.PerformCallback(lastCountry);
                lastCountry = null;

            }
            document.getElementById('foo').style.cssText = 'display: none;';
        }
        var lastDirektorat = null;
        function OnDirektoratChanged(cmbDirektoratNya) {

            if (cmbUnit.InCallback())
                lastDirektorat = cmbDirektorat.GetValue().toString();
            else
                cmbUnit.PerformCallback(cmbDirektorat.GetValue().toString());

            lastDirektorat = cmbDirektorat.GetValue().toString();
            document.getElementById('foo').style.cssText = 'display: inline;';
            //alert(cmbCountry.GetValue().toString());
        }
        function OnEndCallbackDirektorat(s, e) {
            //alert(lastCountry);
            if (lastDirektorat) {
                cmbUnit.PerformCallback(lastDirektorat);
                lastDirektorat = null;
            }
            document.getElementById('foo').style.cssText = 'display: none;';
        }
        var lastUnit = null;
        function OnUnitChanged(cmbUnitNya) {

            if (cmbDivisi.InCallback())
                lastUnit = cmbUnit.GetValue().toString();
            else
                cmbDivisi.PerformCallback(cmbUnit.GetValue().toString());

            lastUnit = cmbUnit.GetValue().toString();
            //alert(cmbCountry.GetValue().toString());
            document.getElementById('foo').style.cssText = 'display: inline;';
        }
        function OnEndCallbackUnit(s, e) {
            //alert(lastCountry);
            if (lastUnit) {
                cmbDivisi.PerformCallback(lastUnit);

                lastUnit = null;
            }
            document.getElementById('foo').style.cssText = 'display: none;';
        }


        var lastUnitCase = null;
        function OnDivisiChanged(cmbUnitCaseNya) {

            if (cmbUnitCaseNya.InCallback())
                lastUnitCase = cmb_UnitCase.GetValue().toString();
            else
                cmb_UnitCase.PerformCallback(cmbDivisi.GetValue().toString());

            lastUnitCase = cmb_UnitCase.GetValue().toString();
            //alert(cmbCountry.GetValue().toString());
            document.getElementById('foo').style.cssText = 'display: inline;';
        }
        function OnEndCallbackUnitCase(s, e) {
            //alert(lastCountry);
            if (lastUnitCase) {
                cmb_UnitCase.PerformCallback(lastUnitCase);
                lastUnit = null;
            }
            document.getElementById('foo').style.cssText = 'display: none;';
        }
        function OnEndCallbackDivisi(s, e) {

            if (lastDivisi) {
                cmbDept.PerformCallback(lastDivisi);
                lastDivisi = null;
            }
            document.getElementById('foo').style.cssText = 'display: none;';
        }


        //Ini script untuk cascade combobox//
        function OnJenisNotifChange(cmbJenisNotif) {

            var comboJenisNotifValue = cmbJenisNotif.GetSelectedItem().value;
            console.log("Jenis_Notif " + comboJenisNotifValue)
            if (comboJenisNotifValue)
                gv_EmailNotif_Related.GetEditor("JenisKategori").PerformCallback(comboJenisNotifValue.toString());
            //alert(comboValue.toString());
        }
        function OnJenisNotif_listChange(cmbJenisNotiflist) {

            var comboJenisNotif_listValue = cmbJenisNotiflist.GetSelectedItem().value;
            console.log("Jenis_Notif_list " + comboJenisNotif_listValue)
            if (comboJenisNotif_listValue)
                gv_EmailNotif_Related_List.GetEditor("JenisKategori").PerformCallback(comboJenisNotif_listValue.toString());
            //alert(comboValue.toString());
        }
        function OnJenisNotifKategori_listChange(cmbJenisNotifKategorilist) {

            var comboJenisNotifKategori_listValue = cmbJenisNotifKategorilist.GetSelectedItem().value;
            console.log("Jenis_Notif_list " + comboJenisNotifKategori_listValue)
            if (comboJenisNotifKategori_listValue)
                gv_EmailNotif_Related_List.GetEditor("JenisRelated").PerformCallback(comboJenisNotifKategori_listValue.toString());
            //alert(comboValue.toString());
        }

        //End
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div runat="server" id="aa" visible="false">
        <div class="row">
            <div class="col-md-2">
                <label>temp_EmailNotif</label>
                <dx:ASPxComboBox runat="server" ID="cmbPerusahaan" ClientInstanceName="cmbPerusahaan"
                    DropDownStyle="DropDownList" IncrementalFilteringMode="Contains" Theme="MetropolisBlue"
                    TextField="JenisNotif" CssClass="form-control input-sm" TextFormatString="{0} ({1})" ValueField="ID" Width="100%" DataSourceID="SqlDataSource1"
                    EnableSynchronization="False">
                    <Columns>
                        <dx:ListBoxColumn FieldName="ID" Width="100px" />
                        <dx:ListBoxColumn FieldName="JenisNotif" Width="300px" />
                    </Columns>
                    <ItemStyle>
                        <HoverStyle BackColor="#0076c4" ForeColor="#ffffff">
                        </HoverStyle>
                    </ItemStyle>
                    <ClientSideEvents SelectedIndexChanged="function(s, e) { OnCountryChanged(s); }" />
                </dx:ASPxComboBox>
                <asp:SqlDataSource ID="SqlDataSource1" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>" SelectCommand="select * from temp_EmailNotif" runat="server"></asp:SqlDataSource>
            </div>

            <div class="col-md-2">
                <label>temp_EmailNotif_Kategori</label>

                <dx:ASPxComboBox runat="server" ID="cmbDirektorat" ClientInstanceName="cmbDirektorat" Theme="MetropolisBlue"
                    DropDownStyle="DropDown" TextField="JenisKategori" TextFormatString="{0} ({1})" Width="100%" CssClass="form-control input-sm"
                    ValueField="ID" IncrementalFilteringMode="Contains" EnableSynchronization="False">
                    <Columns>
                        <dx:ListBoxColumn FieldName="ID" Width="100px" />
                        <dx:ListBoxColumn FieldName="JenisKategori" Width="300px" />
                    </Columns>
                    <ItemStyle>
                        <HoverStyle BackColor="#0076c4" ForeColor="#ffffff">
                        </HoverStyle>
                    </ItemStyle>
                    <ClientSideEvents EndCallback="OnEndCallback" SelectedIndexChanged="function(s, e) { OnDirektoratChanged(s); }" />
                </dx:ASPxComboBox>
                <asp:SqlDataSource ID="SqlDirektorat" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>" runat="server"></asp:SqlDataSource>
            </div>
            <div class="col-md-2">
                <label>temp_EmailNotif_Related</label>
                <dx:ASPxComboBox ID="cmbUnit" ClientInstanceName="cmbUnit" runat="server" TextField="JenisRelated"
                    Theme="MetropolisBlue" CssClass="form-control input-sm" TextFormatString="{0} ({1})" IncrementalFilteringMode="Contains"
                    ValueField="ID" ItemStyle-HoverStyle-BackColor="#F37021" AutoPostBack="false"
                    Width="100%">
                    <Columns>
                        <dx:ListBoxColumn FieldName="ID" Width="100px" />
                        <dx:ListBoxColumn FieldName="JenisRelated" Width="300px" />
                    </Columns>
                    <ItemStyle>
                        <HoverStyle BackColor="#0076c4" ForeColor="#ffffff">
                        </HoverStyle>
                    </ItemStyle>
                    <ClientSideEvents EndCallback="OnEndCallbackDirektorat" SelectedIndexChanged="function(s, e) { OnUnitChanged(s); }"></ClientSideEvents>
                </dx:ASPxComboBox>

                <asp:SqlDataSource ID="SqlUnit" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>" runat="server"></asp:SqlDataSource>
            </div>
            <div class="col-md-2">
                <label>temp_EmailNotif_Related_List</label>
                <dx:ASPxComboBox ID="cmbDivisi" ClientInstanceName="cmbDivisi" runat="server" TextField="JenisList"
                    Theme="MetropolisBlue" CssClass="form-control input-sm" TextFormatString="{0} ({1})" IncrementalFilteringMode="Contains"
                    ValueField="ID" ItemStyle-HoverStyle-BackColor="#F37021" AutoPostBack="false"
                    Width="100%">
                    <Columns>
                        <dx:ListBoxColumn FieldName="ID" Width="100px" />
                        <dx:ListBoxColumn FieldName="JenisList" Width="300px" />
                    </Columns>
                    <ItemStyle>
                        <HoverStyle BackColor="#0076c4" ForeColor="#ffffff">
                        </HoverStyle>
                    </ItemStyle>
                    <ClientSideEvents EndCallback="OnEndCallbackUnit" SelectedIndexChanged="function(s, e) { OnDivisiChanged(s); }"></ClientSideEvents>
                </dx:ASPxComboBox>
                <asp:SqlDataSource ID="SqlDivisi" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>" runat="server"></asp:SqlDataSource>
            </div>
            <div class="col-md-2">
                <label>Unit Case</label>
                <dx:ASPxComboBox ID="cmb_UnitCase" ClientInstanceName="cmb_UnitCase" runat="server" TextField="ORGANIZATION_NAME"
                    Theme="MetropolisBlue" CssClass="form-control input-sm" TextFormatString="{0} ({1})" IncrementalFilteringMode="Contains"
                    ValueField="ORGANIZATION_ID" ItemStyle-HoverStyle-BackColor="#F37021" AutoPostBack="false"
                    Width="100%">
                    <Columns>
                        <dx:ListBoxColumn FieldName="ORGANIZATION_ID" Width="100px" />
                        <dx:ListBoxColumn FieldName="ORGANIZATION_NAME" Width="300px" />
                    </Columns>
                    <ItemStyle>
                        <HoverStyle BackColor="#0076c4" ForeColor="#ffffff">
                        </HoverStyle>
                    </ItemStyle>
                    <ClientSideEvents EndCallback="OnEndCallbackUnitCase"></ClientSideEvents>
                </dx:ASPxComboBox>
                <asp:SqlDataSource ID="sql_cmb_UnitCase" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>" runat="server"></asp:SqlDataSource>
            </div>
        </div>
        <div class="row">
            <asp:Label runat="server" ID="Label1"></asp:Label>
            <div class="col-md-2">
                <label>Email Address</label>
                <dx:ASPxTextBox runat="server" ID="txtEmailNya" CssClass="form-control input-sm"></dx:ASPxTextBox>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <label>Data Email Notif</label>
                <dx:ASPxGridView ID="ASPxGridView1" runat="server" DataSourceID="sql_database"
                    AutoGenerateColumns="False" Width="100%" Font-Size="Small"
                    Settings-ShowFilterRow="true" Styles-Header-Font-Size="12px" Styles-Cell-Font-Size="12px"
                    Settings-HorizontalScrollBarMode="Visible" Theme="MetropolisBlue"
                    SettingsBehavior-ConfirmDelete="true" SettingsPager-PageSize="50" Settings-ShowGroupPanel="true">
                    <%--<GroupSummary>
                        <dx:ASPxSummaryItem SummaryType="Count" />
                    </GroupSummary>--%>

                    <Columns>
                        <dx:GridViewDataTextColumn Caption="JenisNotif" FieldName="JenisNotif" Visible="True" VisibleIndex="1" Width="200px" GroupIndex="0"></dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="Kategori" FieldName="JenisKategori" Visible="True" VisibleIndex="2" Width="200px"></dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="Related" FieldName="JenisRelated" Visible="True" VisibleIndex="3" Width="200px"></dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="Divisi" FieldName="UnitCaseID" Visible="True" VisibleIndex="4" Width="200px"></dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="Department" FieldName="LayerID" Visible="True" VisibleIndex="5" Width="200px"></dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="Jabatan" FieldName="EmailAddress" Visible="True" VisibleIndex="6" Width="200px"></dx:GridViewDataTextColumn>

                    </Columns>
                </dx:ASPxGridView>
                <asp:SqlDataSource ID="sql_database" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>" SelectCommand="select * from vw_EmailList"></asp:SqlDataSource>
            </div>
            <div class="col-md-12">
                <label>Simpan</label>
                <dx:ASPxButton runat="server" ID="btnSimpan" Text="GetData"></dx:ASPxButton>
                <br />
                <asp:Label runat="server" ID="aaa"></asp:Label>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <label>Data Setup Email CSAT Parent</label>
            <dx:ASPxGridView ID="ASPxGridView3" runat="server" KeyFieldName="ID"
                DataSourceID="sql_sourceCSAT_Parent" Width="100%" Theme="Metropolis" Styles-Header-Font-Bold="true" Font-Size="X-Small">
                <SettingsPager>
                    <AllButton Text="All">
                    </AllButton>
                    <NextPageButton Text="Next &gt;">
                    </NextPageButton>
                    <PrevPageButton Text="&lt; Prev">
                    </PrevPageButton>
                    <PageSizeItemSettings Visible="true" Items="10, 15, 20" ShowAllItem="true" />
                </SettingsPager>
                <SettingsEditing Mode="Inline" />
                <Settings ShowFilterRow="true" ShowFilterRowMenu="false" ShowGroupPanel="true"
                    ShowVerticalScrollBar="false" ShowHorizontalScrollBar="false" />
                <SettingsBehavior ConfirmDelete="true" />
                <Columns>
                    <dx:GridViewCommandColumn Caption="Action" HeaderStyle-HorizontalAlign="Center" VisibleIndex="0"
                        ButtonType="Image" FixedStyle="Left" Width="130px">
                        <EditButton Visible="True">
                            <Image ToolTip="Edit" Url="img/icon/Text-Edit-icon2.png" />
                        </EditButton>

                        <DeleteButton Visible="false">
                            <Image ToolTip="Delete" Url="img/icon/Actions-edit-clear-icon2.png" />
                        </DeleteButton>
                        <CancelButton>
                            <Image ToolTip="Cancel" Url="img/icon/cancel1.png">
                            </Image>
                        </CancelButton>
                        <UpdateButton>
                            <Image ToolTip="Update" Url="img/icon/Updated1.png" />
                        </UpdateButton>
                    </dx:GridViewCommandColumn>
                    <dx:GridViewDataTextColumn Caption="ID" FieldName="ID" ReadOnly="true" Visible="false" PropertiesTextEdit-ReadOnlyStyle-BackColor="LightGray"></dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="Icon CSAT" FieldName="IconCSAT"></dx:GridViewDataTextColumn>

                    <dx:GridViewDataTextColumn Caption="UserCreate" FieldName="UserCreate">
                    </dx:GridViewDataTextColumn>
                </Columns>
            </dx:ASPxGridView>
            <asp:SqlDataSource ID="sql_sourceCSAT_Parent" runat="server"
                SelectCommand="select top 1 * from Temp_SettingSurvey "
                UpdateCommand="update Temp_SettingSurvey set IconCSAT=@IconCSAT"
                ConnectionString="<%$ ConnectionStrings:DefaultConnection %>"></asp:SqlDataSource>
        </div>
        <div class="col-md-12">
            <label>Data Setup Email CSAT</label>
            <dx:ASPxGridView ID="ASPxGridView2" runat="server" KeyFieldName="ID"
                DataSourceID="sql_sourceCSAT" Width="100%" Theme="Metropolis" Styles-Header-Font-Bold="true" Font-Size="X-Small">
                <SettingsPager>
                    <AllButton Text="All">
                    </AllButton>
                    <NextPageButton Text="Next &gt;">
                    </NextPageButton>
                    <PrevPageButton Text="&lt; Prev">
                    </PrevPageButton>
                    <PageSizeItemSettings Visible="true" Items="10, 15, 20" ShowAllItem="true" />
                </SettingsPager>
                <SettingsEditing Mode="Inline" />
                <Settings ShowFilterRow="true" ShowFilterRowMenu="false" ShowGroupPanel="true"
                    ShowVerticalScrollBar="false" ShowHorizontalScrollBar="false" />
                <SettingsBehavior ConfirmDelete="true" />
                <Columns>
                    <dx:GridViewCommandColumn Caption="Action" HeaderStyle-HorizontalAlign="Center" VisibleIndex="0"
                        ButtonType="Image" FixedStyle="Left" Width="130px">
                        <EditButton Visible="True">
                            <Image ToolTip="Edit" Url="img/icon/Text-Edit-icon2.png" />
                        </EditButton>

                        <DeleteButton Visible="false">
                            <Image ToolTip="Delete" Url="img/icon/Actions-edit-clear-icon2.png" />
                        </DeleteButton>
                        <CancelButton>
                            <Image ToolTip="Cancel" Url="img/icon/cancel1.png">
                            </Image>
                        </CancelButton>
                        <UpdateButton>
                            <Image ToolTip="Update" Url="img/icon/Updated1.png" />
                        </UpdateButton>
                    </dx:GridViewCommandColumn>
                    <dx:GridViewDataTextColumn Caption="ID" FieldName="ID" ReadOnly="true" Visible="false" PropertiesTextEdit-ReadOnlyStyle-BackColor="LightGray"></dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="URL BKE" FieldName="URL_BKE"></dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="Emoticon" FieldName="URL_Emoticon"></dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="Value" ReadOnly="true" FieldName="Type"></dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="UserCreate" FieldName="UserCreate">
                    </dx:GridViewDataTextColumn>
                </Columns>
            </dx:ASPxGridView>
            <asp:SqlDataSource ID="sql_sourceCSAT" runat="server"
                SelectCommand="select * from Temp_SettingSurvey "
                UpdateCommand="update Temp_SettingSurvey set URL_BKE=@URL_BKE,URL_Emoticon=@URL_Emoticon where ID=@ID"
                ConnectionString="<%$ ConnectionStrings:DefaultConnection %>"></asp:SqlDataSource>
        </div>
        <br />
        <div class="col-md-12">
            <label>Data Jenis Notifikasi</label>
            <dx:ASPxGridView ID="gv_transaction_type" runat="server" KeyFieldName="ID"
                DataSourceID="sql_transaction_type" Width="100%" Theme="Metropolis" Styles-Header-Font-Bold="true" Font-Size="X-Small">
                <SettingsPager>
                    <AllButton Text="All">
                    </AllButton>
                    <NextPageButton Text="Next &gt;">
                    </NextPageButton>
                    <PrevPageButton Text="&lt; Prev">
                    </PrevPageButton>
                    <PageSizeItemSettings Visible="true" Items="10, 15, 20" ShowAllItem="true" />
                </SettingsPager>
                <SettingsEditing Mode="Inline" />
                <Settings ShowFilterRow="true" ShowFilterRowMenu="false" ShowGroupPanel="true"
                    ShowVerticalScrollBar="false" ShowHorizontalScrollBar="false" />
                <SettingsBehavior ConfirmDelete="true" />
                <Columns>
                    <dx:GridViewCommandColumn Caption="Action" HeaderStyle-HorizontalAlign="Center" VisibleIndex="0"
                        ButtonType="Image" FixedStyle="Left" Width="130px">
                        <EditButton Visible="True">
                            <Image ToolTip="Edit" Url="img/icon/Text-Edit-icon2.png" />
                        </EditButton>
                        <NewButton Visible="True">
                            <Image ToolTip="New" Url="img/icon/Apps-text-editor-icon2.png" />
                        </NewButton>
                        <DeleteButton Visible="false">
                            <Image ToolTip="Delete" Url="img/icon/Actions-edit-clear-icon2.png" />
                        </DeleteButton>
                        <CancelButton>
                            <Image ToolTip="Cancel" Url="img/icon/cancel1.png">
                            </Image>
                        </CancelButton>
                        <UpdateButton>
                            <Image ToolTip="Update" Url="img/icon/Updated1.png" />
                        </UpdateButton>
                    </dx:GridViewCommandColumn>
                    <dx:GridViewDataTextColumn Caption="ID" FieldName="ID" ReadOnly="true" Visible="false" PropertiesTextEdit-ReadOnlyStyle-BackColor="LightGray"></dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="Jenis Notif" FieldName="JenisNotif"></dx:GridViewDataTextColumn>
                    <dx:GridViewDataComboBoxColumn Caption="Status" FieldName="NA" Width="80px">
                        <PropertiesComboBox>
                            <Items>
                                <dx:ListEditItem Text="Active" Value="Y" />
                                <dx:ListEditItem Text="In Active" Value="N" />
                            </Items>
                        </PropertiesComboBox>
                    </dx:GridViewDataComboBoxColumn>
                </Columns>
            </dx:ASPxGridView>
            <asp:SqlDataSource ID="sql_transaction_type" runat="server"
                SelectCommand="select * from temp_EmailNotif " 
                InsertCommand="insert into temp_EmailNotif(JenisNotif, NA, UserCreate, DateCreate) values(@JenisNotif, @NA, @username, GETDATE())"
                UpdateCommand="update temp_EmailNotif set JenisNotif=@JenisNotif, NA=@NA, UserUpdate=@username, DateUpdate=GETDATE() WHERE ID=@ID"
                ConnectionString="<%$ ConnectionStrings:DefaultConnection %>">
                <InsertParameters>
                    <asp:Parameter Name="username" />
                </InsertParameters>
                <UpdateParameters>
                    <asp:Parameter Name="username" />
                </UpdateParameters>
            </asp:SqlDataSource>
        </div>

        <div class="col-md-12">
            <label>Data Notifikasi Kategori</label>
            <dx:ASPxGridView ID="GV_EmailNotif_Kategori" runat="server" KeyFieldName="ID"
                DataSourceID="sql_EmailNotif_Kategori" Width="100%" Theme="Metropolis" Styles-Header-Font-Bold="true" Font-Size="X-Small">
                <SettingsPager>
                    <AllButton Text="All">
                    </AllButton>
                    <NextPageButton Text="Next &gt;">
                    </NextPageButton>
                    <PrevPageButton Text="&lt; Prev">
                    </PrevPageButton>
                    <PageSizeItemSettings Visible="true" Items="10, 15, 20" ShowAllItem="true" />
                </SettingsPager>
                <SettingsEditing Mode="Inline" />
                <Settings ShowFilterRow="true" ShowFilterRowMenu="false" ShowGroupPanel="true"
                    ShowVerticalScrollBar="false" ShowHorizontalScrollBar="false" />
                <SettingsBehavior ConfirmDelete="true" />
                <Columns>
                    <dx:GridViewCommandColumn Caption="Action" HeaderStyle-HorizontalAlign="Center" VisibleIndex="0"
                        ButtonType="Image" FixedStyle="Left" Width="130px">
                        <EditButton Visible="True">
                            <Image ToolTip="Edit" Url="img/icon/Text-Edit-icon2.png" />
                        </EditButton>
                        <NewButton Visible="True">
                            <Image ToolTip="New" Url="img/icon/Apps-text-editor-icon2.png" />
                        </NewButton>
                        <DeleteButton Visible="false">
                            <Image ToolTip="Delete" Url="img/icon/Actions-edit-clear-icon2.png" />
                        </DeleteButton>
                        <CancelButton>
                            <Image ToolTip="Cancel" Url="img/icon/cancel1.png">
                            </Image>
                        </CancelButton>
                        <UpdateButton>
                            <Image ToolTip="Update" Url="img/icon/Updated1.png" />
                        </UpdateButton>
                    </dx:GridViewCommandColumn>
                    <dx:GridViewDataTextColumn Caption="ID" FieldName="ID" ReadOnly="true" Visible="false" PropertiesTextEdit-ReadOnlyStyle-BackColor="LightGray"></dx:GridViewDataTextColumn>

                    <dx:GridViewDataComboBoxColumn Caption="Jenis Notif" FieldName="JenisNotif" Settings-AutoFilterCondition="Contains" VisibleIndex="1" Width="350px">
                        <PropertiesComboBox TextFormatString="{1}" TextField="JenisNotif" ValueField="ID" DataSourceID="sql_cmb_JenisNotif">
                            <Columns>
                                <dx:ListBoxColumn Caption="ID" FieldName="ID" Width="100px" />
                                <dx:ListBoxColumn Caption="Jenis" FieldName="JenisNotif" Width="180px" />
                            </Columns>
                        </PropertiesComboBox>
                    </dx:GridViewDataComboBoxColumn>
                    <dx:GridViewDataTextColumn Caption="Jenis Kategori" FieldName="JenisKategori"></dx:GridViewDataTextColumn>
                    <dx:GridViewDataComboBoxColumn Caption="Status" FieldName="NA" Width="80px">
                        <PropertiesComboBox>
                            <Items>
                                <dx:ListEditItem Text="Active" Value="Y" />
                                <dx:ListEditItem Text="In Active" Value="N" />
                            </Items>
                        </PropertiesComboBox>
                    </dx:GridViewDataComboBoxColumn>
                </Columns>
            </dx:ASPxGridView>
            <asp:SqlDataSource ID="sql_cmb_JenisNotif" SelectCommand="select * from temp_EmailNotif" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>"></asp:SqlDataSource>
            <asp:SqlDataSource ID="sql_cmb_JenisNotifKategori" SelectCommand="select * from temp_EmailNotif_Kategori where IDEmailNotif=@ID " runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>">
                <SelectParameters>
                    <asp:Parameter DefaultValue="0" Name="ID" Type="String" />
                </SelectParameters>
            </asp:SqlDataSource>
            <asp:SqlDataSource ID="sql_cmb_JenisNotifRelated" SelectCommand="select * from temp_EmailNotif_Related where IDEmailKategori=@ID" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>">
                <SelectParameters>
                    <asp:Parameter DefaultValue="0" Name="ID" Type="String" />
                </SelectParameters>
            </asp:SqlDataSource>
            <asp:SqlDataSource ID="sql_EmailNotif_Kategori" runat="server"
                SelectCommand="select a.*,b.JenisNotif from temp_EmailNotif_Kategori a left outer join temp_EmailNotif b on a.IDEmailNotif=b.ID"
                InsertCommand="insert into temp_EmailNotif_Kategori (IDEmailNotif,JenisKategori,NA) values (@JenisNotif,@JenisKategori,@NA)"
                ConnectionString="<%$ ConnectionStrings:DefaultConnection %>"></asp:SqlDataSource>
        </div>

        <div class="col-md-12">
            <label>Data Notifikasi Related</label>
            <dx:ASPxGridView ID="gv_EmailNotif_Related" ClientInstanceName="gv_EmailNotif_Related" runat="server" KeyFieldName="ID"
                DataSourceID="sql_EmailNotif_Related" Width="100%" Theme="Metropolis" Styles-Header-Font-Bold="true" Font-Size="X-Small">
                <SettingsPager>
                    <AllButton Text="All">
                    </AllButton>
                    <NextPageButton Text="Next &gt;">
                    </NextPageButton>
                    <PrevPageButton Text="&lt; Prev">
                    </PrevPageButton>
                    <PageSizeItemSettings Visible="true" Items="10, 15, 20" ShowAllItem="true" />
                </SettingsPager>
                <SettingsEditing Mode="Inline" />
                <Settings ShowFilterRow="true" ShowFilterRowMenu="false" ShowGroupPanel="true"
                    ShowVerticalScrollBar="false" ShowHorizontalScrollBar="false" />
                <SettingsBehavior ConfirmDelete="true" />
                <Columns>
                    <dx:GridViewCommandColumn Caption="Action" HeaderStyle-HorizontalAlign="Center" VisibleIndex="0"
                        ButtonType="Image" FixedStyle="Left" Width="130px">
                        <EditButton Visible="True">
                            <Image ToolTip="Edit" Url="img/icon/Text-Edit-icon2.png" />
                        </EditButton>
                        <NewButton Visible="True">
                            <Image ToolTip="New" Url="img/icon/Apps-text-editor-icon2.png" />
                        </NewButton>
                        <DeleteButton Visible="false">
                            <Image ToolTip="Delete" Url="img/icon/Actions-edit-clear-icon2.png" />
                        </DeleteButton>
                        <CancelButton>
                            <Image ToolTip="Cancel" Url="img/icon/cancel1.png">
                            </Image>
                        </CancelButton>
                        <UpdateButton>
                            <Image ToolTip="Update" Url="img/icon/Updated1.png" />
                        </UpdateButton>
                    </dx:GridViewCommandColumn>
                    <dx:GridViewDataTextColumn Caption="ID" FieldName="ID" ReadOnly="true" Visible="false" PropertiesTextEdit-ReadOnlyStyle-BackColor="LightGray"></dx:GridViewDataTextColumn>
                    <dx:GridViewDataComboBoxColumn Caption="Jenis Notif" FieldName="JenisNotif" Settings-AutoFilterCondition="Contains" VisibleIndex="1" Width="350px">
                        <PropertiesComboBox TextFormatString="{1}" TextField="JenisNotif" ValueField="ID" DataSourceID="sql_cmb_JenisNotif">
                            <Columns>
                                <dx:ListBoxColumn Caption="ID" FieldName="ID" Width="100px" />
                                <dx:ListBoxColumn Caption="Jenis" FieldName="JenisNotif" Width="180px" />
                            </Columns>
                            <ClientSideEvents SelectedIndexChanged="function(s, e) { OnJenisNotifChange(s); }"></ClientSideEvents>
                        </PropertiesComboBox>

                    </dx:GridViewDataComboBoxColumn>
                    <dx:GridViewDataComboBoxColumn Caption="Jenis Kategori" FieldName="JenisKategori" Settings-AutoFilterCondition="Contains" VisibleIndex="1" Width="350px">
                        <PropertiesComboBox TextFormatString="{1}" TextField="JenisKategori" ValueField="ID" DataSourceID="sql_cmb_JenisNotifKategori">
                            <%--<Columns>
                                <dx:ListBoxColumn Caption="ID" FieldName="ID" Width="100px" />
                                <dx:ListBoxColumn Caption="Jenis" FieldName="JenisKategori" Width="180px" />
                            </Columns>--%>
                        </PropertiesComboBox>
                    </dx:GridViewDataComboBoxColumn>
                    <dx:GridViewDataTextColumn Caption="Jenis Related" FieldName="JenisRelated"></dx:GridViewDataTextColumn>
                    <dx:GridViewDataComboBoxColumn Caption="Status" FieldName="NA" Width="80px">
                        <PropertiesComboBox>
                            <Items>
                                <dx:ListEditItem Text="Active" Value="Y" />
                                <dx:ListEditItem Text="In Active" Value="N" />
                            </Items>
                        </PropertiesComboBox>
                    </dx:GridViewDataComboBoxColumn>
                </Columns>
            </dx:ASPxGridView>
            <asp:SqlDataSource ID="sql_EmailNotif_Related" runat="server"
                SelectCommand="select a.*,b.JenisKategori,c.JenisNotif from temp_EmailNotif_Related a left outer join temp_EmailNotif_Kategori b on b.ID=a.IDEmailKategori
                left outer join temp_EmailNotif c on c.ID=b.IDEmailNotif"
                InsertCommand="insert into temp_EmailNotif_Related (IDEmailKategori,JenisRelated,NA) values (@JenisKategori,@JenisRelated,@NA)" 
                UpdateCommand="update temp_EmailNotif_Related set IDEmailKategori=@IDEmailKategori, JenisRelated=@JenisRelated, NA=@NA where ID=@ID"
                ConnectionString="<%$ ConnectionStrings:DefaultConnection %>"></asp:SqlDataSource>
        </div>

        <div class="col-md-12">
            <label>Data Notifikasi Related Detail</label>
            <dx:ASPxGridView ID="gv_EmailNotif_Related_List" Visible="false" ClientInstanceName="gv_EmailNotif_Related_List" runat="server" KeyFieldName="ID"
                DataSourceID="sql_EmailNotif_Related_List" Width="100%" Theme="Metropolis" Styles-Header-Font-Bold="true" Font-Size="X-Small">
                <SettingsPager>
                    <AllButton Text="All">
                    </AllButton>
                    <NextPageButton Text="Next &gt;">
                    </NextPageButton>
                    <PrevPageButton Text="&lt; Prev">
                    </PrevPageButton>
                    <PageSizeItemSettings Visible="true" Items="10, 15, 20" ShowAllItem="true" />
                </SettingsPager>
                <SettingsEditing Mode="Inline" />
                <Settings ShowFilterRow="true" ShowFilterRowMenu="false" ShowGroupPanel="true"
                    ShowVerticalScrollBar="false" ShowHorizontalScrollBar="false" />
                <SettingsBehavior ConfirmDelete="true" />
                <Columns>
                    <dx:GridViewCommandColumn Caption="Action" HeaderStyle-HorizontalAlign="Center" VisibleIndex="0"
                        ButtonType="Image" FixedStyle="Left" Width="130px">
                        <EditButton Visible="True">
                            <Image ToolTip="Edit" Url="img/icon/Text-Edit-icon2.png" />
                        </EditButton>
                        <NewButton Visible="True">
                            <Image ToolTip="New" Url="img/icon/Apps-text-editor-icon2.png" />
                        </NewButton>
                        <DeleteButton Visible="false">
                            <Image ToolTip="Delete" Url="img/icon/Actions-edit-clear-icon2.png" />
                        </DeleteButton>
                        <CancelButton>
                            <Image ToolTip="Cancel" Url="img/icon/cancel1.png">
                            </Image>
                        </CancelButton>
                        <UpdateButton>
                            <Image ToolTip="Update" Url="img/icon/Updated1.png" />
                        </UpdateButton>
                    </dx:GridViewCommandColumn>
                    <dx:GridViewDataTextColumn Caption="ID" FieldName="ID" ReadOnly="true" Visible="false" PropertiesTextEdit-ReadOnlyStyle-BackColor="LightGray"></dx:GridViewDataTextColumn>
                    <dx:GridViewDataComboBoxColumn Caption="Jenis Notif" FieldName="JenisNotif" Settings-AutoFilterCondition="Contains" VisibleIndex="1" Width="350px">
                        <PropertiesComboBox TextFormatString="{1}" TextField="JenisNotif" ValueField="ID" DataSourceID="sql_cmb_JenisNotif">
                            <Columns>
                                <dx:ListBoxColumn Caption="ID" FieldName="ID" Width="100px" />
                                <dx:ListBoxColumn Caption="Jenis" FieldName="JenisNotif" Width="180px" />
                            </Columns>
                            <ClientSideEvents SelectedIndexChanged="function(s, e) { OnJenisNotif_listChange(s); }"></ClientSideEvents>
                        </PropertiesComboBox>
                    </dx:GridViewDataComboBoxColumn>
                    <dx:GridViewDataComboBoxColumn Caption="Jenis Kategori" FieldName="JenisKategori" Settings-AutoFilterCondition="Contains" VisibleIndex="1" Width="350px">
                        <PropertiesComboBox TextFormatString="{1}" TextField="JenisKategori" ValueField="ID" DataSourceID="sql_cmb_JenisNotifKategori">
                            <%--<Columns>
                                <dx:ListBoxColumn Caption="ID" FieldName="ID" Width="100px" />
                                <dx:ListBoxColumn Caption="Jenis" FieldName="JenisKategori" Width="180px" />
                            </Columns>--%>
                            <ClientSideEvents SelectedIndexChanged="function(s, e) { OnJenisNotifKategori_listChange(s); }"></ClientSideEvents>
                        </PropertiesComboBox>
                    </dx:GridViewDataComboBoxColumn>
                    <dx:GridViewDataComboBoxColumn Caption="Jenis Related" FieldName="JenisRelated" Settings-AutoFilterCondition="Contains" VisibleIndex="1" Width="350px">
                        <PropertiesComboBox TextFormatString="{1}" TextField="JenisRelated" ValueField="ID" DataSourceID="sql_cmb_JenisNotifRelated">
                            <%-- <Columns>
                                <dx:ListBoxColumn Caption="ID" FieldName="ID" Width="100px" />
                                <dx:ListBoxColumn Caption="Jenis" FieldName="JenisRelated" Width="180px" />
                            </Columns>--%>
                        </PropertiesComboBox>
                    </dx:GridViewDataComboBoxColumn>
                    <dx:GridViewDataTextColumn Caption="Jenis List" FieldName="JenisList"></dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="Jenis Layer" FieldName="LayerID"></dx:GridViewDataTextColumn>

                    <dx:GridViewDataComboBoxColumn Caption="Unit Case" FieldName="UnitCaseID" Settings-AutoFilterCondition="Contains" VisibleIndex="1" Width="350px">
                        <PropertiesComboBox TextFormatString="{1}" TextField="ORGANIZATION_NAME" ValueField="ORGANIZATION_ID" DataSourceID="sql_UnitUser">
                            <Columns>
                                <dx:ListBoxColumn Caption="ID" FieldName="ORGANIZATION_ID" Width="100px" />
                                <dx:ListBoxColumn Caption="Name" FieldName="ORGANIZATION_NAME" Width="180px" />
                            </Columns>

                        </PropertiesComboBox>
                    </dx:GridViewDataComboBoxColumn>

                    <dx:GridViewDataTextColumn Caption="Email" FieldName="EmailAddress" Width="300px"></dx:GridViewDataTextColumn>

                    <dx:GridViewDataComboBoxColumn Caption="Status" FieldName="NA" Width="80px">
                        <PropertiesComboBox>
                            <Items>
                                <dx:ListEditItem Text="Active" Value="YES" />
                                <dx:ListEditItem Text="In Active" Value="NO" />
                            </Items>
                        </PropertiesComboBox>
                    </dx:GridViewDataComboBoxColumn>
                </Columns>
            </dx:ASPxGridView>
            <asp:SqlDataSource ID="sql_EmailNotif_Related_List" runat="server"
                SelectCommand="select a.*,b.JenisRelated,c.JenisKategori,d.JenisNotif from temp_EmailNotif_Related_List a left outer join temp_EmailNotif_Related b on a.IDEmailRelated=b.ID
                            left outer join temp_EmailNotif_Kategori c on c.ID=b.IDEmailKategori
                            left outer join temp_EmailNotif d on d.ID=c.IDEmailNotif"
                ConnectionString="<%$ ConnectionStrings:DefaultConnection %>"></asp:SqlDataSource>
            <asp:SqlDataSource ID="sql_UnitUser" runat="server"
                SelectCommand="select CONVERT(varchar(10), ORGANIZATION_ID) as ORGANIZATION_ID,ORGANIZATION_NAME from mOrganization where NA='Y' union select 'None' , 'None'"
                ConnectionString="<%$ ConnectionStrings:DefaultConnection %>"></asp:SqlDataSource>
        </div>
    </div>

</asp:Content>
