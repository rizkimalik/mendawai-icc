﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'     Runtime Version:4.0.30319.42000
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated.
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On

Imports System
Imports System.Collections.Generic
Imports System.ComponentModel
Imports System.Data
Imports System.Data.Linq
Imports System.Data.Linq.Mapping
Imports System.Linq
Imports System.Linq.Expressions
Imports System.Reflection


<Global.System.Data.Linq.Mapping.DatabaseAttribute(Name:="HelpdeskMendawai")>
Partial Public Class T_TransactionLoad_DBMLDataContext
	Inherits System.Data.Linq.DataContext
	
	Private Shared mappingSource As System.Data.Linq.Mapping.MappingSource = New AttributeMappingSource()
	
  #Region "Extensibility Method Definitions"
  Partial Private Sub OnCreated()
  End Sub
  #End Region
	
	Public Sub New()
		MyBase.New(Global.System.Configuration.ConfigurationManager.ConnectionStrings("DefaultConnection").ConnectionString, mappingSource)
		OnCreated
	End Sub
	
	Public Sub New(ByVal connection As String)
		MyBase.New(connection, mappingSource)
		OnCreated
	End Sub
	
	Public Sub New(ByVal connection As System.Data.IDbConnection)
		MyBase.New(connection, mappingSource)
		OnCreated
	End Sub
	
	Public Sub New(ByVal connection As String, ByVal mappingSource As System.Data.Linq.Mapping.MappingSource)
		MyBase.New(connection, mappingSource)
		OnCreated
	End Sub
	
	Public Sub New(ByVal connection As System.Data.IDbConnection, ByVal mappingSource As System.Data.Linq.Mapping.MappingSource)
		MyBase.New(connection, mappingSource)
		OnCreated
	End Sub
	
	<Global.System.Data.Linq.Mapping.FunctionAttribute(Name:="dbo.L_TransactionLoad")>  _
	Public Function L_TransactionLoad(<Global.System.Data.Linq.Mapping.ParameterAttribute(Name:="TrxCustomerID", DbType:="VarChar(50)")> ByVal trxCustomerID As String, <Global.System.Data.Linq.Mapping.ParameterAttribute(Name:="TrxUsername", DbType:="VarChar(50)")> ByVal trxUsername As String, <Global.System.Data.Linq.Mapping.ParameterAttribute(Name:="TrxGenesysID", DbType:="VarChar(50)")> ByVal trxGenesysID As String) As ISingleResult(Of L_TransactionLoadResult)
		Dim result As IExecuteResult = Me.ExecuteMethodCall(Me, CType(MethodInfo.GetCurrentMethod,MethodInfo), trxCustomerID, trxUsername, trxGenesysID)
		Return CType(result.ReturnValue,ISingleResult(Of L_TransactionLoadResult))
	End Function
End Class

Partial Public Class L_TransactionLoadResult
	
	Private _ID As Integer
	
	Private _NIK As String
	
	Private _TicketNumber As String
	
	Private _GroupTicketNumber As String
	
	Private _Channel_Code As String
	
	Private _UnitID As String
	
	Private _TicketSource As String
	
	Private _TicketSourceName As String
	
	Private _TicketGroup As String
	
	Private _TicketGroupName As String
	
	Private _ComplaintLevel As String
	
	Private _CategoryID As String
	
	Private _CategoryName As String
	
	Private _SubCategory1ID As String
	
	Private _SubCategory1Name As String
	
	Private _SubCategory2ID As String
	
	Private _SubCategory2Name As String
	
	Private _SubCategory3ID As String
	
	Private _SubCategory3Name As String
	
	Private _DetailComplaint As String
	
	Private _ResponComplaint As String
	
	Private _DateAgentResponse As System.Nullable(Of Date)
	
	Private _SLAResponseAgent As System.Nullable(Of Integer)
	
	Private _SLA As Integer
	
	Private _Severity As String
	
	Private _Status As String
	
	Private _UserCreate As String
	
	Private _DateCreate As System.Nullable(Of Date)
	
	Private _UserClose As String
	
	Private _DateClose As System.Nullable(Of Date)
	
	Private _TicketPosition As String
	
	Private _ClosedBy As String
	
	Private _KirimEmail As String
	
	Private _KirimEmailLayer As String
	
	Private _NA As String
	
	Private _DateCreateReal As System.Nullable(Of Date)
	
	Private _OverClockSystem As String
	
	Private _Dispatch_user As String
	
	Private _Dispatch_tgl As System.Nullable(Of Date)
	
	Private _Divisi As String
	
	Private _Dispatch_divisi_tgl As System.Nullable(Of Date)
	
	Private _Attch As String
	
	Private _Posting As String
	
	Private _IdTabel As String
	
	Private _GroupID As String
	
	Private _CompID As String
	
	Private _OrganizationID As String
	
	Private _channelid As String
	
	Private _NAMA_PELAPOR As String
	
	Private _EMAIL_PELAPOR As String
	
	Private _PHONE_PELAPOR As String
	
	Private _ALAMAT_PELAPOR As String
	
	Private _FLAG_GROUP_TICKET As String
	
	Private _AccountInbound As String
	
	Private _NomorRekening As String
	
	Private _SkalaPrioritas As String
	
	Private _JenisNasabah As String
	
	Private _IDLevel3 As String
	
	Private _Phone As String
	
	Private _SumberInformasi As String
	
	Private _ExtendID As System.Nullable(Of Integer)
	
	Private _ExtendSLA As System.Nullable(Of Integer)
	
	Private _ExtendName As String
	
	Private _ThreadID As String
	
	Private _GenesysID As String
	
	Private _ReleaseUser As String
	
	Private _UnitKerjaAgent As String
	
	Private _SLAORI As System.Nullable(Of Integer)
	
	Public Sub New()
		MyBase.New
	End Sub
	
	<Global.System.Data.Linq.Mapping.ColumnAttribute(Storage:="_ID", DbType:="Int NOT NULL")>  _
	Public Property ID() As Integer
		Get
			Return Me._ID
		End Get
		Set
			If ((Me._ID = value)  _
						= false) Then
				Me._ID = value
			End If
		End Set
	End Property
	
	<Global.System.Data.Linq.Mapping.ColumnAttribute(Storage:="_NIK", DbType:="VarChar(50) NOT NULL", CanBeNull:=false)>  _
	Public Property NIK() As String
		Get
			Return Me._NIK
		End Get
		Set
			If (String.Equals(Me._NIK, value) = false) Then
				Me._NIK = value
			End If
		End Set
	End Property
	
	<Global.System.Data.Linq.Mapping.ColumnAttribute(Storage:="_TicketNumber", DbType:="VarChar(50) NOT NULL", CanBeNull:=false)>  _
	Public Property TicketNumber() As String
		Get
			Return Me._TicketNumber
		End Get
		Set
			If (String.Equals(Me._TicketNumber, value) = false) Then
				Me._TicketNumber = value
			End If
		End Set
	End Property
	
	<Global.System.Data.Linq.Mapping.ColumnAttribute(Storage:="_GroupTicketNumber", DbType:="VarChar(100)")>  _
	Public Property GroupTicketNumber() As String
		Get
			Return Me._GroupTicketNumber
		End Get
		Set
			If (String.Equals(Me._GroupTicketNumber, value) = false) Then
				Me._GroupTicketNumber = value
			End If
		End Set
	End Property
	
	<Global.System.Data.Linq.Mapping.ColumnAttribute(Storage:="_Channel_Code", DbType:="VarChar(5)")>  _
	Public Property Channel_Code() As String
		Get
			Return Me._Channel_Code
		End Get
		Set
			If (String.Equals(Me._Channel_Code, value) = false) Then
				Me._Channel_Code = value
			End If
		End Set
	End Property
	
	<Global.System.Data.Linq.Mapping.ColumnAttribute(Storage:="_UnitID", DbType:="VarChar(150)")>  _
	Public Property UnitID() As String
		Get
			Return Me._UnitID
		End Get
		Set
			If (String.Equals(Me._UnitID, value) = false) Then
				Me._UnitID = value
			End If
		End Set
	End Property
	
	<Global.System.Data.Linq.Mapping.ColumnAttribute(Storage:="_TicketSource", DbType:="VarChar(20)")>  _
	Public Property TicketSource() As String
		Get
			Return Me._TicketSource
		End Get
		Set
			If (String.Equals(Me._TicketSource, value) = false) Then
				Me._TicketSource = value
			End If
		End Set
	End Property
	
	<Global.System.Data.Linq.Mapping.ColumnAttribute(Storage:="_TicketSourceName", DbType:="VarChar(300)")>  _
	Public Property TicketSourceName() As String
		Get
			Return Me._TicketSourceName
		End Get
		Set
			If (String.Equals(Me._TicketSourceName, value) = false) Then
				Me._TicketSourceName = value
			End If
		End Set
	End Property
	
	<Global.System.Data.Linq.Mapping.ColumnAttribute(Storage:="_TicketGroup", DbType:="VarChar(20)")>  _
	Public Property TicketGroup() As String
		Get
			Return Me._TicketGroup
		End Get
		Set
			If (String.Equals(Me._TicketGroup, value) = false) Then
				Me._TicketGroup = value
			End If
		End Set
	End Property
	
	<Global.System.Data.Linq.Mapping.ColumnAttribute(Storage:="_TicketGroupName", DbType:="VarChar(300)")>  _
	Public Property TicketGroupName() As String
		Get
			Return Me._TicketGroupName
		End Get
		Set
			If (String.Equals(Me._TicketGroupName, value) = false) Then
				Me._TicketGroupName = value
			End If
		End Set
	End Property
	
	<Global.System.Data.Linq.Mapping.ColumnAttribute(Storage:="_ComplaintLevel", DbType:="VarChar(20)")>  _
	Public Property ComplaintLevel() As String
		Get
			Return Me._ComplaintLevel
		End Get
		Set
			If (String.Equals(Me._ComplaintLevel, value) = false) Then
				Me._ComplaintLevel = value
			End If
		End Set
	End Property
	
	<Global.System.Data.Linq.Mapping.ColumnAttribute(Storage:="_CategoryID", DbType:="VarChar(50) NOT NULL", CanBeNull:=false)>  _
	Public Property CategoryID() As String
		Get
			Return Me._CategoryID
		End Get
		Set
			If (String.Equals(Me._CategoryID, value) = false) Then
				Me._CategoryID = value
			End If
		End Set
	End Property
	
	<Global.System.Data.Linq.Mapping.ColumnAttribute(Storage:="_CategoryName", DbType:="VarChar(300) NOT NULL", CanBeNull:=false)>  _
	Public Property CategoryName() As String
		Get
			Return Me._CategoryName
		End Get
		Set
			If (String.Equals(Me._CategoryName, value) = false) Then
				Me._CategoryName = value
			End If
		End Set
	End Property
	
	<Global.System.Data.Linq.Mapping.ColumnAttribute(Storage:="_SubCategory1ID", DbType:="VarChar(50) NOT NULL", CanBeNull:=false)>  _
	Public Property SubCategory1ID() As String
		Get
			Return Me._SubCategory1ID
		End Get
		Set
			If (String.Equals(Me._SubCategory1ID, value) = false) Then
				Me._SubCategory1ID = value
			End If
		End Set
	End Property
	
	<Global.System.Data.Linq.Mapping.ColumnAttribute(Storage:="_SubCategory1Name", DbType:="VarChar(250) NOT NULL", CanBeNull:=false)>  _
	Public Property SubCategory1Name() As String
		Get
			Return Me._SubCategory1Name
		End Get
		Set
			If (String.Equals(Me._SubCategory1Name, value) = false) Then
				Me._SubCategory1Name = value
			End If
		End Set
	End Property
	
	<Global.System.Data.Linq.Mapping.ColumnAttribute(Storage:="_SubCategory2ID", DbType:="VarChar(50) NOT NULL", CanBeNull:=false)>  _
	Public Property SubCategory2ID() As String
		Get
			Return Me._SubCategory2ID
		End Get
		Set
			If (String.Equals(Me._SubCategory2ID, value) = false) Then
				Me._SubCategory2ID = value
			End If
		End Set
	End Property
	
	<Global.System.Data.Linq.Mapping.ColumnAttribute(Storage:="_SubCategory2Name", DbType:="VarChar(250) NOT NULL", CanBeNull:=false)>  _
	Public Property SubCategory2Name() As String
		Get
			Return Me._SubCategory2Name
		End Get
		Set
			If (String.Equals(Me._SubCategory2Name, value) = false) Then
				Me._SubCategory2Name = value
			End If
		End Set
	End Property
	
	<Global.System.Data.Linq.Mapping.ColumnAttribute(Storage:="_SubCategory3ID", DbType:="VarChar(50) NOT NULL", CanBeNull:=false)>  _
	Public Property SubCategory3ID() As String
		Get
			Return Me._SubCategory3ID
		End Get
		Set
			If (String.Equals(Me._SubCategory3ID, value) = false) Then
				Me._SubCategory3ID = value
			End If
		End Set
	End Property
	
	<Global.System.Data.Linq.Mapping.ColumnAttribute(Storage:="_SubCategory3Name", DbType:="VarChar(250) NOT NULL", CanBeNull:=false)>  _
	Public Property SubCategory3Name() As String
		Get
			Return Me._SubCategory3Name
		End Get
		Set
			If (String.Equals(Me._SubCategory3Name, value) = false) Then
				Me._SubCategory3Name = value
			End If
		End Set
	End Property
	
	<Global.System.Data.Linq.Mapping.ColumnAttribute(Storage:="_DetailComplaint", DbType:="VarChar(MAX) NOT NULL", CanBeNull:=false)>  _
	Public Property DetailComplaint() As String
		Get
			Return Me._DetailComplaint
		End Get
		Set
			If (String.Equals(Me._DetailComplaint, value) = false) Then
				Me._DetailComplaint = value
			End If
		End Set
	End Property
	
	<Global.System.Data.Linq.Mapping.ColumnAttribute(Storage:="_ResponComplaint", DbType:="VarChar(MAX) NOT NULL", CanBeNull:=false)>  _
	Public Property ResponComplaint() As String
		Get
			Return Me._ResponComplaint
		End Get
		Set
			If (String.Equals(Me._ResponComplaint, value) = false) Then
				Me._ResponComplaint = value
			End If
		End Set
	End Property
	
	<Global.System.Data.Linq.Mapping.ColumnAttribute(Storage:="_DateAgentResponse", DbType:="DateTime")>  _
	Public Property DateAgentResponse() As System.Nullable(Of Date)
		Get
			Return Me._DateAgentResponse
		End Get
		Set
			If (Me._DateAgentResponse.Equals(value) = false) Then
				Me._DateAgentResponse = value
			End If
		End Set
	End Property
	
	<Global.System.Data.Linq.Mapping.ColumnAttribute(Storage:="_SLAResponseAgent", DbType:="Int")>  _
	Public Property SLAResponseAgent() As System.Nullable(Of Integer)
		Get
			Return Me._SLAResponseAgent
		End Get
		Set
			If (Me._SLAResponseAgent.Equals(value) = false) Then
				Me._SLAResponseAgent = value
			End If
		End Set
	End Property
	
	<Global.System.Data.Linq.Mapping.ColumnAttribute(Storage:="_SLA", DbType:="Int NOT NULL")>  _
	Public Property SLA() As Integer
		Get
			Return Me._SLA
		End Get
		Set
			If ((Me._SLA = value)  _
						= false) Then
				Me._SLA = value
			End If
		End Set
	End Property
	
	<Global.System.Data.Linq.Mapping.ColumnAttribute(Storage:="_Severity", DbType:="VarChar(50)")>  _
	Public Property Severity() As String
		Get
			Return Me._Severity
		End Get
		Set
			If (String.Equals(Me._Severity, value) = false) Then
				Me._Severity = value
			End If
		End Set
	End Property
	
	<Global.System.Data.Linq.Mapping.ColumnAttribute(Storage:="_Status", DbType:="VarChar(20)")>  _
	Public Property Status() As String
		Get
			Return Me._Status
		End Get
		Set
			If (String.Equals(Me._Status, value) = false) Then
				Me._Status = value
			End If
		End Set
	End Property
	
	<Global.System.Data.Linq.Mapping.ColumnAttribute(Storage:="_UserCreate", DbType:="VarChar(50)")>  _
	Public Property UserCreate() As String
		Get
			Return Me._UserCreate
		End Get
		Set
			If (String.Equals(Me._UserCreate, value) = false) Then
				Me._UserCreate = value
			End If
		End Set
	End Property
	
	<Global.System.Data.Linq.Mapping.ColumnAttribute(Storage:="_DateCreate", DbType:="DateTime")>  _
	Public Property DateCreate() As System.Nullable(Of Date)
		Get
			Return Me._DateCreate
		End Get
		Set
			If (Me._DateCreate.Equals(value) = false) Then
				Me._DateCreate = value
			End If
		End Set
	End Property
	
	<Global.System.Data.Linq.Mapping.ColumnAttribute(Storage:="_UserClose", DbType:="VarChar(50)")>  _
	Public Property UserClose() As String
		Get
			Return Me._UserClose
		End Get
		Set
			If (String.Equals(Me._UserClose, value) = false) Then
				Me._UserClose = value
			End If
		End Set
	End Property
	
	<Global.System.Data.Linq.Mapping.ColumnAttribute(Storage:="_DateClose", DbType:="DateTime")>  _
	Public Property DateClose() As System.Nullable(Of Date)
		Get
			Return Me._DateClose
		End Get
		Set
			If (Me._DateClose.Equals(value) = false) Then
				Me._DateClose = value
			End If
		End Set
	End Property
	
	<Global.System.Data.Linq.Mapping.ColumnAttribute(Storage:="_TicketPosition", DbType:="VarChar(20)")>  _
	Public Property TicketPosition() As String
		Get
			Return Me._TicketPosition
		End Get
		Set
			If (String.Equals(Me._TicketPosition, value) = false) Then
				Me._TicketPosition = value
			End If
		End Set
	End Property
	
	<Global.System.Data.Linq.Mapping.ColumnAttribute(Storage:="_ClosedBy", DbType:="VarChar(2)")>  _
	Public Property ClosedBy() As String
		Get
			Return Me._ClosedBy
		End Get
		Set
			If (String.Equals(Me._ClosedBy, value) = false) Then
				Me._ClosedBy = value
			End If
		End Set
	End Property
	
	<Global.System.Data.Linq.Mapping.ColumnAttribute(Storage:="_KirimEmail", DbType:="VarChar(10)")>  _
	Public Property KirimEmail() As String
		Get
			Return Me._KirimEmail
		End Get
		Set
			If (String.Equals(Me._KirimEmail, value) = false) Then
				Me._KirimEmail = value
			End If
		End Set
	End Property
	
	<Global.System.Data.Linq.Mapping.ColumnAttribute(Storage:="_KirimEmailLayer", DbType:="VarChar(10)")>  _
	Public Property KirimEmailLayer() As String
		Get
			Return Me._KirimEmailLayer
		End Get
		Set
			If (String.Equals(Me._KirimEmailLayer, value) = false) Then
				Me._KirimEmailLayer = value
			End If
		End Set
	End Property
	
	<Global.System.Data.Linq.Mapping.ColumnAttribute(Storage:="_NA", DbType:="VarChar(1)")>  _
	Public Property NA() As String
		Get
			Return Me._NA
		End Get
		Set
			If (String.Equals(Me._NA, value) = false) Then
				Me._NA = value
			End If
		End Set
	End Property
	
	<Global.System.Data.Linq.Mapping.ColumnAttribute(Storage:="_DateCreateReal", DbType:="DateTime")>  _
	Public Property DateCreateReal() As System.Nullable(Of Date)
		Get
			Return Me._DateCreateReal
		End Get
		Set
			If (Me._DateCreateReal.Equals(value) = false) Then
				Me._DateCreateReal = value
			End If
		End Set
	End Property
	
	<Global.System.Data.Linq.Mapping.ColumnAttribute(Storage:="_OverClockSystem", DbType:="VarChar(50)")>  _
	Public Property OverClockSystem() As String
		Get
			Return Me._OverClockSystem
		End Get
		Set
			If (String.Equals(Me._OverClockSystem, value) = false) Then
				Me._OverClockSystem = value
			End If
		End Set
	End Property
	
	<Global.System.Data.Linq.Mapping.ColumnAttribute(Storage:="_Dispatch_user", DbType:="VarChar(500)")>  _
	Public Property Dispatch_user() As String
		Get
			Return Me._Dispatch_user
		End Get
		Set
			If (String.Equals(Me._Dispatch_user, value) = false) Then
				Me._Dispatch_user = value
			End If
		End Set
	End Property
	
	<Global.System.Data.Linq.Mapping.ColumnAttribute(Storage:="_Dispatch_tgl", DbType:="DateTime")>  _
	Public Property Dispatch_tgl() As System.Nullable(Of Date)
		Get
			Return Me._Dispatch_tgl
		End Get
		Set
			If (Me._Dispatch_tgl.Equals(value) = false) Then
				Me._Dispatch_tgl = value
			End If
		End Set
	End Property
	
	<Global.System.Data.Linq.Mapping.ColumnAttribute(Storage:="_Divisi", DbType:="VarChar(50)")>  _
	Public Property Divisi() As String
		Get
			Return Me._Divisi
		End Get
		Set
			If (String.Equals(Me._Divisi, value) = false) Then
				Me._Divisi = value
			End If
		End Set
	End Property
	
	<Global.System.Data.Linq.Mapping.ColumnAttribute(Storage:="_Dispatch_divisi_tgl", DbType:="DateTime")>  _
	Public Property Dispatch_divisi_tgl() As System.Nullable(Of Date)
		Get
			Return Me._Dispatch_divisi_tgl
		End Get
		Set
			If (Me._Dispatch_divisi_tgl.Equals(value) = false) Then
				Me._Dispatch_divisi_tgl = value
			End If
		End Set
	End Property
	
	<Global.System.Data.Linq.Mapping.ColumnAttribute(Storage:="_Attch", DbType:="VarChar(MAX)")>  _
	Public Property Attch() As String
		Get
			Return Me._Attch
		End Get
		Set
			If (String.Equals(Me._Attch, value) = false) Then
				Me._Attch = value
			End If
		End Set
	End Property
	
	<Global.System.Data.Linq.Mapping.ColumnAttribute(Storage:="_Posting", DbType:="VarChar(50)")>  _
	Public Property Posting() As String
		Get
			Return Me._Posting
		End Get
		Set
			If (String.Equals(Me._Posting, value) = false) Then
				Me._Posting = value
			End If
		End Set
	End Property
	
	<Global.System.Data.Linq.Mapping.ColumnAttribute(Storage:="_IdTabel", DbType:="VarChar(200)")>  _
	Public Property IdTabel() As String
		Get
			Return Me._IdTabel
		End Get
		Set
			If (String.Equals(Me._IdTabel, value) = false) Then
				Me._IdTabel = value
			End If
		End Set
	End Property
	
	<Global.System.Data.Linq.Mapping.ColumnAttribute(Storage:="_GroupID", DbType:="VarChar(50)")>  _
	Public Property GroupID() As String
		Get
			Return Me._GroupID
		End Get
		Set
			If (String.Equals(Me._GroupID, value) = false) Then
				Me._GroupID = value
			End If
		End Set
	End Property
	
	<Global.System.Data.Linq.Mapping.ColumnAttribute(Storage:="_CompID", DbType:="VarChar(50)")>  _
	Public Property CompID() As String
		Get
			Return Me._CompID
		End Get
		Set
			If (String.Equals(Me._CompID, value) = false) Then
				Me._CompID = value
			End If
		End Set
	End Property
	
	<Global.System.Data.Linq.Mapping.ColumnAttribute(Storage:="_OrganizationID", DbType:="VarChar(50)")>  _
	Public Property OrganizationID() As String
		Get
			Return Me._OrganizationID
		End Get
		Set
			If (String.Equals(Me._OrganizationID, value) = false) Then
				Me._OrganizationID = value
			End If
		End Set
	End Property
	
	<Global.System.Data.Linq.Mapping.ColumnAttribute(Storage:="_channelid", DbType:="VarChar(250)")>  _
	Public Property channelid() As String
		Get
			Return Me._channelid
		End Get
		Set
			If (String.Equals(Me._channelid, value) = false) Then
				Me._channelid = value
			End If
		End Set
	End Property
	
	<Global.System.Data.Linq.Mapping.ColumnAttribute(Storage:="_NAMA_PELAPOR", DbType:="VarChar(250)")>  _
	Public Property NAMA_PELAPOR() As String
		Get
			Return Me._NAMA_PELAPOR
		End Get
		Set
			If (String.Equals(Me._NAMA_PELAPOR, value) = false) Then
				Me._NAMA_PELAPOR = value
			End If
		End Set
	End Property
	
	<Global.System.Data.Linq.Mapping.ColumnAttribute(Storage:="_EMAIL_PELAPOR", DbType:="VarChar(250)")>  _
	Public Property EMAIL_PELAPOR() As String
		Get
			Return Me._EMAIL_PELAPOR
		End Get
		Set
			If (String.Equals(Me._EMAIL_PELAPOR, value) = false) Then
				Me._EMAIL_PELAPOR = value
			End If
		End Set
	End Property
	
	<Global.System.Data.Linq.Mapping.ColumnAttribute(Storage:="_PHONE_PELAPOR", DbType:="VarChar(250)")>  _
	Public Property PHONE_PELAPOR() As String
		Get
			Return Me._PHONE_PELAPOR
		End Get
		Set
			If (String.Equals(Me._PHONE_PELAPOR, value) = false) Then
				Me._PHONE_PELAPOR = value
			End If
		End Set
	End Property
	
	<Global.System.Data.Linq.Mapping.ColumnAttribute(Storage:="_ALAMAT_PELAPOR", DbType:="VarChar(250)")>  _
	Public Property ALAMAT_PELAPOR() As String
		Get
			Return Me._ALAMAT_PELAPOR
		End Get
		Set
			If (String.Equals(Me._ALAMAT_PELAPOR, value) = false) Then
				Me._ALAMAT_PELAPOR = value
			End If
		End Set
	End Property
	
	<Global.System.Data.Linq.Mapping.ColumnAttribute(Storage:="_FLAG_GROUP_TICKET", DbType:="VarChar(5)")>  _
	Public Property FLAG_GROUP_TICKET() As String
		Get
			Return Me._FLAG_GROUP_TICKET
		End Get
		Set
			If (String.Equals(Me._FLAG_GROUP_TICKET, value) = false) Then
				Me._FLAG_GROUP_TICKET = value
			End If
		End Set
	End Property
	
	<Global.System.Data.Linq.Mapping.ColumnAttribute(Storage:="_AccountInbound", DbType:="VarChar(100)")>  _
	Public Property AccountInbound() As String
		Get
			Return Me._AccountInbound
		End Get
		Set
			If (String.Equals(Me._AccountInbound, value) = false) Then
				Me._AccountInbound = value
			End If
		End Set
	End Property
	
	<Global.System.Data.Linq.Mapping.ColumnAttribute(Storage:="_NomorRekening", DbType:="VarChar(100)")>  _
	Public Property NomorRekening() As String
		Get
			Return Me._NomorRekening
		End Get
		Set
			If (String.Equals(Me._NomorRekening, value) = false) Then
				Me._NomorRekening = value
			End If
		End Set
	End Property
	
	<Global.System.Data.Linq.Mapping.ColumnAttribute(Storage:="_SkalaPrioritas", DbType:="VarChar(100)")>  _
	Public Property SkalaPrioritas() As String
		Get
			Return Me._SkalaPrioritas
		End Get
		Set
			If (String.Equals(Me._SkalaPrioritas, value) = false) Then
				Me._SkalaPrioritas = value
			End If
		End Set
	End Property
	
	<Global.System.Data.Linq.Mapping.ColumnAttribute(Storage:="_JenisNasabah", DbType:="VarChar(100)")>  _
	Public Property JenisNasabah() As String
		Get
			Return Me._JenisNasabah
		End Get
		Set
			If (String.Equals(Me._JenisNasabah, value) = false) Then
				Me._JenisNasabah = value
			End If
		End Set
	End Property
	
	<Global.System.Data.Linq.Mapping.ColumnAttribute(Storage:="_IDLevel3", DbType:="VarChar(100)")>  _
	Public Property IDLevel3() As String
		Get
			Return Me._IDLevel3
		End Get
		Set
			If (String.Equals(Me._IDLevel3, value) = false) Then
				Me._IDLevel3 = value
			End If
		End Set
	End Property
	
	<Global.System.Data.Linq.Mapping.ColumnAttribute(Storage:="_Phone", DbType:="VarChar(50)")>  _
	Public Property Phone() As String
		Get
			Return Me._Phone
		End Get
		Set
			If (String.Equals(Me._Phone, value) = false) Then
				Me._Phone = value
			End If
		End Set
	End Property
	
	<Global.System.Data.Linq.Mapping.ColumnAttribute(Storage:="_SumberInformasi", DbType:="VarChar(150)")>  _
	Public Property SumberInformasi() As String
		Get
			Return Me._SumberInformasi
		End Get
		Set
			If (String.Equals(Me._SumberInformasi, value) = false) Then
				Me._SumberInformasi = value
			End If
		End Set
	End Property
	
	<Global.System.Data.Linq.Mapping.ColumnAttribute(Storage:="_ExtendID", DbType:="Int")>  _
	Public Property ExtendID() As System.Nullable(Of Integer)
		Get
			Return Me._ExtendID
		End Get
		Set
			If (Me._ExtendID.Equals(value) = false) Then
				Me._ExtendID = value
			End If
		End Set
	End Property
	
	<Global.System.Data.Linq.Mapping.ColumnAttribute(Storage:="_ExtendSLA", DbType:="Int")>  _
	Public Property ExtendSLA() As System.Nullable(Of Integer)
		Get
			Return Me._ExtendSLA
		End Get
		Set
			If (Me._ExtendSLA.Equals(value) = false) Then
				Me._ExtendSLA = value
			End If
		End Set
	End Property
	
	<Global.System.Data.Linq.Mapping.ColumnAttribute(Storage:="_ExtendName", DbType:="VarChar(100)")>  _
	Public Property ExtendName() As String
		Get
			Return Me._ExtendName
		End Get
		Set
			If (String.Equals(Me._ExtendName, value) = false) Then
				Me._ExtendName = value
			End If
		End Set
	End Property
	
	<Global.System.Data.Linq.Mapping.ColumnAttribute(Storage:="_ThreadID", DbType:="VarChar(100) NOT NULL", CanBeNull:=false)>  _
	Public Property ThreadID() As String
		Get
			Return Me._ThreadID
		End Get
		Set
			If (String.Equals(Me._ThreadID, value) = false) Then
				Me._ThreadID = value
			End If
		End Set
	End Property
	
	<Global.System.Data.Linq.Mapping.ColumnAttribute(Storage:="_GenesysID", DbType:="VarChar(100) NOT NULL", CanBeNull:=false)>  _
	Public Property GenesysID() As String
		Get
			Return Me._GenesysID
		End Get
		Set
			If (String.Equals(Me._GenesysID, value) = false) Then
				Me._GenesysID = value
			End If
		End Set
	End Property
	
	<Global.System.Data.Linq.Mapping.ColumnAttribute(Storage:="_ReleaseUser", DbType:="VarChar(150)")>  _
	Public Property ReleaseUser() As String
		Get
			Return Me._ReleaseUser
		End Get
		Set
			If (String.Equals(Me._ReleaseUser, value) = false) Then
				Me._ReleaseUser = value
			End If
		End Set
	End Property
	
	<Global.System.Data.Linq.Mapping.ColumnAttribute(Storage:="_UnitKerjaAgent", DbType:="VarChar(50) NOT NULL", CanBeNull:=false)>  _
	Public Property UnitKerjaAgent() As String
		Get
			Return Me._UnitKerjaAgent
		End Get
		Set
			If (String.Equals(Me._UnitKerjaAgent, value) = false) Then
				Me._UnitKerjaAgent = value
			End If
		End Set
	End Property
	
	<Global.System.Data.Linq.Mapping.ColumnAttribute(Storage:="_SLAORI", DbType:="Int")>  _
	Public Property SLAORI() As System.Nullable(Of Integer)
		Get
			Return Me._SLAORI
		End Get
		Set
			If (Me._SLAORI.Equals(value) = false) Then
				Me._SLAORI = value
			End If
		End Set
	End Property
End Class
