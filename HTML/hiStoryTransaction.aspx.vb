﻿Imports System.Data
Imports System.Data.SqlClient
Imports DevExpress.Web.ASPxGridView
Imports DevExpress.Web.ASPxGridLookup
Public Class hiStoryTransaction
    Inherits System.Web.UI.Page

    Dim strExecute As New ClsConn
    Dim sqlDr As SqlDataReader
    Dim _upage As String = String.Empty
    Dim _strSql As String = String.Empty
    Dim _TicketNumber As String = String.Empty
    Dim _strTrxDetail As String = String.Empty
    Dim strSql As String = String.Empty
    Dim strLogTime As String = DateTime.Now.ToString("yyyy")
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        _updatePage()
        TrxLoginTypeAngka.Value = Session("LoginTypeAngka")
        ds_query.SelectParameters("username").DefaultValue = Session("username")
        ds_query.InsertParameters("username").DefaultValue = Session("username")
        ds_query.UpdateParameters("username").DefaultValue = Session("username")
        TrxUserName.Value = Session("username")
        SqlDataSource1.SelectParameters("username").DefaultValue = Session("username")
        SqlDataSource1.InsertParameters("username").DefaultValue = Session("username")
        SqlDataSource1.UpdateParameters("username").DefaultValue = Session("username")
    End Sub
    Private Sub grid_Load(sender As Object, e As EventArgs) Handles grid.Load
        Try
            Dim strSql As New hiStoryTransactionDBMLDataContext
            strSql.CommandTimeout = 480
            grid.DataSource = strSql.SP_Temp_hiStoryTransaction(Session("username"), "History").ToList()
            grid.DataBind()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Private Sub ReadonlyCustomer()
        CmbCountry.ReadOnly = True
        ASPxComboBox1.ReadOnly = True
        CmbCity.ReadOnly = True
        ASPxDescription.ReadOnly = True
        txt_channel.ReadOnly = True
        txt_sla.ReadOnly = True
    End Sub
    Private Sub ASPxCallbackPanel4_Callback(sender As Object, e As DevExpress.Web.ASPxClasses.CallbackEventArgsBase) Handles ASPxCallbackPanel4.Callback
        Dim strCommands As String = "exec SP_Temp_Interaction '" & hd_ticketid.Value & "'"
        dsInteraction.SelectCommand = strCommands
    End Sub
    Private Sub _updatePage()
        Try
            _upage = "update user1 set Activity='N'"
            strExecute.ExecuteNonQuery(_upage)
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
        Try
            _upage = "update user1 set Activity='Y' where MenuID='" & Request.QueryString("idpage") & "'"
            strExecute.ExecuteNonQuery(_upage)
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
        Try
            _upage = "update user2 set Activity='N'"
            strExecute.ExecuteNonQuery(_upage)
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
        Try
            _upage = "update user2 set Activity='Y' where SubMenuID='" & Request.QueryString("idtable") & "'"
            strExecute.ExecuteNonQuery(_upage)
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Private Sub btnClear_Click(sender As Object, e As EventArgs) Handles btnClear.Click
        Response.Redirect("hiStoryTransaction.aspx?idpage=" & Request.QueryString("idpage") & "")
    End Sub
    Private Sub ASPxGridView1_Load(sender As Object, e As EventArgs) Handles ASPxGridView1.Load
        Try
            Dim strSql As New hiStoryTransactionDBMLDataContext
            strSql.CommandTimeout = 480
            ASPxGridView1.DataSource = strSql.SP_Temp_hiStoryTransaction(Session("username"), "ParentChild").ToList()
            ASPxGridView1.DataBind()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Protected Sub ASPxGridView3_CustomColumnGroup(sender As Object, e As CustomColumnSortEventArgs)
        CompareColumnValues(e)
    End Sub
    Private Sub CompareColumnValues(ByVal e As DevExpress.Web.ASPxGridView.CustomColumnSortEventArgs)
        If e.Column.FieldName = "ParentNumberID" Then
            e.Handled = True
        End If
    End Sub
    Protected Sub ASPxGridView3_CustomGroupDisplayText(sender As Object, e As ASPxGridViewColumnDisplayTextEventArgs)
        If e.Column.FieldName = "ParentNumberID" Then
            'e.DisplayText = "Data : "
        End If
    End Sub
    Protected Sub ASPxGridView3_CustomColumnSort(sender As Object, e As CustomColumnSortEventArgs)
        CompareColumnValues(e)
    End Sub
    Private Sub ASPxGridView3_Load(sender As Object, e As EventArgs) Handles ASPxGridView3.Load
        CType(ASPxGridView3.Columns("ParentNumberID"), GridViewDataTextColumn).GroupBy()
        Try
            Dim strSql As New ParentNumberDBMLDataContext
            strSql.CommandTimeout = 480
            ASPxGridView3.DataSource = strSql.SP_Temp_ParentChildTicketDetail(hd_ticketid.Value).ToList()
            ASPxGridView3.DataBind()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    'Private Sub ASPxCallbackPanel2_Callback(sender As Object, e As DevExpress.Web.ASPxClasses.CallbackEventArgsBase) Handles ASPxCallbackPanel2.Callback
    '    CType(ASPxGridView3.Columns("ParentNumberID"), GridViewDataTextColumn).GroupBy()
    '    Try
    '        Dim strSql As New ParentNumberDBMLDataContext
    '        strSql.CommandTimeout = 480
    '        ASPxGridView3.DataSource = strSql.SP_Temp_ParentChildTicketDetail(hd_ticketid.Value).ToList()
    '        ASPxGridView3.DataBind()
    '    Catch ex As Exception
    '        Response.Write(ex.Message)
    '    End Try
    'End Sub

    Private Sub GridLookup_Load(sender As Object, e As EventArgs) Handles ASPxGridLookup1.Load
        Try
            Dim strSql As New hiStoryTransactionDBMLDataContext
            strSql.CommandTimeout = 480
            ASPxGridLookup1.DataSource = strSql.SP_Temp_hiStoryTransaction(Session("username"), "ParentChild").ToList()
            ASPxGridLookup1.DataBind()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Private Sub ASPxGridView2_RowInserting(sender As Object, e As DevExpress.Web.Data.ASPxDataInsertingEventArgs) Handles ASPxGridView2.RowInserting
        Dim _strScript As String = String.Empty
        strSql = "select * from Temp_SettingHiStoryTransaction WHERE CreatedBy='" & Session("username") & "' and Type='ParentChild'"
        sqldr = strExecute.ExecuteReader(strSql)
        If sqldr.HasRows() Then
            sqldr.Read()
            e.Cancel = True
            Throw New Exception("For user " & Session("username") & " to setting query data transaksi already exists, please update data existing")
        Else
            e.Cancel = False
            _strScript = "insert into Temp_SettingHiStoryTransaction([Day], [FilterDate], [CreatedBy], [Type]) VALUES (@Day, @FilterDate, '" & Session("username") & "', 'ParentChild')"
            strExecute.LogSuccess(strLogTime, _strScript)
            ds_query.InsertCommand = _strScript
        End If
        sqldr.Close()
    End Sub

    Private Sub GridView_RowInserting(sender As Object, e As DevExpress.Web.Data.ASPxDataInsertingEventArgs) Handles GridView.RowInserting
        Dim _strScript As String = String.Empty
        strSql = "select * from Temp_SettingHiStoryTransaction WHERE CreatedBy='" & Session("username") & "' and Type='History'"
        sqlDr = strExecute.ExecuteReader(strSql)
        If sqlDr.HasRows() Then
            sqlDr.Read()
            e.Cancel = True
            Throw New Exception("For user " & Session("username") & " to setting query history data transaksi already exists, please update data existing")
        Else
            e.Cancel = False
            _strScript = "insert into Temp_SettingHiStoryTransaction([Day], [FilterDate], [CreatedBy], [Type]) VALUES (@Day, @FilterDate, '" & Session("username") & "', 'History')"
            strExecute.LogSuccess(strLogTime, _strScript)
            ds_query.InsertCommand = _strScript
        End If
        sqlDr.Close()
    End Sub
End Class