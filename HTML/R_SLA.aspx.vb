﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.IO
Imports DevExpress

Public Class R_SLA
    Inherits System.Web.UI.Page

    Dim Cls As New WebServiceTransaction
    Dim proses As New ClsConn
    Dim _upage As String
    Dim sqlRead As SqlDataReader
    Dim strLogTime As String = DateTime.Now.ToString("yyyy")
    Private Sub ASPxGridView1_Load(sender As Object, e As EventArgs) Handles ASPxGridView1.Load
        'LinqDataSource1.WhereParameters("StartTanggalFilter").DefaultValue = dt_strdate.Text & " 00:01"
        'LinqDataSource1.WhereParameters("EndTanggalFilter").DefaultValue = dt_endate.Text & " 23:59"
        LinqDataSource1.WhereParameters("StartTanggalFilter").DefaultValue = Format(dt_strdate.Value, "yyyy-MM-dd 00:00:01")
        LinqDataSource1.WhereParameters("EndTanggalFilter").DefaultValue = Format(dt_endate.Value, "yyyy-MM-dd 23:59:59")

        'Try
        '    Dim strSql As New R_SLA_DBMLDataContext
        '    strSql.CommandTimeout = 480
        '    ASPxGridView1.DataSource = strSql.R_SLA("" & Format(dt_strdate.Value, "yyyy-MM-dd") & "", "" & Format(dt_endate.Value, "yyyy-MM-dd") & "").ToList()
        '    ASPxGridView1.DataBind()

        '    Dim strQuery As String = "LoadReportingBaseonSLA_" & Format(dt_strdate.Value, "yyyy-MM-dd") & "," & Format(dt_endate.Value, "yyyy-MM-dd") & ""
        '    Cls.LogSuccess(strLogTime, strQuery)
        'Catch ex As Exception
        '    Dim strQuery As String = "LoadReportingBaseonSLA_" & Format(dt_strdate.Value, "yyyy-MM-dd") & "," & Format(dt_endate.Value, "yyyy-MM-dd") & ""
        '    Cls.LogError(strLogTime, ex, strQuery)
        'End Try
    End Sub
    Private Sub btn_Submit_Click(sender As Object, e As EventArgs) Handles btn_Submit.Click
        'LinqDataSource1.WhereParameters("StartTanggalFilter").DefaultValue = dt_strdate.Text & " 00:01"
        'LinqDataSource1.WhereParameters("EndTanggalFilter").DefaultValue = dt_endate.Text & " 23:59"
        LinqDataSource1.WhereParameters("StartTanggalFilter").DefaultValue = Format(dt_strdate.Value, "yyyy-MM-dd 00:00:01")
        LinqDataSource1.WhereParameters("EndTanggalFilter").DefaultValue = Format(dt_endate.Value, "yyyy-MM-dd 23:59:59")

        'Try
        '    Dim strSql As New R_SLA_DBMLDataContext
        '    strSql.CommandTimeout = 480
        '    ASPxGridView1.DataSource = strSql.R_SLA("" & Format(dt_strdate.Value, "yyyy-MM-dd") & "", "" & Format(dt_endate.Value, "yyyy-MM-dd") & "").ToList()
        '    ASPxGridView1.DataBind()

        '    Dim strQuery As String = "SubmitReportingBaseonSLA_" & Format(dt_strdate.Value, "yyyy-MM-dd") & "," & Format(dt_endate.Value, "yyyy-MM-dd") & ""
        '    Cls.LogSuccess(strLogTime, strQuery)
        'Catch ex As Exception
        '    Dim strQuery As String = "SubmitReportingBaseonSLA_" & Format(dt_strdate.Value, "yyyy-MM-dd") & "," & Format(dt_endate.Value, "yyyy-MM-dd") & ""
        '    Cls.LogError(strLogTime, ex, strQuery)
        'End Try
    End Sub
    Private Sub dt_strdate_Init(sender As Object, e As EventArgs) Handles dt_strdate.Init
        dt_strdate.Value = DateTime.Now
    End Sub
    Private Sub dt_endate_Init(sender As Object, e As EventArgs) Handles dt_endate.Init
        dt_endate.Value = DateTime.Now
    End Sub
    Private Sub btn_Export_Click(sender As Object, e As EventArgs) Handles btn_Export.Click
        Dim TimeS As DateTime
        Dim Val As String = Cls.FunctionDataSetting("Report")

        Dim fromDate As DateTime = Format(dt_strdate.Value, "yyyy-MM-dd")
        Dim toDate As DateTime = Format(dt_endate.Value, "yyyy-MM-dd")
        Dim ddate As String
        ddate = DateDiff(DateInterval.Day, toDate, fromDate)
        Dim Idat As Integer = Convert.ToInt32(ddate)
        Dim Ival As Integer = Convert.ToInt32(Val)
        'If dt_strdate.Value < strTgl Then

        If Ival > Idat Then
            alertJS("Export maximum " & ReplaceSpecialLetter(Cls.FunctionDataSetting("Report")) & " days")
        Else

            Dim casses As String = ddList.SelectedValue
            Select Case casses
                Case "xlsx"
                    ASPxGridViewExporter1.WriteXlsxToResponse("ReportingBaseonSLA_" & DateTime.Now.ToString("yyyyMMddhhmmss"), True)
                Case "xls"
                    ASPxGridViewExporter1.WriteXlsToResponse("ReportingBaseonSLA_" & DateTime.Now.ToString("yyyyMMddhhmmss"), True)
                Case "rtf"
                    ASPxGridViewExporter1.Landscape = True
                    ASPxGridViewExporter1.LeftMargin = 35
                    ASPxGridViewExporter1.RightMargin = 30
                    ASPxGridViewExporter1.ExportedRowType = DevExpress.Web.ASPxGridView.Export.GridViewExportedRowType.All
                    ASPxGridViewExporter1.MaxColumnWidth = 108
                    ASPxGridViewExporter1.WriteRtfToResponse("ReportingBaseonSLA_" & DateTime.Now.ToString("yyyyMMddhhmmss"), True)
                Case "pdf"
                    ASPxGridViewExporter1.Landscape = True
                    ASPxGridViewExporter1.LeftMargin = 35
                    ASPxGridViewExporter1.RightMargin = 30
                    ASPxGridViewExporter1.ExportedRowType = DevExpress.Web.ASPxGridView.Export.GridViewExportedRowType.All
                    ASPxGridViewExporter1.MaxColumnWidth = 108
                    ASPxGridViewExporter1.WritePdfToResponse("ReportingBaseonSLA_" & DateTime.Now.ToString("yyyyMMddhhmmss"), True)
                Case "csv"
                    ASPxGridViewExporter1.WriteCsvToResponse("ReportingBaseonSLA_" & DateTime.Now.ToString("yyyyMMddhhmmss"), True)
            End Select
        End If
        'Cls.LogSuccess(strLogTime, strString)
        'ExportGridToExcel()
    End Sub

    'Private Sub ExportGridToExcel()
    '    Response.Clear()
    '    Response.Buffer = True
    '    Response.ClearContent()
    '    Response.ClearHeaders()
    '    Response.Charset = ""
    '    Dim FileName As String = "ReportingBaseonSLA_" & DateTime.Now & ".xls"
    '    Dim strwritter As StringWriter = New StringWriter()
    '    Dim htmltextwrtter As HtmlTextWriter = New HtmlTextWriter(strwritter)
    '    Response.Cache.SetCacheability(HttpCacheability.NoCache)
    '    Response.ContentType = "application/vnd.ms-excel"
    '    Response.AddHeader("Content-Disposition", "attachment;filename=" & FileName)
    '    ASPxGridView1.GridLines = GridLines.Both
    '    ASPxGridView1.HeaderStyle.Font.Bold = True
    '    ASPxGridView1.RenderControl(htmltextwrtter)
    '    Response.Write(strwritter.ToString())
    '    Response.[End]()
    'End Sub

    Function alertJS(ByVal alert As String)
        Dim message As String = alert
        Dim sb As New System.Text.StringBuilder()
        sb.Append("<script type = 'text/javascript'>")
        sb.Append("window.onload=function(){")
        sb.Append("alert('")
        sb.Append(message)
        sb.Append("')};")
        sb.Append("</script>")
        ClientScript.RegisterClientScriptBlock(Me.GetType(), "alert", sb.ToString())
    End Function
    Private Sub R_SLA_Load(sender As Object, e As EventArgs) Handles Me.Load
        _updatePage()
    End Sub
    Private Sub _updatePage()
        Try
            _upage = "update user1 Set Activity='N'"
            proses.ExecuteNonQuery(_upage)
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
        Try
            _upage = "update user1 set Activity='Y' where MenuID='" & Request.QueryString("idpage") & "'"
            proses.ExecuteNonQuery(_upage)
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
        Try
            _upage = "update user2 set Activity='N'"
            proses.ExecuteNonQuery(_upage)
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
        Try
            _upage = "update user2 set Activity='Y' where SubMenuID='" & Request.QueryString("idtable") & "'"
            proses.ExecuteNonQuery(_upage)
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Public Function ReplaceSpecialLetter(ByVal str)
        Dim TmpStr As String
        TmpStr = str
        TmpStr = Replace(TmpStr, "-", "")
        ReplaceSpecialLetter = TmpStr
    End Function
End Class