﻿Imports System.IO
Imports System.Data.SqlClient
Imports Microsoft.VisualBasic
Imports System.Web.Configuration

Public Class cls_globe
    Inherits System.Web.UI.Page
    Protected Da As SqlDataAdapter
    Protected Ds As DataSet
    Protected Dt As DataTable
    Dim mycom As New SqlCommand()
    Dim mycon As New SqlConnection()
    Dim cs, TmpStr As String
    Dim f As System.IO.FileStream
    Dim p As DirectoryInfo
    Dim objreader As System.IO.StreamWriter

    Public Sub koneksi(dbName As String)
        CloseKoneksi()
        Try
            If dbName = "fax" Then
                cs = WebConfigurationManager.ConnectionStrings("DefaultConnection").ConnectionString()
            ElseIf dbName = "email" Then
                cs = WebConfigurationManager.ConnectionStrings("DefaultConnection").ConnectionString()
            ElseIf dbName = "sms" Then
                cs = WebConfigurationManager.ConnectionStrings("DefaultConnection").ConnectionString()
            ElseIf dbName = "chat" Then
                cs = WebConfigurationManager.ConnectionStrings("DefaultConnection").ConnectionString()
            ElseIf dbName = "master" Then
                cs = WebConfigurationManager.ConnectionStrings("DefaultConnection").ConnectionString()
            ElseIf dbName = "KM" Then
                cs = WebConfigurationManager.ConnectionStrings("dbKM").ConnectionString()
            ElseIf dbName = "vicon" Then
                cs = WebConfigurationManager.ConnectionStrings("vicon").ConnectionString()
            ElseIf dbName = "ticket" Then
                cs = WebConfigurationManager.ConnectionStrings("DefaultConnection").ConnectionString()
            End If
            mycon.ConnectionString = cs
            mycon.Open()
            mycom.Connection = mycon
        Catch ex As Exception
            writedata("Error", "Open Database", dbName, "", "")
            Session("Error") = "Can't Open Database, please contact Team Web"
            Exit Sub
        End Try
    End Sub
    Public Sub CloseKoneksi()
        mycon.Close()
        SqlConnection.ClearAllPools()
    End Sub

    Public Sub writedata(ByVal id_user As String, ByVal action As String, ByVal keterangan As String, ByVal query As String, ByVal ChName As String)
        Dim strdata As String = id_user & " | " & action & " | " & ReplaceSpecialLetter(keterangan) & " | " & ReplaceSpecialLetter(query) & " | " & ChName

        Dim FolderName As String = DateTime.Now.ToString("yyyyMMdd")
        Dim dt As String = Date.Now.ToString("MMddyyyy")
        Dim namalog, nmdir As String
        Try            
       
            nmdir = Server.MapPath(ConfigurationManager.AppSettings.Item("Logsys") & "/" & FolderName)
            If System.IO.Directory.Exists(nmdir) = False Then
                p = Directory.CreateDirectory(nmdir)
                p.Create()
            End If

            nmdir = ConfigurationManager.AppSettings.Item("Logsys")
            namalog = Server.MapPath("~/log/" & FolderName & "/" & ChName & ".txt")
            'namalog = Server.MapPath(nmdir & "logFile-" & dt & ".txt")
            If System.IO.File.Exists(namalog) = False Then
                f = File.Create(namalog)
                f.Close()
            End If
            objreader = New System.IO.StreamWriter(namalog, True)
            objreader.Write(Now & " : " & strdata)
            objreader.WriteLine()
            objreader.Close()
        Catch ex As Exception
            Session("Error") = ex.Message
            writedata("System", "WriteData", ex.Message, strdata, Session("dbName"))
        End Try
    End Sub
    Public Sub LogSys(ByVal id_user As String, ByVal action As String, ByVal keterangan As String, ByVal query As String, ByVal ChName As String)
        If Session("Error") <> "" Then Exit Sub
        If Session("IPAddress") = "::1" Then
            Session("IPAddress") = "127.0.0.1"
        End If
        If id_user <> "" Then
            koneksi("ticket")
            mycom.CommandText = "insert LogSys (usr_id,menu,action,ddate,keterangan,query,IPAddress) values " & _
                "('" & id_user & "','" & ChName & "','" & action & "',getdate(),'" & ReplaceSpecialLetter(keterangan) & "','" & ReplaceSpecialLetter(query) & "','" & Session("IPAddress") & "')"
            mycom.ExecuteNonQuery()
        End If
    End Sub
    Public Function ReplaceSpecialLetter(ByVal str)
        TmpStr = str
        TmpStr = Replace(TmpStr, "'", "&#39;")
        ReplaceSpecialLetter = TmpStr
    End Function
    Public Function ReplaceQuery(ByVal str)
        TmpStr = str
        TmpStr = Replace(TmpStr, "'", "")
        ReplaceQuery = TmpStr
    End Function
    Public Function TrayCTIPORT() As String
        Return ConfigurationManager.AppSettings.Item("TryCTIPORT")
    End Function
    Public Function kodeCall() As String
        Return ConfigurationManager.AppSettings.Item("CTIkedeCall")
    End Function
    Public Function kdblender() As String
        Return ConfigurationManager.AppSettings.Item("Blender")
    End Function
    Public Function InFileFAX() As String
        Return ConfigurationManager.AppSettings.Item("InFileFAX")
    End Function
    Public Function filePath() As String
        Return ConfigurationManager.AppSettings.Item("filePath")
    End Function
    Public Function filePathKTP() As String
        Return ConfigurationManager.AppSettings.Item("filePathKTP")
    End Function
    Public Function LicLogin() As String
        Return ConfigurationManager.AppSettings.Item("LicLogin")
    End Function
    Public Function PrefixFAX() As String
        Return ConfigurationManager.AppSettings.Item("PrefixFAX")
    End Function
    Public Function BedaExcel() As String
        Return ConfigurationManager.AppSettings.Item("BedaExcel")
    End Function

    Public Function ExecuteQuery(ByVal Query As String) As DataTable
        koneksi(Session("dbName"))
        Session("Error") = Nothing
        If Session("Error") <> "" Then Exit Function
        Try
            mycom.CommandText = Query
            Da = New SqlDataAdapter
            Da.SelectCommand = mycom

            Ds = New Data.DataSet
            Da.Fill(Ds)

            Dt = Ds.Tables(0)

            Return Dt
        Catch ex As Exception
            Session("Error") = ex.Message
            writedata("System", "ExecuteQuery", ex.Message, Query, Session("dbName"))
        End Try
    End Function
    Public Function ExecuteNonQuery(ByVal Query As String)
        koneksi(Session("dbName"))
        Session("Error") = Nothing
        If Session("Error") <> "" Then Exit Function
        Try
            mycom.CommandText = Query
            mycom.ExecuteNonQuery()
        Catch ex As Exception
            Session("Error") = ex.Message
            writedata("System", "ExecuteNonQuery", ex.Message, Query, Session("dbName"))
        End Try
    End Function
End Class
