import { url } from "./config.js";

document.addEventListener('DOMContentLoaded', (event) => {
    $.ajaxSetup({ cache: false });

    function isNotEmpty(value) {
        return value !== undefined && value !== null && value !== '';
    }

    function LoadReportSla() {
        const start_date = $('#MainContent_dt_strdate_I').val();
        const end_date = $('#MainContent_dt_endate_I').val();

        const store_report_sla = new DevExpress.data.CustomStore({
            key: 'TicketNumber',
            load(options) {
                const deferred = $.Deferred();
                const args = {};
                [
                    'skip',
                    'take',
                    'requireTotalCount',
                    'requireGroupCount',
                    'sort',
                    'filter',
                    'totalSummary',
                    'group',
                    'groupSummary',
                ].forEach((i) => {
                    if (i in options && isNotEmpty(options[i])) {
                        args[i] = JSON.stringify(options[i]);
                    }
                });

                $.ajax({
                    url: `${url}/report_sla.php?action=data&start_date=${start_date}&end_date=${end_date}`,
                    dataType: 'json',
                    method: 'GET',
                    cache: false,
                    async: true,
                    data: args,
                    success(result) {
                        if (result.status === 200) {
                            deferred.resolve(result.data, {
                                totalCount: result.totalCount,
                                summary: result.summary,
                                groupCount: result.groupCount,
                            });
                        } else {
                            alert(result.data)
                            deferred.resolve([], {
                                totalCount: 0,
                                summary: result.summary,
                                groupCount: result.groupCount,
                            });
                        }
                    },
                    error() {
                        deferred.reject('Data Loading');
                    },
                    // timeout: 10000,
                });

                return deferred.promise();
            },
        });

        $('#dxReportBaseOnSLA').dxDataGrid({
            dataSource: store_report_sla,
            remoteOperations: true,
            paging: {
                pageSize: 10,
            },
            pager: {
                visible: true,
                allowedPageSizes: [10, 20, 50],
                showPageSizeSelector: true,
                showInfo: true,
                showNavigationButtons: true,
            },
            allowColumnResizing: true,
            columnWidth: 120,
            showBorders: true,
            showRowLines: true,
            hoverStateEnabled: true,
            filterRow: {
                visible: true,
            },
            columns: [{
                caption: 'Interaction ID',
                dataField: 'GenesysID',
            }, {
                dataField: 'ThreadID'
            }, {
                caption: 'User Status',
                dataField: 'StrStatusPelapor'
            }, {
                caption: 'Thread Ticket',
                dataField: 'ThreadTicket'
            }, {
                dataField: 'TicketNumber',
                width: 200
            }, {
                caption: 'Account',
                dataField: 'AccountInbound',
            }, {
                caption: 'Channel',
                dataField: 'TicketSourceName',
            }, {
                caption: 'Source Information',
                dataField: 'SumberInformasi',
                width: 200
            }, {
                caption: 'Account ID',
                dataField: 'AccountID',
            }, {
                caption: 'Customer ID',
                dataField: 'NIK',
            }, {
                dataField: 'CIF',
            }, {
                caption: 'Customer Name',
                dataField: 'Name',
            }, {
                caption: 'Phone Number',
                dataField: 'HP',
            }, {
                caption: 'Department',
                dataField: 'ORGANIZATION_NAME',
            }, {
                caption: 'Category',
                dataField: 'CategoryName',
            }, {
                caption: 'Priority Scale',
                dataField: 'SkalaPrioritas',
            }, {
                caption: 'Enquiry Type',
                dataField: 'SubCategory1Name',
            }, {
                caption: 'Enquiry Detail',
                dataField: 'SubCategory2Name',
            }, {
                caption: 'Reason',
                dataField: 'SubCategory3Name',
                width: 300
            }, {
                caption: 'User Issue Remark',
                dataField: 'DetailComplaint',
                width: 300
            }, {
                caption: 'Agent Response',
                dataField: 'ResponComplaint',
                width: 300
            }, {
                dataField: 'Amount',
            }, {
                caption: 'Ticket Status',
                dataField: 'Status',
            }, {
                caption: 'Created By',
                dataField: 'UserCreate',
            }, {
                caption: 'Created Date',
                dataField: 'DateCreate',
                // dataType: 'datetime',
            }, {
                caption: 'Date Transaction',
                dataField: 'DateTransaction',
                // dataType: 'datetime',
            }, {
                caption: 'Claim Status',
                dataField: 'ClaimStatus',
            }, {
                caption: 'Bank Name',
                dataField: 'BankName',
            }, {
                caption: 'Solved By',
                dataField: 'UserSolved',
            }, {
                caption: 'Solved Date',
                dataField: 'DateSolved',
                // dataType: 'datetime',
            }, {
                caption: 'Closed By',
                dataField: 'UserClose',
            }, {
                caption: 'Closed Date',
                dataField: 'DateClose',
                // dataType: 'datetime',
            }, {
                dataField: 'LastResponseBy',
            }, {
                dataField: 'LastResponseDate',
                // dataType: 'datetime',
            }, {
                dataField: 'SLA',
            }, {
                caption: 'Description SLA',
                dataField: 'UsedDaySLAOK',
            }],
        }).dxDataGrid('instance');
    }
    LoadReportSla();

    $('#btn-submit').click(function () {
        LoadReportSla();
    });

    $('#btn-export').click(function (e) {
        $('#btn-export').prop('disabled', true);
        const start_date = $('#MainContent_dt_strdate_I').val();
        const end_date = $('#MainContent_dt_endate_I').val();
        const format = $('#MainContent_ddList').val();

        $.ajax({
            url: `${url}/report_sla.php?action=export&start_date=${start_date}&end_date=${end_date}`,
            dataType: 'json',
            method: 'GET',
            cache: false,
            async: true,
            success({ status, data }) {
                if (status === 200) {
                    const workbook = new ExcelJS.Workbook();
                    const worksheet = workbook.addWorksheet('Report base on SLA');

                    worksheet.columns = [{
                        header: 'Interaction ID',
                        key: 'GenesysID',
                        width: 30
                    }, {
                        header: 'Thread ID',
                        key: 'ThreadID',
                        width: 20
                    }, {
                        header: 'User Status',
                        key: 'StrStatusPelapor',
                        width: 20
                    }, {
                        header: 'Thread Ticket',
                        key: 'ThreadTicket',
                        width: 30
                    }, {
                        header: 'Ticket Number',
                        key: 'TicketNumber',
                        width: 20
                    },{
                        header: 'Account',
                        key: 'AccountInbound',
                        width: 20
                    }, {
                        header: 'Channel',
                        key: 'TicketSourceName',
                        width: 10
                    }, {
                        header: 'Source Information',
                        key: 'SumberInformasi',
                        width: 20
                    }, {
                        header: 'Account ID',
                        key: 'AccountID',
                        width: 20
                    }, {
                        header: 'Customer ID',
                        key: 'NIK',
                        width: 25
                    }, {
                        header: 'CIF',
                        key: 'CIF',
                        width: 25
                    }, {
                        header: 'Customer Name',
                        key: 'Name',
                        width: 25
                    }, {
                        header: 'Phone Number',
                        key: 'HP',
                        width: 20
                    }, {
                        header: 'Department',
                        key: 'ORGANIZATION_NAME',
                        width: 15
                    }, {
                        header: 'Category',
                        key: 'CategoryName',
                        width: 20
                    }, {
                        header: 'Priority Scale',
                        key: 'SkalaPrioritas',
                        width: 20
                    }, {
                        header: 'Enquiry Type',
                        key: 'SubCategory1Name',
                        width: 30
                    }, {
                        header: 'Enquiry Detail',
                        key: 'SubCategory2Name',
                        width: 30
                    }, {
                        header: 'Reason',
                        key: 'SubCategory3Name',
                        width: 30
                    }, {
                        header: 'User Issue Remark',
                        key: 'DetailComplaint',
                        width: 40
                    }, {
                        header: 'Agent Response',
                        key: 'ResponComplaint',
                        width: 40
                    }, {
                        header: 'Amount',
                        key: 'Amount',
                        width: 15
                    }, {
                        header: 'Ticket Status',
                        key: 'Status',
                        width: 15
                    }, {
                        header: 'Created By',
                        key: 'UserCreate',
                        width: 20
                    }, {
                        header: 'Created Date',
                        key: 'DateCreate',
                        width: 20
                    }, {
                        header: 'Date Transaction',
                        key: 'DateTransaction',
                        width: 20
                    }, {
                        header: 'Claim Status',
                        key: 'ClaimStatus',
                        width: 15
                    }, {
                        header: 'Bank Name',
                        key: 'BankName',
                        width: 20
                    }, {
                        header: 'Solved By',
                        key: 'UserSolved',
                        width: 20
                    }, {
                        header: 'Solved Date',
                        key: 'DateSolved',
                        width: 20
                    }, {
                        header: 'Closed By',
                        key: 'UserClose',
                        width: 20
                    }, {
                        header: 'Closed Date',
                        key: 'DateClose',
                        width: 20
                    }, {
                        header: 'Last Response By',
                        key: 'LastResponseBy',
                        width: 20
                    }, {
                        header: 'Last Response Date',
                        key: 'LastResponseDate',
                        width: 20
                    }, {
                        header: 'SLA',
                        key: 'SLA',
                        width: 5
                    }, {
                        header: 'Description SLA',
                        key: 'UsedDaySLAOK',
                        width: 20
                    }];
                    worksheet.addRows(data);
                    worksheet.autoFilter = 'A1:AI1';
                    worksheet.eachRow(function (row, rowNumber) {
                        row.eachCell((cell, colNumber) => {
                            if (rowNumber === 1) {
                                cell.alignment = { vertical: 'middle', horizontal: 'center' },
                                    cell.fill = {
                                        type: 'pattern',
                                        pattern: 'solid',
                                        fgColor: { argb: '666666' }
                                    },
                                    cell.font = {
                                        color: { argb: 'FFFFFF' },
                                        name: 'Times New Roman',
                                        size: 10,
                                    }
                            }
                            else {
                                cell.alignment = { vertical: 'middle', horizontal: 'left', wrapText: true },
                                    cell.font = {
                                        name: 'Times New Roman',
                                        size: 10,
                                    }
                            }
                        })
                        row.commit();
                    });
                    worksheet.columns.forEach(column => {
                        // column.width = column.header.length < 10 ? 10 : column.header.length;
                        column.border = { top: { style: "thin" }, left: { style: "thin" }, bottom: { style: "thin" }, right: { style: "thin" } }
                    });

                    if (format === 'xlsx') {
                        workbook.xlsx.writeBuffer().then((buffer) => {
                            saveAs(new Blob([buffer], { type: 'application/octet-stream' }), `ReportingBaseonSLA_${start_date}_to_${end_date}.xlsx`);
                        });
                    }
                    else if (format === 'xls') {
                        workbook.xlsx.writeBuffer().then((buffer) => {
                            saveAs(new Blob([buffer], { type: 'application/octet-stream' }), `ReportingBaseonSLA_${start_date}_to_${end_date}.xls`);
                        });
                    }
                    else if (format === 'csv') {
                        workbook.csv.writeBuffer().then((buffer) => {
                            saveAs(new Blob([buffer], { type: 'application/octet-stream' }), `ReportingBaseonSLA_${start_date}_to_${end_date}.csv`);
                        });
                    }
                    else if (format === 'pdf') {
                        const doc = new jsPDF({
                            orientation: "landscape",
                            unit: "px",
                            format: "a0",
                            putOnlyUsedFonts: true,
                        });

                        doc.autoTable({
                            theme: 'grid',
                            headStyles: {
                                fillColor: '#666666',
                                textColor: '#FFFFFF',
                            },
                            body: data,
                            margin: 5,
                            columns: [{
                                header: 'Interaction ID',
                                dataKey: 'GenesysID',
                            }, {
                                header: 'Thread ID',
                                dataKey: 'ThreadID',
                            }, {
                                header: 'User Status',
                                dataKey: 'StrStatusPelapor',
                            }, {
                                header: 'Thread Ticket',
                                dataKey: 'ThreadTicket',
                            }, {
                                header: 'Ticket Number',
                                dataKey: 'TicketNumber',
                            }, {
                                header: 'Account',
                                dataKey: 'AccountInbound',
                            }, {
                                header: 'Channel',
                                dataKey: 'TicketSourceName',
                            }, {
                                header: 'Source Information',
                                dataKey: 'SumberInformasi',
                            }, {
                                header: 'Account ID',
                                dataKey: 'AccountID',
                            }, {
                                header: 'Customer ID',
                                dataKey: 'NIK',
                            }, {
                                header: 'CIF',
                                dataKey: 'CIF',
                            }, {
                                header: 'Customer Name',
                                dataKey: 'Name',
                            }, {
                                header: 'Phone Number',
                                dataKey: 'HP',
                            }, {
                                header: 'Department',
                                dataKey: 'ORGANIZATION_NAME',
                            }, {
                                header: 'Category',
                                dataKey: 'CategoryName',
                            }, {
                                header: 'Priority Scale',
                                dataKey: 'SkalaPrioritas',
                            }, {
                                header: 'Enquiry Type',
                                dataKey: 'SubCategory1Name',
                                overflow: 'linebreak',
                            }, {
                                header: 'Enquiry Detail',
                                dataKey: 'SubCategory2Name',
                                overflow: 'linebreak',
                            }, {
                                header: 'Reason',
                                dataKey: 'SubCategory3Name',
                                overflow: 'linebreak',
                            }, {
                                header: 'User Issue Remark',
                                dataKey: 'DetailComplaint',
                                overflow: 'linebreak',
                            }, {
                                header: 'Agent Response',
                                dataKey: 'ResponComplaint',
                                overflow: 'linebreak',
                            }, {
                                header: 'Amount',
                                dataKey: 'Amount',
                            }, {
                                header: 'Ticket Status',
                                dataKey: 'Status',
                            }, {
                                header: 'Created By',
                                dataKey: 'UserCreate',
                            }, {
                                header: 'Created Date',
                                dataKey: 'DateCreate',
                            }, {
                                header: 'Date Transaction',
                                dataKey: 'DateTransaction',
                            }, {
                                header: 'Claim Status',
                                dataKey: 'ClaimStatus',
                            }, {
                                header: 'Bank Name',
                                dataKey: 'BankName',
                            }, {
                                header: 'Solved By',
                                dataKey: 'UserSolved',
                            }, {
                                header: 'Solved Date',
                                dataKey: 'DateSolved',
                            }, {
                                header: 'Closed By',
                                dataKey: 'UserClose',
                            }, {
                                header: 'Closed Date',
                                dataKey: 'DateClose',
                            }, {
                                header: 'Last Response By',
                                dataKey: 'LastResponseBy',
                            }, {
                                header: 'Last Response Date',
                                dataKey: 'LastResponseDate',
                            }, {
                                header: 'SLA',
                                dataKey: 'SLA',
                            }, {
                                header: 'Description SLA',
                                dataKey: 'UsedDaySLAOK',
                            }],
                        });
                        doc.save(`ReportingBaseonSLA_${start_date}_to_${end_date}.pdf`);
                    }
                    else if (format === 'rtf') {
                        alert('cannot export')
                    }
                }
                else {
                    alert(data); //alert max range.
                }

                $('#btn-export').prop('disabled', false);
            },
            error() {
                alert('Data Loading');
                $('#btn-export').prop('disabled', false);
            },
            // timeout: 60 * 1000,
        });

        e.cancel = true;
    });

});
