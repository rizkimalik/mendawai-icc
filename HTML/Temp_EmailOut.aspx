﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/HTML/Ticket.Master" CodeBehind="Temp_EmailOut.aspx.vb" Inherits="ICC.Temp_EmailOut" %>

<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxCallbackPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxHtmlEditor.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxHtmlEditor" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxCallback" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxGridView.Export" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="FeaturedContent" runat="server">
    <script>
        function bodyShow(TxrIVCID) {
            ASPxPopupControl1.Show();
            if (confirm("Do you want to process?")) {
                $.ajax({
                    type: "POST",
                    url: "WebServiceTransaction.asmx/Temp_SelectEmailOut",
                    data: "{TxrIVCID: '" + TxrIVCID + "'}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        var json = JSON.parse(data.d);
                        var i, x = "";
                        var result = "";
                        for (i = 0; i < json.length; i++) {
                            //alert(json[i].TrxBodyHTML)
                            $("#divBody").html(json[i].TrxBodyHTML);
                        }
                    },
                    error: function (xmlHttpRequest, textStatus, errorThrown) {
                        console.log(xmlHttpRequest.responseText);
                        console.log(textStatus);
                        console.log(errorThrown);
                    }
                })
            }
            else
                return false;
        }
    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
    <div>
        <h4 class="headline">Data Send Email Notifikasi
			<span class="line bg-warning"></span>
        </h4>
    </div>
    <div class="row" style="margin-bottom: -15px;">
        <div class="col-sm-2">
            <label>Start Date</label>
            <dx:ASPxDateEdit ID="dt_strdate" runat="server" CssClass="form-control input-sm" Width="100%" EditFormatString="yyyy-MM-dd">
                <ValidationSettings ErrorTextPosition="Bottom" ErrorDisplayMode="ImageWithText" ValidationGroup="SMLvalidationGroup">
                    <RequiredField IsRequired="true" ErrorText="Must be filled" />
                </ValidationSettings>
            </dx:ASPxDateEdit>
        </div>
        <div class="col-sm-2">
            <label>End Date</label>
            <dx:ASPxDateEdit ID="dt_endate" runat="server" CssClass="form-control input-sm" Width="100%" EditFormatString="yyyy-MM-dd">
                <ValidationSettings ErrorTextPosition="Bottom" ErrorDisplayMode="ImageWithText" ValidationGroup="SMLvalidationGroup">
                    <RequiredField IsRequired="true" ErrorText="Must be filled" />
                </ValidationSettings>
            </dx:ASPxDateEdit>
        </div>
        <div class="col-sm-2" style="margin-top: 5px;">
            <br />
            <dx:ASPxButton ID="btn_Submit" runat="server" Theme="Metropolis" AutoPostBack="False" Text="Submit" ValidationGroup="SMLvalidationGroup"
                HoverStyle-BackColor="#EE4D2D" Height="30px" Width="100%">
            </dx:ASPxButton>
        </div>
    </div>
    <hr />
    <div class="row">
        <div class="col-sm-12">
            <div style="overflow: hidden;">
                <dx:ASPxGridView ID="ASPxGridView1" ClientInstanceName="ASPxGridView1" runat="server" Width="100%" Theme="Metropolis"
                    Font-Size="X-Small" Styles-Header-Font-Bold="true" DataSourceID="LinqDataSource1">
                    <SettingsPager>
                        <AllButton Text="All">
                        </AllButton>
                        <NextPageButton Text="Next &gt;">
                        </NextPageButton>
                        <PrevPageButton Text="&lt; Prev">
                        </PrevPageButton>
                        <PageSizeItemSettings Visible="true" Items="10, 15, 20" ShowAllItem="true" />
                    </SettingsPager>
                    <Settings ShowFilterRow="true" ShowFilterRowMenu="false" ShowGroupPanel="true" ShowVerticalScrollBar="false" ShowHorizontalScrollBar="true" />
                    <Columns>
                        <dx:GridViewDataTextColumn Caption="Action" VisibleIndex="0" CellStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" Width="50px">
                            <DataItemTemplate>
                                <a href="#" onclick="bodyShow('<%# Eval("IVC_ID")%>')">
                                    <asp:Image ImageUrl="img/icon/Text-Edit-icon2.png" ID="img_insert" runat="server" ToolTip="Release Transaction" />
                                </a>
                            </DataItemTemplate>
                        </dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="ID" FieldName="IVC_ID" Width="80px" CellStyle-HorizontalAlign="Center" Settings-AutoFilterCondition="Contains"></dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="Email ID" FieldName="EMAIL_ID" Width="150px" Settings-AutoFilterCondition="Contains"></dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="From" FieldName="EFROM" Width="200px" Settings-AutoFilterCondition="Contains"></dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="To" FieldName="ETO" Width="200px" Settings-AutoFilterCondition="Contains"></dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="Subject" FieldName="ESUBJECT" Width="400px" Settings-AutoFilterCondition="Contains"></dx:GridViewDataTextColumn>
                        <%--<dx:GridViewDataTextColumn Caption="Body" FieldName="EBODY_TEXT" Width="150px"></dx:GridViewDataTextColumn>--%>
                        <%--<dx:GridViewDataTextColumn Caption="Account" FieldName="EBODY_HTML" Width="150px"></dx:GridViewDataTextColumn>--%>
                        <dx:GridViewDataTextColumn Caption="Agent" FieldName="agent" Width="150px" Settings-AutoFilterCondition="Contains"></dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="TicketNumber" FieldName="TicketNumber" Width="150px" Settings-AutoFilterCondition="Contains"></dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="Email Type" FieldName="JENIS_EMAIL_INTERNAL" Width="150px" Settings-AutoFilterCondition="Contains"></dx:GridViewDataTextColumn>
                        <dx:GridViewDataTextColumn Caption="Date" FieldName="Email_Date" Width="150px" Settings-AutoFilterCondition="Contains"></dx:GridViewDataTextColumn>
                    </Columns>
                </dx:ASPxGridView>
            </div>
        </div>
    </div>
    <hr />
    <div class="row">
        <div class="col-sm-2">
            <asp:DropDownList runat="server" ID="ddList" Height="30" CssClass="form-control input-sm">
                <asp:ListItem Value="xlsx" Text="Excel" />
                <asp:ListItem Value="xls" Text="Excel 97-2003" />
                <asp:ListItem Value="pdf" Text="PDF" />
                <asp:ListItem Value="rtf" Text="RTF" />
                <asp:ListItem Value="csv" Text="CSV" />
            </asp:DropDownList>
        </div>
        <div class="col-sm-2">
            <dx:ASPxButton ID="btn_Export" runat="server" Text="Export" Theme="Metropolis" ValidationGroup="SMLvalidationGroup"
                HoverStyle-BackColor="#EE4D2D" Height="30px" Width="100%" AutoPostBack="true">
            </dx:ASPxButton>
        </div>
    </div>
    <hr />
    <dx:ASPxGridViewExporter ID="ASPxGridViewExporter1" runat="server" GridViewID="ASPxGridView1"></dx:ASPxGridViewExporter>
     <asp:LinqDataSource ID="LinqDataSource1" runat="server" ContextTypeName="ICC.Temp_EmailOut_View_DBMLDataContext"
        EntityTypeName="" TableName="V_EmailNotifikasiS" Where="Email_Date >= @StartTanggalFilter And Email_Date <= @EndTanggalFilter" OrderBy="Email_Date ASC">
        <WhereParameters>
            <asp:SessionParameter Name="StartTanggalFilter" SessionField="StartTanggalFilter" Type="DateTime"/>
            <asp:SessionParameter Name="EndTanggalFilter" SessionField="EndTanggalFilter" Type="DateTime"/>
        </WhereParameters>
    </asp:LinqDataSource>
    <dx:ASPxPopupControl ID="ASPxPopupControl1" ClientInstanceName="ASPxPopupControl1" runat="server" CloseAction="CloseButton" Modal="true" Width="1000px"
        closeonescape="true" Height="600px"
        PopupVerticalAlign="WindowCenter"
        PopupHorizontalAlign="WindowCenter" AllowDragging="True" Theme="SoftOrange"
        ShowFooter="True" HeaderText="Form Email Body Notification" FooterText="" AutoUpdatePosition="true">
        <ContentCollection>
            <dx:PopupControlContentControl ID="PopupControlContentControl1" runat="server">
                <dx:ASPxCallbackPanel ID="ASPxCallbackPanel1" runat="server" Width="1000px" ShowLoadingPanel="false" ClientInstanceName="ASPxCallbackPanel1">
                    <PanelCollection>
                        <dx:PanelContent>
                            <div class="row">
                                <div class="col-md-12">
                                    <div id="divBody"></div>
                                </div>
                            </div>
                        </dx:PanelContent>
                    </PanelCollection>
                </dx:ASPxCallbackPanel>
            </dx:PopupControlContentControl>
        </ContentCollection>
    </dx:ASPxPopupControl>
</asp:Content>
