﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/HTML/Ticket.Master" CodeBehind="mailbox.aspx.vb" Inherits="ICC.mailbox" %>

<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxCallbackPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxHtmlEditor.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxHtmlEditor" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxSpellChecker.v13.2, Version=13.2.6.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxSpellChecker" TagPrefix="dx" %>


<asp:Content ID="Content1" ContentPlaceHolderID="FeaturedContent" runat="server">
    <script type="module">
        import { url } from "./scripts/config.js";

        document.addEventListener('DOMContentLoaded', (event) => {
            function isNotEmpty(value) {
                return value !== undefined && value !== null && value !== '';
            }

            const store_inbound = new DevExpress.data.CustomStore({
                key: 'ivc_id',
                load(options) {
                    const deferred = $.Deferred();
                    const args = {};
                    ['skip', 'take', 'requireTotalCount', 'requireGroupCount', 'sort', 'filter', 'totalSummary', 'group', 'groupSummary']
                    .forEach((i) => {
                        if (i in options && isNotEmpty(options[i])) {
                            args[i] = JSON.stringify(options[i]);
                        }
                    });

                    $.ajax({
                        url: `${url}/mailbox.php?action=email_inbound`,
                        dataType: 'json',
                        method: 'GET',
                        cache: false,
                        async: true,
                        data: args,
                        success(result) {
                            deferred.resolve(result.data, {
                                totalCount: result.totalCount,
                                summary: result.summary,
                                groupCount: result.groupCount,
                            });
                        },
                        error() {
                            deferred.reject('Data Loading Error');
                        },
                        timeout: 5000,
                    });

                    return deferred.promise();
                }
            });

            function DxGridDataEmail() {
                $('#DxGridDataEmail').dxDataGrid({
                    dataSource: store_inbound,
                    remoteOperations: true,
                    paging: {
                        pageSize: 15,
                    },
                    pager: {
                        visible: true,
                        allowedPageSizes: [15, 30, 50],
                        showPageSizeSelector: true,
                        showInfo: true,
                        showNavigationButtons: true,
                    },
                    allowColumnResizing: true,
                    columnMinWidth: 100,
                    showBorders: true,
                    showRowLines: true,
                    hoverStateEnabled: true,
                    filterRow: {
                        visible: true,
                    },
                    columns: [{
                        dataField: 'email_id',
                        dataType: 'string',
                        allowEditing: false
                    }, {
                        dataField: 'efrom',
                        dataType: 'string',
                    }, {
                        dataField: 'eto',
                        dataType: 'string',
                    }, {
                        // caption: 'Phone Number',
                        dataField: 'esubject',
                        dataType: 'string',
                    }, {
                        // caption: 'Gender',
                        dataField: 'agent',
                        dataType: 'string',
                    }],
                }).dxDataGrid('instance');
            }
            DxGridDataEmail();
        });
    </script>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
    <h4 class="headline">
        Channel Email
        <span class="line bg-danger" style="margin-top: 15px;"></span>
    </h4>

    <div class="row">
        <div class="col-md-3 col-sm-6">
            <div class="panel">
                <div class="panel-body">
                    <a class="btn btn-info block">COMPOSE</a>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-body">
                    <strong class="font-16">Menu</strong>
                </div>
                <div class="list-group">
                    <a class="list-group-item">
                        <i class="fa fa-inbox fa-lg grey"></i>
                        <span class="m-left-xs">Inbox</span>
                        <span class="badge badge-info">0</span>
                    </a>
                    <a class="list-group-item">
                        <i class="fa fa-envelope-o fa-lg grey"></i>
                        <span class="m-left-xs">Sent</span>
                        <span class="badge badge-info">0</span>
                    </a>
                    <a class="list-group-item">
                        <i class="fa fa-clock-o fa-lg"></i>
                        <span class="m-left-xs">Queing</span>
                        <span class="badge badge-info">0</span>
                    </a>
                    <a class="list-group-item">
                        <i class="fa fa-history fa-lg"></i>
                        <span class="m-left-xs">Revert</span>
                        <span class="badge badge-info">0</span>
                    </a>
                    <a class="list-group-item">
                        <i class="fa fa-warning fa-lg"></i>
                        <span class="m-left-xs">Spam</span>
                        <span class="badge badge-info">0</span>
                    </a>
                </div>
            </div>
        </div>
        <!-- /.col -->

        <div class="col-md-9 col-sm-6">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <strong class="font-16">Data Email Inbox</strong>
                </div>
                <div class="panel-body">
                    <div id="DxGridDataEmail"></div>
                </div>
            </div>
        </div>
        <!-- /.col -->
    </div>


</asp:Content>
