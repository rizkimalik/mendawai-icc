﻿Imports System.Data
Imports System.Data.SqlClient
Imports DevExpress.Web.ASPxGridView
Public Class R_SurveyCSAT
    Inherits System.Web.UI.Page

    Dim Cls As New WebServiceTransaction
    Dim proses As New ClsConn
    Dim _upage As String
    Dim sqlRead As SqlDataReader
    Dim strLogTime As String = DateTime.Now.ToString("yyyy")
    Dim strStartDate As String
    Dim strEndDate As String

    Private Sub Page_Init(sender As Object, e As EventArgs) Handles Me.Init
        'dt_strdate.Value = DateTime.Now.ToString("yyyy-MM-dd")
        'dt_endate.Value = DateTime.Now.ToString("yyyy-MM-dd")

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If (Not IsPostBack) Then
            dt_strdate.Text = DateTime.Now.ToString("yyyy-MM-dd")
            dt_endate.Text = DateTime.Now.ToString("yyyy-MM-dd")
        End If
        _updatePage()

    End Sub
    Private Sub ASPxGridView1_Load(sender As Object, e As EventArgs) Handles ASPxGridView1.Load
        'CType(ASPxGridView1.Columns("ResultCSAT"), GridViewDataTextColumn).GroupBy()
        strStartDate = dt_strdate.Text
        strEndDate = dt_endate.Text

        If cbCSAT.Value = "0" Then
            Try
                Dim strSql As New R_SurveyCSAT_DBMLDataContext
                strSql.CommandTimeout = 480
                ASPxGridView1.DataSource = strSql.R_SurveyCSAT(strStartDate, strEndDate, 0).ToList()
                'ASPxGridView1.DataSource = strSql.R_SurveyCSAT("2021-03-01", "2021-03-23", 0).ToList()
                ASPxGridView1.DataBind()

                Dim strQuery As String = "Submit_R_SurveyCSAT_" & strStartDate & "," & strEndDate & ", 0"
                Cls.LogSuccess(strLogTime, strQuery)
            Catch ex As Exception
                Dim strQuery As String = "Submit_R_SurveyCSAT_" & strStartDate & "," & strEndDate & ", 0"
                Cls.LogError(strLogTime, ex, strQuery)
            End Try
        Else
            Try
                Dim strSql As New R_SurveyCSAT_DBMLDataContext
                strSql.CommandTimeout = 480
                'ASPxGridView1.DataSource = strSql.R_SurveyCSAT("" & Format(dt_strdate.Value, "yyyy-MM-dd") & "", "" & Format(dt_endate.Value, "yyyy-MM-dd") & "," & Format(chkCSAT.Value, "1") & "").ToList()
                ASPxGridView1.DataSource = strSql.R_SurveyCSAT(strStartDate, strEndDate, 1).ToList()
                'ASPxGridView1.DataSource = strSql.R_SurveyCSAT("2021-03-01", "2021-03-23", 1).ToList()
                ASPxGridView1.DataBind()

                Dim strQuery As String = "Submit_R_SurveyCSAT_" & strStartDate & "," & strEndDate & ", 1"
                Cls.LogSuccess(strLogTime, strQuery)
            Catch ex As Exception
                Dim strQuery As String = "Submit_R_SurveyCSAT_" & strStartDate & "," & strEndDate & ", 1"
                Cls.LogError(strLogTime, ex, strQuery)
            End Try
        End If
        'Try
        '    Dim strSql As New R_SurveyCSAT_DBMLDataContext
        '    strSql.CommandTimeout = 480
        '    'ASPxGridView1.DataSource = strSql.R_SurveyCSAT("" & Format(dt_strdate.Value, "yyyy-MM-dd") & "", "" & Format(dt_endate.Value, "yyyy-MM-dd") & "," & chkCSAT.Value & "").ToList()
        '    ASPxGridView1.DataSource = strSql.R_SurveyCSAT("2021-03-01", "2021-03-23", -1).ToList()
        '    'ASPxGridView1.DataSource = strSql.R_SurveyCSAT(dt_strdate.Value, dt_endate.Value, 0).ToList()
        '    ASPxGridView1.DataBind()

        '    Dim strQuery As String = "Process_ReportSurveyCSAT_" & Format(dt_strdate.Value, "yyyy-MM-dd") & "," & Format(dt_endate.Value, "yyyy-MM-dd") & ""
        '    Cls.LogSuccess(strLogTime, strQuery)
        'Catch ex As Exception
        '    Dim strQuery As String = "Process_ReportSurveyCSAT_" & Format(dt_strdate.Value, "yyyy-MM-dd") & "," & Format(dt_endate.Value, "yyyy-MM-dd") & ""
        '    Cls.LogError(strLogTime, ex, strQuery)
        'End Try

    End Sub
    Private Sub btn_Submit_Click(sender As Object, e As EventArgs) Handles btn_Submit.Click
        'CType(ASPxGridView1.Columns("ResultCSAT"), GridViewDataTextColumn).GroupBy()
        'strStartDate = dt_strdate.Value
        'strEndDate = dt_endate.Value

        strStartDate = Format(dt_strdate.Value, "yyyy-MM-dd")
        strEndDate = Format(dt_endate.Value, "yyyy-MM-dd")

        If cbCSAT.Value = "0" Then
            Try
                Dim strSql As New R_SurveyCSAT_DBMLDataContext
                strSql.CommandTimeout = 480
                ASPxGridView1.DataSource = strSql.R_SurveyCSAT(strStartDate, strEndDate, 0).ToList()
                'ASPxGridView1.DataSource = strSql.R_SurveyCSAT("2021-03-01", "2021-03-23", 0).ToList()
                ASPxGridView1.DataBind()

                Dim strQuery As String = "Submit_R_SurveyCSAT_" & strStartDate & "," & strEndDate & ", 0"
                Cls.LogSuccess(strLogTime, strQuery)
            Catch ex As Exception
                Dim strQuery As String = "Submit_R_SurveyCSAT_" & strStartDate & "," & strEndDate & ", 0"
                Cls.LogError(strLogTime, ex, strQuery)
            End Try
        Else
            Try
                Dim strSql As New R_SurveyCSAT_DBMLDataContext
                strSql.CommandTimeout = 480
                'ASPxGridView1.DataSource = strSql.R_SurveyCSAT("" & Format(dt_strdate.Value, "yyyy-MM-dd") & "", "" & Format(dt_endate.Value, "yyyy-MM-dd") & "," & Format(chkCSAT.Value, "1") & "").ToList()
                ASPxGridView1.DataSource = strSql.R_SurveyCSAT(strStartDate, strEndDate, 1).ToList()
                'ASPxGridView1.DataSource = strSql.R_SurveyCSAT("2021-03-01", "2021-03-23", 1).ToList()
                ASPxGridView1.DataBind()

                Dim strQuery As String = "Submit_R_SurveyCSAT_" & strStartDate & "," & strEndDate & ", 1"
                Cls.LogSuccess(strLogTime, strQuery)
            Catch ex As Exception
                Dim strQuery As String = "Submit_R_SurveyCSAT_" & strStartDate & "," & strEndDate & ", 1"
                Cls.LogError(strLogTime, ex, strQuery)
            End Try
        End If

    End Sub
    Private Sub dt_strdate_Init(sender As Object, e As EventArgs) Handles dt_strdate.Init
        dt_strdate.Value = DateTime.Now.ToString("yyyy-MM-dd")
    End Sub
    Private Sub dt_endate_Init(sender As Object, e As EventArgs) Handles dt_endate.Init
        dt_endate.Value = DateTime.Now.ToString("yyyy-MM-dd")
    End Sub

    'Private Sub chkCSAT_Init(sender As Object, e As EventArgs) Handles chkCSAT.Init
    '    If chkCSAT.Checked = False Then
    '        Try
    '            Dim strSql As New R_SurveyCSAT_DBMLDataContext
    '            strSql.CommandTimeout = 480
    '            ASPxGridView1.DataSource = strSql.R_SurveyCSAT("" & Format(dt_strdate.Value, "yyyy-MM-dd") & "", "" & Format(dt_endate.Value, "yyyy-MM-dd") & "," & Format(chkCSAT.Value, "False") & "").ToList()
    '            ASPxGridView1.DataBind()

    '            Dim strQuery As String = "Submit_R_SurveyCSAT_" & Format(dt_strdate.Value, "yyyy-MM-dd") & "," & Format(dt_endate.Value, "yyyy-MM-dd") & "," & Format(chkCSAT.Value, "False") & ""
    '            Cls.LogSuccess(strLogTime, strQuery)
    '        Catch ex As Exception
    '            Dim strQuery As String = "Submit_R_SurveyCSAT_" & Format(dt_strdate.Value, "yyyy-MM-dd") & "," & Format(dt_endate.Value, "yyyy-MM-dd") & "," & Format(chkCSAT.Value, "False") & ""
    '            Cls.LogError(strLogTime, ex, strQuery)
    '        End Try
    '    Else
    '        Try
    '            Dim strSql As New R_SurveyCSAT_DBMLDataContext
    '            strSql.CommandTimeout = 480
    '            ASPxGridView1.DataSource = strSql.R_SurveyCSAT("" & Format(dt_strdate.Value, "yyyy-MM-dd") & "", "" & Format(dt_endate.Value, "yyyy-MM-dd") & "," & Format(chkCSAT.Value, "True") & "").ToList()
    '            ASPxGridView1.DataBind()

    '            Dim strQuery As String = "Submit_R_SurveyCSAT_" & Format(dt_strdate.Value, "yyyy-MM-dd") & "," & Format(dt_endate.Value, "yyyy-MM-dd") & "," & Format(chkCSAT.Value, "True") & ""
    '            Cls.LogSuccess(strLogTime, strQuery)
    '        Catch ex As Exception
    '            Dim strQuery As String = "Submit_R_SurveyCSAT_" & Format(dt_strdate.Value, "yyyy-MM-dd") & "," & Format(dt_endate.Value, "yyyy-MM-dd") & "," & Format(chkCSAT.Value, "True") & ""
    '            Cls.LogError(strLogTime, ex, strQuery)
    '        End Try
    '    End If
    'End Sub
    Private Sub btn_Export_Click(sender As Object, e As EventArgs) Handles btn_Export.Click
        'CType(ASPxGridView1.Columns("ResultCSAT"), GridViewDataTextColumn).GroupBy()
        Dim TimeS As DateTime
        TimeS = dt_endate.Value


        Dim casses As String = ddList.SelectedValue
        Select Case casses
            Case "xlsx"
                ASPxGridViewExporter1.WriteXlsxToResponse("ReportSurveyCSAT_" & DateTime.Now.ToString("yyyyMMddhhmmss"))
            Case "xls"
                ASPxGridViewExporter1.WriteXlsToResponse("ReportSurveyCSAT_" & DateTime.Now.ToString("yyyyMMddhhmmss"))
            Case "rtf"
                ASPxGridViewExporter1.Landscape = True
                ASPxGridViewExporter1.LeftMargin = 35
                ASPxGridViewExporter1.RightMargin = 30
                ASPxGridViewExporter1.ExportedRowType = DevExpress.Web.ASPxGridView.Export.GridViewExportedRowType.All
                ASPxGridViewExporter1.MaxColumnWidth = 108
                ASPxGridViewExporter1.WriteRtfToResponse("ReportSurveyCSAT_" & DateTime.Now.ToString("yyyyMMddhhmmss"))
            Case "pdf"
                ASPxGridViewExporter1.Landscape = True
                ASPxGridViewExporter1.LeftMargin = 35
                ASPxGridViewExporter1.RightMargin = 30
                ASPxGridViewExporter1.ExportedRowType = DevExpress.Web.ASPxGridView.Export.GridViewExportedRowType.All
                ASPxGridViewExporter1.MaxColumnWidth = 108
                ASPxGridViewExporter1.WritePdfToResponse("ReportSurveyCSAT_" & DateTime.Now.ToString("yyyyMMddhhmmss"))
            Case "csv"
                ASPxGridViewExporter1.WriteCsvToResponse("ReportSurveyCSAT_" & DateTime.Now.ToString("yyyyMMddhhmmss"))
        End Select
        'Cls.LogSuccess(strLogTime, strString)

        'Try
        '    Dim strSql As New R_SurveyCSAT_DBMLDataContext
        '    strSql.CommandTimeout = 480
        '    ASPxGridView1.DataSource = strSql.R_SurveyCSAT("" & Format(dt_strdate.Value, "yyyy-MM-dd") & "", "" & Format(dt_endate.Value, "yyyy-MM-dd") & "").ToList()
        '    ASPxGridView1.DataBind()

        '    Dim strQuery As String = "Export_ReportSurveyCSAT_" & Format(dt_strdate.Value, "yyyy-MM-dd") & "," & Format(dt_endate.Value, "yyyy-MM-dd") & ""
        '    Cls.LogSuccess(strLogTime, strQuery)
        'Catch ex As Exception
        '    Dim strQuery As String = "Export_ReportSurveyCSAT_" & Format(dt_strdate.Value, "yyyy-MM-dd") & "," & Format(dt_endate.Value, "yyyy-MM-dd") & ""
        '    Cls.LogError(strLogTime, ex, strQuery)
        'End Try

        'Dim strTgl As String = String.Empty
        'Dim strString As String = "SELECT DATEADD(DAY," & Cls.FunctionDataSetting("Report") & ",'" & TimeS & "') as TglSelected"
        'Try
        '    sqlRead = proses.ExecuteReader(strString)
        '    If sqlRead.HasRows Then
        '        sqlRead.Read()
        '        strTgl = sqlRead("TglSelected").ToString
        '    End If
        'Catch ex As Exception
        '    Cls.LogError(strLogTime, ex, strString)
        'End Try

        'If dt_strdate.Value < strTgl Then
        '    alertJS("Export maximum " & ReplaceSpecialLetter(Cls.FunctionDataSetting("Report")) & " days")
        'Else
        '    Cls.LogSuccess(strLogTime, strString)
        '    Dim casses As String = ddList.SelectedValue
        '    Select Case casses
        '        Case "xlsx"
        '            ASPxGridViewExporter1.WriteXlsxToResponse("ReportSurveyCSAT_" & DateTime.Now.ToString("yyyyMMddhhmmss"))
        '        Case "xls"
        '            ASPxGridViewExporter1.WriteXlsToResponse("ReportSurveyCSAT_" & DateTime.Now.ToString("yyyyMMddhhmmss"))
        '        Case "rtf"
        '            ASPxGridViewExporter1.Landscape = True
        '            ASPxGridViewExporter1.LeftMargin = 35
        '            ASPxGridViewExporter1.RightMargin = 30
        '            ASPxGridViewExporter1.ExportedRowType = DevExpress.Web.ASPxGridView.Export.GridViewExportedRowType.All
        '            ASPxGridViewExporter1.MaxColumnWidth = 108
        '            ASPxGridViewExporter1.WriteRtfToResponse("ReportSurveyCSAT_" & DateTime.Now.ToString("yyyyMMddhhmmss"))
        '        Case "pdf"
        '            ASPxGridViewExporter1.Landscape = True
        '            ASPxGridViewExporter1.LeftMargin = 35
        '            ASPxGridViewExporter1.RightMargin = 30
        '            ASPxGridViewExporter1.ExportedRowType = DevExpress.Web.ASPxGridView.Export.GridViewExportedRowType.All
        '            ASPxGridViewExporter1.MaxColumnWidth = 108
        '            ASPxGridViewExporter1.WritePdfToResponse("ReportSurveyCSAT_" & DateTime.Now.ToString("yyyyMMddhhmmss"))
        '        Case "csv"
        '            ASPxGridViewExporter1.WriteCsvToResponse("ReportSurveyCSAT_" & DateTime.Now.ToString("yyyyMMddhhmmss"))
        '    End Select
        'End If
    End Sub
    Function alertJS(ByVal alert As String)
        Dim message As String = alert
        Dim sb As New System.Text.StringBuilder()
        sb.Append("<script type = 'text/javascript'>")
        sb.Append("window.onload=function(){")
        sb.Append("alert('")
        sb.Append(message)
        sb.Append("')};")
        sb.Append("</script>")
        ClientScript.RegisterClientScriptBlock(Me.GetType(), "alert", sb.ToString())
    End Function
    Private Sub _updatePage()
        Try
            _upage = "update user1 set Activity='N'"
            proses.ExecuteNonQuery(_upage)
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
        Try
            _upage = "update user1 set Activity='Y' where MenuID='" & Request.QueryString("idpage") & "'"
            proses.ExecuteNonQuery(_upage)
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
        Try
            _upage = "update user2 set Activity='N'"
            proses.ExecuteNonQuery(_upage)
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
        Try
            _upage = "update user2 set Activity='Y' where SubMenuID='" & Request.QueryString("idtable") & "'"
            proses.ExecuteNonQuery(_upage)
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Public Function ReplaceSpecialLetter(ByVal str)
        Dim TmpStr As String
        TmpStr = str
        TmpStr = Replace(TmpStr, "-", "")
        ReplaceSpecialLetter = TmpStr
    End Function


    'Protected Sub ASPxGridView1_CustomColumnGroup(sender As Object, e As CustomColumnSortEventArgs)
    '    CompareColumnValues(e)
    'End Sub
    'Private Sub CompareColumnValues(ByVal e As DevExpress.Web.ASPxGridView.CustomColumnSortEventArgs)
    '    If e.Column.FieldName = "ResultCSAT" Then
    '        e.Handled = True
    '    End If
    'End Sub
    'Protected Sub ASPxGridView1_CustomGroupDisplayText(sender As Object, e As ASPxGridViewColumnDisplayTextEventArgs)
    '    If e.Column.FieldName = "ResultCSAT" Then
    '        'e.DisplayText = "Data : "
    '    End If
    'End Sub
    'Protected Sub ASPxGridView1_CustomColumnSort(sender As Object, e As CustomColumnSortEventArgs)
    '    CompareColumnValues(e)
    'End Sub
End Class