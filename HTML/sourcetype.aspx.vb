﻿Imports System
Imports System.Data.SqlClient
Public Class sourcetype
    Inherits System.Web.UI.Page

    Dim Proses As New ClsConn
    Dim Sqldr As SqlDataReader
    Dim sql As String
    Dim value As String
    Dim status As String
    Dim con As New SqlConnection(ConfigurationManager.ConnectionStrings("DefaultConnection").ConnectionString)
    Dim sqlcom As New SqlCommand
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Session("NamaForm") = IO.Path.GetFileName(Request.Path)
        sql_source_type.SelectCommand = "select * from mSourceType"
        sql_source_type.InsertCommand = "insert into mSourceType (Name, TicketIDCode, NA, UserCreate, DateCreate) values (@Name, @TicketIDCode, @NA, '" & Session("username") & "', getdate())"
        sql_source_type.UpdateCommand = "update mSourceType set Name=@Name, NA=@NA, UserUpdate='" & Session("username") & "', DateUpdate=getdate() where TypeID=@TypeID"
        sql_source_type.DeleteCommand = "delete from mSourceType where TypeID=@TypeID"
        updateAlert()
    End Sub
    Private Sub updateAlert()
        Dim updateActivity As String = "update user1 set Activity='N'"
        Try
            sqlcom = New SqlCommand(updateActivity, con)
            con.Open()
            sqlcom.ExecuteNonQuery()
            con.Close()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
        Dim IdupdateActivity As String = "update user1 set Activity='Y' where MenuID='" & Request.QueryString("idpage") & "'"
        Try
            sqlcom = New SqlCommand(IdupdateActivity, con)
            con.Open()
            sqlcom.ExecuteNonQuery()
            con.Close()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
End Class